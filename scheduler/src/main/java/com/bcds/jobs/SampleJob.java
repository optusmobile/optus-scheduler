package com.bcds.jobs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
//import java.util.List;
import java.util.List;
import java.util.Scanner;

import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;

import com.bcds.file.helper.MM01Enum;
import com.bcds.file.mapping.TestFileType1;
import com.bcds.jpa.entity.Asset;
import com.bcds.jpa.repo.AssetRepository;
import com.bcds.task.TaskManagerBase;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class SampleJob {
  
	@Autowired	
	private AssetRepository assetRepo;
	
	@Autowired
	MessageChannel sftpChannel;

	public void sampleJobMethod() {

		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
  
		System.out.println("Invoked on " + dateFormat.format(System.currentTimeMillis()));
  
		try{
			/**
			List<Asset> assets = assetRepo.findAll();
			
			for(Asset asset:assets){
				System.out.println("Asset type: " + asset.getAssetType());
			}
			
			FlatFileItemReader<TestFileType1> reader = TaskManagerBase.reader();
			reader.open(new ExecutionContext());
			TestFileType1 fileType1 = null;
			
			System.out.println(">>>>>>>>>>>> processing flat file.....");
			
			do{
				fileType1 = reader.read();
				System.out.println("id: " + fileType1.getId());
				System.out.println("timestamp: " + fileType1.getTimestamp());
				System.out.println("asset id: " + fileType1.getAssetId());
				System.out.println("quantity: " + fileType1.getQuantity());
			}while (fileType1 != null);
  **/
			Message<File> message = MessageBuilder.withPayload(new File("c:\\data.csv")).build();
			
			sftpChannel.send(message);
			
			
		}catch (Exception e){
			e.printStackTrace();
		}
		
  
 }
	
	public static void main (String[] args){
		
		
		try{
			
			FileReader fr = new FileReader(new File("c:\\matmas.csv"));
		    BufferedReader bf = new BufferedReader(fr);
		    
			//Scanner scanner = new Scanner(new File("c:\\data.csv"));
		    Scanner scanner = new Scanner(bf);
			 
	        //scanner.useDelimiter("|");
			String line = "";
	        while(scanner.hasNextLine()){
	        	String oneLine = scanner.nextLine();
	        	//System.out.println("------------ " + oneLine);
	        	//System.out.println("------------ " + MM01Enum.E1MARAM.toString());
	            	        	
	        	String[] fields = oneLine.split("\\|");
	        	System.out.println("------------ " + fields[0]);
	        	
	        	if (fields[0].equals(MM01Enum.E1MARAM.toString())){
	        		
	        		//int[] positions = MM01Enum.E1MARAM.getFieldsPosition();
	        		
	        		//for (int i : positions){
	        		//	System.out.println(">>>>>>>>> " + fields[i]);
	        		//}
		            //System.out.println(">>>>>>>>> " + fields[2]);
	        	}
	        	
	        	if (fields[0].equals(MM01Enum.E1MARCM.toString())){
	        		
	        		//int[] positions = MM01Enum.E1MARCM.getFieldsPosition();
	        		
	        		//for (int i : positions){
	        		//	System.out.println(">>>>>>>>> " + fields[i]);
	        		//}
		            //System.out.println(">>>>>>>>> " + fields[2]);
	        	}
	            //System.out.println(">>>>>>>>> " + fields[0]);
	            //System.out.println(">>>>>>>>> " + fields[1]);
	            //System.out.println(">>>>>>>>> " + fields[2]);
	            //System.out.println(">>>>>>>>> " + fields[3]);
	        }
	        scanner.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		/** ftp working
		String SFTPHOST = "52.63.125.128";
        int SFTPPORT = 22;
        String SFTPUSER = "optus_oliver3";
        String SFTPPASS = "saveIt4Oliver";
        String SFTPWORKINGDIR = "/home/optus_oliver3/files/in";

        Session session = null;
        Channel channel = null;
        ChannelSftp channelSftp = null;
        System.out.println("preparing the host information for sftp.");
        try {
            JSch jsch = new JSch();
            session = jsch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);
            session.setPassword(SFTPPASS);
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            System.out.println("Host connected.");
            channel = session.openChannel("sftp");
            channel.connect();
            System.out.println("sftp channel opened and connected.");
            channelSftp = (ChannelSftp) channel;
            channelSftp.cd(SFTPWORKINGDIR);
            File f = new File("c:/data.csv");
            channelSftp.put(new FileInputStream(f), f.getName());
            //log.info("File transfered successfully to host.");
        } catch (Exception ex) {
             ex.printStackTrace();
        }
        finally{

            channelSftp.exit();
            System.out.println("sftp Channel exited.");
            channel.disconnect();
            System.out.println("Channel disconnected.");
            session.disconnect();
            System.out.println("Host Session disconnected.");
        }
        end ftp working **/ 
	}
}
