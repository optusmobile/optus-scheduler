package com.bcds.jobs;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import com.bcds.file.helper.GeneralHelper;
import com.bcds.ftp.util.FileUtil;
import com.bcds.jobs.beans.CompanyAssetParts;
import com.bcds.jobs.beans.CompanyLocation;
import com.bcds.jobs.beans.StockVarianceBean;
import com.bcds.jobs.exception.BCDSDataException;
import com.bcds.jobs.exception.BCDSEmailException;
import com.bcds.jobs.exception.FileProcessingException;
import com.bcds.jpa.entity.InterfaceError;
import com.bcds.jpa.entity.StockVarianceEntity;
import com.bcds.jpa.model.r01.R01;
import com.bcds.jpa.model.r01.R01StkVarLine;
import com.bcds.service.iface.StockVarianceServiceInterface;
import com.bcds.util.DateUtil;

public class R01Jobs extends AbstractIncomingJob {

	final static Logger logger = Logger.getLogger(R01Jobs.class);

	@Value( "${r01.filename}" )
	protected String filename;

	@Value( "${r01.backup.folder}" )
	protected String backupFolder;

	@Value( "${r01.source.folder}" )
	protected String sourceFolder;
	
	@Value( "${serverType}" )
	protected String serverType;
	
	@Autowired
	private StockVarianceServiceInterface stockVarianceService;
	
	@Override
	public void executeJob() {
		
		logger.info("----------- R01 Stock Variance Job Started ---------");
		setFilename(this.filename);
		setBackupFolder(this.backupFolder);
		setSourceFolder(this.sourceFolder);
		logger.info("Whats the name?: " + getFilename());
		List<String> allR01s = new ArrayList<String>();
		try {
			allR01s = FileUtil.getSimilarBatchFiles(getSourceFolder(), getFilename());
		} catch (NoSuchFileException e) {
		}
		logger.info("R01 size: " + allR01s.size());
		File file = null;
		for (String oneR01 : allR01s) {
			logger.info("oneR01 filename: " + oneR01);
			try{		
				R01 r01 = processInterfaceFile(oneR01);
				stockVarianceService.addSapStock(r01);
				stockVarianceService.addIntrackStock();
				stockVarianceService.updateIntrackQuantity();
				List<StockVarianceEntity> stockVarianceList = stockVarianceService.getStockVarianceList();
				if(stockVarianceList.size() == 0){
					sendEmailWithAttachment(serverType+"_O3_StockVarianceReport_"+DateUtil.getSimpleDateFormat("yyyyMMdd"), "A comparison has been done between SAP and InTrack.\nThere is no variance.", null);
				} else {
					file = stockVarianceService.generateSapStockVarianceReport(stockVarianceList);
					sendEmailWithAttachment(serverType+"_O3_StockVarianceReport_"+DateUtil.getSimpleDateFormat("yyyyMMdd"), "A comparison has been done between SAP and InTrack.\nPlease find the report in the attachment.", file);
				}
			} catch (FileProcessingException fpe){
				logger.error("R01 FileProcessingException thrown: " + fpe.getMessage());
				interfaceErrorRepo.save(new InterfaceError(GeneralHelper.R01, new File(oneR01).getName(), new Date(), GeneralHelper.DB_TABLE, fpe.getMessage(), GeneralHelper.PROCESSING_ERROR, getProcessingErrorFolder()));
				sendEmailNotification(oneR01, fpe.getMessage());
				doBackup(new File(oneR01), getProcessingErrorFolder());
			} catch (BCDSDataException dbe){
				logger.error("R01 Exception thrown: " + dbe.getMessage());
				interfaceErrorRepo.save(new InterfaceError(GeneralHelper.R01, new File(oneR01).getName(), new Date(), GeneralHelper.DB_TABLE, dbe.getMessage(), GeneralHelper.PROCESSING_ERROR, getProcessingErrorFolder()));
				sendEmailNotification(oneR01, dbe.getMessage());
				doBackup(new File(oneR01), getProcessingErrorFolder());
			} catch (BCDSEmailException ee) {
				interfaceErrorRepo.save(new InterfaceError(GeneralHelper.R01, new File(oneR01).getName(), new Date(), GeneralHelper.DB_TABLE, ee.getMessage(), GeneralHelper.PROCESSING_ERROR, getProcessingErrorFolder()));
				logger.error("Email failed to send to recipients: " + ee.getMessage());
				sendEmailNotification(oneR01, ee.getMessage());
				doBackup(new File(oneR01), getProcessingErrorFolder());
			} catch (FileNotFoundException fnfe){
				logger.error(fnfe.getMessage());
				interfaceErrorRepo.save(new InterfaceError(GeneralHelper.R01, new File(oneR01).getName(), new Date(), GeneralHelper.DB_TABLE, fnfe.getMessage(), GeneralHelper.PROCESSING_ERROR, getProcessingErrorFolder()));
				sendEmailNotification(oneR01, fnfe.getMessage());
			} catch (Exception e) {
				doBackup(new File(oneR01), getProcessingErrorFolder());
				logger.error(e, e);
				interfaceErrorRepo.save(new InterfaceError(GeneralHelper.R01, new File(oneR01).getName(), new Date(), GeneralHelper.DB_TABLE, e.getMessage(), GeneralHelper.PROCESSING_ERROR, getProcessingErrorFolder()));
				sendEmailNotification(oneR01, e.getMessage());
			} finally {
				File r01File = new File(oneR01);
				if(r01File != null && r01File.exists()){
					logger.info(oneR01.toString() + " processed successfully: " + oneR01);
					doBackup(new File(oneR01), getBackupFolder());	
				}
			}
		}
	}
	
//	@SuppressWarnings("resource")
	private R01 processInterfaceFile(String oneR01) throws FileNotFoundException, FileProcessingException{
		File file = new File(oneR01);
		FileReader fr = new FileReader(file);			
		BufferedReader bf = new BufferedReader(fr);
		Scanner scanner = new Scanner(bf);
		try {
			List<R01StkVarLine> r01StkVarLines = new ArrayList<R01StkVarLine>();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		    R01StkVarLine r01StkVarLine = null;
		    StockVarianceBean stockVarianceBean = null;
		    CompanyAssetParts companyAssetParts = null;
			CompanyLocation companyLocation = null;
			while(scanner.hasNextLine()){
		    	String oneLine = scanner.nextLine();
	        	String[] fields = oneLine.split("\\|");
	        	if(fields.length < 5){
	        		throw new FileProcessingException("Format of Line: "+oneLine+" is incorrect.");
	        	} else {
	        		r01StkVarLine = new R01StkVarLine();
	        		stockVarianceBean = new StockVarianceBean();
	        		try {
						stockVarianceBean.setStockReceiveDate(sdf.parse(fields[0]));
						companyAssetParts = new CompanyAssetParts();
		        		companyAssetParts.setPartName(fields[1]);
		        		stockVarianceBean.setCompanyAssetParts(companyAssetParts);
		        		stockVarianceBean.setPlantName(fields[2]);
		        		companyLocation = new CompanyLocation();
		        		companyLocation.setBarcode(fields[2] + fields[3]);
		        		stockVarianceBean.setCompanyLocation(companyLocation);
		        		stockVarianceBean.setSapQuantity(new Double(fields[4]).intValue());
		        		stockVarianceBean.setBatchName(fields[5]);
		        		stockVarianceBean.setStockType(fields[fields.length-1]);
					} catch (ParseException e) {
						throw new FileProcessingException("Date format of Line: "+oneLine+" is incorrect.");
					} catch (Exception e){
						throw new FileProcessingException(e.getMessage());
					}
	        		r01StkVarLine.setStockVarianceBean(stockVarianceBean);
	        		r01StkVarLines.add(r01StkVarLine);
	        	}
	        }
			R01 r01 = new R01();
			r01.setR01StkVarLine(r01StkVarLines);
			return r01;
		} finally {
			try {
				scanner.close();
				bf.close();
				fr.close();
			} catch (IOException e) {
				logger.error(e.getMessage());
			}
		}
	}
	
	private void sendEmailNotification(String fileName,String message){
		try {
			sendEmailWithAttachment(fileName+" failed", message, new File(fileName));
		} catch (BCDSEmailException e) {
			logger.error("Email failed to send to recipients: " + e.getMessage());
		}
	}

}
