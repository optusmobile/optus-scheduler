package com.bcds.jobs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import org.apache.log4j.Logger;
import org.codehaus.plexus.util.StringUtils;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleTrigger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import com.bcds.file.helper.GeneralHelper;
import com.bcds.ftp.util.FileUtil;
import com.bcds.jobs.beans.AssetBean;
import com.bcds.jobs.beans.CompanyAssetParts;
import com.bcds.jobs.beans.CompanyLocation;
import com.bcds.jobs.beans.PidBean;
import com.bcds.jobs.beans.PidListBean;
import com.bcds.jobs.exception.BCDSDataException;
import com.bcds.jobs.exception.BCDSEmailException;
import com.bcds.jobs.exception.FileProcessingException;
import com.bcds.jpa.entity.InterfaceError;
import com.bcds.jpa.model.in10.IN10;
import com.bcds.jpa.model.in10.IN10PIDLine;

public class IN10Job extends AbstractIncomingJob {

	final static Logger logger = Logger.getLogger(IN10Job.class);
	
	@Value("${in10.filename}")
	protected String filename;

	@Value("${in10.backup.folder}")
	protected String backupFolder;

	@Value("${in10.source.folder}")
	protected String sourceFolder;
	
	@Override
	public void executeJob() {
		logger.info("-----------Starting IN10 Job--------");
		error = new InterfaceError();
		//error.setModule(GeneralHelper.IN10);
//		error.setFileName(this.filename);
		setFilename(this.filename);
		setBackupFolder(this.backupFolder);
		setSourceFolder(this.sourceFolder);
		List<String> allIN10s = new ArrayList<String>();
		try {
			allIN10s = FileUtil.getSimilarBatchFiles(getSourceFolder(), getFilename());
		} catch (NoSuchFileException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		logger.info("IN10 size: " + allIN10s.size());
		for (String oneIN10 : allIN10s) {
			try {
				IN10 in10 = processInterfaceFile(oneIN10);
				physicalInventoryService.addPhysicalInventoryDocument(in10);
			} catch (FileProcessingException fpe) {
				//generate an error report
				logger.error("Exception thrown: " + fpe.getMessage());			
				//error.setProcessingDate(new Date());
				//error.setDescription(fpe.getMessage());
				error.setErrorType(GeneralHelper.PROCESSING_ERROR);
				//error.setSection(GeneralHelper.DB_TABLE);
				error.setFilePath(getProcessingErrorFolder());
				//error.setFilename(new File(oneIN10).getName());
				interfaceErrorRepo.save(error);
				doBackup(new File(oneIN10), getProcessingErrorFolder());
	//			doBackup(new File(oneIN10), getProcessingErrorFolder());
//			} catch (BCDSDataException dbe){
//				logger.error("Exception thrown: " + dbe.getMessage());
//				interfaceErrorRepo.save(new InterfaceError(GeneralHelper.IN10, new File(oneIN10).getName(), new Date(), GeneralHelper.DB_TABLE, dbe.getMessage(), GeneralHelper.PROCESSING_ERROR, getProcessingErrorFolder()));
//				sendEmailNotification(oneIN10,dbe.getMessage());
//				doBackup(new File(oneIN10), getProcessingErrorFolder());
//				continue;
//			} catch (ParseException parseException){
//				logger.error("Exception thrown: " + parseException.getMessage());
//				interfaceErrorRepo.save(new InterfaceError(GeneralHelper.IN10, new File(oneIN10).getName(), new Date(), GeneralHelper.DB_TABLE, parseException.getMessage(), GeneralHelper.PROCESSING_ERROR, getProcessingErrorFolder()));
//				sendEmailNotification(oneIN10,parseException.getMessage());
//				doBackup(new File(oneIN10), getProcessingErrorFolder());
//				continue;
			} catch (Exception e) {
				logger.error("Exception thrown: " + e.getMessage());
				interfaceErrorRepo.save(new InterfaceError(GeneralHelper.IN10, new File(oneIN10).getName(), new Date(), GeneralHelper.DB_TABLE, e.getMessage(), GeneralHelper.PROCESSING_ERROR, getProcessingErrorFolder()));
				sendEmailNotification(oneIN10, e.getMessage());
				doBackup(new File(oneIN10), getProcessingErrorFolder());
				continue;
			}
//			logger.info(oneIN10.toString() + " processed successfully: " + oneIN10);
			doBackup(new File(oneIN10), getBackupFolder());
		}
	}

	
	private IN10 processInterfaceFile(String oneIN10) throws FileProcessingException, ParseException {
		Scanner scanner = null;
		IN10 in10 = new IN10();
		File file = new File(oneIN10);
		FileReader fr = null;
		BufferedReader bf = null;
		List<IN10PIDLine> in10PidLineList = new ArrayList<IN10PIDLine>();
		try {
			fr = new FileReader(file);			
		    bf = new BufferedReader(fr);
		    scanner = new Scanner(bf);				 
		    IN10PIDLine in10PIDLine = new IN10PIDLine();
		    PidBean pidBean = new PidBean();
		    List<PidListBean> pidListBeanList = new ArrayList<PidListBean>();
	        while(scanner.hasNextLine()){
	        	String oneLine = scanner.nextLine();
	        	String[] fields = oneLine.split("\\|");
//	        	Map<String, Integer> positions =new HashMap<String, Integer>();
	        	if(fields.length > 15  && fields[0].contains("EDI_DC40")) {
	        		pidBean.setPidNumber(Integer.parseInt(fields[2]));
	        	}
	        	 if(fields.length > 5  && StringUtils.isNumeric(fields[0])) {
	        		PidListBean pidListBean = new  PidListBean();
	        		AssetBean asset = new AssetBean();
	        		CompanyAssetParts companyAssetParts = new CompanyAssetParts();
	        		companyAssetParts.setPartName(fields[6]);
	        		asset.setCompanyAssetParts(companyAssetParts);
	        		pidListBean.setAsset(asset);
	        		CompanyLocation companyLocation = new CompanyLocation();
	        		String barcode = fields[2] + fields[3];
	        		companyLocation.setBarcode(barcode);
	        		pidBean.setPidNumber(Integer.parseInt(fields[0]));
	        		pidListBean.setLocation(companyLocation);
	        		pidBean.setFiscalYear(fields[1]);
	        		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
	        		pidBean.setDateOfLastCount(format.parse(fields[4]));
	        		pidBean.setPlant(fields[2]);
	        		pidBean.setsLocs(fields[3]);
//	        		System.out.println("fields[8] value::" + fields[8]);
	        		pidListBean.setActualQuantity(Double.parseDouble(fields[8]));
		        	pidListBean.setExpectedQuantity(Double.parseDouble(fields[8]));
		        	pidListBean.setLineNumber(fields[5]);
	        		pidListBean.setPidBean(pidBean);
	        		pidListBeanList.add(pidListBean);
	        		pidBean.setPidList(pidListBeanList);
	        		in10PIDLine.setPidBean(pidBean);
	        		if (StringUtils.isNumeric(fields[9])) {
	        			in10PIDLine.setUnitOfMeasure(Integer.parseInt(fields[9]));
	        		}
	        	 }
	        }
	        in10PidLineList.add(in10PIDLine);
	        in10.setIn10PIDLine(in10PidLineList);
			}catch(FileNotFoundException fnfe){
				//file should always be there			
				logger.error(fnfe.getMessage());
			} catch(NumberFormatException numberFormatException){
				logger.error(numberFormatException.getMessage());
			}finally{
				if (scanner != null)
					scanner.close();
				if (bf != null){
					try{
						bf.close();
					}catch(IOException ioe){
						logger.error("BufferedReader not closed" + ioe.getMessage());
					}
				}
				
				if (fr != null){
					try{
						fr.close();
					}catch(IOException ioe){
						logger.error("FileReader not closed" + ioe.getMessage());
					}
				}				
			}	        
		return in10;
	}
	
	public static void main(String agrs[]){
		try {
			@SuppressWarnings("resource")
			ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
			 SchedulerFactoryBean schedulerFactoryBean = new SchedulerFactoryBean();
			 SimpleTrigger trigger = context.getBean("in10JobTrigger", SimpleTrigger.class);
			 JobDetail jobDetail =  context.getBean("in10JobBean", JobDetail.class);
			 schedulerFactoryBean.setJobDetails(jobDetail);
			 schedulerFactoryBean.setTriggers(trigger);
			 Scheduler scheduler= (Scheduler) context.getBean("schedulerBean");		 
			 scheduler.scheduleJob(jobDetail, trigger);
			} catch (Exception  e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	private void sendEmailNotification(String fileName,String message){
		try {
			sendEmailWithAttachment(fileName+" failed", message, new File(fileName));
		} catch (BCDSEmailException e) {
			logger.error("Email failed to send to recipients: " + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
