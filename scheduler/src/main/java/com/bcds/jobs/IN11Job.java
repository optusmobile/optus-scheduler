package com.bcds.jobs;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleTrigger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import com.bcds.file.helper.CSVUtil;
import com.bcds.file.helper.GeneralHelper;
import com.bcds.file.helper.OrderStatusEnum;
import com.bcds.ftp.util.FileUtil;
import com.bcds.jobs.exception.BCDSEmailException;
import com.bcds.jobs.exception.BCDSFTPException;
import com.bcds.jpa.entity.InterfaceError;
import com.bcds.jpa.entity.OrderStatus;
import com.bcds.jpa.entity.PhysicalInventoryDocument;
import com.bcds.jpa.entity.PhysicalInventoryDocumentList;
import com.bcds.jpa.model.in11.IN11;
import com.bcds.jpa.model.in11.IN11Header;
import com.bcds.jpa.model.in11.IN11Line;
import com.bcds.util.DateUtil;

import au.com.bytecode.opencsv.CSVWriter;

/*
 * IN11 - PID stock-take completion message
 * 
 * Date				Change description
 * -----       	   ---------------------
 * 
 * 9 May 2016		Creation.
 * 					Created model classes.
 * 					
 * 11 May 2016 		Completed the IN11 module. Tested ok!!			
 * 
 * 12 May 2016		Added Error Folders. Changed the FTP function and re-tested ok!!	
 * 					Completed email function. Send to recipients when error occurs.
 * 
 * 13 May 2016		Improved the error handling codes.
 * 	
 */

public class IN11Job extends AbstractOutgoingJob {

	final static Logger logger = Logger.getLogger(IN11Job.class);
	
	final static String EXPIRED_BATCH_NUMBER = "V5.7.2";

	@Value( "${in11.filename}" )
	protected String filename;

	@Value( "${in11.backup.folder}" )
	protected String backupFolder;

	@Value( "${in11.source.folder}" )
	protected String sourceFolder;

	@Override
	public void executeJob() {
		
		logger.info("----------- IN11 Job Started ---------");
		setFilename(this.filename);
		setBackupFolder(this.backupFolder);
		setSourceFolder(this.sourceFolder);
		prepareInterfaceFile();					
		//if no files don't ftp			
		List<String> allIN11s = new ArrayList<String>();
		try {
			allIN11s = FileUtil.getSimilarBatchFiles(getSourceFolder(), getFilename());
		} catch (NoSuchFileException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		for (String oneIN11 : allIN11s) {
			File file = new File(oneIN11);	
			try{
				executeFTP(file);
			}catch (BCDSFTPException ftpe){
				logger.error("FTP failed to send file to remote server: " + ftpe.getMessage());
				interfaceErrorRepo.save(new InterfaceError(GeneralHelper.IN11, file.getName(), new Date(), GeneralHelper.FTP_SERVER, "FTP failed to execute.",GeneralHelper.PROCESSING_ERROR, getProcessingErrorFolder()));
				try{
					sendEmailWithAttachment("OFT failure", "FTP failed, please contect OFT team.", file);
				} catch (BCDSEmailException ee){
					logger.error("Email failed to send to recipients: " + ee.getMessage());
					interfaceErrorRepo.save(new InterfaceError(GeneralHelper.IN11, file.getName(), new Date(), GeneralHelper.SMTP_SERVER, "Email failed to execute.", GeneralHelper.PROCESSING_ERROR, getProcessingErrorFolder()));
				}
				doBackup(file, getSendingErrorFolder());
				continue;
			}
			//all good
			doBackup(file, getBackupFolder());	
		}
	}

	@Override
	public void prepareInterfaceFile()  {
		CSVWriter writer = null;
		FileWriter fileWriter = null;
		List<IN11> in11s = getIn11ReturnRecords();
		logger.info("IN11 size " + in11s.size());
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd-HHmmss-SSS");
		int count = 1;
		for (IN11 in11 : in11s){
			File in11File = new File(getSourceFolder()+"O3"+"_"+"STCC" + format.format(new Date()) + ".csv");
			try {
				
				fileWriter = new FileWriter(in11File);
				writer = new CSVWriter(fileWriter, '|', CSVWriter.NO_QUOTE_CHARACTER);
				DynaBean bean = CSVUtil.prepareIN11CSVLinePosition(in11);
				List<String[]> fileLines = new ArrayList<String[]>();
				List<?> headerList = (ArrayList<?>)bean.get(IN11Header.lineType);
				//only one header line
				fileLines.add(headerList.toArray(new String[headerList.size()]));
				//producing multiple pid lines here
				for (int i=0; i<in11.getIn11Lines().size(); i++){
					List<?> lineList = (ArrayList<?>)bean.get(IN11Line.lineType+i);
					fileLines.add(lineList.toArray(new String[lineList.size()]));
				}
				writer.writeAll(fileLines);
				// delay 1 milliseconds to prevent file name overlapping 
				TimeUnit.MILLISECONDS.sleep(30);
			} catch (InterruptedException e) {
				logger.error(e, e);
			} catch (IOException ioe) {

				interfaceErrorRepo.save(new InterfaceError(GeneralHelper.IN11, in11File.getName(), new Date(), GeneralHelper.FILE_IO, ioe.getMessage(), 
						GeneralHelper.PROCESSING_ERROR, getProcessingErrorFolder()));
				
			}finally{
				try{
					if (writer != null)
						writer.close();
					
					if (fileWriter != null)
						fileWriter.close();
				}catch (Exception ex){
					logger.error("Error closing IN11 output file.");
				}
			}
			count++;
		}		
	}

	private List<IN11> getIn11ReturnRecords() {
		//one file for one Finished PID record
		OrderStatus orderStatus = orderStatusRepo.findByOrderStatus(OrderStatusEnum.FINISHED.toString());		
		List<PhysicalInventoryDocument> pids = pidRepo.findByOrderStatus(orderStatus);
		List<IN11> in11s = new ArrayList<IN11>();
		int locationId = 0;
		IN11 in11 = new IN11();
		IN11Header header = new IN11Header();
		List<IN11Line> lines = new ArrayList<IN11Line>();
		IN11Line line = new IN11Line();
		for (PhysicalInventoryDocument pid : pids){
			if(locationId == 0 || locationId == pid.getLocation().getId()){
				locationId = pid.getLocation().getId();
				in11 = new IN11();
				header = new IN11Header();
				lines = new ArrayList<IN11Line>();
				header.setInterfaceType(GeneralHelper.IN11);
				header.setPhysicalInventoryDocument("0"+pid.getPidNumber());
				header.setFiscalYear(pid.getFiscalYear());			
				header.setDateOfLastCount(DateUtil.getLocalDateInSimpleDateFormat("yyyyMMdd",pid.getDateOfLastCount()));
				in11.setIn11Header(header);
				for (PhysicalInventoryDocumentList list : pid.getPidList()){
					line = new IN11Line();
					line.setInterfaceType(GeneralHelper.IN11);
					line.setLineNumber(list.getLineNumber());
					line.setMaterialNumber(list.getCompanyAssetParts().getPartName());
					line.setBatchNumber(StringUtils.isBlank(list.getCompanyAssetParts().getBatchName()) ? "" : list.getCompanyAssetParts().getBatchName());
					line.setQuantity(list.getActualQuantity().toString());
					line.setUnitOfEntry("EA");
					line.setZeroCount( list.getActualQuantity() == 0 ? "X" : "");
					if(lines.size()>0){
						if(lines.get(lines.size()-1).getMaterialNumber().equalsIgnoreCase(list.getCompanyAssetParts().getPartName())){
							lines.get(lines.size()-1).setBatchNumber(EXPIRED_BATCH_NUMBER);
						}
					}
					lines.add(line);
				}
				in11.setIn11Lines(lines);
				in11s.add(in11);
				assetService.updatePIDStatusToClosed(pid);
			}
		}
		return in11s;
	}
	
	public static void main(String agrs[]) {
		try {
			@SuppressWarnings("resource")
			ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
			 SchedulerFactoryBean schedulerFactoryBean = new SchedulerFactoryBean();
			 SimpleTrigger trigger = context.getBean("in11JobTrigger", SimpleTrigger.class);
			 JobDetail jobDetail =  context.getBean("in11JobBean", JobDetail.class);
			 schedulerFactoryBean.setJobDetails(jobDetail);
			 schedulerFactoryBean.setTriggers(trigger);
			 Scheduler scheduler= (Scheduler) context.getBean("schedulerBean");		 
			 scheduler.scheduleJob(jobDetail, trigger);
			} catch (Exception  e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

	
}
