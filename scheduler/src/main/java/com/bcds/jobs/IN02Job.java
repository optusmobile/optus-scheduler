package com.bcds.jobs;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleTrigger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Component;
import com.bcds.file.helper.CSVUtil;
import com.bcds.file.helper.GeneralHelper;
import com.bcds.ftp.util.FileUtil;
import com.bcds.jobs.beans.BeanToEntityClass;
import com.bcds.jobs.beans.CompanyLocation;
import com.bcds.jobs.beans.EntityToBeanClass;
import com.bcds.jobs.beans.OrderMeterialList;
import com.bcds.jobs.beans.OrderType;
import com.bcds.jobs.exception.BCDSEmailException;
import com.bcds.jobs.exception.BCDSFTPException;
import com.bcds.jpa.entity.InterfaceError;
import com.bcds.jpa.entity.Order;
import com.bcds.jpa.entity.OrderRelationshipEntity;
import com.bcds.jpa.model.in02.IN02;
import com.bcds.jpa.model.in02.IN02Header;
import com.bcds.jpa.model.in02.IN02Line;
import com.bcds.jpa.repo.CompanyLocationRepository;
import com.bcds.jpa.repo.OrderRelationshopRepo;
import com.bcds.jpa.repo.OrderRepository;
import com.bcds.service.iface.CompanyLocationServiceInterface;
import com.bcds.util.DateUtil;
import au.com.bytecode.opencsv.CSVWriter;

/*
 * IN02 - Return confirmation of IN01
 * 
 * Date				Change description
 * -----       	   ---------------------
 * 
 * 4 May 2016		Received latest format and changed (again) accordingly. Has 'H' for header and 'L' for line.
 * 
 * 9 May 2016		Changed codes to cater for multiple output files.
 * 					Tested OK as of 5pm 9 May 2016.
 * 
 *  12 May 2016		Added Error Folders. Changed the FTP function and retested ok!!	
 *  				Completed email function. Send to recipients when error occurs.
 *  
 *  13 May 2016		Improved the error handling codes.
 */


@Component
public class IN02Job extends AbstractOutgoingJob {

	final static Logger logger = Logger.getLogger(IN02Job.class);

	@Value( "${in02.filename}" )
	protected String filename;

	@Value( "${in02.backup.folder}" )
	protected String backupFolder;

	@Value( "${in02.source.folder}" )
	protected String sourceFolder;
	
	@Autowired
	OrderRelationshopRepo orderRelationshipRepo;
	
	@Autowired
	BeanToEntityClass beanToEntityClass;
	
	@Autowired
	EntityToBeanClass entityToBeanClass;
	
	@Autowired
	OrderRepository orderRepo;
	
	@Autowired
	CompanyLocationServiceInterface companyLocationService;
	
	@Autowired
	CompanyLocationRepository companyLocationRepository;
	
	@Override
	public void executeJob() {
		
		logger.info("----------- IN02 Job Started ---------");		
		setFilename(this.filename);
		setBackupFolder(this.backupFolder);
		setSourceFolder(this.sourceFolder);
		prepareInterfaceFile();			
		//if no files don't ftp
		List<String> allIN02s = new ArrayList<String>();
		try {
			allIN02s = FileUtil.getSimilarBatchFiles(getSourceFolder(), getFilename());
		} catch (NoSuchFileException e) {
			
		}
		int n =0;
		if(allIN02s!=null){
			n = allIN02s.size();
		}
		logger.debug("Total "+ n + " files found to send.");
		for (String oneIN02 : allIN02s) {
			File file = new File(oneIN02);
			try{
				logger.debug("Sending file: "+ file.getName());
				if(file.exists()) {
					executeFTP(file);
				}
				logger.debug("Sending file success.");
			}catch (BCDSFTPException ftpe){
				logger.error("FTP failed to send file to remote server: " + ftpe.getMessage());
				interfaceErrorRepo.save(new InterfaceError(GeneralHelper.IN02, file.getName(), new Date(), GeneralHelper.FTP_SERVER, "FTP failed to execute.",
						GeneralHelper.SENDING_ERROR, getProcessingErrorFolder()));
				try{
					sendEmailWithAttachment("OFT failure", "FTP failed, please contect OFT team.", file);
				} catch (BCDSEmailException ee){
					logger.error("Email failed to send to recipients: " + ee.getMessage());
					interfaceErrorRepo.save(new InterfaceError(GeneralHelper.IN02, file.getName(), new Date(), GeneralHelper.SMTP_SERVER, "Email failed to execute.", 
							GeneralHelper.PROCESSING_ERROR, getProcessingErrorFolder()));
				}
				doBackup(file, getSendingErrorFolder());	
				continue;
			}
			//all good
			doBackup(file, getBackupFolder());
			logger.debug("Backup file success.");
		}
	}	
	

	@Override
	public void prepareInterfaceFile() {
		
		CSVWriter writer = null;
		FileWriter fileWriter = null;
		List<IN02> in02s = getIn02ReturnRecords();
		int count = 1;
		for (IN02 in02 : in02s){
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd-HHmmss-SSS");
			System.out.println("format.format(new Date():: " + format.format(new Date()));
			String sample1 = (format.format(new Date()).substring(0, 10));
			Integer dateLength = format.format(new Date()).length();
			String sample2 = sample1.replaceAll("-", "");
			File in02File = new File(getSourceFolder()+"O3"+"_"+"PURM" + format.format(new Date()) + ".csv");
			logger.info("Output file name  for the Job IN02 ::" + in02File.getName());
			try {
				fileWriter = new FileWriter(in02File);
				writer = new CSVWriter(fileWriter, '|', CSVWriter.NO_QUOTE_CHARACTER);
				//DynaBean bean = CSVUtil.prepareIN02CSVLinePosition(in02);
				DynaBean bean = CSVUtil.prepareIN02CSVLinePosition(in02);
				List<String[]> fileLines = new ArrayList<String[]>();
				List<?> headerList = (ArrayList<?>)bean.get(IN02Header.lineType);
				fileLines.add(headerList.toArray(new String[headerList.size()]));
				//producing multiple pid lines here
				for (int i=0; i<in02.getIn02Line().size(); i++){
					List<?> lineList = (ArrayList<?>)bean.get(IN02Line.lineType+i);
					fileLines.add(lineList.toArray(new String[lineList.size()]));
				}
				writer.writeAll(fileLines);
				// delay 1 milliseconds to prevent file name overlapping 
				TimeUnit.MILLISECONDS.sleep(30);
			} catch (InterruptedException e) {
				logger.error(e, e);
				} catch (IOException ioe) {
					if(in02File.exists()){
						doBackup(in02File, getProcessingErrorFolder());	
						interfaceErrorRepo.save(new InterfaceError(GeneralHelper.IN02, in02File.getName(), new Date(), 
								GeneralHelper.FILE_IO, ioe.getMessage(), GeneralHelper.PROCESSING_ERROR, getProcessingErrorFolder()));
					} else {
					interfaceErrorRepo.save(new InterfaceError(GeneralHelper.IN02, in02File.getName(), new Date(), 
							GeneralHelper.FILE_IO, ioe.getMessage(), GeneralHelper.PROCESSING_ERROR, getProcessingErrorFolder()));
				}
				}finally{
					try{
						if (writer != null)
							writer.close();
						if (fileWriter != null)
							fileWriter.close();
					}catch (Exception ex){
						logger.error("Error closing IN02 output file.");
					}
				}
				count++;
			}
	}	

	
	private List<IN02> getIn02ReturnRecords(){
		List<com.bcds.jobs.beans.Order> orders = orderService.fillIN02Records();
		List<IN02> in02s = new ArrayList<IN02>();
		logger.info("IN02 orders size: " + orders.size());		
		for (com.bcds.jobs.beans.Order order : orders){
			IN02 in02 = new IN02();
			IN02Header in02Header = new IN02Header();
			in02Header.setInterfaceType(GeneralHelper.IN02);
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
			in02Header.setPostingDate(DateUtil.getLocalDateInSimpleDateFormat("yyyyMMdd",new Date()));
			String salesOrderNumber = "";
			if (order.getOrderReceivedDate() != null) {
				in02Header.setDocumentDate(DateUtil.getLocalDateInSimpleDateFormat("yyyyMMdd",order.getOrderReceivedDate()));
			}
			//question - is order number ok?
			in02Header.setDeliveryNumber(getAssociatedSTO(order));
			//question - what's this?
			in02Header.setHeaderText("");
			//question - how to determine which type of movement?
			if (order.getOrderActionType().getActionName().equalsIgnoreCase("GR")) {
				in02Header.setGmCode("01");
			}
			else {
				in02Header.setGmCode("04");
			}
			if (!StringUtils.isEmpty(order.getMrf_ref())) {
				in02Header.setGmCode("01");
			}
			List<IN02Line> orderLineList = new ArrayList<IN02Line>();
			List<OrderMeterialList> list = order.getOrderMeterialList();
			if (!list.isEmpty()) {
				for (OrderMeterialList orderMeterialList : list) {
				IN02Line in02Line = new IN02Line();	
				in02Line.setInterfaceType(GeneralHelper.IN02);
				OrderType orderType = order.getOrderType();
				String mrfRef = order.getMrf_ref();
				in02Line.setMovementType("973");
				if (!StringUtils.isEmpty(mrfRef)) {
					if (order.getOrderType().getOrderTypeName().equalsIgnoreCase("SCPO") && mrfRef.equalsIgnoreCase("A.1")) {
						in02Line.setMovementType(orderService.getByActionTypeAndDocType(order.getOrderType().getOrderTypeName(), 
							order.getMrf_ref()).getActionTypeSap());
					}
					if ( mrfRef.equalsIgnoreCase("A.2")) {
						in02Line.setMovementType(orderService.getByActionTypeAndDocType("STO", order.getMrf_ref()).getActionTypeSap());
					}
					if (mrfRef.equalsIgnoreCase("A.4")) {
						in02Line.setMovementType(orderService.getByActionTypeAndDocType("STO", order.getMrf_ref()).getActionTypeSap());
					}
					if (mrfRef.equalsIgnoreCase("A.5")) {
						in02Line.setMovementType(orderService.getByActionTypeAndDocType("SCPO", order.getMrf_ref()).getActionTypeSap());
					}
					if (mrfRef.equalsIgnoreCase("A.6")) {
						in02Line.setMovementType(orderService.getByActionTypeAndDocType("SCPO", order.getMrf_ref()).getActionTypeSap());
					}
					if (mrfRef.equalsIgnoreCase("A.7")) {
						in02Line.setMovementType(orderService.getByActionTypeAndDocType("STO", order.getMrf_ref()).getActionTypeSap());
					}
				}
				if (orderType.getOrderTypeName().equalsIgnoreCase("DD") && order.getDdType().equalsIgnoreCase("Z1NL")) {
					String movementType = orderService.getByActionTypeAndDocType("GR", "Z1NL").getActionTypeSap();
					in02Line.setMovementType(orderService.getByActionTypeAndDocType("GR", "Z1NL").getActionTypeSap());
					List<CompanyLocation> companyLocationListBean = companyLocationService.getCompanyLicationByUserId(order.getUpdatedBy().getUser_id().toString());
					if(order.getCompanyLocation()!= null && order.getCompanyLocation().getBarcode().equalsIgnoreCase("1W00VE01") && !companyLocationListBean.isEmpty()){
						String fullLocationName = companyLocationRepository.getOne(companyLocationListBean.get(0).getParentLocation()).getLocationName();
						String visitingEngSloc = companyLocationListBean.get(0).getLocationName();
						in02Line.setVisitingEngSloc(visitingEngSloc);
						in02Line.setVisitingEngPlant(fullLocationName);
					} else {
						in02Line.setVisitingEngSloc("");
						in02Line.setVisitingEngPlant("");
					}
				}
				else {
					in02Line.setVisitingEngSloc("");
					in02Line.setVisitingEngPlant("");
				}
				if (order.getDdType() != null && order.getDdType().equalsIgnoreCase("NL")) {
					in02Line.setMovementType(orderService.getByActionTypeAndDocType("GR", "NL").getActionTypeSap());
				}
				in02Line.setMaterialNumber(orderMeterialList.getMeterial().getPartName());//part_num field not used, use part_name!!
				in02Line.setQuantity(orderMeterialList.getQuantity()+"");
				String fullLocationName = companyService.findPlantLocation(order.getCompanyLocation().getParentLocation()).getFullLocationName();
				in02Line.setPlant(fullLocationName);
				in02Line.setSourceLocation(order.getCompanyLocation().getLocationName());
				if (order.getDdType() != null && order.getDdType().equalsIgnoreCase("Z1NL")){
					in02Line.setBatchNumber(orderMeterialList.getBatchNumber());
				} else {
					in02Line.setBatchNumber(orderMeterialList.getMeterial().getBatchNumber());
				}
				in02Line.setUom("EA");
				in02Line.setSalesOrderLineNum("00010");
				if (orderType.getOrderTypeName().equalsIgnoreCase("DD")){
					List<OrderRelationshipEntity> orderRelationshipEntityList = orderRelationshipRepo.findByParentOrderId(beanToEntityClass.convertBeanToEntity(order));
					for (OrderRelationshipEntity orderRelationshipEntity : orderRelationshipEntityList) {
						if (orderRelationshipEntity.getChildOrderId().getOrderType().getOrderTypeName().equalsIgnoreCase("SO")) {
							salesOrderNumber = orderRepo.getOne(orderRelationshipEntity.getChildOrderId().getId()).getOrderNumber();
							 }
						}
					}
				in02Line.setSalesOrderNum(salesOrderNumber);
				if (!StringUtils.isEmpty(orderMeterialList.getSalesOrderLineNumber())) {
					in02Line.setSalesOrderLineNum(orderMeterialList.getSalesOrderLineNumber());
				}
				orderLineList.add(in02Line);
				}
			}	
			/*else {
				//return  new ArrayList<IN02>();
			}*/
			in02.setIn02Header(in02Header);
			in02.setIn02Line(orderLineList);
			in02s.add(in02);
			orderService.updateOrderStatusToClosed(order);
		}
		return in02s;
	}

	
	private String getAssociatedSTO(com.bcds.jobs.beans.Order order) {
		if (order.getOrderType().getOrderTypeName().equalsIgnoreCase("DD")) {
			List<OrderRelationshipEntity> orderRelationshipEntityList = orderRelationshipRepo.findByParentOrderId(beanToEntityClass.convertBeanToEntity(order));
			for (OrderRelationshipEntity orderRelationshipEntity: orderRelationshipEntityList) {
				Order childOrder = orderRelationshipEntity.getChildOrderId();
				if (childOrder.getOrderType().getOrderTypeName().equalsIgnoreCase("STO")) {
					return childOrder.getOrderNumber();
				}
			}
		}
		return order.getOrderNumber();
	}
	
	public static void main (String[] args) {
		try {
		@SuppressWarnings("resource")
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		 SchedulerFactoryBean schedulerFactoryBean = new SchedulerFactoryBean();
		 SimpleTrigger trigger = context.getBean("in02JobTrigger", SimpleTrigger.class);
		 JobDetail jobDetail =  context.getBean("in02JobBean", JobDetail.class);
		 schedulerFactoryBean.setJobDetails(jobDetail);
		 schedulerFactoryBean.setTriggers(trigger);
		 Scheduler scheduler= (Scheduler) context.getBean("schedulerBean");		 
		 scheduler.scheduleJob(jobDetail, trigger);
		} catch (Exception  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
