package com.bcds.jobs;

import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.bcds.jobs.exception.BCDSEmailException;
import com.bcds.jpa.entity.InterfaceError;
import com.bcds.jpa.repo.CompanyLocationRepository;
import com.bcds.jpa.repo.InterfaceErrorRepository;
import com.bcds.jpa.repo.OrderStatusRepository;
import com.bcds.jpa.repo.PhysicalInventoryDocumentRepository;
import com.bcds.service.iface.AssetServiceInterface;
import com.bcds.service.iface.CompanyServiceInterface;
import com.bcds.service.iface.OrderServiceInterface;
import com.bcds.service.iface.PhysicalInventoryService;
import com.bcds.service.iface.ReturnTransactionsService;
import com.bcds.service.iface.StockVarianceServiceInterface;
import com.bcds.util.EmailUtil;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class AbstractBaseJob {
	
	final static Logger logger = Logger.getLogger(AbstractBaseJob.class);
	
	@Autowired
	AssetServiceInterface assetService;
	
	@Autowired
	OrderServiceInterface orderService;
	
	@Autowired
	OrderStatusRepository orderStatusRepo;
	
	@Autowired
	CompanyServiceInterface companyService;
	
	@Autowired
	InterfaceErrorRepository interfaceErrorRepo;
	
	@Autowired
	PhysicalInventoryService physicalInventoryService;
	
	@Autowired
	StockVarianceServiceInterface stockVarianceService;
	
	@Autowired
	CompanyLocationRepository companyLocationRepository;
	
	@Autowired
	ReturnTransactionsService returnTransactionsService; 
	
	public ReturnTransactionsService getReturnTransactionsService() {
		return returnTransactionsService;
	}

	public void setReturnTransactionsService(ReturnTransactionsService returnTransactionsService) {
		this.returnTransactionsService = returnTransactionsService;
	}
	
		
	public StockVarianceServiceInterface getStockVarianceService() {
		return stockVarianceService;
	}

	public void setStockVarianceService(StockVarianceServiceInterface stockVarianceService) {
		this.stockVarianceService = stockVarianceService;
	}
	
	public PhysicalInventoryService getPhysicalInventoryService() {
		return physicalInventoryService;
	}


	public void setPhysicalInventoryService(PhysicalInventoryService physicalInventoryService) {
		this.physicalInventoryService = physicalInventoryService;
	}


	public AssetServiceInterface getAssetService() {
		return assetService;
	}


	public void setAssetService(AssetServiceInterface assetService) {
		this.assetService = assetService;
	}


	public OrderServiceInterface getOrderService() {
		return orderService;
	}


	public void setOrderService(OrderServiceInterface orderService) {
		this.orderService = orderService;
	}


	public OrderStatusRepository getOrderStatusRepo() {
		return orderStatusRepo;
	}


	public void setOrderStatusRepo(OrderStatusRepository orderStatusRepo) {
		this.orderStatusRepo = orderStatusRepo;
	}


	public CompanyServiceInterface getCompanyService() {
		return companyService;
	}


	public void setCompanyService(CompanyServiceInterface companyService) {
		this.companyService = companyService;
	}


	public InterfaceErrorRepository getInterfaceErrorRepo() {
		return interfaceErrorRepo;
	}


	public void setInterfaceErrorRepo(InterfaceErrorRepository interfaceErrorRepo) {
		this.interfaceErrorRepo = interfaceErrorRepo;
	}


	public PhysicalInventoryDocumentRepository getPidRepo() {
		return pidRepo;
	}


	public void setPidRepo(PhysicalInventoryDocumentRepository pidRepo) {
		this.pidRepo = pidRepo;
	}


	public EmailUtil getEmailUtil() {
		return emailUtil;
	}


	public void setEmailUtil(EmailUtil emailUtil) {
		this.emailUtil = emailUtil;
	}


	public String getFilename() {
		return filename;
	}


	public void setFilename(String filename) {
		this.filename = filename;
	}


	public String getBackupFolder() {
		return backupFolder;
	}


	public void setBackupFolder(String backupFolder) {
		this.backupFolder = backupFolder;
	}


	public String getSourceFolder() {
		return sourceFolder;
	}


	public void setSourceFolder(String sourceFolder) {
		this.sourceFolder = sourceFolder;
	}


	public String getProcessingErrorFolder() {
		return processingErrorFolder;
	}


	public void setProcessingErrorFolder(String processingErrorFolder) {
		this.processingErrorFolder = processingErrorFolder;
	}


	public String getSendingErrorFolder() {
		return sendingErrorFolder;
	}


	public void setSendingErrorFolder(String sendingErrorFolder) {
		this.sendingErrorFolder = sendingErrorFolder;
	}


	public InterfaceError getError() {
		return error;
	}


	public void setError(InterfaceError error) {
		this.error = error;
	}

	@Autowired 
	PhysicalInventoryDocumentRepository pidRepo;
	
	@Autowired
	EmailUtil emailUtil;
	
	@Value( "" )
	protected String filename;

	@Value( "" )
	protected String backupFolder;

	@Value( "" )
	protected String sourceFolder;
	
	@Value( "${processing.error.folder}" )
	protected String processingErrorFolder;
	
	@Value( "${sending.error.folder}" )
	protected String sendingErrorFolder;
	
	protected InterfaceError error;
	
	
	public abstract void executeJob();
	
	
	protected void saveErrors(List<InterfaceError> errors){
		interfaceErrorRepo.save(errors);
	}
	
/**
	protected void doBackup(){
		File[] files = new File(getSourceFolder()).listFiles();
		for (File file : files){
			file.renameTo(new File(getBackupFolder()+file.getName()));
		}
	}
**/	
	protected void doBackup(File file, String folder){
		if(file!=null && file.exists()){
//			logger.debug("------ Start to backup file: "+file.getName()+" ----------");
			Path moveFrom = FileSystems.getDefault().getPath(file.getAbsolutePath());
//			logger.debug("------ File from: "+moveFrom.toString()+" ----------");
	        //Path targetDir = FileSystems.getDefault().getPath(getBackupFolder()+file.getName());
			Path targetDir = FileSystems.getDefault().getPath(folder+file.getName());
//			logger.debug("------ File to: "+targetDir.toString()+" ----------");
	        try{
//	        	logger.debug("------ Start to move file: "+file.getName()+" ----------");
	        	Files.move(moveFrom, targetDir, StandardCopyOption.REPLACE_EXISTING);
//	        	logger.debug("------ File transfer success! ----------");
	        }catch(Exception ioe){
	        	//ioe.printStackTrace();
	        	//logger.debug("********** Exception: "+ ioe);
	        }
		} else {
//			logger.debug("------ File: "+file.getName()+" not found! ----------");
		}
	}
	
	protected void sendEmailWithAttachment(String subject, String message, File attachment) throws BCDSEmailException{
		emailUtil.sendMail(subject , message, attachment);		
	}	
}
