package com.bcds.jobs.beans;

import java.util.Date;

public class OrderActionType {
	
	private Integer id;
	private String actionName;
	private String actionTypeSap;
	private Date lastUpdatedDate;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getActionName() {
		return actionName;
	}
	public void setActionName(String actionName) {
		this.actionName = actionName;
	}
	public String getActionTypeSap() {
		return actionTypeSap;
	}
	public void setActionTypeSap(String actionTypeSap) {
		this.actionTypeSap = actionTypeSap;
	}
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	
}
