package com.bcds.jobs.beans;

import java.util.Date;

public class AssetConditionBean {
	
	private int id;
	private String condition;
	private Company compnay;
	private Character updateFlag;
	private Date lastUpdateDate;
	private Integer updatedBy;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public Company getCompnay() {
		return compnay;
	}
	public void setCompnay(Company compnay) {
		this.compnay = compnay;
	}
	public Character getUpdateFlag() {
		return updateFlag;
	}
	public void setUpdateFlag(Character updateFlag) {
		this.updateFlag = updateFlag;
	}
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

}
