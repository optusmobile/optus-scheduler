package com.bcds.jobs.beans;

import java.util.HashMap;
import java.util.Map;
import com.bcds.file.helper.OperatorEnum;
import com.bcds.file.helper.OrderStatusEnum;

public class OrderSearchCriteriaBean {
	
	private OrderStatusEnum orderStatus;
	private Map<OperatorEnum, String> mrfMap = new HashMap<OperatorEnum, String>();
	private String mrfValue;
	
	public OrderStatusEnum getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(OrderStatusEnum orderStatus) {
		this.orderStatus = orderStatus;
	}
	public Map<OperatorEnum, String> getMrfMap() {
		return mrfMap;
	}
	public void setMrfMap(Map<OperatorEnum, String> mrfMap) {
		this.mrfMap = mrfMap;
	}
	public String getMrfValue() {
		return mrfValue;
	}
	public void setMrfValue(String mrfValue) {
		this.mrfValue = mrfValue;
	}
	
}
