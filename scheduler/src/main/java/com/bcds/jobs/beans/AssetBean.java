package com.bcds.jobs.beans;

import java.math.BigDecimal;
import java.util.Date;

public class AssetBean {
	
	private Integer assetId;
	private Integer deviceId;
	private Integer assetType;
	private String tag;
	private String barcode;
	private String serialNumber;
	private CompanyAssetParts partId;
	private String responsibleUserId;
	private String longDesc;
	private String title;
	private CompanyLocation location;
	private String categoryId;
	private BigDecimal  gpsLat;
	private BigDecimal  gpsLong;
	private Date purchaseDate;
	private Double assetValue;
	private String assetImage;
	private Date createDate;
	private User createdBy;
	private String status;
	private Integer company;
	private String conditionId;
	private String companyAssetId;
	private String updateFlag;
	private Date lastUpdateDate;
	private Date imgLastUpdateDate;
	private Integer lastUpdatedBy;
	private Integer previousPlantId;
	private Date lastInspectionDate;
	private String weight;
	private String weightUom;
	private String batchNumber;
	private CompanyAssetParts companyAssetParts;
	
	public CompanyAssetParts getCompanyAssetParts() {
		return companyAssetParts;
	}
	public void setCompanyAssetParts(CompanyAssetParts companyAssetParts) {
		this.companyAssetParts = companyAssetParts;
	}
	public Integer getAssetId() {
		return assetId;
	}
	public void setAssetId(Integer assetId) {
		this.assetId = assetId;
	}
	public Integer getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(Integer deviceId) {
		this.deviceId = deviceId;
	}
	public Integer getAssetType() {
		return assetType;
	}
	public void setAssetType(Integer assetType) {
		this.assetType = assetType;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public CompanyAssetParts getPartId() {
		return partId;
	}
	public void setPartId(CompanyAssetParts partId) {
		this.partId = partId;
	}
	public String getResponsibleUserId() {
		return responsibleUserId;
	}
	public void setResponsibleUserId(String responsibleUserId) {
		this.responsibleUserId = responsibleUserId;
	}
	public String getLongDesc() {
		return longDesc;
	}
	public void setLongDesc(String longDesc) {
		this.longDesc = longDesc;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public CompanyLocation getLocation() {
		return location;
	}
	public void setLocation(CompanyLocation location) {
		this.location = location;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public BigDecimal getGpsLat() {
		return gpsLat;
	}
	public void setGpsLat(BigDecimal gpsLat) {
		this.gpsLat = gpsLat;
	}
	public BigDecimal getGpsLong() {
		return gpsLong;
	}
	public void setGpsLong(BigDecimal gpsLong) {
		this.gpsLong = gpsLong;
	}
	public Date getPurchaseDate() {
		return purchaseDate;
	}
	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	public Double getAssetValue() {
		return assetValue;
	}
	public void setAssetValue(Double assetValue) {
		this.assetValue = assetValue;
	}
	public String getAssetImage() {
		return assetImage;
	}
	public void setAssetImage(String assetImage) {
		this.assetImage = assetImage;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public User getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getCompany() {
		return company;
	}
	public void setCompany(Integer company) {
		this.company = company;
	}
	public String getConditionId() {
		return conditionId;
	}
	public void setConditionId(String conditionId) {
		this.conditionId = conditionId;
	}
	public String getCompanyAssetId() {
		return companyAssetId;
	}
	public void setCompanyAssetId(String companyAssetId) {
		this.companyAssetId = companyAssetId;
	}
	public String getUpdateFlag() {
		return updateFlag;
	}
	public void setUpdateFlag(String updateFlag) {
		this.updateFlag = updateFlag;
	}
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public Date getImgLastUpdateDate() {
		return imgLastUpdateDate;
	}
	public void setImgLastUpdateDate(Date imgLastUpdateDate) {
		this.imgLastUpdateDate = imgLastUpdateDate;
	}
	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Integer getPreviousPlantId() {
		return previousPlantId;
	}
	public void setPreviousPlantId(Integer previousPlantId) {
		this.previousPlantId = previousPlantId;
	}
	public Date getLastInspectionDate() {
		return lastInspectionDate;
	}
	public void setLastInspectionDate(Date lastInspectionDate) {
		this.lastInspectionDate = lastInspectionDate;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getWeightUom() {
		return weightUom;
	}
	public void setWeightUom(String weightUom) {
		this.weightUom = weightUom;
	}
	public String getBatchNumber() {
		return batchNumber;
	}
	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}

}
