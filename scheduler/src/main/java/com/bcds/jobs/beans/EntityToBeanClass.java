package com.bcds.jobs.beans;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import com.bcds.file.helper.OrderStatusEnum;
import com.bcds.jpa.entity.Asset;
import com.bcds.jpa.entity.AssetConditionEntity;
import com.bcds.jpa.entity.AssetMovementInfo;
import com.bcds.jpa.entity.AssetStatusEntity;
import com.bcds.jpa.entity.CompanyAssetCategoryEntity;
import com.bcds.jpa.entity.CompanyEntity;
import com.bcds.jpa.entity.CompanyLocation;
import com.bcds.jpa.entity.LicenseeEntity;
import com.bcds.jpa.entity.OrderMaterialList;
import com.bcds.jpa.entity.OrderStatus;
import com.bcds.jpa.entity.ReturnTransactionsEntity;


@Service("entityToBeanClass")
public class EntityToBeanClass {
	
	private EntityToBeanClass(){
	}
	
	public OrderStatusBean convertEntityToBean(OrderStatus orderStatus) {
		OrderStatusBean orderStatusBean = new OrderStatusBean();
		orderStatusBean.setId(orderStatus.getId());
		orderStatusBean.setLastUpdatedDate(orderStatus.getLastUpdatedDate());
		orderStatusBean.setOrderStatus(orderStatus.getOrderStatus());
		return orderStatusBean;
	}
	
	public AssetBean convertEntityToBean(Asset assetEntity) {
		if (assetEntity == null) {
			return null;
		}
		AssetBean assetBean = new AssetBean();
		assetBean.setAssetId(assetEntity.getAssetCompositeId().getAssetId());
		assetBean.setAssetImage(assetEntity.getAssetImage());
		assetBean.setAssetType(assetEntity.getAssetType());
		if (assetEntity.getAssetValue() != null){
			assetBean.setAssetValue(assetEntity.getAssetValue().doubleValue());
		}
		assetBean.setBarcode(assetEntity.getBarcode());
		assetBean.setBatchNumber(assetEntity.getBatchNumber());
		assetBean.setCategoryId(assetEntity.getCategoryId().toString());
		assetBean.setCompany(assetEntity.getCompanyId());
		assetBean.setCompanyAssetId(assetEntity.getCompanyAssetId());
		assetBean.setCompanyAssetParts(convertEntityToBean(assetEntity.getCompanyAssetParts()));
		if (assetEntity.getConditionId() != null){
			assetBean.setConditionId(convertEntityToBean(assetEntity.getConditionId()).getCondition());
		}
		assetBean.setCreateDate(assetEntity.getCreateDate());
		assetBean.setDeviceId(assetEntity.getAssetCompositeId().getDeviceId());
		assetBean.setGpsLat(assetEntity.getGpsLat());
		assetBean.setGpsLong(assetEntity.getGpsLong());
		assetBean.setImgLastUpdateDate(assetEntity.getImgLastUpdateDate());
		assetBean.setLastInspectionDate(assetEntity.getLastInspectionDate());
		assetBean.setLastUpdateDate(assetEntity.getLastUpdateDate());
		assetBean.setLastUpdatedBy(assetEntity.getLastUpdatedBy());
		assetBean.setLocation(convertEntityToBean(assetEntity.getLocationId()));
		assetBean.setLongDesc(assetEntity.getLongDesc());
		assetBean.setPartId(convertEntityToBean(assetEntity.getCompanyAssetParts()));
		assetBean.setPreviousPlantId(assetEntity.getPreviousPlantId());
		assetBean.setPurchaseDate(assetEntity.getPurchaseDate());
		if (assetEntity.getResponsibleUserId() != null) {
			assetBean.setResponsibleUserId(assetEntity.getResponsibleUserId().toString());
		}
		assetBean.setSerialNumber(assetEntity.getSerialNumber());
		if (assetEntity.getStatusId() != null) {
			assetBean.setStatus(convertEntityToBean(assetEntity.getStatusId()).getStatus());
		}
		assetBean.setTag(assetEntity.getTag());
		assetBean.setTitle(assetEntity.getTitle());
		assetBean.setUpdateFlag(assetEntity.getUpdateFlag().toString());
		assetBean.setWeight(assetEntity.getWeight());
		assetBean.setWeightUom(assetEntity.getWeightUom());
		return assetBean;
	}
	
	public AssetStatusBean convertEntityToBean(AssetStatusEntity  assetStatusEntity) {
		if (assetStatusEntity == null) {
			return null;
		}
		AssetStatusBean assetStatusBean = new AssetStatusBean();
		assetStatusBean.setCompany(convertEntityToBean(assetStatusEntity.getCompany()));
		assetStatusBean.setId(assetStatusEntity.getId());
		assetStatusBean.setLastUpdateDate(assetStatusEntity.getLastUpdateDate());
		assetStatusBean.setStatus(assetStatusEntity.getStatus());
		assetStatusBean.setUpdatedBy(assetStatusEntity.getUpdatedBy());
		assetStatusBean.setUpdateFlag(assetStatusEntity.getUpdateFlag());
		return assetStatusBean;
	}
	
	public AssetConditionBean convertEntityToBean(AssetConditionEntity assetConditionEntity) {
		AssetConditionBean assetConditionBean = new AssetConditionBean();
		if (assetConditionEntity == null) {
			return null;
		}
		assetConditionBean.setCompnay(convertEntityToBean(assetConditionEntity.getCompnay()));
		assetConditionBean.setCondition(assetConditionEntity.getCondition());
		assetConditionBean.setId(assetConditionEntity.getId());
		assetConditionBean.setLastUpdateDate(assetConditionEntity.getLastUpdateDate());
		assetConditionBean.setUpdatedBy(assetConditionEntity.getUpdatedBy());
		assetConditionBean.setUpdateFlag(assetConditionEntity.getUpdateFlag());
		return assetConditionBean;
	}
	
	public Company convertEntityToBean(CompanyEntity companyEntity) {
		if (companyEntity == null) {
			return null;
		}
		Company companyBean = new Company();
		companyBean.setActive(companyEntity.getIsActive());
		companyBean.setAdmin(convertEntityToBean(companyEntity.getAdmin()));
		companyBean.setCloneCompanyId(companyEntity.getCloneCompanyId());
		companyBean.setCompanyName(companyEntity.getCompanyName());
		companyBean.setDeliveryAddress(companyEntity.getDeliveryAddress());
		companyBean.setLastUpdateDate(companyEntity.getLastUpdateDate());
		companyBean.setLicensee(convertEntityToBean(companyEntity.getLicenseeId()));
		companyBean.setMultiScan(companyEntity.getMultiScan().toString());
		companyBean.setPostalAddress(companyEntity.getPostalAddress());
		companyBean.setUpdateFlag(companyEntity.getUpdateFlag().toString());
		return companyBean;
	}
	
	public Licensee convertEntityToBean(LicenseeEntity licenseeEntity) {
		if (licenseeEntity == null) {
			return null;
		}
		Licensee licenseeBean = new Licensee();
		licenseeBean.setId(licenseeEntity.getId());
		licenseeBean.setAccountExpireDate(licenseeEntity.getAccountExpireDate());
		licenseeBean.setAccountType((int)licenseeEntity.getAccountType());
		licenseeBean.setDeliveryAddress(licenseeEntity.getDeliveryAddress());
		licenseeBean.setLicenseeKey(licenseeEntity.getLicenseeKey());
		licenseeBean.setLicenseeLogo(licenseeEntity.getLicenseeLogo());
		licenseeBean.setNumberOfAssets(licenseeEntity.getNumbersOfAsssets());
		licenseeBean.setNumberOfCompany(licenseeEntity.getNumbersOfCompany());
		licenseeBean.setNumberOfDeviceUsers(licenseeEntity.getNumbersOfDeviceUsers());
		licenseeBean.setNumberOfInspectionUsers(licenseeEntity.getNumbersOfInspectionUsers());
		licenseeBean.setNumberOfLocation(licenseeEntity.getNumbersOfLocation());
		licenseeBean.setNumberOfMaintenanceUser(licenseeEntity.getNumbersOfmaintenanceUsers());
		licenseeBean.setNumberOfWebUsers(licenseeEntity.getNumbersOfWebUsers());
		licenseeBean.setOrganizationName(licenseeEntity.getOrganizationName());
		licenseeBean.setUser(convertEntityToBean(licenseeEntity.getUser()));
		return licenseeBean;
	}
	
	
	public com.bcds.jobs.beans.CompanyLocation convertEntityToBean(CompanyLocation companyLocation) {
		com.bcds.jobs.beans.CompanyLocation companyLocationBean = new com.bcds.jobs.beans.CompanyLocation();
		if (companyLocation == null) {
			return companyLocationBean;
		}
		companyLocationBean.setLocationId(companyLocation.getId());
		companyLocationBean.setBarcode(companyLocation.getBarcode());
		companyLocationBean.setCloneLocationId(companyLocation.getCloneLocationId());
		companyLocationBean.setCompany(convertEntityToBean(companyLocation.getCompanyId()));
		companyLocationBean.setLocationAddress(companyLocation.getLocationAddress());
		companyLocationBean.setLocationName(companyLocation.getLocationName());
		companyLocationBean.setLocationType(companyLocation.getLocationType());
		companyLocationBean.setParentLocation(companyLocation.getParentLocationId());
		companyLocationBean.setResponsibleUserId(convertEntityToBean(companyLocation.getResponsibleUserId()));
		companyLocationBean.setRfidReaderId(companyLocation.getRfidReaderId());
		companyLocationBean.setRootLocation(companyLocation.isRootLocation());
		companyLocationBean.setTag(companyLocation.getTag());
		companyLocationBean.setUpdateFlag(companyLocation.getUpdateFlag().toString());
		return companyLocationBean;
	}
	
	public AssetMovementInfoBean convertEntityToBean(AssetMovementInfo assetMovementInfoEntity) {
		if (assetMovementInfoEntity == null) {
			return null;
		}
		AssetMovementInfoBean assetMovementInfoBean = new AssetMovementInfoBean();
		assetMovementInfoBean.setId(assetMovementInfoEntity.getAssetMovementInfoCompositeId().getId());
		assetMovementInfoBean.setAsset(convertEntityToBean(assetMovementInfoEntity.getAsset()));
		assetMovementInfoBean.setAttributeDeviceId(assetMovementInfoEntity.getAssetMovementInfoCompositeId().getAttributeDeviceId());
		assetMovementInfoBean.setDeviceId(assetMovementInfoEntity.getAsset().getAssetCompositeId().getDeviceId());
		assetMovementInfoBean.setLastUpdateDate(assetMovementInfoEntity.getLastUpdateDate());
		assetMovementInfoBean.setMovedBy(assetMovementInfoEntity.getMovedBy());
		assetMovementInfoBean.setMovementDate(assetMovementInfoEntity.getMovementDate());
		assetMovementInfoBean.setMovementRecordedBy(assetMovementInfoEntity.getMovementRecordedBy());
		assetMovementInfoBean.setMovementTypeEnum(assetMovementInfoEntity.getMovementTypeEnum());
		assetMovementInfoBean.setOrderStatus(OrderStatusEnum.valueOf(assetMovementInfoEntity.getOrderStatus().getOrderStatus().toString()));
		assetMovementInfoBean.setSourceLocation(convertEntityToBean(assetMovementInfoEntity.getSourceLocation()));
		assetMovementInfoBean.setDestinationLocation(convertEntityToBean(assetMovementInfoEntity.getDestinationLocation()));
		assetMovementInfoBean.setUpdatedBy(assetMovementInfoEntity.getUpdatedBy());
		assetMovementInfoBean.setUpdateFlag(assetMovementInfoEntity.getUpdateFlag());
		assetMovementInfoBean.setWorkOrderNumber(assetMovementInfoEntity.getWorkOrderNumber());
		assetMovementInfoBean.setPlantType(assetMovementInfoEntity.getPlantType());
		return assetMovementInfoBean;
	}
	
	
	public com.bcds.jobs.beans.User convertEntityToBean(com.bcds.jpa.entity.User userEntity) {
		com.bcds.jobs.beans.User userBean = new com.bcds.jobs.beans.User();
		if (userEntity == null) {
			return userBean;
		}
		if (userEntity.getIsActive() == 1) {
			userBean.setActive(true);
		}
		userBean.setCurrentDeviceUdid(userEntity.getCurrentDeviceUdid());
		userBean.setDeviceType(userEntity.getCurrentDeviceType());
		if (userEntity.getIsDeviceUser() == 1) {
			userBean.setDeviceUser(true);
		}
		userBean.setEmailId(userEntity.getEmail());
		userBean.setFirstName(userEntity.getFirstName());
		if (userEntity.getIsInspectionUser() == 1) {
			userBean.setInspectionUser(true);
		}
		userBean.setLastName(userEntity.getLastName());
		userBean.setLicensee(userEntity.getLicenseeId());
		userBean.setLoginName(userEntity.getLoginname());
		if (userEntity.getIsMaintenanceUser() == 1) {
			userBean.setMaintenanceUser(true);
		}
		userBean.setMiddleName(userEntity.getMiddleName());
		userBean.setMobileDeviceUser(userEntity.isMobileDeviceUser());
		userBean.setParentUser(userEntity.getParentUserId());
		userBean.setPassword(userEntity.getPassword());
		userBean.setPasswordGeneratorNumber(userEntity.getPasswordGeneratorNumber());
		userBean.setPhoneNumber(userEntity.getPhone());
		userBean.setRhdMtUser(userEntity.isRhdMtUser());
		userBean.setUser_id(userEntity.getId());
		userBean.setWebUser(userEntity.isWebUser());
		return userBean;
	}
	
	public com.bcds.jobs.beans.CompanyAssetParts convertEntityToBean(com.bcds.jpa.entity.CompanyAssetParts companyAssetPartEntity) {
		if(companyAssetPartEntity == null) {
			return null;
		}
		com.bcds.jobs.beans.CompanyAssetParts companyAssetPartsBean = new com.bcds.jobs.beans.CompanyAssetParts();
		companyAssetPartsBean.setPartId(companyAssetPartEntity.getId());
		CompanyEntity companyEntity = companyAssetPartEntity.getCompanyId();
		if (companyEntity != null) {
			companyAssetPartsBean.setCompany(convertEntityToBean(companyEntity));
		}
		companyAssetPartsBean.setBatchNumber(companyAssetPartEntity.getBatchName());
		companyAssetPartsBean.setCompany(convertEntityToBean(companyAssetPartEntity.getCompanyId()));
		companyAssetPartsBean.setLastUpdateDate(companyAssetPartEntity.getLastUpdateDate());
		companyAssetPartsBean.setPartDescription(companyAssetPartEntity.getPartDesc());
		companyAssetPartsBean.setPartName(companyAssetPartEntity.getPartName());
		companyAssetPartsBean.setPartNumber(companyAssetPartEntity.getPartNum());
		companyAssetPartsBean.setUpdatedBy(companyAssetPartEntity.getUpdatedBy());
		companyAssetPartsBean.setUpdateFlag(companyAssetPartEntity.getUpdateFlag().toString());
		return companyAssetPartsBean;
	}
	
	public OrderMeterialList convertEntityToBean(OrderMaterialList orderMaterialListEntity) {
		OrderMeterialList orderMeterialList = new OrderMeterialList();
		if (orderMaterialListEntity == null){
			return orderMeterialList;
		}
		orderMeterialList.setId(orderMaterialListEntity.getId());
		orderMeterialList.setLastUpdateDate(orderMaterialListEntity.getLastUpdatedDate());
		orderMeterialList.setLocation(convertEntityToBean(orderMaterialListEntity.getCompanyLocation()));
		orderMeterialList.setMeterial(convertEntityToBean(orderMaterialListEntity.getCompanyAssetParts()));
		orderMeterialList.setSalesOrderLineNumber(orderMaterialListEntity.getSalesOrderLineNumber());
		orderMeterialList.setBatchNumber(orderMaterialListEntity.getBatchNumber());
		//orderMeterialList.setOrder(convertEntityToBean(orderMaterialListEntity.getOrder()));
		orderMeterialList.setQuantity(orderMaterialListEntity.getQuantity().doubleValue());
		orderMeterialList.setLineNumber(orderMaterialListEntity.getLineNumber());
		orderMeterialList.setStatus(OrderStatusEnum.valueOf(orderMaterialListEntity.getStatus().getOrderStatus()));
		return orderMeterialList;
	}
	
	public com.bcds.jobs.beans.Order convertEntityToBean(com.bcds.jpa.entity.Order orderEntity) {
		com.bcds.jobs.beans.Order orderBean = new com.bcds.jobs.beans.Order();
		List<OrderMeterialList> orderMaterialBeanList = new ArrayList<OrderMeterialList>();
		orderBean.setCompanyLocation(convertEntityToBean(orderEntity.getCompanyLocation()));
		orderBean.setCrossRefNumber(orderEntity.getCrossRef());
		orderBean.setId(orderEntity.getId());
		orderBean.setLastUpdatedDate(orderEntity.getLastUpdateDate());
		if (orderEntity.getCompanyLocation() != null) {
			orderBean.setLocationName(convertEntityToBean(orderEntity.getCompanyLocation()).getLocationName());
		}
		orderBean.setMrf_ref(orderEntity.getMrfRef());
		orderBean.setOrderActionType(convertEntityToBean(orderEntity.getOrderActionType()));
		List<OrderMaterialList> orderMaterialEntityList = orderEntity.getOrderMeterialList();
		if (orderMaterialEntityList != null) {
			for (OrderMaterialList orderMeterialEntity: orderMaterialEntityList) {
				orderMaterialBeanList.add(convertEntityToBean(orderMeterialEntity));
			}
		}
		orderBean.setOrderMeterialList(orderMaterialBeanList);
		orderBean.setOrderNumber(orderEntity.getOrderNumber());
		orderBean.setOrderReceivedDate(orderEntity.getOrderReceiveDate());
		orderBean.setOrderStatus(OrderStatusEnum.valueOf(orderEntity.getOrderStatus().getOrderStatus()));
		orderBean.setOrderType(convertEntityToBean(orderEntity.getOrderType()));
		orderBean.setDdType(orderEntity.getDdType());
		orderBean.setRef4(orderEntity.getRef4());
		orderBean.setSerialNumber(orderEntity.getSerialNumberRef());
		orderBean.setUpdatedBy(convertEntityToBean(orderEntity.getUser()));
		return orderBean;
	}
	
	public OrderType convertEntityToBean(com.bcds.jpa.entity.OrderType orderTypeEntity) {
		OrderType orderType = new OrderType();
		if (orderTypeEntity == null) {
			return orderType;
		}
		orderType.setId(orderTypeEntity.getId());
		orderType.setLastUpdatedDate(orderTypeEntity.getLastUpdatedDate());
		orderType.setOrderTypeId(orderTypeEntity.getOrderTypeId());
		orderType.setOrderTypeName(orderTypeEntity.getOrderTypeName());
		return orderType;
	}
	
	public OrderActionType convertEntityToBean(com.bcds.jpa.entity.OrderActionType orderActionTypeEntity) {
		OrderActionType orderActionType = new OrderActionType();
		if (orderActionTypeEntity == null) {
			return orderActionType;
		}
		orderActionType.setActionName(orderActionTypeEntity.getActionName());
		orderActionType.setActionTypeSap(orderActionTypeEntity.getActionTypeSap());
		orderActionType.setId(orderActionTypeEntity.getId());
		orderActionType.setLastUpdatedDate(orderActionTypeEntity.getLastUpdatedDate());
		return orderActionType;
	}
	
	public com.bcds.jobs.beans.OrderRelationshipBean convertEntityToBean(
			com.bcds.jpa.entity.OrderRelationshipEntity orderRelationshipEntityEntity) {
		com.bcds.jobs.beans.OrderRelationshipBean orderRelationshipBean = new com.bcds.jobs.beans.OrderRelationshipBean();
		if (orderRelationshipEntityEntity == null) {
			return orderRelationshipBean;
		}
		orderRelationshipBean.setChildOrderId(convertEntityToBean(orderRelationshipEntityEntity.getChildOrderId()));
		orderRelationshipBean.setParenOrderId(convertEntityToBean(orderRelationshipEntityEntity.getParentOrderId()));
		orderRelationshipBean.setId(orderRelationshipEntityEntity.getId());
		orderRelationshipBean.setLastUpdateDate(orderRelationshipEntityEntity.getLastUpdateDate());
		return orderRelationshipBean;
	}
	
	public ReturnTransactionBean convertEntityToBean(ReturnTransactionsEntity returnTransactionEntity) {
		ReturnTransactionBean returnTransactionBean = new ReturnTransactionBean();
		if (returnTransactionEntity == null) {
			return returnTransactionBean;
		}
		returnTransactionBean.setBatchNumber(returnTransactionEntity.getBatchNumber());
		returnTransactionBean.setCreateDate(returnTransactionEntity.getCreateDate());
		returnTransactionBean.setCreatedBy(convertEntityToBean(returnTransactionEntity.getCreatedBy()));
		returnTransactionBean.setDestinationLocation(convertEntityToBean(returnTransactionEntity.getDestinationLocationId()));
		returnTransactionBean.setId(returnTransactionEntity.getId());
		returnTransactionBean.setLastUpdateDate(returnTransactionEntity.getLastUpdateDate());
		returnTransactionBean.setOrder(convertEntityToBean(returnTransactionEntity.getOrder()));
		returnTransactionBean.setOrderStatus(OrderStatusEnum.valueOf(returnTransactionEntity.getOrderStatus().getOrderStatus()));
		returnTransactionBean.setParts(convertEntityToBean(returnTransactionEntity.getParts()));
		returnTransactionBean.setQuantity(returnTransactionEntity.getQuantity());
		returnTransactionBean.setLineNumber(returnTransactionEntity.getLineNumber());
		returnTransactionBean.setSourceLocation(convertEntityToBean(returnTransactionEntity.getSourceLocationId()));
		return returnTransactionBean;
	}
	
	public CompanyAssetCategoryBean convertEntityToBean(CompanyAssetCategoryEntity companyAssetCategoryEntity) {
		if (companyAssetCategoryEntity == null) {
			return null;
		}
		CompanyAssetCategoryBean companyAssetCategoryBean = new CompanyAssetCategoryBean();
		companyAssetCategoryBean.setCategoryName(companyAssetCategoryEntity.getCategoryName());
		companyAssetCategoryBean.setCompany(convertEntityToBean(companyAssetCategoryEntity.getCompany()));
		companyAssetCategoryBean.setId(companyAssetCategoryEntity.getId());
		companyAssetCategoryBean.setLastUpdateDate(companyAssetCategoryEntity.getLastUpdateDate());
		companyAssetCategoryBean.setParentCatId(companyAssetCategoryEntity.getParentCatId());
		if (companyAssetCategoryEntity.getResponsibleUserId() != null) {
			companyAssetCategoryBean.setResponsibleUserId(convertEntityToBean(companyAssetCategoryEntity.getResponsibleUserId()));
		}
		companyAssetCategoryBean.setRootCategory(companyAssetCategoryEntity.isRootCategory());
		companyAssetCategoryBean.setUpdatedBy(companyAssetCategoryEntity.getUpdatedBy());
		companyAssetCategoryBean.setUpdateFlag(companyAssetCategoryEntity.getUpdateFlag());
		return companyAssetCategoryBean;
	}
}
