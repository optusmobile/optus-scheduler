package com.bcds.jobs.beans;

import java.util.Date;

import com.bcds.file.helper.OrderStatusEnum;

public class OrderMeterialList {
	
	private Integer id;
	private Order order;
	private CompanyAssetParts meterial;
	private Double quantity;
	private CompanyLocation location;
	private Date lastUpdateDate;
	private OrderStatusEnum status;
	private String salesOrderLineNumber;
	private String batchNumber;
	private String lineNumber;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	public CompanyAssetParts getMeterial() {
		return meterial;
	}
	public void setMeterial(CompanyAssetParts meterial) {
		this.meterial = meterial;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public CompanyLocation getLocation() {
		return location;
	}
	public void setLocation(CompanyLocation location) {
		this.location = location;
	}
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public OrderStatusEnum getStatus() {
		return status;
	}
	public void setStatus(OrderStatusEnum status) {
		this.status = status;
	}
	public String getSalesOrderLineNumber() {
		return salesOrderLineNumber;
	}
	public void setSalesOrderLineNumber(String salesOrderLineNumber) {
		this.salesOrderLineNumber = salesOrderLineNumber;
	}
	public String getBatchNumber() {
		return batchNumber;
	}
	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}
	public String getLineNumber() {
		return lineNumber;
	}
	public void setLineNumber(String lineNumber) {
		this.lineNumber = lineNumber;
	}

}
