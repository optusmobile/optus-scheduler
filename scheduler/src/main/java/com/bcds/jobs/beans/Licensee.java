package com.bcds.jobs.beans;

import java.util.Date;

public class Licensee {
	
	private Integer id;
	private User user;
	private String organizationName;
	private String deliveryAddress;
	private String postalAddress;
	private Integer numberOfWebUsers;
	private Integer numberOfDeviceUsers;
	private Integer numberOfInspectionUsers;
	private Integer numberOfMaintenanceUser;
	private String licenseeLogo;
	private Integer numberOfCompany;
	private Integer numberOfLocation;
	private Integer numberOfAssets;
	private Date accountExpireDate;
	private Integer accountType;
	private String licenseeKey;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getOrganizationName() {
		return organizationName;
	}
	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}
	public String getDeliveryAddress() {
		return deliveryAddress;
	}
	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}
	public String getPostalAddress() {
		return postalAddress;
	}
	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}
	public Integer getNumberOfWebUsers() {
		return numberOfWebUsers;
	}
	public void setNumberOfWebUsers(Integer numberOfWebUsers) {
		this.numberOfWebUsers = numberOfWebUsers;
	}
	public Integer getNumberOfDeviceUsers() {
		return numberOfDeviceUsers;
	}
	public void setNumberOfDeviceUsers(Integer numberOfDeviceUsers) {
		this.numberOfDeviceUsers = numberOfDeviceUsers;
	}
	public Integer getNumberOfInspectionUsers() {
		return numberOfInspectionUsers;
	}
	public void setNumberOfInspectionUsers(Integer numberOfInspectionUsers) {
		this.numberOfInspectionUsers = numberOfInspectionUsers;
	}
	public Integer getNumberOfMaintenanceUser() {
		return numberOfMaintenanceUser;
	}
	public void setNumberOfMaintenanceUser(Integer numberOfMaintenanceUser) {
		this.numberOfMaintenanceUser = numberOfMaintenanceUser;
	}
	public String getLicenseeLogo() {
		return licenseeLogo;
	}
	public void setLicenseeLogo(String licenseeLogo) {
		this.licenseeLogo = licenseeLogo;
	}
	public Integer getNumberOfCompany() {
		return numberOfCompany;
	}
	public void setNumberOfCompany(Integer numberOfCompany) {
		this.numberOfCompany = numberOfCompany;
	}
	public Integer getNumberOfLocation() {
		return numberOfLocation;
	}
	public void setNumberOfLocation(Integer numberOfLocation) {
		this.numberOfLocation = numberOfLocation;
	}
	public Integer getNumberOfAssets() {
		return numberOfAssets;
	}
	public void setNumberOfAssets(Integer numberOfAssets) {
		this.numberOfAssets = numberOfAssets;
	}
	public Date getAccountExpireDate() {
		return accountExpireDate;
	}
	public void setAccountExpireDate(Date accountExpireDate) {
		this.accountExpireDate = accountExpireDate;
	}
	public Integer getAccountType() {
		return accountType;
	}
	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}
	public String getLicenseeKey() {
		return licenseeKey;
	}
	public void setLicenseeKey(String licenseeKey) {
		this.licenseeKey = licenseeKey;
	}
	
}
