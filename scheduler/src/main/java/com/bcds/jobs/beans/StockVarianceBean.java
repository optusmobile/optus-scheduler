package com.bcds.jobs.beans;

import java.util.Date;

import lombok.Data;

@Data
public class StockVarianceBean {
	
	private int id;
	private Date stockReceiveDate;
	private CompanyAssetParts companyAssetParts;
	private String plantName;
	private CompanyLocation companyLocation;
	private Integer sapQuantity;
	private Integer intrackQuantity;
	private String stockType;
	private String batchName;
}
