package com.bcds.jobs.beans;

public class User {
	
	private Integer user_id;
	private String firstName;
	private String middleName;
	private String lastName;
	private String emailId;
	private String phoneNumber;
	private String loginName;
	private String password;
	private Integer parentUser;
	private String currentDeviceUdid;
	private Integer deviceType; 
	private Integer licensee;
	private boolean active;
	private String passwordGeneratorNumber;
	private boolean webUser;
	private boolean deviceUser;
	private boolean inspectionUser;
	private boolean maintenanceUser;
	private boolean mobileDeviceUser;
	private boolean rhdMtUser;
	public Integer getUser_id() {
		return user_id;
	}
	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Integer getParentUser() {
		return parentUser;
	}
	public void setParentUser(Integer parentUser) {
		this.parentUser = parentUser;
	}
	public String getCurrentDeviceUdid() {
		return currentDeviceUdid;
	}
	public void setCurrentDeviceUdid(String currentDeviceUdid) {
		this.currentDeviceUdid = currentDeviceUdid;
	}
	public Integer getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(Integer deviceType) {
		this.deviceType = deviceType;
	}
	public Integer getLicensee() {
		return licensee;
	}
	public void setLicensee(Integer licensee) {
		this.licensee = licensee;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public String getPasswordGeneratorNumber() {
		return passwordGeneratorNumber;
	}
	public void setPasswordGeneratorNumber(String passwordGeneratorNumber) {
		this.passwordGeneratorNumber = passwordGeneratorNumber;
	}
	public boolean isWebUser() {
		return webUser;
	}
	public void setWebUser(boolean webUser) {
		this.webUser = webUser;
	}
	public boolean isDeviceUser() {
		return deviceUser;
	}
	public void setDeviceUser(boolean deviceUser) {
		this.deviceUser = deviceUser;
	}
	public boolean isInspectionUser() {
		return inspectionUser;
	}
	public void setInspectionUser(boolean inspectionUser) {
		this.inspectionUser = inspectionUser;
	}
	public boolean isMaintenanceUser() {
		return maintenanceUser;
	}
	public void setMaintenanceUser(boolean maintenanceUser) {
		this.maintenanceUser = maintenanceUser;
	}
	public boolean isMobileDeviceUser() {
		return mobileDeviceUser;
	}
	public void setMobileDeviceUser(boolean mobileDeviceUser) {
		this.mobileDeviceUser = mobileDeviceUser;
	}
	public boolean isRhdMtUser() {
		return rhdMtUser;
	}
	public void setRhdMtUser(boolean rhdMtUser) {
		this.rhdMtUser = rhdMtUser;
	}
	

}
