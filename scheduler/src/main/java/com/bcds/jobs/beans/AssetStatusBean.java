package com.bcds.jobs.beans;

import java.util.Date;

public class AssetStatusBean {

	private int id;
	private String status;
	private Company company;
	private Character updateFlag;
	private Date lastUpdateDate;
	private int updatedBy;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public Character getUpdateFlag() {
		return updateFlag;
	}
	public void setUpdateFlag(Character updateFlag) {
		this.updateFlag = updateFlag;
	}
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public int getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}
}
