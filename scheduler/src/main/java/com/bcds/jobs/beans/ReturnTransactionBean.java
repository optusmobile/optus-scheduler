package com.bcds.jobs.beans;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bcds.file.helper.OrderStatusEnum;

public class ReturnTransactionBean {
	
	private Integer id;
	private Order order;
	private CompanyAssetParts parts;
	private CompanyLocation sourceLocation;
	private CompanyLocation destinationLocation;
	private Integer quantity;
	private String batchNumber;
	private Date createDate;
	private User createdBy;
	private Date lastUpdateDate;
	private OrderStatusEnum orderStatus;
	private String lineNumber;
	Map<String,List<ReturnTransactionBean>> returnTransBeanMap = new HashMap<String,List<ReturnTransactionBean>>();
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	public Map<String, List<ReturnTransactionBean>> getReturnTransBeanMap() {
		return returnTransBeanMap;
	}
	public void setReturnTransBeanMap(Map<String, List<ReturnTransactionBean>> returnTransBeanMap) {
		this.returnTransBeanMap = returnTransBeanMap;
	}
	public CompanyAssetParts getParts() {
		return parts;
	}
	public void setParts(CompanyAssetParts parts) {
		this.parts = parts;
	}
	public CompanyLocation getSourceLocation() {
		return sourceLocation;
	}
	public void setSourceLocation(CompanyLocation sourceLocation) {
		this.sourceLocation = sourceLocation;
	}
	public CompanyLocation getDestinationLocation() {
		return destinationLocation;
	}
	public void setDestinationLocation(CompanyLocation destinationLocation) {
		this.destinationLocation = destinationLocation;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String getBatchNumber() {
		return batchNumber;
	}
	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public User getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public OrderStatusEnum getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(OrderStatusEnum orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getLineNumber() {
		return lineNumber;
	}
	public void setLineNumber(String lineNumber) {
		this.lineNumber = lineNumber;
	}
	
}
