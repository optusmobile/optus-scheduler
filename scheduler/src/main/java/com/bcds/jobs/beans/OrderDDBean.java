package com.bcds.jobs.beans;

import java.util.ArrayList;
import java.util.List;

public class OrderDDBean {
	
	private List<Order> orderListDD = new ArrayList<Order>();
	private List<OrderRelationshipBean> orderRelationshipList = new ArrayList<OrderRelationshipBean>();
	
	public List<Order> getOrderListDD() {
		return orderListDD;
	}
	public void setOrderListDD(List<Order> orderListDD) {
		this.orderListDD = orderListDD;
	}
	public List<OrderRelationshipBean> getOrderRelationshipList() {
		return orderRelationshipList;
	}
	public void setOrderRelationshipList(List<OrderRelationshipBean> orderRelationshipList) {
		this.orderRelationshipList = orderRelationshipList;
	}

}
