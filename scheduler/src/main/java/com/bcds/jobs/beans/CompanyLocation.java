package com.bcds.jobs.beans;

public class CompanyLocation {
	
	private Integer locationId;
	private String locationName;
	private boolean rootLocation;
	private Integer parentLocation;
	private Company company;
	private User responsibleUserId;
	private String locationAddress;
	private String rfidReaderId;
	private Integer cloneLocationId;
	private String tag;
	private String barcode;
	private String updateFlag;
	private String locationType;
	
	public Integer getLocationId() {
		return locationId;
	}
	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public boolean isRootLocation() {
		return rootLocation;
	}
	public void setRootLocation(boolean rootLocation) {
		this.rootLocation = rootLocation;
	}
	public Integer getParentLocation() {
		return parentLocation;
	}
	public void setParentLocation(Integer parentLocation) {
		this.parentLocation = parentLocation;
	}
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public User getResponsibleUserId() {
		return responsibleUserId;
	}
	public void setResponsibleUserId(User responsibleUserId) {
		this.responsibleUserId = responsibleUserId;
	}
	public String getLocationAddress() {
		return locationAddress;
	}
	public void setLocationAddress(String locationAddress) {
		this.locationAddress = locationAddress;
	}
	public String getRfidReaderId() {
		return rfidReaderId;
	}
	public void setRfidReaderId(String rfidReaderId) {
		this.rfidReaderId = rfidReaderId;
	}
	public Integer getCloneLocationId() {
		return cloneLocationId;
	}
	public void setCloneLocationId(Integer cloneLocationId) {
		this.cloneLocationId = cloneLocationId;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public String getUpdateFlag() {
		return updateFlag;
	}
	public void setUpdateFlag(String updateFlag) {
		this.updateFlag = updateFlag;
	}
	public String getLocationType() {
		return locationType;
	}
	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}

}
