package com.bcds.jobs.beans;

import java.util.Date;

public class Company {
	
	private Integer companyId;
	private String companyName;
	private Licensee licensee;
	private User admin;
	private String updateFlag;
	private Date lastUpdateDate;
	private boolean active;
	private String deliveryAddress;
	private String postalAddress;
	private Integer cloneCompanyId;
	private String multiScan;
	
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Licensee getLicensee() {
		return licensee;
	}
	public void setLicensee(Licensee licensee) {
		this.licensee = licensee;
	}
	public User getAdmin() {
		return admin;
	}
	public void setAdmin(User admin) {
		this.admin = admin;
	}
	public String getUpdateFlag() {
		return updateFlag;
	}
	public void setUpdateFlag(String updateFlag) {
		this.updateFlag = updateFlag;
	}
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public String getDeliveryAddress() {
		return deliveryAddress;
	}
	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}
	public String getPostalAddress() {
		return postalAddress;
	}
	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}
	public Integer getCloneCompanyId() {
		return cloneCompanyId;
	}
	public void setCloneCompanyId(Integer cloneCompanyId) {
		this.cloneCompanyId = cloneCompanyId;
	}
	public String getMultiScan() {
		return multiScan;
	}
	public void setMultiScan(String multiScan) {
		this.multiScan = multiScan;
	}

}
