package com.bcds.jobs.beans;

import java.util.Date;

public class PidListBean {
	
	private Integer id;
	private Integer pidId;
	private CompanyLocation location;
	private AssetBean  asset;
	private Double expectedQuantity;
	private Double actualQuantity;
	private Date lastUpdateDate;
	private PidBean pidBean;
	private String lineNumber;
	
	public PidBean getPidBean() {
		return pidBean;
	}
	public void setPidBean(PidBean pidBean) {
		this.pidBean = pidBean;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getPidId() {
		return pidId;
	}
	public void setPidId(Integer pidId) {
		this.pidId = pidId;
	}
	public CompanyLocation getLocation() {
		return location;
	}
	public void setLocation(CompanyLocation location) {
		this.location = location;
	}
	public AssetBean getAsset() {
		return asset;
	}
	public void setAsset(AssetBean asset) {
		this.asset = asset;
	}
	public Double getExpectedQuantity() {
		return expectedQuantity;
	}
	public void setExpectedQuantity(Double expectedQuantity) {
		this.expectedQuantity = expectedQuantity;
	}
	public Double getActualQuantity() {
		return actualQuantity;
	}
	public void setActualQuantity(Double actualQuantity) {
		this.actualQuantity = actualQuantity;
	}
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public String getLineNumber() {
		return lineNumber;
	}
	public void setLineNumber(String lineNumber) {
		this.lineNumber = lineNumber;
	}

}
