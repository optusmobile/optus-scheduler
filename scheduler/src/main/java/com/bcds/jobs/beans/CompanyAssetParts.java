package com.bcds.jobs.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CompanyAssetParts {
	
	private Integer partId;
	private String partName;
	private String partNumber;
	private String partDescription;
	private Company company;
	private String updateFlag;
	private Date lastUpdateDate;
	private Integer updatedBy;
	private String batchNumber;
	private List<MaterialLocationBean> materialLocationBeanList = new ArrayList<MaterialLocationBean>();
	
	public Integer getPartId() {
		return partId;
	}
	public void setPartId(Integer partId) {
		this.partId = partId;
	}
	public String getPartName() {
		return partName;
	}
	public void setPartName(String partName) {
		this.partName = partName;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getPartDescription() {
		return partDescription;
	}
	public void setPartDescription(String partDescription) {
		this.partDescription = partDescription;
	}
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public String getUpdateFlag() {
		return updateFlag;
	}
	public void setUpdateFlag(String updateFlag) {
		this.updateFlag = updateFlag;
	}
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getBatchNumber() {
		return batchNumber;
	}
	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}
	public List<MaterialLocationBean> getMaterialLocationBeanList() {
		return materialLocationBeanList;
	}
	public void setMaterialLocationBeanList(List<MaterialLocationBean> materialLocationBeanList) {
		this.materialLocationBeanList = materialLocationBeanList;
	}
}
