package com.bcds.jobs.beans;

import java.util.Date;

public class OrderRelationshipBean {
	
	private Integer id;
	private Order parenOrderId;
	private Order childOrderId;
	private Date lastUpdateDate;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Order getParenOrderId() {
		return parenOrderId;
	}
	public void setParenOrderId(Order parenOrderId) {
		this.parenOrderId = parenOrderId;
	}
	public Order getChildOrderId() {
		return childOrderId;
	}
	public void setChildOrderId(Order childOrderId) {
		this.childOrderId = childOrderId;
	}
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

}
