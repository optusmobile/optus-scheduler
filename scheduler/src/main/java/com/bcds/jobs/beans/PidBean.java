package com.bcds.jobs.beans;

import java.util.Date;
import java.util.List;
import com.bcds.file.helper.OrderStatusEnum;

public class PidBean {
	
	private Integer id;
	private Integer pidNumber;
	private String fiscalYear;
	private Date dateOfLastCount;
	private OrderStatusEnum status;
	private Date lastUpdateDate;
	private List<PidListBean> pidList;
	private String plant;
	private String sLocs;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getPidNumber() {
		return pidNumber;
	}
	public void setPidNumber(Integer pidNumber) {
		this.pidNumber = pidNumber;
	}
	public String getFiscalYear() {
		return fiscalYear;
	}
	public void setFiscalYear(String fiscalYear) {
		this.fiscalYear = fiscalYear;
	}
	public Date getDateOfLastCount() {
		return dateOfLastCount;
	}
	public void setDateOfLastCount(Date dateOfLastCount) {
		this.dateOfLastCount = dateOfLastCount;
	}
	public OrderStatusEnum getStatus() {
		return status;
	}
	public void setStatus(OrderStatusEnum status) {
		this.status = status;
	}
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public List<PidListBean> getPidList() {
		return pidList;
	}
	public void setPidList(List<PidListBean> pidList) {
		this.pidList = pidList;
	}
	public String getPlant() {
		return plant;
	}
	public void setPlant(String plant) {
		this.plant = plant;
	}
	public String getsLocs() {
		return sLocs;
	}
	public void setsLocs(String sLocs) {
		this.sLocs = sLocs;
	}
}
