package com.bcds.jobs.beans;

import java.util.Date;

public class CompanyAssetCategoryBean {
	
	private Integer id;
	private String categoryName;
	private User responsibleUserId;
	private Company company;
	private Integer parentCatId;
	private Character updateFlag;
	private Date lastUpdateDate;
	private Integer updatedBy;
	private boolean isRootCategory;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public User getResponsibleUserId() {
		return responsibleUserId;
	}
	public void setResponsibleUserId(User responsibleUserId) {
		this.responsibleUserId = responsibleUserId;
	}
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	
	public Integer getParentCatId() {
		return parentCatId;
	}
	public void setParentCatId(Integer parentCatId) {
		this.parentCatId = parentCatId;
	}
	public Character getUpdateFlag() {
		return updateFlag;
	}
	public void setUpdateFlag(Character updateFlag) {
		this.updateFlag = updateFlag;
	}
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	public boolean isRootCategory() {
		return isRootCategory;
	}
	public void setRootCategory(boolean isRootCategory) {
		this.isRootCategory = isRootCategory;
	}

}
