package com.bcds.jobs.beans;

public class MaterialLocationBean {
	private Integer id;
	private CompanyAssetParts companyAssetParts;
	private CompanyLocation companyLocation;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public CompanyAssetParts getCompanyAssetParts() {
		return companyAssetParts;
	}
	public void setCompanyAssetParts(CompanyAssetParts companyAssetParts) {
		this.companyAssetParts = companyAssetParts;
	}
	public CompanyLocation getCompanyLocation() {
		return companyLocation;
	}
	public void setCompanyLocation(CompanyLocation companyLocation) {
		this.companyLocation = companyLocation;
	}

}
