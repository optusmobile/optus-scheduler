package com.bcds.jobs.beans;

import java.util.Date;
import com.bcds.file.helper.MovementTypeEnum;
import com.bcds.file.helper.OrderStatusEnum;

public class AssetMovementInfoBean {
	
	private Integer id;
	private Integer attributeDeviceId;
	private AssetBean asset;
	private int deviceId;
	private Date movementDate;
	private int movedBy;
	private MovementTypeEnum movementTypeEnum;
	private int movementRecordedBy;
	private CompanyLocation sourceLocation;
	private CompanyLocation destinationLocation;
	private String workOrderNumber;
	private char updateFlag;
	private Date lastUpdateDate;
	private int updatedBy;
	private OrderStatusEnum orderStatus;
	private String plantType;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getAttributeDeviceId() {
		return attributeDeviceId;
	}
	public void setAttributeDeviceId(Integer attributeDeviceId) {
		this.attributeDeviceId = attributeDeviceId;
	}
	public AssetBean getAsset() {
		return asset;
	}
	public void setAsset(AssetBean asset) {
		this.asset = asset;
	}
	public int getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}
	public Date getMovementDate() {
		return movementDate;
	}
	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}
	public int getMovedBy() {
		return movedBy;
	}
	public void setMovedBy(int movedBy) {
		this.movedBy = movedBy;
	}
	public MovementTypeEnum getMovementTypeEnum() {
		return movementTypeEnum;
	}
	public void setMovementTypeEnum(MovementTypeEnum movementTypeEnum) {
		this.movementTypeEnum = movementTypeEnum;
	}
	public int getMovementRecordedBy() {
		return movementRecordedBy;
	}
	public void setMovementRecordedBy(int movementRecordedBy) {
		this.movementRecordedBy = movementRecordedBy;
	}
	public CompanyLocation getSourceLocation() {
		return sourceLocation;
	}
	public void setSourceLocation(CompanyLocation sourceLocation) {
		this.sourceLocation = sourceLocation;
	}
	public CompanyLocation getDestinationLocation() {
		return destinationLocation;
	}
	public void setDestinationLocation(CompanyLocation destinationLocation) {
		this.destinationLocation = destinationLocation;
	}
	public String getWorkOrderNumber() {
		return workOrderNumber;
	}
	public void setWorkOrderNumber(String workOrderNumber) {
		this.workOrderNumber = workOrderNumber;
	}
	public char getUpdateFlag() {
		return updateFlag;
	}
	public void setUpdateFlag(char updateFlag) {
		this.updateFlag = updateFlag;
	}
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public int getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}
	public OrderStatusEnum getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(OrderStatusEnum orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getPlantType() {
		return plantType;
	}
	public void setPlantType(String plantType) {
		this.plantType = plantType;
	}

}
