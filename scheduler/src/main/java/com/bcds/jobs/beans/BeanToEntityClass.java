package com.bcds.jobs.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bcds.file.helper.OrderStatusEnum;
import com.bcds.jpa.entity.CompanyAssetCategoryEntity;
import com.bcds.jpa.entity.CompanyEntity;
import com.bcds.jpa.entity.CompanyLocation;
import com.bcds.jpa.entity.Order;
import com.bcds.jpa.entity.OrderActionType;
import com.bcds.jpa.entity.OrderRelationshipEntity;
import com.bcds.jpa.entity.OrderType;
import com.bcds.jpa.entity.PhysicalInventoryDocument;
import com.bcds.jpa.repo.CompanyAssetCategoryRepo;
import com.bcds.jpa.repo.CompanyAssetPartsRepository;
import com.bcds.jpa.repo.CompanyLocationRepository;
import com.bcds.jpa.repo.CompanyRepo;
import com.bcds.jpa.repo.OrderActionTypeRepository;
import com.bcds.jpa.repo.OrderRepository;
import com.bcds.jpa.repo.OrderStatusRepository;
import com.bcds.jpa.repo.OrderTypeRepository;
import com.bcds.jpa.repo.UserRepository;

@Service("beanToEntityClass")
public class BeanToEntityClass {
	
	private static final String OPTUS = "Optus";
	
	@Autowired
	CompanyRepo companyRepo;
	
	@Autowired
	OrderActionTypeRepository orderActionTypeRepo;
	
	@Autowired
	OrderTypeRepository orderTypeRepo;
	
	@Autowired
	CompanyLocationRepository companyLocationRepo;
	
	@Autowired
	OrderRepository orderRepo;
	
	@Autowired
	OrderStatusRepository orderStatusRepo;
	
	@Autowired
	UserRepository userRepo;
	
	@Autowired
	CompanyAssetPartsRepository companyAssetPartsRepo;
	
	@Autowired
	CompanyAssetCategoryRepo companyAssetCategoryRepo;
	

	public  com.bcds.jpa.entity.CompanyAssetParts convertBeanToEntity(com.bcds.jobs.beans.CompanyAssetParts companyassetParts) {
		if (companyassetParts == null) {
			return null;
		}
		com.bcds.jpa.entity.CompanyAssetParts companyAssetParts = new com.bcds.jpa.entity.CompanyAssetParts();
		if (companyassetParts.getPartId() != null) {
			companyAssetParts.setId(companyassetParts.getPartId());
		}
		companyAssetParts.setPartName(companyassetParts.getPartName());
		companyAssetParts.setPartDesc(companyassetParts.getPartDescription());
		companyAssetParts.setPartNum(companyassetParts.getPartNumber());
		//hard coded for optus 
		CompanyEntity companyEntity = companyRepo.findByCompanyName(OPTUS);
		if (companyEntity == null) {
			throw new EntityNotFoundException("OPTUS is not prsent in database as a Company" );
		}
		companyAssetParts.setCompanyId(companyEntity);
		companyAssetParts.setLastUpdateDate(new Date());
		companyAssetParts.setUpdatedBy(1);
		companyAssetParts.setUpdateFlag('U');
		return companyAssetParts;
	}
	
	public  OrderRelationshipEntity converBeanToEntity(OrderRelationshipBean orderRelationshipBean) {
		OrderRelationshipEntity orderRelationshipEntity = new OrderRelationshipEntity();
		OrderActionType parentOrderActionTypeEntity = new OrderActionType();
		OrderActionType childOrderActionTypeEntity = new OrderActionType();
		CompanyLocation parentCompanyLocationEntity = new CompanyLocation();
		CompanyLocation childCompanyLocationEntity = new CompanyLocation();
		OrderType parentOrderType = new OrderType();
		OrderType childOrderType = new OrderType();
		
		if (orderRelationshipBean.getParenOrderId().getOrderActionType() != null && 
				orderRelationshipBean.getParenOrderId().getOrderActionType().getActionName() != null){
			 parentOrderActionTypeEntity = 
					orderActionTypeRepo.findByActionNameAndDeliveyDocumentType(orderRelationshipBean.getParenOrderId().getOrderActionType().getActionName(),"");
		}
		if (orderRelationshipBean.getChildOrderId().getOrderActionType() != null && 
				orderRelationshipBean.getChildOrderId().getOrderActionType().getActionName() != null){
			childOrderActionTypeEntity = 
					orderActionTypeRepo.findByActionNameAndDeliveyDocumentType(orderRelationshipBean.getChildOrderId().getOrderActionType().getActionName(),"");
		}
		if (orderRelationshipBean.getParenOrderId().getOrderType() != null && 
				orderRelationshipBean.getParenOrderId().getOrderType().getOrderTypeName() != null){
			parentOrderType = 
					orderTypeRepo.findByOrderTypeName(orderRelationshipBean.getParenOrderId().getOrderType().getOrderTypeName());
		}
		if (orderRelationshipBean.getChildOrderId().getOrderType() != null && 
				orderRelationshipBean.getChildOrderId().getOrderType().getOrderTypeName() != null){
			childOrderType = 
					orderTypeRepo.findByOrderTypeName(orderRelationshipBean.getChildOrderId().getOrderType().getOrderTypeName());
		}
		if (orderRelationshipBean.getParenOrderId().getCompanyLocation() != null ) {
			parentCompanyLocationEntity = companyLocationRepo.findByBarcodeAndTag(orderRelationshipBean.getParenOrderId().getCompanyLocation().getBarcode(), 
					orderRelationshipBean.getParenOrderId().getCompanyLocation().getTag());
		}
		if (orderRelationshipBean.getChildOrderId().getCompanyLocation() != null ) {
			childCompanyLocationEntity = companyLocationRepo.findByBarcodeAndTag(orderRelationshipBean.getChildOrderId().getCompanyLocation().getBarcode(),
					orderRelationshipBean.getChildOrderId().getCompanyLocation().getTag());
		}
		Order parentOrderEntity = orderRepo.findByOrderNumberAndOrderActionTypeAndOrderTypeAndCompanyLocation(orderRelationshipBean.getParenOrderId().getOrderNumber(),
				parentOrderActionTypeEntity, parentOrderType, parentCompanyLocationEntity);
		Order childOrderEntity = orderRepo.findByOrderNumberAndOrderActionTypeAndOrderTypeAndCompanyLocation(orderRelationshipBean.getChildOrderId().getOrderNumber(),
				childOrderActionTypeEntity,childOrderType, childCompanyLocationEntity);
		orderRelationshipEntity.setParentOrderId(parentOrderEntity);
		orderRelationshipEntity.setChildOrderId(childOrderEntity);
		orderRelationshipEntity.setLastUpdateDate(new Date());
		return orderRelationshipEntity;
	}
	
	public  com.bcds.jpa.entity.Order convertBeanToEntity(com.bcds.jobs.beans.Order orderBean) {
		com.bcds.jpa.entity.Order orderEntity = new com.bcds.jpa.entity.Order();
		if (orderBean == null) {
			return orderEntity;
		}
		if (orderBean.getId() != null) {
			orderEntity.setId(orderBean.getId());
		}
		orderEntity.setLastUpdateDate(new Date());
		orderEntity.setOrderReceiveDate(new Date());
		orderEntity.setOrderNumber(orderBean.getOrderNumber());
		orderEntity.setDdType(orderBean.getDdType());
		orderEntity.setOrderType(orderTypeRepo.findByOrderTypeName(orderBean.getOrderType().getOrderTypeName()));
		if (orderBean.getOrderActionType() != null && orderBean.getOrderActionType().getActionName() != null){
			orderEntity.setOrderActionType(orderActionTypeRepo.findByActionNameAndDeliveyDocumentType(orderBean.getOrderActionType().getActionName(),""));
		}
		else {
			orderEntity.setOrderActionType(orderActionTypeRepo.findByActionNameAndDeliveyDocumentType("GR",""));
		}
		if (orderBean.getOrderType() != null && 
				orderBean.getOrderType().toString().equalsIgnoreCase("SCPO") 
				&& orderBean.getMrf_ref().equalsIgnoreCase("A.5")) {
			orderEntity.setOrderStatus(orderStatusRepo.findByOrderStatus("PENDING"));
		}
		else {
			orderEntity.setOrderStatus(orderStatusRepo.findByOrderStatus("RECEIVED"));
		}
		orderEntity.setCompanyLocation(companyLocationRepo.findByBarcodeAndTag(orderBean.getCompanyLocation().getBarcode(), orderBean.getCompanyLocation().getTag()));
		orderEntity.setUser(userRepo.findByFirstNameAndLastName("Batch", "Scheduler"));
		orderEntity.setMrfRef(orderBean.getMrf_ref());
		orderEntity.setSerialNumberRef(orderBean.getSerialNumber());
		List<OrderMeterialList> orderMeterialList = orderBean.getOrderMeterialList();
		List<com.bcds.jpa.entity.OrderMaterialList> orderMeterialEntityList = new ArrayList<com.bcds.jpa.entity.OrderMaterialList>();
		for(OrderMeterialList orderMeterialListObj : orderMeterialList) {
			com.bcds.jpa.entity.OrderMaterialList orderMeterialEntity = new com.bcds.jpa.entity.OrderMaterialList();
			CompanyAssetParts companyAssetParts = orderMeterialListObj.getMeterial();
			//String partNameString = companyAssetParts.getPartName().replaceAll("^0*", "");
			com.bcds.jpa.entity.CompanyAssetParts companyAssetPartsEntityForSubString = companyAssetPartsRepo.findByPartName(companyAssetParts.getPartName());
			com.bcds.jpa.entity.CompanyAssetParts companyAssetPartsEntity = companyAssetPartsRepo.findByPartName(companyAssetParts.getPartName());
			if ((companyAssetPartsEntity == null) && (companyAssetPartsEntityForSubString == null)) {
				throw new EntityNotFoundException("CompanyAssetParts Entity not found for company asset parts meterial number "+ companyAssetParts.getPartName() );
			}
			orderMeterialEntity.setCompanyAssetParts(companyAssetPartsEntity);
			CompanyLocation companyLocationEntity = companyLocationRepo.findByBarcodeAndTag(orderMeterialListObj.getLocation().getBarcode(), 
					orderMeterialListObj.getLocation().getTag());
			if (companyLocationEntity == null) {
				throw new EntityNotFoundException("CompanyLocationEntity Entity not found for barcode " + orderMeterialListObj.getLocation().getBarcode());
			}
			orderMeterialEntity.setCompanyLocation(companyLocationRepo.findByBarcodeAndTag(orderMeterialListObj.getLocation().getBarcode(), 
													orderMeterialListObj.getLocation().getTag()));
			orderMeterialEntity.setQuantity(orderMeterialListObj.getQuantity().intValue());
			orderMeterialEntity.setStatus(orderStatusRepo.findByOrderStatus("RECEIVED"));
			orderMeterialEntity.setSalesOrderLineNumber(orderMeterialListObj.getSalesOrderLineNumber());
			orderMeterialEntity.setBatchNumber(orderMeterialListObj.getBatchNumber());
			orderMeterialEntity.setLineNumber(orderMeterialListObj.getLineNumber());
			orderMeterialEntity.setOrder(orderEntity);
			orderMeterialEntityList.add(orderMeterialEntity);
		}
		orderEntity.setOrderMeterialList(orderMeterialEntityList);
		return orderEntity;
	}
	
	public  com.bcds.jpa.entity.CompanyAssetCategoryEntity convertBeanToEntity(com.bcds.jobs.beans.CompanyAssetCategoryBean companyAssetCategoryBean) {
		CompanyAssetCategoryEntity companyAssetCategoryEntity = new CompanyAssetCategoryEntity();
		companyAssetCategoryEntity.setCategoryName(companyAssetCategoryBean.getCategoryName());
		CompanyEntity companyEntity = companyRepo.findByCompanyName(OPTUS);
		companyAssetCategoryEntity.setCompany(companyEntity);
		companyAssetCategoryEntity.setLastUpdateDate(new Date());
		companyAssetCategoryEntity.setParentCatId(companyAssetCategoryBean.getParentCatId());
		companyAssetCategoryEntity.setRootCategory(companyAssetCategoryBean.isRootCategory());
		companyAssetCategoryEntity.setUpdatedBy(1);
		companyAssetCategoryEntity.setUpdateFlag('U');
		return companyAssetCategoryEntity;
	}
	
}
