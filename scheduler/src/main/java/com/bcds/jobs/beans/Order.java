package com.bcds.jobs.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.bcds.file.helper.OrderStatusEnum;

public class Order  {
	
	private Integer id;
	private String orderNumber;
	private OrderType orderType;
	private OrderActionType orderActionType;
	private String locationName;
	private Date orderReceivedDate;
	private OrderStatusEnum orderStatus;
	private Date lastUpdatedDate;
	private User updatedBy;
	private String mrf_ref;
	private String serialNumber;
	private String crossRefNumber;
	private String ref2;
	private String ddType;
	private String ref4;
	private CompanyLocation companyLocation;
	private List<OrderMeterialList> orderMeterialList = new ArrayList<OrderMeterialList>();
	private List<OrderRelationshipBean> orderRelationshipBeanList = new ArrayList<OrderRelationshipBean>();
	private String batchNumber;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public OrderType getOrderType() {
		return orderType;
	}
	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}
	public OrderActionType getOrderActionType() {
		return orderActionType;
	}
	public void setOrderActionType(OrderActionType orderActionType) {
		this.orderActionType = orderActionType;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public Date getOrderReceivedDate() {
		return orderReceivedDate;
	}
	public void setOrderReceivedDate(Date orderReceivedDate) {
		this.orderReceivedDate = orderReceivedDate;
	}
	public OrderStatusEnum getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(OrderStatusEnum orderStatus) {
		this.orderStatus = orderStatus;
	}
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public User getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getMrf_ref() {
		return mrf_ref;
	}
	public void setMrf_ref(String mrf_ref) {
		this.mrf_ref = mrf_ref;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getCrossRefNumber() {
		return crossRefNumber;
	}
	public void setCrossRefNumber(String crossRefNumber) {
		this.crossRefNumber = crossRefNumber;
	}
	public String getRef2() {
		return ref2;
	}
	public void setRef2(String ref2) {
		this.ref2 = ref2;
	}
	public String getDdType() {
		return ddType;
	}
	public void setDdType(String ddType) {
		this.ddType = ddType;
	}
	public String getRef4() {
		return ref4;
	}
	public void setRef4(String ref4) {
		this.ref4 = ref4;
	}
	public List<OrderMeterialList> getOrderMeterialList() {
		return orderMeterialList;
	}
	public void setOrderMeterialList(List<OrderMeterialList> orderMeterialList) {
		this.orderMeterialList = orderMeterialList;
	}
	public CompanyLocation getCompanyLocation() {
		return companyLocation;
	}
	public void setCompanyLocation(CompanyLocation companyLocation) {
		this.companyLocation = companyLocation;
	}
	public List<OrderRelationshipBean> getOrderRelationshipBeanList() {
		return orderRelationshipBeanList;
	}
	public void setOrderRelationshipBeanList(List<OrderRelationshipBean> orderRelationshipBeanList) {
		this.orderRelationshipBeanList = orderRelationshipBeanList;
	}
	public String getBatchNumber() {
		return batchNumber;
	}
	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}
	
}