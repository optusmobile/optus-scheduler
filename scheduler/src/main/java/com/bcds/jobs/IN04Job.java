package com.bcds.jobs;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleTrigger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Component;
import com.bcds.file.helper.CSVUtil;
import com.bcds.file.helper.GeneralHelper;
import com.bcds.ftp.util.FileUtil;
import com.bcds.jobs.beans.BeanToEntityClass;
import com.bcds.jobs.beans.EntityToBeanClass;
import com.bcds.jobs.beans.OrderMeterialList;
import com.bcds.jobs.beans.OrderType;
import com.bcds.jobs.exception.BCDSEmailException;
import com.bcds.jobs.exception.BCDSFTPException;
import com.bcds.jpa.entity.InterfaceError;
import com.bcds.jpa.entity.OrderRelationshipEntity;
import com.bcds.jpa.model.in04.IN04;
import com.bcds.jpa.model.in04.IN04Header;
import com.bcds.jpa.model.in04.IN04Line;
import com.bcds.jpa.repo.OrderRelationshopRepo;
import com.bcds.jpa.repo.OrderRepository;
import com.bcds.util.DateUtil;
import au.com.bytecode.opencsv.CSVWriter;

/*
 * IN04 - PGI
 * 
 * Date				Change description
 * -----       	   ---------------------
 * 
 */


@Component
public class IN04Job extends AbstractOutgoingJob {

	final static Logger logger = Logger.getLogger(IN04Job.class);

	@Value( "${in04.filename}" )
	protected String filename;

	@Value( "${in04.backup.folder}" )
	protected String backupFolder;

	@Value( "${in04.source.folder}" )
	protected String sourceFolder;
	
	@Autowired
	OrderRelationshopRepo orderRelationshipRepo;
	
	@Autowired
	BeanToEntityClass beanToEntityClass;
	
	@Autowired
	EntityToBeanClass entityToBeanClass;
	
	@Autowired
	OrderRepository orderRepo;
	
	@Override
	public void executeJob() {
		
		logger.info("----------- IN04 Job Started ---------");		
		setFilename(this.filename);
		setBackupFolder(this.backupFolder);
		setSourceFolder(this.sourceFolder);
		prepareInterfaceFile();			
		//if no files don't ftp
		List<String> allIN04s = new ArrayList<String>();
		try {
			allIN04s = FileUtil.getSimilarBatchFiles(getSourceFolder(), getFilename());
		} catch (NoSuchFileException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		int n =0;
		if(allIN04s!=null){
			n = allIN04s.size();
		}
		logger.debug("Total "+ n + " files found to send.");
		for (String oneIN04 : allIN04s) {
			File file = new File(oneIN04);
			try{
				logger.debug("Sending file: "+ file.getName());
				executeFTP(file);
				logger.debug("Sending file success.");
			}catch (BCDSFTPException ftpe){
				logger.error("FTP failed to send file to remote server: " + ftpe.getMessage());
				interfaceErrorRepo.save(new InterfaceError(GeneralHelper.IN04, file.getName(), new Date(), GeneralHelper.FTP_SERVER, "FTP failed to execute.",
						GeneralHelper.PROCESSING_ERROR, getProcessingErrorFolder()));
				try{
					sendEmailWithAttachment("OFT failure", "FTP failed, please contect OFT team.", file);
				} catch (BCDSEmailException ee){
					logger.error("Email failed to send to recipients: " + ee.getMessage());
					interfaceErrorRepo.save(new InterfaceError(GeneralHelper.IN04, file.getName(), new Date(), GeneralHelper.SMTP_SERVER, "Email failed to execute.", 
							GeneralHelper.PROCESSING_ERROR, getProcessingErrorFolder()));
				}
				doBackup(file, getSendingErrorFolder());	
				continue;
			}
			//all good
			doBackup(file, getBackupFolder());
			logger.debug("Backup file success.");
		}
	}	
	

	@Override
	public void prepareInterfaceFile() {
		
		CSVWriter writer = null;
		FileWriter fileWriter = null;
		List<IN04> in04s = getIn04ReturnRecords();
		
		int count = 1;
		for (IN04 in04 : in04s){
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd-HHmmss-SSS");
			System.out.println("format.format(new Date():: " + format.format(new Date()));
			String sample1 = (format.format(new Date()).substring(0, 10));
			Integer dateLength = format.format(new Date()).length();
			int lastIndex = 28;
			if (dateLength<29){
				lastIndex=dateLength;
			}
			String sample2 = sample1.replaceAll("-", "");
			File in04File = new File(getSourceFolder()+"O3"+"_"+"DELIM" + format.format(new Date()) + ".csv");
			logger.info("Output file name  for the Job IN04 ::" + in04File.getName());
			try {
				fileWriter = new FileWriter(in04File);
				writer = new CSVWriter(fileWriter, '|', CSVWriter.NO_QUOTE_CHARACTER);
				DynaBean bean = CSVUtil.prepareIN04CSVLinePosition(in04);
				List<String[]> fileLines = new ArrayList<String[]>();
				List<?> headerList = (ArrayList<?>)bean.get(IN04Header.lineType);
				fileLines.add(headerList.toArray(new String[headerList.size()]));
				//producing multiple pid lines here
				for (int i=0; i<in04.getIn04Line().size(); i++){
					List<?> lineList = (ArrayList<?>)bean.get(IN04Line.lineType+i);
					fileLines.add(lineList.toArray(new String[lineList.size()]));
				}
				writer.writeAll(fileLines);
				// delay 1 milliseconds to prevent file name overlapping 
				TimeUnit.MILLISECONDS.sleep(30);
			} catch (InterruptedException e) {
				logger.error(e, e);
				} catch (IOException ioe) {
					if(in04File.exists()){
						doBackup(in04File, getProcessingErrorFolder());	
						interfaceErrorRepo.save(new InterfaceError(GeneralHelper.IN04, in04File.getName(), new Date(), 
								GeneralHelper.FILE_IO, ioe.getMessage(), GeneralHelper.PROCESSING_ERROR, getProcessingErrorFolder()));
					} else {
					interfaceErrorRepo.save(new InterfaceError(GeneralHelper.IN04, in04File.getName(), new Date(), 
							GeneralHelper.FILE_IO, ioe.getMessage(), GeneralHelper.PROCESSING_ERROR, getProcessingErrorFolder()));
				}
				}finally{
					try{
						if (writer != null)
							writer.close();
						if (fileWriter != null)
							fileWriter.close();
					}catch (Exception ex){
						logger.error("Error closing IN04 output file.");
					}
				}
				count++;
			}
	}	

	private List<IN04> getIn04ReturnRecords(){
		List<com.bcds.jobs.beans.Order> orders = orderService.findAllFinishedDNs();
		List<IN04> in04s = new ArrayList<IN04>();
		logger.info("IN04 orders size: " + orders.size());		
		for (com.bcds.jobs.beans.Order order : orders){
			IN04 in04 = new IN04();
			IN04Header in04Header = new IN04Header();
			in04Header.setInterfaceType(GeneralHelper.IN04);
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
			in04Header.setPostingDate(DateUtil.getLocalDateInSimpleDateFormat("yyyyMMdd",new Date()));
			String salesOrderNumber = "";
			if (order.getOrderReceivedDate() != null) {
				in04Header.setDocumentDate(DateUtil.getLocalDateInSimpleDateFormat("yyyyMMdd",order.getOrderReceivedDate()));
			}
			//question - is order number ok?
			in04Header.setDeliveryNumber(order.getOrderNumber());
			//question - what's this?
			in04Header.setHeaderText("");
			//question - how to determine which type of movement?
			in04Header.setGmCode("");
			List<IN04Line> orderLineList = new ArrayList<IN04Line>();
			List<OrderMeterialList> list = order.getOrderMeterialList();
			if (!list.isEmpty()) {
				for (OrderMeterialList orderMeterialList : list) {
				IN04Line in04Line = new IN04Line();	
				in04Line.setInterfaceType(GeneralHelper.IN04);
				OrderType orderType = order.getOrderType();
				in04Line.setMovementType("");
				if (orderType.getOrderTypeName().equalsIgnoreCase("DD") && order.getDdType().equalsIgnoreCase("Z1NL")) {
					in04Line.setMovementType(orderService.getByActionTypeAndDocType("GR", "Z1NL").getActionTypeSap());
				}
				if (order.getDdType() != null && order.getDdType().equalsIgnoreCase("NL")) {
					in04Line.setMovementType(orderService.getByActionTypeAndDocType("GR", "NL").getActionTypeSap());
				}
				in04Line.setMaterialNumber(orderMeterialList.getMeterial().getPartName());//part_num field not used, use part_name!!
				in04Line.setQuantity(orderMeterialList.getQuantity()+"");
				String fullLocationName = companyService.findPlantLocation(order.getCompanyLocation().getParentLocation()).getFullLocationName();
				in04Line.setPlant(fullLocationName);
				in04Line.setSourceLocation(order.getCompanyLocation().getLocationName());
				//in04Line.setDestinationLocation(assetMovementInfo.getDestinationLocation().getLocationName());
				if (orderMeterialList.getMeterial() != null && order.getDdType().equalsIgnoreCase("NL")) {
					in04Line.setBatchNumber(orderMeterialList.getMeterial().getBatchNumber());
				}
				else {
					in04Line.setBatchNumber(orderMeterialList.getBatchNumber());
				}
				in04Line.setUom("EA");
				in04Line.setVisitingEngSloc("");
				in04Line.setSalesOrderLineNum("00010");
				if (orderType.getOrderTypeName().equalsIgnoreCase("DD")){
					List<OrderRelationshipEntity> orderRelationshipEntityList = orderRelationshipRepo.findByParentOrderId(beanToEntityClass.convertBeanToEntity(order));
					for (OrderRelationshipEntity orderRelationshipEntity : orderRelationshipEntityList) {
						if (orderRelationshipEntity.getChildOrderId().getOrderType().getOrderTypeName().equalsIgnoreCase("SO")) {
							salesOrderNumber = orderRepo.getOne(orderRelationshipEntity.getChildOrderId().getId()).getOrderNumber();
							//salesOrderNumber = "00000"+salesOrderNumber.replaceAll("^0*", "");
							 }
						}
					}
				if (orderType.getOrderTypeName().equalsIgnoreCase("DN")){
					List<OrderRelationshipEntity> orderDDEntityList = orderRelationshipRepo.findByChildOrderId(beanToEntityClass.convertBeanToEntity(order));
					for (OrderRelationshipEntity orderDDList : orderDDEntityList) {
						if (orderDDList.getParentOrderId().getOrderType().getOrderTypeName().equalsIgnoreCase("DD")) {
							com.bcds.jpa.entity.Order orderDDEntity = orderDDList.getParentOrderId();
							List<OrderRelationshipEntity> orderRelationshipEntityList = orderRelationshipRepo.findByParentOrderId(orderDDEntity);
							if (orderRelationshipEntityList.get(0).getChildOrderId().getOrderType().getOrderTypeName().equalsIgnoreCase("SO")) {
								salesOrderNumber = orderRepo.getOne(orderRelationshipEntityList.get(0).getChildOrderId().getId()).getOrderNumber();
								//salesOrderNumber = "00000"+salesOrderNumber.replaceAll("^0*", "");
								 }
							}
						}
					}
				in04Line.setSalesOrderNum(salesOrderNumber);
				if (order.getDdType() != null && order.getDdType().equalsIgnoreCase("NL") && !StringUtils.isEmpty(orderMeterialList.getSalesOrderLineNumber())) {
					in04Line.setSalesOrderLineNum(orderMeterialList.getSalesOrderLineNumber());
				}
				else {
					in04Line.setSalesOrderLineNum(orderMeterialList.getLineNumber());
				}
				orderLineList.add(in04Line);
				}
			}	
			else {
				return  new ArrayList<IN04>();
			}
			in04.setIn04Header(in04Header);
			in04.setIn04Line(orderLineList);
			in04s.add(in04);
			orderService.updateOrderStatusToClosed(order);
		}
		return in04s;
	}

	public static void main(String[] args){
		try {
			@SuppressWarnings("resource")
			ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
			 SchedulerFactoryBean schedulerFactoryBean = new SchedulerFactoryBean();
			 SimpleTrigger trigger = context.getBean("in04JobTrigger", SimpleTrigger.class);
			 JobDetail jobDetail =  context.getBean("in04JobBean", JobDetail.class);
			 schedulerFactoryBean.setJobDetails(jobDetail);
			 schedulerFactoryBean.setTriggers(trigger);
			 Scheduler scheduler= (Scheduler) context.getBean("schedulerBean");		 
			 scheduler.scheduleJob(jobDetail, trigger);
			} catch (Exception  e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}
}
