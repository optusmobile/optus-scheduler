package com.bcds.jobs.exception;

public class BCDSDataException extends Exception{

	private static final long serialVersionUID = 1L;

	public BCDSDataException(){
		super();
	}
	
	public BCDSDataException(String message){
		super(message);
	}
}
