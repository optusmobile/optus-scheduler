package com.bcds.jobs.exception;

public class FileProcessingException extends Exception{

	private static final long serialVersionUID = 1L;

	public FileProcessingException(){
		super();
	}
	
	public FileProcessingException(String msg){
		super(msg);
	}
	
}
