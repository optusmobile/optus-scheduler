package com.bcds.jobs.exception;

public class BCDSEmailException extends Exception {

	private static final long serialVersionUID = 1L;

	public BCDSEmailException(){
		super();
	}
	
	public BCDSEmailException(String message){
		super(message);
	}
	
	public BCDSEmailException(Exception e){
		super(e);
	}
}
