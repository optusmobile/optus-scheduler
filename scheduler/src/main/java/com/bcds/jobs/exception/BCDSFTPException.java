package com.bcds.jobs.exception;

public class BCDSFTPException extends Exception {

	private static final long serialVersionUID = 1L;

	public BCDSFTPException(){
		super();
	}
	
	public BCDSFTPException(String message){
		super(message);
	}
	
	public BCDSFTPException(Exception e){
		super(e);
	}
}
