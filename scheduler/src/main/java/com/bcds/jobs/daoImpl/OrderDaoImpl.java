package com.bcds.jobs.daoImpl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.bcds.jobs.dao.OrderDao;
import com.bcds.jpa.entity.Order;
import com.bcds.jpa.entity.OrderRelationshipEntity;

@Repository
public class OrderDaoImpl implements OrderDao{

	@PersistenceContext
	public EntityManager entityManager;
	
	 public EntityManager getEntityManager() {
	        return entityManager;
	    }
	

	@Transactional
	public List<Order> filterOrderBySCPOAndA7() {
		String queryName = "filterOrderBySCPOAndA7";
			 return getEntityManager().createNamedQuery(queryName, Order.class).getResultList();
	}

	
	@Transactional
	public List<Order> filterOrderBySCPOAndA5() {
		String queryName = "filterOrderBySCPOAndA5";
			 return getEntityManager().createNamedQuery(queryName, Order.class).getResultList();
	}


	@Override
	@Transactional
	public List<OrderRelationshipEntity> findAllDNs() {
		String queryName = "findAllDNs";
			 return getEntityManager().createNamedQuery(queryName, OrderRelationshipEntity.class).getResultList();
		
	}

	@Override
	@Transactional
	public List<Order> filterOrderOutScpoWithA5OrA7() {
		String queryName = "filterOrderOutScpoWithA5OrA7";
		return getEntityManager().createNamedQuery(queryName, Order.class).getResultList();
	}
	
	@Override
	@Transactional
	public List<Order> findAllFinishedDNs() {
		String queryName = "findAllFinishedDNs";
		 return getEntityManager().createNamedQuery(queryName, Order.class).getResultList();
	}


	@Override
	@Transactional
	public List<Order> fillIN02Records() {
		String queryName = "findAllIN02Records";
		 return getEntityManager().createNamedQuery(queryName, Order.class).getResultList();
	}


	@Override
	public List<Order> findAllDDNotAssociatedWithDN() {
		String queryName = "findAllDDNotAssociatedWithDN";
		return getEntityManager().createNamedQuery(queryName, Order.class).getResultList();
	}
}
