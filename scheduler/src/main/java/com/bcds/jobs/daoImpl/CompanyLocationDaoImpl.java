package com.bcds.jobs.daoImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;
import com.bcds.jobs.dao.CompanyLocationDao;
import com.bcds.jpa.entity.CompanyLocation;
import com.bcds.jpa.entity.Order;

@Repository
public class CompanyLocationDaoImpl implements CompanyLocationDao {

	@PersistenceContext
	public EntityManager entityManager;
	
	 public EntityManager getEntityManager() {
	        return entityManager;
	    }
	 
	@Override
	@Transactional
	public List<CompanyLocation> getCompanyLicationByUserId(String userid) {
		String queryName = "getCompanyLicationByUserId";
		 return getEntityManager().createNamedQuery(queryName, CompanyLocation.class).setParameter("userId", userid).getResultList();
		 
	}

}
