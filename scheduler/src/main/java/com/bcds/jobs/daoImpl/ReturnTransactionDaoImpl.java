package com.bcds.jobs.daoImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bcds.jobs.dao.ReturnTransactionDao;
import com.bcds.jpa.entity.ReturnTransactionsEntity;
import com.bcds.jpa.repo.ReturnTransactionsRepo;

@Repository
public class ReturnTransactionDaoImpl implements ReturnTransactionDao{
	
	
	@PersistenceContext
	public EntityManager entityManager;
	
	 public EntityManager getEntityManager() {
	        return entityManager;
	    }
	 
	 @Autowired
	 ReturnTransactionsRepo returnTransactionsRepo;
	 
	@Override
	@Transactional
	public List<ReturnTransactionsEntity> filterByOrderStatus() {
		String queryName = "filterByOrderTransactionStatus";
		Query  query = getEntityManager().createNamedQuery(queryName, ReturnTransactionsEntity.class);
		List<ReturnTransactionsEntity> returnTransactionEntityList = query.getResultList();
			 return getEntityManager().createNamedQuery(queryName, ReturnTransactionsEntity.class).getResultList();
	}


	@Override
	public ReturnTransactionsEntity getReturnById(Integer id) {
		return returnTransactionsRepo.getOne(id);
	}

}
