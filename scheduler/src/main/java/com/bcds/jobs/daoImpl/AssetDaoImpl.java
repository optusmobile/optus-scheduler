package com.bcds.jobs.daoImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bcds.jobs.dao.AssetDao;
import com.bcds.jpa.entity.Asset;
import com.bcds.jpa.entity.AssetMovementInfo;

@Repository
public class AssetDaoImpl implements  AssetDao{
	
	@PersistenceContext
	public EntityManager entityManager;
	
	 public EntityManager getEntityManager() {
	        return entityManager;
	    }

	@Override
	@Transactional
	public List<AssetMovementInfo> filterByAssetStatus() {
		String queryName = "filterByAssetStatus";
//		EntityManager entityManager = getEntityManager();
//		Query  query = getEntityManager().createNamedQuery(queryName, AssetMovementInfo.class);
//		List<Order> orderEntityList = query.getResultList();
		return getEntityManager().createNamedQuery(queryName, AssetMovementInfo.class).getResultList();
	}

	@Override
	@Transactional
	public List<Asset> getAssetsAssociatedWithZ1NL(String barcode) {
		String queryName = "getAssetsAssociatedWithZ1NL";
		 return getEntityManager().createNamedQuery(queryName, Asset.class).setParameter("barcode", barcode).getResultList();
	}

}
