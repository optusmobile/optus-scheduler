package com.bcds.jobs;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;

import com.bcds.jobs.exception.BCDSFTPException;
import com.bcds.jobs.exception.FileProcessingException;

public abstract class AbstractOutgoingJob extends AbstractBaseJob {
	
	@Autowired
	MessageChannel sftpChannel;
	
	public void executeFTP(File file) throws BCDSFTPException {	
		try{
//			Message<File> message = MessageBuilder.withPayload(file).build();
			sftpChannel.send(MessageBuilder.withPayload(file).build());
		}catch (Exception e){
			if(!e.getMessage().matches("^.*Failed to remove file:.* is a directory, use remove directory command to remove$")){
				throw new BCDSFTPException(e);
			}
		}
	}

	public abstract void prepareInterfaceFile() throws FileProcessingException; 
}
