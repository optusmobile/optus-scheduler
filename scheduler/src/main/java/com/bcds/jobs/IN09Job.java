package com.bcds.jobs;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleTrigger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import com.bcds.file.helper.CSVUtil;
import com.bcds.file.helper.GeneralHelper;
import com.bcds.ftp.util.FileUtil;
import com.bcds.jobs.beans.ReturnTransactionBean;
import com.bcds.jobs.exception.BCDSEmailException;
import com.bcds.jobs.exception.BCDSFTPException;
import com.bcds.jpa.entity.InterfaceError;
import com.bcds.jpa.model.in04.IN04Line;
import com.bcds.jpa.model.in09.IN09;
import com.bcds.jpa.model.in09.IN09Header;
import com.bcds.jpa.model.in09.IN09Line;
import com.bcds.util.DateUtil;
import au.com.bytecode.opencsv.CSVWriter;

/*
 * IN039 - Sales Return transactions @Intrack to be sent to SAP
 * 
 * 
 */

public class IN09Job extends AbstractOutgoingJob {

		
	final static Logger logger = Logger.getLogger(IN09Job.class);

	@Value("${in09.filename}")
	protected String filename;

	@Value("${in09.backup.folder}")
	protected String backupFolder;

	@Value( "${in09.source.folder}")
	protected String sourceFolder;
	
	@Override
	public void executeJob() {
		
		logger.info("----------- IN09 Job Started ---------");
		setFilename(this.filename);
		setBackupFolder(this.backupFolder);
		setSourceFolder(this.sourceFolder);
		prepareInterfaceFile();
	
		List<String> allIN09s = new ArrayList<String>();
		try {
			allIN09s = FileUtil.getSimilarBatchFiles(getSourceFolder(), getFilename());
		} catch (NoSuchFileException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		for (String oneIN09 : allIN09s) {
			File file = new File(oneIN09);
			try{
				executeFTP(file);
			}catch (BCDSFTPException ftpe){
				logger.error("FTP failed to send file to remote server: " + ftpe.getMessage());
				interfaceErrorRepo.save(new InterfaceError(GeneralHelper.IN09, file.getName(), new Date(), 
						GeneralHelper.FTP_SERVER, "FTP failed to execute.", GeneralHelper.PROCESSING_ERROR, 
						getProcessingErrorFolder()));
				try{
					sendEmailWithAttachment("OFT failure", "FTP failed, please contect OFT team.", file);
				} catch (BCDSEmailException ee){
					logger.error("Email failed to send to recipients: " + ee.getMessage());
					interfaceErrorRepo.save(new InterfaceError(GeneralHelper.IN09, file.getName(), new Date(), 
							GeneralHelper.SMTP_SERVER, "Email failed to execute.", GeneralHelper.PROCESSING_ERROR, 
							getProcessingErrorFolder()));
				}
				doBackup(file, getSendingErrorFolder());	
				continue;
			}
			doBackup(file, getBackupFolder());
		}
	}

	
	@Override
	public void prepareInterfaceFile() {
		CSVWriter writer = null;
		FileWriter fileWriter = null;
		List<IN09> in09s = getIn09ReturnRecords();
		
		int count = 1;
		for (IN09 in09 : in09s) {
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd-HHmmss-SSS");
			System.out.println("format.format(new Date():: " + format.format(new Date()));
			File in09File = new File(getSourceFolder()+"O3"+"_"+"ORDS" + format.format(new Date()) + ".csv");
			logger.info("Output file name  for the Job IN09 ::" + in09File.getName());
			try {
				fileWriter = new FileWriter(in09File);
				writer = new CSVWriter(fileWriter, '|', CSVWriter.NO_QUOTE_CHARACTER);
				DynaBean bean = CSVUtil.prepareIN09CSVLinePosition(in09);
				List<String[]> fileLines = new ArrayList<String[]>();
				List<?> headerList = (ArrayList<?>)bean.get(IN09Header.lineType);
				fileLines.add(headerList.toArray(new String[headerList.size()]));
				//List<?> lineList = (ArrayList<?>)bean.get(IN09Line.lineType);
				for (int i=0; i<in09.getListOfLines().size(); i++){
					List<?> lineList = (ArrayList<?>)bean.get(IN04Line.lineType+i);
					fileLines.add(lineList.toArray(new String[lineList.size()]));
				}
				writer.writeAll(fileLines);
				TimeUnit.MILLISECONDS.sleep(30);
			}catch (InterruptedException e) {
				logger.error(e, e);
				} catch (IOException ioe) {
				if(in09File.exists()){
					doBackup(in09File, getProcessingErrorFolder());
					interfaceErrorRepo.save(new InterfaceError(GeneralHelper.IN09, in09File.getName(), new Date(), GeneralHelper.FILE_IO, 
							ioe.getMessage(), GeneralHelper.PROCESSING_ERROR, getProcessingErrorFolder()));
				} else {
					interfaceErrorRepo.save(new InterfaceError(GeneralHelper.IN09, in09File.getName(), new Date(), GeneralHelper.FILE_IO, 
							ioe.getMessage(), GeneralHelper.PROCESSING_ERROR, null));
				}
				
			}finally{
				try{
					if (writer != null)
						writer.close();
					if (fileWriter != null)
						fileWriter.close();
				}catch (Exception ex){
					logger.error("Error closing IN03 output file.");
				}
			}
			count++;
		}		
	}
	
	private List<IN09> getIn09ReturnRecords()  {
		Map<String,List<ReturnTransactionBean>> returnTransactionMap = returnTransactionsService.returnTransBeanMap();
		List<IN09> in09s = new ArrayList<IN09>();
		 Iterator<Entry<String, List<ReturnTransactionBean>>> mapIterator = returnTransactionMap.entrySet().iterator();
		 while (mapIterator.hasNext()) {
			 IN09 in09 = new IN09();
			 IN09Header in09Header = new IN09Header();
			 in09Header.setInterfaceType(GeneralHelper.IN09);
			 List<IN09Line> listOfLines = new ArrayList<IN09Line>();
			 Map.Entry pair = (Map.Entry)mapIterator.next();
			 List<ReturnTransactionBean> returnTransBeanList = (List<ReturnTransactionBean>) pair.getValue();
			 String deliveryNumber = (String) pair.getKey();
			 Date createdDate = null;
			 for(ReturnTransactionBean returnTransactionBean : returnTransBeanList){
				 createdDate = returnTransactionBean.getCreateDate();
				 IN09Line in09Line = createReturnTransLine(returnTransactionBean);
				 listOfLines.add(in09Line);
				 returnTransactionsService.updateReturnMovementStatusToClosed(returnTransactionBean);
			 }
			 SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
			 in09Header.setPostingDate(DateUtil.getLocalDateInSimpleDateFormat("yyyyMMdd",new Date()));
			 in09Header.setDocumentDate(DateUtil.getLocalDateInSimpleDateFormat("yyyyMMdd",createdDate));
			 in09Header.setDeliveryNumber(deliveryNumber);
			 in09Header.setHeaderText("");
			 in09Header.setGmCode("");
			 in09.setIn09Header(in09Header);
			 in09.setListOfLines(listOfLines);
			 in09s.add(in09);
		 }
		 return in09s;
	}
	
	
	public static void main (String[] args) {
		try {
			@SuppressWarnings("resource")
			ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
			 SchedulerFactoryBean schedulerFactoryBean = new SchedulerFactoryBean();
			 SimpleTrigger trigger = context.getBean("in09JobTrigger", SimpleTrigger.class);
			 JobDetail jobDetail =  context.getBean("in09JobBean", JobDetail.class);
			 schedulerFactoryBean.setJobDetails(jobDetail);
			 schedulerFactoryBean.setTriggers(trigger);
			 Scheduler scheduler= (Scheduler) context.getBean("schedulerBean");		 
			 scheduler.scheduleJob(jobDetail, trigger);
			} catch (Exception  e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	
	private IN09Line createReturnTransLine(ReturnTransactionBean returnTransBean){
		IN09Line in09Line = new IN09Line();
		in09Line.setInterfaceType(GeneralHelper.IN09);
		in09Line.setMovementType("");
		in09Line.setMaterialNumber(returnTransBean.getParts().getPartName());
		in09Line.setQuantity(returnTransBean.getQuantity().toString());
		String fullLocationName ="";
		if (returnTransBean.getDestinationLocation() != null &&  returnTransBean.getDestinationLocation().getParentLocation() != null) {
		 fullLocationName = companyLocationRepository.getOne(returnTransBean.getDestinationLocation().getParentLocation()).getLocationName();
		if (fullLocationName.contains("/")) {
			fullLocationName = fullLocationName.substring(0, fullLocationName.indexOf("/"));
			}
		}
		in09Line.setPlant(fullLocationName);
		in09Line.setSourceLocation(returnTransBean.getSourceLocation().getLocationName());
		in09Line.setDestinationLocation(returnTransBean.getDestinationLocation().getLocationName());
		in09Line.setBatchName(returnTransBean.getBatchNumber());
		in09Line.setUom("EA");
		in09Line.setLineNumber(returnTransBean.getLineNumber());
		return in09Line;
	}
	

	}
