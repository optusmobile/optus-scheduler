package com.bcds.jobs.dao;

import java.util.List;
import com.bcds.jpa.entity.Asset;
import com.bcds.jpa.entity.AssetMovementInfo;

public interface AssetDao {

	public List<AssetMovementInfo> filterByAssetStatus();
	
	public List<Asset> getAssetsAssociatedWithZ1NL(String barcode);
	
}
