package com.bcds.jobs.dao;

import java.util.List;
import com.bcds.jpa.entity.CompanyLocation;

public interface CompanyLocationDao {
	List<CompanyLocation> getCompanyLicationByUserId(String userid);

}
