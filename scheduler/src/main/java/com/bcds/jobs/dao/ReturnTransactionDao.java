package com.bcds.jobs.dao;

import java.util.List;
import com.bcds.jpa.entity.ReturnTransactionsEntity;

public interface ReturnTransactionDao {
	
	public List<ReturnTransactionsEntity> filterByOrderStatus();
	
	public ReturnTransactionsEntity getReturnById(Integer id);

}
