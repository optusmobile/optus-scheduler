package com.bcds.jobs.dao;

import java.util.List;
import com.bcds.jpa.entity.Order;
import com.bcds.jpa.entity.OrderRelationshipEntity;

public interface OrderDao {
	
	public List<Order> filterOrderBySCPOAndA7();
	
	public List<Order> filterOrderBySCPOAndA5();
	
	public List<OrderRelationshipEntity> findAllDNs();
	
	public List<Order> filterOrderOutScpoWithA5OrA7();
	
	public List<Order> findAllFinishedDNs();
	
	public List<Order> fillIN02Records();

	public List<Order> findAllDDNotAssociatedWithDN();
	

}
