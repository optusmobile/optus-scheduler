package com.bcds.jobs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.persistence.EntityNotFoundException;

import org.apache.log4j.Logger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleTrigger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Component;

import com.bcds.file.helper.GeneralHelper;
import com.bcds.file.helper.MM01Enum;
import com.bcds.ftp.util.FileUtil;
import com.bcds.jobs.beans.CompanyAssetCategoryBean;
import com.bcds.jobs.beans.CompanyAssetParts;
import com.bcds.jobs.exception.BCDSDataException;
import com.bcds.jobs.exception.BCDSEmailException;
import com.bcds.jobs.exception.FileProcessingException;
import com.bcds.jpa.entity.InterfaceError;
import com.bcds.jpa.model.mm01.MM01;
import com.bcds.jpa.repo.CompanyAssetCategoryRepo;
import com.bcds.service.iface.CompanyAssetCategoryServiceInterface;
import com.bcds.service.iface.CompanyAssetPartsServiceInterface;

/*
 * MM01 - Material Master Import
 * 
 * Date				Change description
 * -----       	   ---------------------
 * 
 * 9 May 2016		Changed codes to cater for multiple input files.
 * 					Tested OK as of 5pm 9 May 2016.
 * 
 * 	12 May 2016		Added Processing Error Folder!!	
 * 
 * 	13 May 2016		Improved the error handling codes.
 * 
 */


@Component
public class MM01Job extends AbstractIncomingJob {
	
	final static Logger logger = Logger.getLogger(MM01Job.class);
	
	@Value( "${mm01.filename}" )
	protected String filename;

	@Value( "${mm01.backup.folder}" )
	protected String backupFolder;

	@Value( "${mm01.source.folder}" )
	protected String sourceFolder;
	
	@Autowired
	CompanyAssetPartsServiceInterface companyAssetPartsServiceInterface;
	
	@Autowired
	CompanyAssetCategoryServiceInterface companyAssetCategoryServiceInterface;
	
	@Autowired
	CompanyAssetCategoryRepo companyAssetCategoryRepo;
	
	@Override
	public void executeJob() {
		
		logger.info("----------- MM01 Job Started ---------");
		setFilename(this.filename);
		setBackupFolder(this.backupFolder);
		setSourceFolder(this.sourceFolder);
		//this may return more than 1 file during each cycle
		List<String> allMM01s = new ArrayList<String>();
		try {
			allMM01s = FileUtil.getSimilarBatchFiles(getSourceFolder(), getFilename());
		} catch (NoSuchFileException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		for (String oneMM01 : allMM01s){
			try{
				MM01 mm01 = processInterfaceFile(oneMM01);
				companyAssetPartsServiceInterface.addCompanyAssetPart(mm01.getCompanyAssetParts());
				/*List<CompanyAssetCategoryBean> companyAssetCategoryBeanList = mm01.getCompanyAssetCategoryBeanList();
				for (CompanyAssetCategoryBean companyAssetCategoryBean: companyAssetCategoryBeanList) {
					companyAssetCategoryServiceInterface.addCompanyAssetCategory(companyAssetCategoryBean);
				}*/
			}
			catch (BCDSDataException bcdsException){
				logger.error("Exception thrown: " + bcdsException.getMessage());	
				interfaceErrorRepo.save(new InterfaceError(GeneralHelper.MM01, new File(oneMM01).getName(), new Date(), GeneralHelper.DB_TABLE, bcdsException.getMessage(),GeneralHelper.PROCESSING_ERROR, getProcessingErrorFolder()));
				sendEmailNotification(oneMM01,bcdsException.getMessage());
				doBackup(new File(oneMM01), getProcessingErrorFolder());
				continue;
			}
			catch (EntityNotFoundException enf){
				logger.error("Exception thrown: " + enf.getMessage());	
				interfaceErrorRepo.save(new InterfaceError(GeneralHelper.MM01, new File(oneMM01).getName(), new Date(), GeneralHelper.DB_TABLE, enf.getMessage(), GeneralHelper.PROCESSING_ERROR, getProcessingErrorFolder()));
				sendEmailNotification(oneMM01,enf.getMessage());
				doBackup(new File(oneMM01), getProcessingErrorFolder());
				continue;
			}
			catch (FileProcessingException fpe){
				logger.error("Exception thrown: " + fpe.getMessage());	
				interfaceErrorRepo.save(new InterfaceError(GeneralHelper.MM01, new File(oneMM01).getName(), new Date(), GeneralHelper.DB_TABLE, fpe.getMessage(), GeneralHelper.PROCESSING_ERROR, getProcessingErrorFolder()));
				sendEmailNotification(oneMM01,fpe.getMessage());
				doBackup(new File(oneMM01), getProcessingErrorFolder());
				continue;
			}
			logger.info(oneMM01.toString() + " processed successfully: " + oneMM01);
			doBackup(new File(oneMM01), getBackupFolder());				
		}
	}

	
	private MM01 processInterfaceFile(String oneMM01) throws FileProcessingException, BCDSDataException {
		
		Scanner scanner = null;
		MM01 mm01 = null;
		File file = new File(oneMM01);
		FileReader fr = null;
		BufferedReader bf = null;
		CompanyAssetParts companyAssetParts = new CompanyAssetParts();
//		List<CompanyAssetCategoryBean> companyAssetCategoryBeanList = new ArrayList<CompanyAssetCategoryBean>();
		Map<String, Integer> positions =new HashMap<String, Integer>();
		try{
			fr = new FileReader(file);			
		    bf = new BufferedReader(fr);
		    scanner = new Scanner(bf);				 
//		    MM01Plant mm01Plant = null;
	        while(scanner.hasNextLine()){
	        	String oneLine = scanner.nextLine();
	        	//logger.info("One Line: " + oneLine);
	        	String[] fields = oneLine.split("\\|");
	        	//looking for header 1st
	        	System.out.println("fields.length value :: "+ fields.length);
	        	if ( (fields.length >= 1)) {
	        		try {
        				mm01 = new MM01();
        				positions = MM01Enum.getFieldPosition();
        				companyAssetParts.setPartName(fields[positions.get(MM01Enum.MATERIAL_NUMBER)]);
        				companyAssetParts.setPartDescription(fields[positions.get(MM01Enum.MATERIAL_DESC)]);
        				if (fields.length == 3) {
//	        				companyAssetParts.setPartNumber(fields[positions.get(MM01Enum.PART_NUMBER)]);
        					CompanyAssetCategoryBean companyAssetCategoryAddedBean = companyAssetCategoryServiceInterface.findByCategoryNameAndParentCatId(companyAssetParts.getPartName(), 0);
        					if(companyAssetCategoryAddedBean == null){
        						CompanyAssetCategoryBean companyAssetCategoryBean = new CompanyAssetCategoryBean();
	        					companyAssetCategoryBean.setCategoryName(fields[positions.get(MM01Enum.MATERIAL_NUMBER)]);
	        					companyAssetCategoryBean.setParentCatId(0);
	        					companyAssetCategoryBean.setRootCategory(true);
	        					companyAssetCategoryAddedBean = companyAssetCategoryServiceInterface.addCompanyAssetCategory(companyAssetCategoryBean);
        					} else {
        						logger.info("Skip: Material Number already exist in tbl_company_asset_category- ".concat(companyAssetParts.getPartName()));
        					}
        					if (companyAssetCategoryAddedBean.getId() == 0) {
        						throw new EntityNotFoundException("CapmanyAssetCategory does not have the parent material id" + fields[positions.get(MM01Enum.MATERIAL_NUMBER)] + " to be sociated with part number" );
        					}
        					if(companyAssetCategoryServiceInterface.findByCategoryNameAndParentCatId(fields[positions.get(MM01Enum.PART_NUMBER)], companyAssetCategoryAddedBean.getId()) == null){
        						CompanyAssetCategoryBean companyAssetChildCategoryBean = new CompanyAssetCategoryBean();
	        					companyAssetChildCategoryBean.setCategoryName(fields[positions.get(MM01Enum.PART_NUMBER)]);
	        					companyAssetChildCategoryBean.setParentCatId(companyAssetCategoryAddedBean.getId());
	        					companyAssetChildCategoryBean.setRootCategory(false);
//		        				companyAssetCategoryBeanList.add(companyAssetCategoryBean);
//		        				companyAssetCategoryBeanList.add(companyAssetChildCategoryBean);
	        					companyAssetCategoryServiceInterface.addCompanyAssetCategory(companyAssetChildCategoryBean);
        					} else {
        						logger.info("Skip: Manufacture Part Number already exist in tbl_company_asset_category- ".concat(fields[positions.get(MM01Enum.PART_NUMBER)]));
        					}
        				}
	        		}catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundException) {
	        			throw new BCDSDataException("Material description not found, Invalid file");
	        		}
        		}
	        }
    		mm01.setCompanyAssetParts(companyAssetParts);
    		//mm01.setCompanyAssetCategoryBeanList(companyAssetCategoryBeanList);
		}catch(FileNotFoundException fnfe){
			//file should always be there	
			logger.error(fnfe.getMessage());
		}finally{
			if (scanner != null)
				scanner.close();
			
			if (bf != null){
				try{
					bf.close();
				}catch(IOException ioe){
					logger.error("BufferedReader not closed" + ioe.getMessage());
				}
			}
			
			if (fr != null){
				try{
					fr.close();
				}catch(IOException ioe){
					logger.error("FileReader not closed" + ioe.getMessage());
				}
			}
		}
		return mm01;
	}
	
	private void sendEmailNotification(String fileName,String message){
		try {
			sendEmailWithAttachment(fileName+" failed", message, new File(fileName));
		} catch (BCDSEmailException e) {
			logger.error("Email failed to send to recipients: " + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String agrs[]) {
		try {
			@SuppressWarnings("resource")
			ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
			 SchedulerFactoryBean schedulerFactoryBean = new SchedulerFactoryBean();
			 SimpleTrigger trigger = context.getBean("mm01JobTrigger", SimpleTrigger.class);
			 JobDetail jobDetail =  context.getBean("mm01JobBean", JobDetail.class);
			 schedulerFactoryBean.setJobDetails(jobDetail);
			 schedulerFactoryBean.setTriggers(trigger);
			 Scheduler scheduler= (Scheduler) context.getBean("schedulerBean");		 
			 scheduler.scheduleJob(jobDetail, trigger);
			} catch (Exception  e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
}
