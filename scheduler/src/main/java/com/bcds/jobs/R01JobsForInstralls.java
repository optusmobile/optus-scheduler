package com.bcds.jobs;


import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

/**
 * @author jason.chen
 *
 */
@Component
public class R01JobsForInstralls extends QuartzJobBean {

	final static Logger logger = Logger.getLogger(R01JobsForInstralls.class);

	@Autowired
	private CronScheduleTask cronScheduleTask;
	
	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		cronScheduleTask.doR01JobsForInstralls();
	}

	public void setCronScheduleTask(CronScheduleTask cronScheduleTask) {
        this.cronScheduleTask = cronScheduleTask;
    }
}
