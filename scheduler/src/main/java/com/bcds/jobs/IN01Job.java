package com.bcds.jobs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import javax.persistence.EntityNotFoundException;
import org.apache.log4j.Logger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleTrigger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Component;
import com.bcds.file.helper.GeneralHelper;
import com.bcds.file.helper.IN01Enum;
import com.bcds.file.helper.OrderStatusEnum;
import com.bcds.ftp.util.FileUtil;
import com.bcds.jobs.beans.BeanToEntityClass;
import com.bcds.jobs.beans.CompanyAssetParts;
import com.bcds.jobs.beans.CompanyLocation;
import com.bcds.jobs.beans.EntityToBeanClass;
import com.bcds.jobs.beans.Order;
import com.bcds.jobs.beans.OrderActionType;
import com.bcds.jobs.beans.OrderDDBean;
import com.bcds.jobs.beans.OrderMeterialList;
import com.bcds.jobs.beans.OrderRelationshipBean;
import com.bcds.jobs.beans.OrderType;
import com.bcds.jobs.exception.BCDSDataException;
import com.bcds.jobs.exception.BCDSEmailException;
import com.bcds.jobs.exception.FileProcessingException;
import com.bcds.jpa.entity.InterfaceError;
import com.bcds.jpa.entity.OrderRelationshipEntity;
import com.bcds.jpa.model.in01.IN01;
import com.bcds.jpa.model.in01.IN01OrderHeader;
import com.bcds.jpa.model.in01.IN01OrderLine;
import com.bcds.jpa.repo.OrderRelationshopRepo;
import com.bcds.jpa.repo.OrderRepository;

/*
 * IN01 - Orders Transaction Import
 * Name - IN01Job
 * 
 * Date				Change description
 * -----       	   ---------------------
 * 19 May 2016		Using new format that includes multiple lines or materials in one order.
 * 
 */

@Component
public class IN01Job extends AbstractIncomingJob {
	
	String orderTypeName = null;
	String docType = null;
	String eocoNumber = null;
	
	final static Logger logger = Logger.getLogger(IN01Job.class);
	
	private static final String IN06_FileName = "O3_IN06";
	
	private static final String IN01_FileName = "O3_IN01";
	
	private static final String INSTALL = "INSTALL";
	
	private static final String SPARE = "SPARE";

	@Value("${in01.filename}")
	protected String filename;

	@Value("${in01.backup.folder}")
	protected String backupFolder;

	@Value("${in01.source.folder}")
	protected String sourceFolder;
	
	@Autowired
	OrderRepository orderRepo;
	
	@Autowired
	EntityToBeanClass entityToBeanClass;
	
	@Autowired
	OrderRelationshopRepo orderRelationshipRepo;
	
	@Autowired
	BeanToEntityClass beanToEntityClass;
	
	@Override
	public void executeJob() {
		logger.info("----------- IN01 Job Started ---------");
		setFilename(this.filename);
		setBackupFolder(this.backupFolder);
		setSourceFolder(this.sourceFolder);
		logger.info("Whats the name?: " + getFilename());
		List<String> allIN0s = new ArrayList<String>();
		try{
		 allIN0s = FileUtil.getSimilarBatchFiles(getSourceFolder(), getFilename());
		}catch(Exception e){
			logger.info("Exception thrown: " + e.getMessage());
		}
		logger.info("IN01 size: " + allIN0s.size());
		String fileType="";
		for (String oneIN0 : allIN0s) {
			if (oneIN0.contains(IN06_FileName)) {
				fileType = INSTALL;
				logger.info("IN06 filename: " + oneIN0);
			}
			if (oneIN0.contains(IN01_FileName)) {
				fileType = SPARE;
				logger.info("IN01 filename: " + oneIN0);
			}
			try{	
				IN01 in01 = processInterfaceFile(oneIN0, fileType);
				orderService.insertIN01Order(in01);	
			} catch(FileProcessingException fpe){
				interfaceErrorRepo.save(new InterfaceError(GeneralHelper.IN01, new File(oneIN0).getName(), new Date(), IN01Enum.EDI_DC40.toString(), 
						fpe.getMessage(), GeneralHelper.PROCESSING_ERROR, getProcessingErrorFolder()));
				logger.error("Exception thrown: " + fpe.getMessage());
				sendEmailNotification(oneIN0,fpe.getMessage());
				doBackup(new File(oneIN0), getProcessingErrorFolder());
				continue;
			} catch(EntityNotFoundException entityNotFoundException) {
				logger.error("Exception thrown: " + entityNotFoundException.getMessage());
				interfaceErrorRepo.save(new InterfaceError(GeneralHelper.IN01, new File(oneIN0).getName(), new Date(), GeneralHelper.DB_TABLE, 
						entityNotFoundException.getMessage(), GeneralHelper.PROCESSING_ERROR, getProcessingErrorFolder()));
				sendEmailNotification(oneIN0,entityNotFoundException.getMessage());
				doBackup(new File(oneIN0), getProcessingErrorFolder());
				continue;
			} catch(ArrayIndexOutOfBoundsException  | BCDSDataException exception) {
				logger.error("Exception thrown: " + exception.getMessage());
				interfaceErrorRepo.save(new InterfaceError(GeneralHelper.IN01, new File(oneIN0).getName(), new Date(), GeneralHelper.DB_TABLE, 
						exception.getMessage(), GeneralHelper.PROCESSING_ERROR, getProcessingErrorFolder()));
				sendEmailNotification(oneIN0,exception.getMessage());
				doBackup(new File(oneIN0), getProcessingErrorFolder());
				continue;
			} catch(Exception exc){
				logger.error("Exception thrown: " + exc.getMessage());
				interfaceErrorRepo.save(new InterfaceError(GeneralHelper.IN01, new File(oneIN0).getName(), new Date(), GeneralHelper.DB_TABLE, 
						exc.getMessage(), GeneralHelper.PROCESSING_ERROR, getProcessingErrorFolder()));
				sendEmailNotification(oneIN0,exc.getMessage());
				doBackup(new File(oneIN0), getProcessingErrorFolder());
			}
			logger.info(oneIN0.toString() + " processed successfully: " + oneIN0);
			doBackup(new File(oneIN0), getBackupFolder());	
		}
	}

	private IN01 processInterfaceFile(String oneIN01, String fileType) throws FileProcessingException, FileNotFoundException, EntityNotFoundException{
		Scanner scanner = null;
		IN01 in01 = new IN01();
		File file = new File(oneIN01);
		FileReader fr = null;
		BufferedReader bf = null;
		boolean parentExist = false;
	
		List<IN01OrderLine> in01OrderLines = new ArrayList<IN01OrderLine>();
		Order order = new Order();
		List<OrderMeterialList> orderMeterialList = new ArrayList<OrderMeterialList>();
		IN01OrderLine in01OrderLine = new IN01OrderLine();
		CompanyLocation companyLocation = new CompanyLocation();
		IN01OrderHeader in01OrderHeader = new IN01OrderHeader();
		
		try {
			fr = new FileReader(file);			
		    bf = new BufferedReader(fr);
		    scanner = new Scanner(bf);				 
		        			
	        while(scanner.hasNextLine()){
	        	String oneLine = scanner.nextLine();
	        	String[] fields = oneLine.split("\\|");
	        	 if (fields[0].equals("AUD")){
	        		if ( in01 == null ) {
						interfaceErrorRepo.save(new InterfaceError(GeneralHelper.IN01, oneIN01, new Date(),
								IN01Enum.EDI_DC40.toString(), "File Control Record EDI_DC40 not found.", oneLine, oneLine));
	        			doBackup(file, getProcessingErrorFolder());
	        			throw new FileProcessingException("File Control Record EDI_DC40 not found. Batch processing terminated.");	
	        		}
	        		Map<String, Integer> positions = IN01Enum.AUD.getFieldPosition();
	        		in01OrderHeader.setDocumentType(fields[positions.get(IN01Enum.DOCUMENT_TYPE)]);
	        		in01OrderHeader.setDocumentNumber(fields[positions.get(IN01Enum.IDOC_DOCUMENT_NUMBER)]);
	        		in01OrderHeader.setAssetTypeHeader(fileType);
	        		in01.setIn01OrderHeader(in01OrderHeader);
				} else if (((fields.length > 1 && fields[1].length() >= 8))
						|| ((fields.length > 1 && fields[1].contains("H")))) {
	        		if (fileType.equalsIgnoreCase(INSTALL)) {
	        			if (fields.length >=3 && fields[2].isEmpty()) {
	        				throw new FileProcessingException("IN06 file header does not contain any document type.");
	        			}
	        			docType = fields[2];
	        			orderTypeName=fields[3];
					} else {
	        				docType = fields[1];
	        				orderTypeName = fields[0];
						if (!fields[0].isEmpty() && fields.length >= 4 && fields[0].equalsIgnoreCase("SCPO")
								&& !fields[3].isEmpty()) {
	        					in01OrderHeader.setA5GIIndicator(fields[3]);
	        				}
						if (!fields[0].isEmpty() && fields.length >= 4 && fields[0].equalsIgnoreCase("UB")
								&& !fields[2].isEmpty()) {
	        					in01OrderHeader.setDocumentType(fields[2].trim());
	        				}
	        		}
	        	} 
	        	if (fields.length < 6 && (orderTypeName.equalsIgnoreCase("Z1NL") || orderTypeName.equalsIgnoreCase("LF"))) {
	        		try {
	        			eocoNumber = fields[4];
	        		}catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundException) {
	        			throw new FileProcessingException(orderTypeName + " does not have EOCO number associated");
	        		}
//	        	}
//	        	if (fields.length < 6 && orderTypeName.equalsIgnoreCase("Z1NL")) {
//	        		try {
//	        			if (fields[4].isEmpty());
//	        		}catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundException) {
//	        			interfaceErrorRepo.save(new InterfaceError(GeneralHelper.IN01, new File(oneIN01).getName(), new Date(), GeneralHelper.DB_TABLE, 
//	        					"Z1NL does not have EOCO number associated", GeneralHelper.PROCESSING_ERROR, getProcessingErrorFolder()));
//	    				doBackup(new File(oneIN01), getProcessingErrorFolder());
//	        		}
				} else if ((fields.length > 6 && orderTypeName.equals("LF"))
						|| (fields.length > 6 && orderTypeName.equals("Z1NL"))
						|| (fields.length > 6 && orderTypeName.equals("NL"))) {
	        		return createOrderDDBean(oneIN01,fileType);
				} else if (fields.length > 6) {
	        		Map<String, Integer> positions =new HashMap<String, Integer>();
	        		in01OrderHeader.setAssetTypeHeader(fileType);
	        		if (IN01Enum.SCPO.getFieldPosition() != null) {
	        			positions = IN01Enum.SCPO.getFieldPosition();
	        		}
	        		if (IN01Enum.UB.getFieldPosition() != null) {
	        			positions = IN01Enum.UB.getFieldPosition();
	        		}
	        		if (fileType.equalsIgnoreCase(INSTALL)) {
	        			positions = IN01Enum.INSTALL.getFieldPosition();
	        			companyLocation.setLocationName(fields[positions.get(IN01Enum.SLOCATION)]);
						String barcode = fields[positions.get(IN01Enum.PLANT)]
								+ fields[positions.get(IN01Enum.SLOCATION)];
	        			OrderMeterialList orderMeterial= new OrderMeterialList();
	        			companyLocation.setBarcode(barcode);
	        			companyLocation.setTag(INSTALL);
	        			orderMeterial.setLocation(companyLocation);
	        			orderMeterial.setQuantity(new Double(fields[positions.get(IN01Enum.QUANTITY)]));
	        			CompanyAssetParts companyAssetParts = new CompanyAssetParts();
	        			companyAssetParts.setPartName(fields[positions.get(IN01Enum.MATERIAL_NUMBER)]);
		        		orderMeterial.setMeterial(companyAssetParts);
		        		orderMeterial.setSalesOrderLineNumber(fields[positions.get(IN01Enum.SALES_ORDER_LINE)]);
		        		orderMeterial.setStatus(OrderStatusEnum.PENDING);
		        		if (fields.length > 11 && !fields[11].isEmpty()) {
		        			orderMeterial.setBatchNumber(fields[positions.get(IN01Enum.BATCH_NUMBER)]);
		        		}	
		        		orderMeterialList.add(orderMeterial);
	        			in01OrderLine.setPlant(fields[positions.get(IN01Enum.PLANT)]);
		        		in01OrderLine.setUnitOfMeasure(fields[positions.get(IN01Enum.UNIT_OF_MEASURE)]);
		        		in01OrderLine.setMaterialNumber(fields[positions.get(IN01Enum.MATERIAL_NUMBER)]);
		        		in01OrderLine.setSalesOrderNumber(fields[positions.get(IN01Enum.SALES_ORDER_NUMBER)]);
		        		in01OrderLine.setSalesOrderLine(fields[positions.get(IN01Enum.SALES_ORDER_LINE)]);
	        			order.setCompanyLocation(companyLocation);
					} else {
						OrderActionType orderActionTypeGR = new OrderActionType();
						orderActionTypeGR.setActionName("GR");
	        			order.setOrderActionType(orderActionTypeGR);
	        			if (orderTypeName.equals("NL")) {
	        				OrderActionType orderActionType = new OrderActionType();
	        				orderActionType.setActionName("GI");
		        			order.setOrderActionType(orderActionType);
		        			positions = IN01Enum.LF.getFieldPosition();
	        			}
	        			String sloction = fields[positions.get(IN01Enum.SLOCATION)];
	        			if (orderTypeName.equalsIgnoreCase("UB")
	        					&& (in01OrderHeader.getDocumentType().equalsIgnoreCase("A00062") || fields[positions.get(IN01Enum.MRFID)].equalsIgnoreCase("A.0"))
	        					&& sloction.isEmpty()) {
	        				sloction="A000";
	        			}
	        			companyLocation.setLocationName(sloction);
						String barcode = fields[positions.get(IN01Enum.PLANT)]
								+ sloction;
	        			companyLocation.setBarcode(barcode);
	        			companyLocation.setTag(SPARE);
	        			OrderMeterialList orderMeterial= new OrderMeterialList();
	        			orderMeterial.setLocation(companyLocation);
	        			orderMeterial.setQuantity(new Double(fields[positions.get(IN01Enum.QUANTITY)]));
	        			CompanyAssetParts companyAssetParts = new CompanyAssetParts();
	        			companyAssetParts.setPartName(fields[positions.get(IN01Enum.MATERIAL_NUMBER)]);
	        			orderMeterial.setMeterial(companyAssetParts);
	        			orderMeterial.setSalesOrderLineNumber(fields[positions.get(IN01Enum.LINE_NUMBER)]);
	        			if (orderTypeName.equals("NL") && fields.length > 11 && !fields[11].isEmpty()) {
	        				orderMeterial.setBatchNumber(fields[positions.get(IN01Enum.BATCH_NUMBER)]);
	        			}
	        			if (fields.length > 8 && !fields[8].isEmpty()) {
	        				orderMeterial.setBatchNumber(fields[positions.get(IN01Enum.BATCH_NUMBER)]);
	        			}
	        			orderMeterial.setStatus(OrderStatusEnum.PENDING);
	        			orderMeterialList.add(orderMeterial);
		        		in01OrderLine.setUnitOfMeasure(fields[positions.get(IN01Enum.UNIT_OF_MEASURE)]);
		        		in01OrderLine.setPlant(fields[positions.get(IN01Enum.PLANT)]);
		        		in01OrderLine.setMrfTypeId(fields[positions.get(IN01Enum.MRFID)]);
		        		in01OrderLine.setMaterialNumber(fields[positions.get(IN01Enum.MATERIAL_NUMBER)]);
		        		order.setMrf_ref(fields[positions.get(IN01Enum.MRFID)]);
		        		order.setSerialNumber(fields[positions.get(IN01Enum.SERIAL_NUMBER)]);
	        			order.setCompanyLocation(companyLocation);
	        			if (orderTypeName.equals("SCPO") &&  in01OrderHeader.getA5GIIndicator() != null 
								&& in01OrderHeader.getA5GIIndicator().equalsIgnoreCase("GI")
								&& order.getMrf_ref() != null && order.getMrf_ref().equalsIgnoreCase("A.5")) {
	        				OrderActionType orderActionType = new OrderActionType();
	        				orderActionType.setActionName("GI");
		        			order.setOrderActionType(orderActionType);
	        			}
	        		}
	        	}
	        }
	        order.setOrderMeterialList(orderMeterialList);
    		order.setOrderNumber(docType);
    		OrderType orderType = new OrderType();
    		orderType.setOrderTypeName(orderTypeName);
    		order.setOrderType(orderType);
    		in01OrderLine.setOrder(order);
    		in01OrderLine.setTag(fileType);
    		in01OrderLines.add(in01OrderLine);
    		in01.setIn01OrderHeader(in01OrderHeader);
    		in01.setIn01OrderLines(in01OrderLines);
    		parentExist = true;
//		} catch(FileProcessingException fpe){
//			throw new FileProcessingException(fpe.getMessage());
//		} catch(FileNotFoundException fnfe){
//			throw new FileNotFoundException(fnfe.getMessage());
//		} catch(EntityNotFoundException enfe){
//			throw new EntityNotFoundException(enfe.getMessage());
//		} catch (Exception e) {
//			logger.error(e,e);
//			throw new FileProcessingException("Fail to parse file: "+ file.getName());
		}finally{
			if (scanner != null)
				scanner.close();
			if (bf != null){
				try{
					bf.close();
				}catch(IOException ioe){
					logger.error("BufferedReader not closed" + ioe.getMessage());
				}
			}
			if (fr != null){
				try{
					fr.close();
				}catch(IOException ioe){
					logger.error("FileReader not closed" + ioe.getMessage());
				}
			}				
		}	        
		return in01;
	}
	
	
	private IN01 createOrderDDBean(String oneIN01, String fileType) throws FileProcessingException,EntityNotFoundException {
		IN01 in01 = new IN01();
		File file = new File(oneIN01);
		Scanner scanner = null;
    	FileReader fr = null;
		BufferedReader bf = null;
		
		int count = 0;
		List<IN01OrderLine> in01OrderLines = new ArrayList<IN01OrderLine>();
		OrderDDBean orderDDBean = new OrderDDBean();
		List<OrderMeterialList> orderMeterialList = new ArrayList<OrderMeterialList>();
		List<Order> orderListDD = new ArrayList<Order>();
		List<OrderRelationshipBean> orderRelationshipList = new ArrayList<OrderRelationshipBean>();
		IN01OrderLine in01OrderLine = new IN01OrderLine();
		CompanyLocation companyLocation = new CompanyLocation();
		String salesOrderNumberLF = "";
		IN01OrderHeader in01OrderHeader = new IN01OrderHeader();
		Order orderDD = new Order();
		Order orderSTO = new Order();
		Order orderSO = new Order();
		Order orderEOCO= new Order();
		OrderRelationshipBean orderRelationshipBeanDDToSO = new OrderRelationshipBean(); 
		OrderRelationshipBean orderRelationshipBeanDDToSTO = new OrderRelationshipBean(); 
		OrderRelationshipBean orderRelationshipBeanSTOToSO = new OrderRelationshipBean(); 
		OrderRelationshipBean orderRelationshipBeanSOToEOCO = new OrderRelationshipBean();
		OrderRelationshipBean orderRelationshipBeanDDToDN = new OrderRelationshipBean();
		
		try {
			fr = new FileReader(file);			
		    bf = new BufferedReader(fr);
		    scanner = new Scanner(bf);
		    while(scanner.hasNextLine()){
		    	String oneLine = scanner.nextLine();
		    	String[] fields = oneLine.split("\\|");
		    	if ( in01 == null ) {
		    		interfaceErrorRepo.save(new InterfaceError(GeneralHelper.IN01, oneIN01, new Date(), IN01Enum.EDI_DC40.toString(), 
		    				"File Control Record EDI_DC40 not found.", GeneralHelper.PROCESSING_ERROR, getProcessingErrorFolder()));
        			throw new FileProcessingException("File Control Record EDI_DC40 not found. Batch processing terminated.");	
        		}
		    	if (fields.length > 3 && fields.length < 6  ) {
		    		Map<String, Integer> positions = IN01Enum.IN06.getFieldPosition();
		    		in01OrderHeader.setDocumentType(fields[positions.get(IN01Enum.DOCUMENT_TYPE)]);
	        		in01OrderHeader.setDocumentNumber(fields[positions.get(IN01Enum.IDOC_DOCUMENT_NUMBER)]);
	        		in01OrderHeader.setAssetTypeHeader(fileType);
	        		docType = fields[2];
    				orderTypeName = fields[3];
		    	}
		      if(fields.length > 6 ) {
        			Map<String, Integer> positions = new HashMap<String, Integer>();
        			in01OrderHeader.setAssetTypeHeader(fileType);
        			OrderActionType orderActionType = new OrderActionType();
        			OrderType orderTypeDD = new OrderType();
        			OrderType orderTypeSO = new OrderType();
        			OrderType orderTypeSTO = new OrderType();
        			OrderType orderTypeDN = new OrderType();
        			OrderType orderTypeEOCO = new OrderType();
        			OrderMeterialList orderMeterial = new OrderMeterialList();
        			//String salesOrderNumberLF ="";
        			com.bcds.jpa.entity.Order salesOrderEntity = null;
        			
        			if (IN01Enum.Z1NL.getFieldPosition() != null && fileType.equalsIgnoreCase(INSTALL) && orderTypeName.equalsIgnoreCase("Z1NL")) {
	        			positions = IN01Enum.Z1NL.getFieldPosition();
	        			CompanyLocation companyLocationLocal = new CompanyLocation();
	        			companyLocationLocal.setLocationName(fields[positions.get(IN01Enum.SLOCATION)]);
	        			companyLocation.setLocationName(fields[positions.get(IN01Enum.SLOCATION)]);
	        			String barcode = fields[positions.get(IN01Enum.PLANT)] + fields[positions.get(IN01Enum.SLOCATION)];
	        			companyLocationLocal.setTag(INSTALL);
	        			companyLocation.setTag(INSTALL);
	        			companyLocationLocal.setBarcode(barcode);
	        			companyLocation.setBarcode(barcode);
	        			orderTypeEOCO.setOrderTypeName("EOCO");
	        			orderEOCO.setOrderType(orderTypeEOCO);
	        			orderActionType.setActionName("GR");
	        			orderEOCO.setCompanyLocation(companyLocation);
	        			orderEOCO.setOrderActionType(orderActionType);
	        			orderEOCO.setOrderNumber(eocoNumber);
	        			orderTypeDD.setOrderTypeName("DD");
	        			orderDD.setOrderType(orderTypeDD);
	        			orderDD.setCompanyLocation(companyLocation);
	        			orderMeterial.setLineNumber(fields[positions.get(IN01Enum.LINE_NUMBER)]);
	        			orderMeterial.setLocation(companyLocationLocal);
	        			orderDD.setDdType("Z1NL");
	        			if (fields.length > 11 && !fields[11].isEmpty()) {
	        				orderMeterial.setBatchNumber(fields[positions.get(IN01Enum.BATCH_NUMBER)]);
	        			}
	        		}
        			if (IN01Enum.LF.getFieldPosition() != null && fileType.equalsIgnoreCase(INSTALL) && orderTypeName.equalsIgnoreCase("LF")) {
	        			positions = IN01Enum.LF.getFieldPosition();
	        			CompanyLocation companyLocationLocal = new CompanyLocation();
	        			companyLocationLocal.setLocationName(fields[positions.get(IN01Enum.SLOCATION)]);
	        			companyLocation.setLocationName(fields[positions.get(IN01Enum.SLOCATION)]);
	        			String barcode = fields[positions.get(IN01Enum.PLANT)] + fields[positions.get(IN01Enum.SLOCATION)];
	        			companyLocationLocal.setTag(INSTALL);
	        			companyLocation.setTag(INSTALL);
	        			companyLocationLocal.setBarcode(barcode);
	        			companyLocation.setBarcode(barcode);
	        			orderActionType.setActionName("GI");
	        			companyLocation.setTag(INSTALL);
	        			orderTypeDN.setOrderTypeName("DN");
	        			orderDD.setOrderType(orderTypeDN);
	        			orderDD.setCompanyLocation(companyLocation);
	        			Order orderBean = new Order();
	        			OrderType orderType = new OrderType();
	        			orderType.setOrderTypeName("DN");
	        			orderBean.setOrderType(orderType);
	        			salesOrderNumberLF = fields[positions.get(IN01Enum.SALES_ORDER_NUMBER)];
	        			salesOrderEntity = orderRepo.findByOrderNumber(salesOrderNumberLF);
	        			orderDD.setDdType("LF");
	        			orderMeterial.setSalesOrderLineNumber(fields[positions.get(IN01Enum.SALES_ORDER_LINE_NUMBER)]);
	        			orderMeterial.setLineNumber(fields[positions.get(IN01Enum.LINE_NUMBER)]);
	        			orderMeterial.setLocation(companyLocationLocal);
	        			if (fields.length > 11 && !fields[11].isEmpty()) {
	        				orderMeterial.setBatchNumber(fields[positions.get(IN01Enum.BATCH_NUMBER)]);
	        			}
	        		}
        			if (IN01Enum.NF.getFieldPosition() != null && fileType.equalsIgnoreCase(INSTALL) && orderTypeName.equalsIgnoreCase("NL")) {
	        			positions = IN01Enum.LF.getFieldPosition();
	        			CompanyLocation companyLocationLocal = new CompanyLocation();
	        			companyLocationLocal.setLocationName(fields[positions.get(IN01Enum.SLOCATION)]);
	        			companyLocation.setLocationName(fields[positions.get(IN01Enum.SLOCATION)]);
	        			String barcode = fields[positions.get(IN01Enum.PLANT)] + fields[positions.get(IN01Enum.SLOCATION)];
	        			companyLocationLocal.setBarcode(barcode);
	        			companyLocation.setBarcode(barcode);
	        			orderActionType.setActionName("GI");
	        			companyLocationLocal.setTag(SPARE);
	        			orderMeterial.setLocation(companyLocationLocal);
	        			companyLocation.setTag(SPARE);
	        			orderTypeDN.setOrderTypeName("STO");
	        			orderDD.setOrderType(orderTypeDN);
	        			orderDD.setCompanyLocation(companyLocation);
	        			orderDD.setDdType("NL");
	        			orderMeterial.setSalesOrderLineNumber(fields[positions.get(IN01Enum.LINE_NUMBER)]);
	        			if (fields.length > 11 && !fields[11].isEmpty()) {
	        				orderMeterial.setBatchNumber(fields[positions.get(IN01Enum.BATCH_NUMBER)]);
	        			}
	        		}
        			//orderMeterial.setLocation(companyLocationLocal);
        			orderMeterial.setQuantity(new Double(fields[positions.get(IN01Enum.QUANTITY)]));
        			CompanyAssetParts companyAssetParts = new CompanyAssetParts();
        			companyAssetParts.setPartName(fields[positions.get(IN01Enum.MATERIAL_NUMBER)]);
        			if (positions != null && fileType.equalsIgnoreCase(INSTALL) && orderTypeName.equalsIgnoreCase("NL")) {
        				orderMeterial.setSalesOrderLineNumber(fields[positions.get(IN01Enum.LINE_NUMBER)]);
        			}
        			else {
        				orderMeterial.setSalesOrderLineNumber(fields[positions.get(IN01Enum.SALES_ORDER_LINE_NUMBER)]);
        			}
        			/*
        			 * Commenting code Reason to set LINE_NUMBER for NL in tbl_order_material_List. Updated by Jason Chen
        			 * 	orderMeterial.setMeterial(companyAssetParts);
        			 *	orderMeterial.setStatus(OrderStatusEnum.PENDING);
        			 *	orderMeterialList.add(orderMeterial);
        			 * */
	        		in01OrderLine.setPlant(fields[positions.get(IN01Enum.PLANT)]);
	        		in01OrderLine.setUnitOfMeasure(fields[positions.get(IN01Enum.UNIT_OF_MEASURE)]);
	        		in01OrderLine.setMaterialNumber(fields[positions.get(IN01Enum.MATERIAL_NUMBER)]);
	        		in01OrderLine.setSalesOrderNumber(fields[positions.get(IN01Enum.SALES_ORDER_LINE_NUMBER)]);
	        		//in01OrderLine.setsLoc(fields[positions.get(IN01Enum.SLOCATION)]);
	        		//in01OrderLine.setSalesOrderLine(fields[positions.get(IN01Enum.SALES_ORDER_NUMBER)]);
	        		if ((count == 0)  && (orderTypeName.equalsIgnoreCase("NL"))) {
	        			orderDD.setCompanyLocation(companyLocation);
		        		orderDD.setOrderActionType(orderActionType);
	        			orderDD.setCompanyLocation(companyLocation);
		        		orderDD.setOrderActionType(orderActionType);
		        		orderDD.setOrderNumber(docType);
		        		orderListDD.add(orderDD);
		        		orderMeterial.setSalesOrderLineNumber(fields[positions.get(IN01Enum.LINE_NUMBER)]);
		        		count++;
	        		}
	        		orderMeterial.setMeterial(companyAssetParts);
	        		orderMeterial.setStatus(OrderStatusEnum.PENDING);
	        		orderMeterialList.add(orderMeterial);
        			
	        		if (count == 0 && !orderTypeName.equalsIgnoreCase("NL")) {
	        			orderDD.setCompanyLocation(companyLocation);
		        		orderDD.setOrderActionType(orderActionType);
	        			orderDD.setCompanyLocation(companyLocation);
		        		orderDD.setOrderActionType(orderActionType);
	        			orderSO.setCompanyLocation(companyLocation);
	        			orderSO.setOrderActionType(orderActionType);
	        			orderTypeSO.setOrderTypeName("SO");
	        			orderSO.setOrderType(orderTypeSO);
	        			String salesOrderNumber = fields[positions.get(IN01Enum.SALES_ORDER_NUMBER)];
	        			orderSO.setOrderNumber(fields[positions.get(IN01Enum.SALES_ORDER_NUMBER)]);
		        		orderDD.setOrderNumber(docType);
		        		orderMeterial.setOrder(orderDD);
		        		
		        		if (orderTypeName.equalsIgnoreCase("LF")) {
		        			salesOrderEntity = orderRepo.findByOrderNumber(salesOrderNumber);
		        			if(salesOrderEntity == null) {
		        				throw new EntityNotFoundException("Sales order number " + salesOrderNumber + " is not present in DB.");
		        			}
		        		}
		        		salesOrderEntity = orderRepo.findByOrderNumber(salesOrderNumber);
		        		if (fileType.equalsIgnoreCase(INSTALL) && orderTypeName.equalsIgnoreCase("Z1NL")) {
		        			orderTypeSTO.setOrderTypeName("STO");
		        			orderSTO.setOrderType(orderTypeSTO);
		        			orderSTO.setOrderNumber(fields[positions.get(IN01Enum.STO)]);
		        			orderSTO.setCompanyLocation(companyLocation);
		        			orderSTO.setOrderActionType(orderActionType);
		        			orderListDD.add(orderDD);
		        			com.bcds.jpa.entity.Order eocoOrderEntity = orderRepo.findByOrderNumber(eocoNumber);
		        			String STONumber = fields[positions.get(IN01Enum.STO)];
		        			com.bcds.jpa.entity.Order STOOrderEntity = orderRepo.findByOrderNumber(STONumber);
		        			if (salesOrderEntity == null) {
		        				orderListDD.add(orderSO);
		        				orderRelationshipBeanDDToSO.setParenOrderId(orderDD);
			        			orderRelationshipBeanDDToSO.setChildOrderId(orderSO);
			        			orderRelationshipBeanSTOToSO.setParenOrderId(orderSTO);
			        			orderRelationshipBeanSTOToSO.setChildOrderId(orderSO);
			        			orderRelationshipList.add(orderRelationshipBeanDDToSO);
			        			orderRelationshipList.add(orderRelationshipBeanSTOToSO);
		        			}
		        			if (salesOrderEntity != null) {
		        				orderRelationshipBeanDDToSO.setParenOrderId(orderDD);
			        			orderRelationshipBeanDDToSO.setChildOrderId(entityToBeanClass.convertEntityToBean(salesOrderEntity));
			        			if (STOOrderEntity == null) {
			        			orderRelationshipBeanSTOToSO.setParenOrderId(orderSTO);
			        			orderRelationshipBeanSTOToSO.setChildOrderId(entityToBeanClass.convertEntityToBean(salesOrderEntity));
			        			orderRelationshipList.add(orderRelationshipBeanSTOToSO);
			        			}
			        			orderRelationshipList.add(orderRelationshipBeanDDToSO);
		        			}
		        			if (eocoOrderEntity != null) {
		        				if (salesOrderEntity == null) {
		        				orderRelationshipBeanSOToEOCO.setChildOrderId(orderSO);
			        			orderRelationshipBeanSOToEOCO.setParenOrderId(entityToBeanClass.convertEntityToBean(eocoOrderEntity));
			        			orderRelationshipList.add(orderRelationshipBeanSOToEOCO);
		        				}
		        			}
		        			if (eocoOrderEntity == null) {
		        				orderListDD.add(orderEOCO);
		        				orderRelationshipBeanSOToEOCO.setChildOrderId(orderSO);
			        			orderRelationshipBeanSOToEOCO.setParenOrderId(orderEOCO);
			        			orderRelationshipList.add(orderRelationshipBeanSOToEOCO);
		        			}
		        			if (STOOrderEntity == null) {
		        				orderListDD.add(orderSTO);
		        				orderRelationshipBeanDDToSTO.setParenOrderId(orderDD);
			        			orderRelationshipBeanDDToSTO.setChildOrderId(orderSTO);
		        			}
		        			if (STOOrderEntity != null) {
		        				orderRelationshipBeanDDToSTO.setParenOrderId(orderDD);
			        			orderRelationshipBeanDDToSTO.setChildOrderId(entityToBeanClass.convertEntityToBean(STOOrderEntity));
		        			}
			        		orderRelationshipList.add(orderRelationshipBeanDDToSTO);
		        		}
		        		
		        		count++;
	        		}
        		}
		    }
		    orderDD.setOrderMeterialList(orderMeterialList);
		    if (fileType.equalsIgnoreCase(INSTALL) && orderTypeName.equalsIgnoreCase("LF")) {
//    			List<Order> orderList = orderService.getAllDDsNotAssociatedWithDNS();
    			/**
    			 * new method getAllDDNotAssociatedWithDN for getting order faster
    			 * 
    			 * @author jason.chen
    			 */
		    	List<Order> orderList = orderService.getAllDDNotAssociatedWithDN();
    			int materialListCount = 0;
//		    	Map<String, Double> allDDMaterialList = new HashMap<String, Double>();
    			for (Order orders: orderList) {
    				String salesOrderNumberReturned = null;
    				List<OrderRelationshipEntity> orderRelationshipEntityList = orderRelationshipRepo.findByParentOrderId(beanToEntityClass.convertBeanToEntity(orders));
					for (OrderRelationshipEntity orderRelationshipEntity : orderRelationshipEntityList) {
						if (orderRelationshipEntity.getChildOrderId().getOrderType().getOrderTypeName().equalsIgnoreCase("SO")) {
							salesOrderNumberReturned = orderRepo.getOne(orderRelationshipEntity.getChildOrderId().getId()).getOrderNumber();
						}
					}
					if (salesOrderNumberLF.equalsIgnoreCase(salesOrderNumberReturned) ) {
						materialListCount = materialListCount + orders.getOrderMeterialList().size();
						// go through each material and merge if same
//						for(OrderMeterialList oml: orders.getOrderMeterialList()){
//							if(allDDMaterialList.get(oml.getMeterial().getPartName())!=null){
//								allDDMaterialList.put(oml.getMeterial().getPartName(), allDDMaterialList.get(oml.getMeterial().getPartName()) + oml.getQuantity());
//							} else {
//								allDDMaterialList.put(oml.getMeterial().getPartName(), oml.getQuantity());
//							}
//						}
					}
					/*if (salesOrderNumberLF.equalsIgnoreCase(salesOrderNumberReturned)) {
						if(orders.getOrderMeterialList().size() == orderDD.getOrderMeterialList().size()) {
							orderRelationshipBeanDDToDN.setParenOrderId(orders);
							orderRelationshipBeanDDToDN.setChildOrderId(orderDD);
							orderRelationshipList.add(orderRelationshipBeanDDToDN);
						}
					}*/
				}
    			
    			// check if each material's quantity is matched
//				if(orderDD.getOrderMeterialList().size() == allDDMaterialList.size()){
//					for(OrderMeterialList oml: orderDD.getOrderMeterialList()){
//						if(allDDMaterialList.get(oml.getMeterial().getPartName()) == null || oml.getQuantity().intValue() != allDDMaterialList.get(oml.getMeterial().getPartName()).intValue()){
//							throw new EntityNotFoundException("Material list does not match, other sto-dlv could just be completed.");
//						}
//					}
//				} else {
//					throw new EntityNotFoundException("Material list does not match, other sto-dlv could just be completed.");
//				}
    			
    			if(orderDD.getOrderMeterialList().size() == materialListCount) {
	    			for (Order newOrderBean: orderList) {
	    				String salesOrderNumberReturned = null;
	    				List<OrderRelationshipEntity> orderRelationshipEntityList = orderRelationshipRepo.findByParentOrderId(beanToEntityClass.convertBeanToEntity(newOrderBean));
						for (OrderRelationshipEntity orderRelationshipEntity : orderRelationshipEntityList) {
							if (orderRelationshipEntity.getChildOrderId().getOrderType().getOrderTypeName().equalsIgnoreCase("SO")) {
								salesOrderNumberReturned = orderRepo.getOne(orderRelationshipEntity.getChildOrderId().getId()).getOrderNumber();
							}
						}
						if (salesOrderNumberLF.equalsIgnoreCase(salesOrderNumberReturned)) {
//							if(orderDD.getOrderMeterialList().size() == materialListCount) {
								OrderRelationshipBean orderRelationshipDDToDN = new OrderRelationshipBean();
								orderRelationshipDDToDN.setParenOrderId(newOrderBean);
								orderRelationshipDDToDN.setChildOrderId(orderDD);
								orderRelationshipList.add(orderRelationshipDDToDN);
//							} else {
//								throw new EntityNotFoundException("Material list does not match, other sto-dlv could just be completed.");
//							}
						}
	    			}
	    			orderListDD.add(orderDD);
    			} else {
					throw new EntityNotFoundException("Material list does not match, other sto-dlv could just be completed.");
				}
    		}
		    orderDDBean.setOrderListDD(orderListDD);
		    orderDDBean.setOrderRelationshipList(orderRelationshipList);
		    in01OrderLine.setOrderDDBean(orderDDBean);
    		in01OrderLine.setTag(fileType);
    		in01OrderLines.add(in01OrderLine);
    		in01.setIn01OrderHeader(in01OrderHeader);
    		in01.setIn01OrderLines(in01OrderLines);
		}catch(FileNotFoundException fnfe){
			//file should always be there			
			logger.error(fnfe.getMessage());
		}finally{
			if (scanner != null)
				scanner.close();
			if (bf != null){
				try{
					bf.close();
				}catch(IOException ioe){
					logger.error("BufferedReader not closed" + ioe.getMessage());
				}
			}
			if (fr != null){
				try{
					fr.close();
				}catch(IOException ioe){
					logger.error("FileReader not closed" + ioe.getMessage());
				}
			}				
		}	        
	return in01;
	}
	
	
	public static void main (String[] args){
			try {
			@SuppressWarnings("resource")
			ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
			 SchedulerFactoryBean schedulerFactoryBean = new SchedulerFactoryBean();
			 SimpleTrigger trigger = context.getBean("in01JobTrigger", SimpleTrigger.class);
			 JobDetail jobDetail =  context.getBean("in01JobBean", JobDetail.class);
			 schedulerFactoryBean.setJobDetails(jobDetail);
			 schedulerFactoryBean.setTriggers(trigger);
			 Scheduler scheduler= (Scheduler) context.getBean("schedulerBean");		 
			 scheduler.scheduleJob(jobDetail, trigger);
			} catch (Exception  e) {
				e.printStackTrace();
			}
	}
	
	private void sendEmailNotification(String fileName,String message){
		try {
			sendEmailWithAttachment(fileName + " failed", message, new File(fileName));
		} catch (BCDSEmailException e) {
			logger.error("Email failed to send to recipients: " + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
