package com.bcds.jobs;
/**
 * 
 */

import java.io.File;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.bcds.file.helper.IN01Enum;
import com.bcds.jobs.beans.StockVarianceBean;
import com.bcds.jpa.entity.CompanyLocation;
import com.bcds.service.iface.CompanyLocationServiceInterface;
import com.bcds.service.iface.StockVarianceServiceInterface;
import com.bcds.util.DateUtil;
import com.bcds.util.EmailUtil;

/**
 * @author jason.chen
 *
 */
@Component
public class CronScheduleTask {

	/** The log. */
	private Logger log = Logger.getLogger(getClass());
	
	@Value( "${r01i.filename}" )
	protected String filename;

	@Value( "${r01i.backup.folder}" )
	protected String backupFolder;
	
	@Value( "${serverType}" )
	protected String serverType;
	
	@Autowired
	private StockVarianceServiceInterface stockVarianceService;
	
	@Autowired
	private EmailUtil emailUtil;
	
	@Autowired
	private CompanyLocationServiceInterface companyLocationServiceInterface;
	
	public static final String VIRTUAL_RDC_1W00 = "1W00";
	
	public static final String INSTALL_PLANT_P01 = "P01";
	
	public static final String INSTALL_PLANT_P22 = "P22";
	
	public void doR01JobsForInstralls() {
		try {
			log.info("------- Start LicenseeExpireCheckTask ---------");
			for(CompanyLocation companyLocation: companyLocationServiceInterface.findByTagAndIsRootLocation(IN01Enum.INSTALL.toString(), true)){
				if (!companyLocation.getUpdateFlag().equals("D")
						&& !companyLocation.getBarcode().equals(INSTALL_PLANT_P01)
						&& !companyLocation.getBarcode().equals(INSTALL_PLANT_P22)
						&& !companyLocation.getBarcode().equals(VIRTUAL_RDC_1W00)) {
	
					List<StockVarianceBean> stockVarianceList = stockVarianceService.getStockVarianceListForInstallByLocationId(companyLocation.getId(), companyLocation.getLocationName());
					
					File file = stockVarianceService.generateSapStockVarianceReportForInstall(stockVarianceList);
					
					if(file != null && file.exists()){
						emailUtil.sendMail(serverType+"_O3_StockVarianceReportForInstall_"+DateUtil.getSimpleDateFormat("yyyyMMdd"), "A stocktake for install plant has been done at " + companyLocation.getLocationName() + ".\nPlease find the report in the attachment.", file);
					} else {
						log.error("Error happened during install stocktake report.");
					}
				}
			}
			log.info("------- End LicenseeExpireCheckTask ---------");
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}
}
