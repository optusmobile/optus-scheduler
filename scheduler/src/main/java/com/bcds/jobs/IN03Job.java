package com.bcds.jobs;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import com.bcds.file.helper.CSVUtil;
import com.bcds.file.helper.GeneralHelper;
import com.bcds.ftp.util.FileUtil;
import com.bcds.jobs.beans.AssetBean;
import com.bcds.jobs.beans.AssetMovementInfoBean;
import com.bcds.jobs.exception.BCDSEmailException;
import com.bcds.jobs.exception.BCDSFTPException;
import com.bcds.jpa.entity.InterfaceError;
import com.bcds.jpa.entity.OrderMaterialList;
import com.bcds.jpa.entity.OrderRelationshipEntity;
import com.bcds.jpa.entity.UserLocation;
import com.bcds.jpa.model.in03.IN03;
import com.bcds.jpa.model.in03.IN03Header;
import com.bcds.jpa.model.in03.IN03Line;
import com.bcds.jpa.repo.AssetStatusRepo;
import com.bcds.jpa.repo.OrderRelationshopRepo;
import com.bcds.jpa.repo.OrderRepository;
import com.bcds.jpa.repo.UserLocationRepo;
import com.bcds.util.DateUtil;
import au.com.bytecode.opencsv.CSVWriter;

/*
 * IN03 - Asset Movement transactions @Intrack to be sent to SAP
 * 
 * Date				Change description
 * -----       	   ---------------------
 * 
 * 3 May 2016		Received latest format and changed (again) accordingly. Has 'H' for header and 'L' for line.
 * 
 * 9 May 2016		Changed codes to cater for multiple output files. * 					
 * 					Tested OK as of 5pm 9 May 2016.
 * 
 * 	12 May 2016		Added Error Folders. Changed the FTP function and retested ok!!	
 * 					Completed email function. Send to recipients when error occurs.
 * 
 *  13 May 2016		Improved the error handling codes.
 * 
 */

public class IN03Job extends AbstractOutgoingJob {
		
	final static Logger logger = Logger.getLogger(IN03Job.class);
	
	@Autowired
	AssetStatusRepo assetStatusRepo;
	
	@Autowired
	OrderRepository orderRepo;
	
	@Autowired
	OrderRelationshopRepo orderRelationshopRepo;
	
	@Autowired
	private UserLocationRepo userLocationRepo;

	@Value("${in03.filename}")
	protected String filename;

	@Value("${in03.backup.folder}")
	protected String backupFolder;

	@Value( "${in03.source.folder}")
	protected String sourceFolder;
	
	@Override
	public void executeJob() {
		
		logger.info("----------- IN03 Job Started ---------");
		setFilename(this.filename);
		setBackupFolder(this.backupFolder);
		setSourceFolder(this.sourceFolder);
		prepareInterfaceFile();
		System.out.println("filename details::" + getFilename());
		// causing conflict with IN02 sftp file sending process
		 List<String> allIN03s = new ArrayList<String>();
		try {
			allIN03s = FileUtil.getSimilarBatchFiles(getSourceFolder(), getFilename());
		} catch (NoSuchFileException e) {
			//TODO Auto-generated catch block
			//e.printStackTrace();
		}
		for (String oneIN03 : allIN03s) {
			File file = new File(oneIN03);
			try {
				if(file.exists()) {
					executeFTP(file);
				}
			} catch (BCDSFTPException ftpe){
				logger.error("FTP failed to send file to remote server: " + ftpe.getMessage());
				interfaceErrorRepo.save(new InterfaceError(GeneralHelper.IN03, file.getName(), new Date(), 
						GeneralHelper.FTP_SERVER, "FTP failed to execute.", GeneralHelper.PROCESSING_ERROR, 
						getSendingErrorFolder()));
				try{
					sendEmailWithAttachment("OFT failure", "FTP failed, please contect OFT team.", file);
				} catch (BCDSEmailException ee){
					logger.error("Email failed to send to recipients: " + ee.getMessage());
					interfaceErrorRepo.save(new InterfaceError(GeneralHelper.IN03, file.getName(), new Date(), 
							GeneralHelper.SMTP_SERVER, "Email failed to execute.", GeneralHelper.PROCESSING_ERROR, 
							getSendingErrorFolder()));
				}
				doBackup(file, getSendingErrorFolder());	
				continue;
			} catch(Exception ex) {
				logger.error("FTP Exception : " + ex.getMessage());
				interfaceErrorRepo.save(new InterfaceError(GeneralHelper.IN03, file.getName(), new Date(), 
						GeneralHelper.SMTP_SERVER, "FTP Exception", GeneralHelper.PROCESSING_ERROR, 
						getSendingErrorFolder()));
				doBackup(file, getSendingErrorFolder());
				continue;
			}
			doBackup(file, getBackupFolder());	
		}
		
	}

	
	@Override
	public void prepareInterfaceFile() {
		CSVWriter writer = null;
		FileWriter fileWriter = null;
		List<IN03> in03s = getIn03ReturnRecords();
		int count = 1;
		for (IN03 in03 : in03s) {
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd-HHmmss-SSS");
			System.out.println("format.format(new Date():: " + format.format(new Date()));
			String sample1 = (format.format(new Date()).substring(0, 10));
			Integer dateLength = format.format(new Date()).length();
			int lastIndex = 28;
			if (dateLength<29){
				lastIndex=dateLength;
			}
			String sample2 = sample1.replaceAll("-", "");
			File in03File = new File(getSourceFolder()+"O3"+"_"+"PURM" + format.format(new Date()) +".csv");
			logger.info("Output file name  for the Job IN03 ::" + in03File.getName());
			try {
				fileWriter = new FileWriter(in03File);
				writer = new CSVWriter(fileWriter, '|', CSVWriter.NO_QUOTE_CHARACTER);
				DynaBean bean = CSVUtil.prepareIN03CSVLinePosition(in03);
				List<String[]> fileLines = new ArrayList<String[]>();
				List<?> headerList = (ArrayList<?>)bean.get(IN03Header.lineType);
				fileLines.add(headerList.toArray(new String[headerList.size()]));
				for (int i=0; i<in03.getIn03Lines().size(); i++){
					List<?> lineList = (ArrayList<?>)bean.get(IN03Line.lineType+i);
					fileLines.add(lineList.toArray(new String[lineList.size()]));
				}
				//fileLines.add(lineList.toArray(new String[lineList.size()]));
				writer.writeAll(fileLines);
				// delay 1 milliseconds to prevent file name overlapping 
				TimeUnit.MILLISECONDS.sleep(30);
			} catch (InterruptedException e) {
				logger.error(e, e);
			} catch (IOException ioe) {
				if(in03File.exists()){
					interfaceErrorRepo.save(new InterfaceError(GeneralHelper.IN03, in03File.getName(), new Date(), GeneralHelper.FILE_IO, 
							ioe.getMessage(), GeneralHelper.PROCESSING_ERROR, getProcessingErrorFolder()));
					doBackup(in03File, getProcessingErrorFolder());
				} else {
					interfaceErrorRepo.save(new InterfaceError(GeneralHelper.IN03, in03File.getName(), new Date(), GeneralHelper.FILE_IO, 
							ioe.getMessage(), GeneralHelper.PROCESSING_ERROR, null));
					doBackup(in03File, getProcessingErrorFolder());
				}
			}finally{
				try{
					if (writer != null)
						writer.close();
					if (fileWriter != null)
						fileWriter.close();
				}catch (Exception ex){
					logger.error("Error closing IN03 output file.");
				}
			}
			count++;
		}		
	}
	
	private List<IN03> getIn03ReturnRecords()  {
		List<AssetMovementInfoBean> assetMovementInfoList = assetService.findAllFinishedAssets();
		logger.info("AssetMovementInfo size: " + assetMovementInfoList.size());
		List<IN03> in03s = new ArrayList<IN03>();
		for (AssetMovementInfoBean assetMovementInfoBean : assetMovementInfoList){
			IN03 in03 = new IN03();
			IN03Header in03Header = new IN03Header();
			in03Header.setInterfaceType(GeneralHelper.IN03);
			List<IN03Line> in03LineList = new ArrayList<IN03Line>();
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
			in03Header.setPostingDate(DateUtil.getLocalDateInSimpleDateFormat("yyyyMMdd",new Date()));
			in03Header.setDocumentDate(DateUtil.getLocalDateInSimpleDateFormat("yyyyMMdd",assetMovementInfoBean.getMovementDate()));
			in03Header.setDeliveryNumber("");
			in03Header.setHeaderText("");
			in03Header.setGmCode("04");
			String plantType = assetMovementInfoBean.getPlantType();
			if (plantType != null && plantType.equalsIgnoreCase("INSTALL")) {
				String workOrderNumber = assetMovementInfoBean.getWorkOrderNumber();
				com.bcds.jpa.entity.Order orderEntity = orderRepo.findByOrderNumber(workOrderNumber);
				List<OrderRelationshipEntity> orderRelationshipEntity = orderRelationshopRepo.findByChildOrderId(orderEntity);
				List<OrderRelationshipEntity> listOfDDs  = orderRelationshopRepo.findByParentOrderId(orderRelationshipEntity.get(0).getParentOrderId());
				List<OrderMaterialList> newOrderMaterialLis = new ArrayList<OrderMaterialList>();
				for (OrderRelationshipEntity orderRelEntity: listOfDDs) {
					newOrderMaterialLis.addAll(orderRelEntity.getParentOrderId().getOrderMeterialList());
				}
				for (OrderMaterialList orderMaterialList1:newOrderMaterialLis) {
					
				}
				String salesOrderNumber = "";
				for (OrderRelationshipEntity orderRelEntity: listOfDDs) {
					if (orderRelEntity.getChildOrderId().getOrderType().getOrderTypeName().equalsIgnoreCase("SO")) {
						salesOrderNumber = orderRepo.findByOrderNumber(orderRelEntity.getChildOrderId().getOrderNumber()).getOrderNumber();
					}
				}
				List<OrderMaterialList> orderMaterialList = orderEntity.getOrderMeterialList();
				for (OrderMaterialList orderMaterial:orderMaterialList) {
					IN03Line in03LineSingle = new IN03Line();
					in03LineSingle.setInterfaceType(GeneralHelper.IN03);
					AssetBean assetBean = assetMovementInfoBean.getAsset();
					if(assetBean.getStatus().equalsIgnoreCase("Q")) {
						in03LineSingle.setMovementType("323");
					}else {
						in03LineSingle.setMovementType("311");
					}
					in03LineSingle.setMaterialNumber(orderMaterial.getCompanyAssetParts().getPartName());
					in03LineSingle.setQuantity(orderMaterial.getQuantity().toString());
					String fullLocationName ="";
					if (assetMovementInfoBean.getDestinationLocation() != null &&  
							assetMovementInfoBean.getDestinationLocation().getParentLocation() != null) {
						 fullLocationName = companyLocationRepository.getOne(assetMovementInfoBean.getDestinationLocation().getParentLocation()).getLocationName();
						}
					in03LineSingle.setPlant(fullLocationName);
					in03LineSingle.setSourceLocation(assetMovementInfoBean.getSourceLocation().getLocationName());
					in03LineSingle.setDestinationLocation(assetMovementInfoBean.getDestinationLocation().getLocationName());
					in03LineSingle.setBatchName(orderMaterial.getBatchNumber());
					in03LineSingle.setUom("EA");
					in03LineSingle.setSalesOrderLineNumber(orderMaterial.getSalesOrderLineNumber());
					//List<OrderRelationshipEntity> orderRelationshipEntity = orderRelationshopRepo.findByChildOrderId(orderMaterial.getOrder());
					//List<OrderRelationshipEntity> listOfDDs  = orderRelationshopRepo.findByParentOrderId(orderRelationshipEntity.get(0).getParentOrderId());
					in03LineSingle.setSalesOrderNumber(salesOrderNumber);
					if(assetMovementInfoBean.getDestinationLocation().getLocationType().equalsIgnoreCase("VS")){
						// populate engineer plant and sloc
						List<UserLocation> userLocationMappingList = userLocationRepo.findByCompanyLocation(companyLocationRepository.getOne(assetMovementInfoBean.getDestinationLocation().getLocationId()));
						if(userLocationMappingList.size()>0){
							for(UserLocation userLocationMapping: userLocationRepo.findByUser(userLocationMappingList.get(0).getUser())){
								if(userLocationMapping.getCompanyLocation().getLocationType().equalsIgnoreCase("ENG")){
									in03LineSingle.setEngineersSloc(userLocationMapping.getCompanyLocation().getLocationName());
									in03LineSingle.setEngineersPlant(companyLocationRepository.getOne(userLocationMapping.getCompanyLocation().getParentLocationId()).getLocationName());
									break;
								}
							}
						}
					}
					if (!listContainsItems(in03LineList,in03LineSingle)) {
						in03LineList.add(in03LineSingle);
					}
				}
			}else 
			{
				IN03Line in03Line = new IN03Line();
				in03Line.setInterfaceType(GeneralHelper.IN03);
				AssetBean assetBean = assetMovementInfoBean.getAsset();
				if(assetBean.getStatus().equalsIgnoreCase("Q")) {
					in03Line.setMovementType("323");
				}else if(assetMovementInfoBean.getSourceLocation().getParentLocation().equals(assetMovementInfoBean.getDestinationLocation().getParentLocation())){
					in03Line.setMovementType("311");
				} else {
					in03Line.setMovementType("301");
					in03Line.setEngineersPlant(companyLocationRepository.getOne(assetMovementInfoBean.getDestinationLocation().getParentLocation()).getLocationName());
					in03Line.setEngineersSloc(assetMovementInfoBean.getDestinationLocation().getLocationName());
				}
				in03Line.setMaterialNumber(assetMovementInfoBean.getAsset().getCompanyAssetParts().getPartName());
				in03Line.setQuantity("1");
				String fullLocationName ="";
				//old code
//				if (assetMovementInfoBean.getDestinationLocation() != null &&  
//						assetMovementInfoBean.getDestinationLocation().getParentLocation() != null) {
//					 fullLocationName = companyLocationRepository.getOne(assetMovementInfoBean.getDestinationLocation().getParentLocation()).getLocationName();
//					}
				// new code added by Jason, reason CR2 changes for different RDC
				if (assetMovementInfoBean.getSourceLocation() != null &&  
						assetMovementInfoBean.getSourceLocation().getParentLocation() != null) {
					 fullLocationName = companyLocationRepository.getOne(assetMovementInfoBean.getSourceLocation().getParentLocation()).getLocationName();
					}
				in03Line.setPlant(fullLocationName);
				in03Line.setSourceLocation(assetMovementInfoBean.getSourceLocation().getLocationName());
				in03Line.setDestinationLocation(assetMovementInfoBean.getDestinationLocation().getLocationName());
				in03Line.setBatchName(assetMovementInfoBean.getAsset().getCompanyAssetParts().getBatchNumber());
				in03Line.setUom("EA");
				in03LineList.add(in03Line);
			}
			in03.setIn03Header(in03Header);
			in03.setIn03Lines(in03LineList);
			in03s.add(in03);
			assetService.updateAssetMovementStatusToClosed(assetMovementInfoBean);
		}
		return in03s;
	}
	
	
	public boolean listContainsItems(List<IN03Line> in03LineList, IN03Line in03LineSingle){
		for(IN03Line in03Line: in03LineList) {
			if (in03Line.getMaterialNumber().equalsIgnoreCase(in03LineSingle.getMaterialNumber())
					&& in03Line.getQuantity().equalsIgnoreCase(in03LineSingle.getQuantity())
					&& in03Line.getSourceLocation().equalsIgnoreCase(in03LineSingle.getSourceLocation())
					&& in03Line.getDestinationLocation().equalsIgnoreCase(in03LineSingle.getDestinationLocation())
					&& in03Line.getPlant().equalsIgnoreCase(in03LineSingle.getPlant())
					&& in03Line.getSalesOrderNumber().equalsIgnoreCase(in03LineSingle.getSalesOrderNumber())
					&& in03Line.getSalesOrderLineNumber().equalsIgnoreCase(in03LineSingle.getSalesOrderLineNumber())) {
				return true;
			}
		}
		return false;
	}
	
	public static void main (String[] args) {
	 try {
		 @SuppressWarnings("resource")
		 ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		 SchedulerFactoryBean schedulerFactoryBean = new SchedulerFactoryBean();
		 Trigger trigger = context.getBean("in03JobTrigger", Trigger.class);
		 JobDetail jobDetail =  context.getBean("in03JobBean", JobDetail.class);
		 schedulerFactoryBean.setJobDetails(jobDetail);
		 schedulerFactoryBean.setTriggers(trigger);
		 Scheduler scheduler= (Scheduler) context.getBean("schedulerBean");		 
		 scheduler.scheduleJob(jobDetail, trigger);
		 //scheduler.
		} catch (Exception  e) {
			e.printStackTrace();
		}
	}
}
