package com.bcds.task;

import java.io.File;
import java.util.List;
import java.util.Scanner;

import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import com.bcds.file.mapping.*;

public class TaskManagerBase {
	
	private Resource resource;

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	
	public static FlatFileItemReader<TestFileType1> reader(){
		FlatFileItemReader<TestFileType1> reader = new FlatFileItemReader<TestFileType1>();
		reader.setResource(new FileSystemResource("c:\\data.csv"));
		reader.setLineMapper(new DefaultLineMapper<TestFileType1>() {{
			setLineTokenizer(new DelimitedLineTokenizer() {{
				setIncludedFields(new int[]{1,3});
				setNames(new String[]{"id", "timestamp", "assetId", "quantity"});
			}});
			setFieldSetMapper(new BeanWrapperFieldSetMapper<TestFileType1>() {{
				setTargetType(TestFileType1.class);
			}});
		}});
		return reader;
	}
	
	
	/**
	public static List<TestFileType1> readFile(){
		
		FileSystemResource resource = new FileSystemResource("c:\\data.csv");
		Scanner scanner = new Scanner(resource.getInputStream());
		String line = scanner.nextLine();
		scanner.close();

		FlatFileItemReader<TestFileType1> itemReader = new FlatFileItemReader<TestFileType1>();
		itemReader.setResource(resource);

		// DelimitedLineTokenizer defaults to comma as its delimiter
		DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer();
		tokenizer.setNames(line.split(","));
		tokenizer.setStrict(false);

		DefaultLineMapper<TestFileType1> lineMapper = new DefaultLineMapper<TestFileType1>();
		lineMapper.setLineTokenizer(tokenizer);
		lineMapper.setFieldSetMapper(StoreFieldSetMapper.INSTANCE);
		itemReader.setLineMapper(lineMapper);
		itemReader.setRecordSeparatorPolicy(new DefaultRecordSeparatorPolicy());
		itemReader.setLinesToSkip(1);
		itemReader.open(new ExecutionContext());

		List<Store> stores = new ArrayList<>();
		Store store = null;

		do {

			store = itemReader.read();

			if (store != null) {
				stores.add(store);
			}

		} while (store != null);

		return stores;
		
	}**/
	

}
