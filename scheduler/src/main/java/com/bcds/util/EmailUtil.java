package com.bcds.util;

import java.io.File;
import java.util.ArrayList;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailParseException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;

import com.bcds.jobs.exception.BCDSEmailException;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
public class EmailUtil {

	@Autowired
	BCDSMailSender mailSender;
			
	public void sendMail(String subject, String message, File attachment) throws BCDSEmailException{

		/**
		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom(mailSender.getFrom());		
		message.setTo(mailSender.getTo().split(","));
		message.setSubject(subject);
		message.setText(msg);
		**/
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		
		try{
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
				
			helper.setFrom(mailSender.getFrom());
			helper.setTo(mailSender.getTo().split(","));
			helper.setSubject(subject);
			helper.setText(message);
			if(attachment!= null && attachment.exists()){
				helper.addAttachment(attachment.getName(), attachment);
			}
			mailSender.send(mimeMessage);
		}catch (Exception e) {		
			throw new BCDSEmailException(e);
		}
		     	
	}
	
	
	public static void main (String[] args){
		ConfigurableApplicationContext  context = 
	             new ClassPathXmlApplicationContext("applicationContext_email.xml");
		EmailUtil email = (EmailUtil) context.getBean("emailUtil");
		File file = new File("c:\\data.csv");
		try{
			email.sendMail("Test Email", "Yo Yo", file);
		}catch (BCDSEmailException ee){
			ee.printStackTrace();
		}
	}
}
