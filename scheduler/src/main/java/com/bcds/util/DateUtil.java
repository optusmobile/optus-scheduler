package com.bcds.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateUtil {

	public static String getSimpleDateFormat(String dateFormat){
		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		return format.format(new Date());
	}
	
	public static String getDateFormat(Date date, String dateFormat){
		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		return format.format(date);
	}
	
	public static Date getUtcToSydneyDate(Date date) {
		Calendar SydneyDate = Calendar.getInstance();
		TimeZone timeSyd = TimeZone.getTimeZone("Australia/Sydney");
		SydneyDate.setTimeInMillis(date.getTime() + timeSyd.getOffset(date.getTime()));
		return SydneyDate.getTime();
	}
	
	public static String getLocalDateInSimpleDateFormat(String dateFormat, Date date) {
		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		return format.format(getUtcToSydneyDate(date));
	}
	
	/*public static void main(String args[]) {
		System.out.println(" Sydney date time::" + DateUtil.getUtcToSydneyDate(new Date()));
		System.out.println(" Sydney date time in date format ::" + DateUtil.getLocalDateInSimpleDateFormat("yyyyMMdd", new Date()));
		
	}*/
}
