package com.bcds.file.helper;


public enum IN02Enum {
	
	EDIDC,
	E1MBGMCR,
	E1BP2017_GM_HEAD_01,
	E1BP2017_GM_ITEM_CREATE;

	public String toString(){
		switch (this){
			case EDIDC: return "EDIDC";
			case E1MBGMCR: return "E1MBGMCR";
			case E1BP2017_GM_HEAD_01: return "E1BP2017_GM_HEAD_01";
			case E1BP2017_GM_ITEM_CREATE: return "E1BP2017_GM_ITEM_CREATE";
			default: return "";
		}
	}
		
}
