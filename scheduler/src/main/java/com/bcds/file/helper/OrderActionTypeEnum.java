package com.bcds.file.helper;

public enum OrderActionTypeEnum {

	I975 (975),
	I02 (973),
	I03 (975);
	
	private final int orderActionCode;
	
	private OrderActionTypeEnum(int orderActionCode){
		this.orderActionCode = orderActionCode;
	}
	public int getOrderActionCode(){
		return this.orderActionCode;
	}
	
}
