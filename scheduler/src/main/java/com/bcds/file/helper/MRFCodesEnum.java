package com.bcds.file.helper;

public enum MRFCodesEnum {
	A1,
	A2,
	A3,
	A4,
	A5,
	A6,
	A7;
	
	@Override
	public  String toString(){
		switch (this){
			case A1: return "A.1";
			case A2: return "A.2";
			case A3: return "A.3";
			case A4: return "A.4";
			case A5: return "A.5";
			case A6: return "A.6";
			case A7: return "A.7";
			default: return "";
		}
	}

}
