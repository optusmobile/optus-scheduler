package com.bcds.file.helper;

public enum AssetConditionEnum {

	NEW,
	USED,
	USEDSWAP,
	VENDOR,
	INTRANSIT
}
