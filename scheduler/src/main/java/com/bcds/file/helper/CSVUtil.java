package com.bcds.file.helper;

import java.util.List;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.LazyDynaMap;
import org.codehaus.plexus.util.StringUtils;
import com.bcds.jpa.model.in02.IN02;
import com.bcds.jpa.model.in02.IN02Header;
import com.bcds.jpa.model.in02.IN02Line;
import com.bcds.jpa.model.in03.IN03;
import com.bcds.jpa.model.in03.IN03Header;
import com.bcds.jpa.model.in03.IN03Line;
import com.bcds.jpa.model.in04.IN04;
import com.bcds.jpa.model.in04.IN04Header;
import com.bcds.jpa.model.in04.IN04Line;
import com.bcds.jpa.model.in09.IN09;
import com.bcds.jpa.model.in09.IN09Header;
import com.bcds.jpa.model.in09.IN09Line;
import com.bcds.jpa.model.in11.IN11;
import com.bcds.jpa.model.in11.IN11Header;
import com.bcds.jpa.model.in11.IN11Line;

public class CSVUtil {

	public static DynaBean prepareIN02CSVLinePosition(IN02 in02){
		DynaBean dynaBean = new LazyDynaMap();
		IN02Header header = in02.getIn02Header();
		List<IN02Line> listOfLines = in02.getIn02Line();
		
		dynaBean.set(IN02Header.lineType, 0, header.getInterfaceType());
		dynaBean.set(IN02Header.lineType, 1, IN02Header.lineType);
		dynaBean.set(IN02Header.lineType, 2, header.getDocumentDate());
		dynaBean.set(IN02Header.lineType, 3, header.getPostingDate());
		dynaBean.set(IN02Header.lineType, 4, header.getDeliveryNumber());
		dynaBean.set(IN02Header.lineType, 5, header.getHeaderText());
		dynaBean.set(IN02Header.lineType, 6, header.getGmCode());
		/*dynaBean.set(IN02Header.lineType, 7, "");
		dynaBean.set(IN02Header.lineType, 8, "");
		dynaBean.set(IN02Header.lineType, 9, "");
		*/
		for (int i=0; i<listOfLines.size(); i++){
		IN02Line line = listOfLines.get(i);
		dynaBean.set(IN02Line.lineType+i, 0, line.getInterfaceType());
		dynaBean.set(IN02Line.lineType+i, 1, IN02Line.lineType);
		dynaBean.set(IN02Line.lineType+i, 2, line.getMovementType());
		dynaBean.set(IN02Line.lineType+i, 3, StringUtils.leftPad(line.getMaterialNumber(), 18, "0"));
		dynaBean.set(IN02Line.lineType+i, 4, line.getQuantity());
		dynaBean.set(IN02Line.lineType+i, 5, line.getPlant());
		dynaBean.set(IN02Line.lineType+i, 6, line.getSourceLocation());
		dynaBean.set(IN02Line.lineType+i, 7, line.getDestinationLocation());
		dynaBean.set(IN02Line.lineType+i, 8, line.getBatchNumber());
		dynaBean.set(IN02Line.lineType+i, 9, line.getUom());
		dynaBean.set(IN02Line.lineType+i, 10,line.getVisitingEngPlant());
		dynaBean.set(IN02Line.lineType+i, 11, line.getVisitingEngSloc());
		dynaBean.set(IN02Line.lineType+i, 12, line.getSalesOrderNum());
		dynaBean.set(IN02Line.lineType+i, 13, line.getSalesOrderLineNum());
		}
		return dynaBean;
	}
	
	public static DynaBean prepareIN04CSVLinePosition(IN04 in04){
		DynaBean dynaBean = new LazyDynaMap();
		IN04Header header = in04.getIn04Header();
		List<IN04Line> listOfLines = in04.getIn04Line();
		
		dynaBean.set(IN04Header.lineType, 0, header.getInterfaceType());
		dynaBean.set(IN04Header.lineType, 1, IN04Header.lineType);
		dynaBean.set(IN04Header.lineType, 2, header.getDocumentDate());
		dynaBean.set(IN04Header.lineType, 3, header.getPostingDate());
		dynaBean.set(IN04Header.lineType, 4, header.getDeliveryNumber());
		dynaBean.set(IN04Header.lineType, 5, header.getHeaderText());
		dynaBean.set(IN04Header.lineType, 6, header.getGmCode());
		/*dynaBean.set(IN02Header.lineType, 7, "");
		dynaBean.set(IN02Header.lineType, 8, "");
		dynaBean.set(IN02Header.lineType, 9, "");
		*/
		for (int i=0; i<listOfLines.size(); i++){
		IN04Line line = listOfLines.get(i);
		dynaBean.set(IN04Line.lineType+i, 0, line.getInterfaceType());
		dynaBean.set(IN04Line.lineType+i, 1, IN04Line.lineType);
		dynaBean.set(IN04Line.lineType+i, 2, line.getMovementType());
		dynaBean.set(IN04Line.lineType+i, 3, StringUtils.leftPad(line.getMaterialNumber(), 18, "0"));
		dynaBean.set(IN04Line.lineType+i, 4, line.getQuantity());
		dynaBean.set(IN04Line.lineType+i, 5, line.getPlant());
		dynaBean.set(IN04Line.lineType+i, 6, line.getSourceLocation());
		dynaBean.set(IN04Line.lineType+i, 7, line.getDestinationLocation());
		dynaBean.set(IN04Line.lineType+i, 8, line.getBatchNumber());
		dynaBean.set(IN04Line.lineType+i, 9, line.getUom());
		//dynaBean.set(IN04Line.lineType+i, 10, line.getSalesOrderNum());
		dynaBean.set(IN04Line.lineType+i, 10, line.getSalesOrderLineNum());
		}
		return dynaBean;
	}
	
	public static DynaBean prepareIN03CSVLinePosition(IN03 in03){
		DynaBean dynaBean = new LazyDynaMap();
		IN03Header header = in03.getIn03Header();
		List<IN03Line> listOfLines = in03.getIn03Lines();
		
		dynaBean.set(IN03Header.lineType, 0, header.getInterfaceType());
		dynaBean.set(IN03Header.lineType, 1, IN03Header.lineType);
		dynaBean.set(IN03Header.lineType, 2, header.getDocumentDate());
		dynaBean.set(IN03Header.lineType, 3, header.getPostingDate());
		dynaBean.set(IN03Header.lineType, 4, header.getDeliveryNumber());
		dynaBean.set(IN03Header.lineType, 5, header.getHeaderText());
		dynaBean.set(IN03Header.lineType, 6, header.getGmCode());
		/*dynaBean.set(IN03Header.lineType, 7, "");
		dynaBean.set(IN03Header.lineType, 8, "");
		dynaBean.set(IN03Header.lineType, 9, "");*/
		
		for (int i=0; i<listOfLines.size(); i++){
		IN03Line line = listOfLines.get(i);
		dynaBean.set(IN03Line.lineType+i, 0, line.getInterfaceType());
		dynaBean.set(IN03Line.lineType+i, 1, IN03Line.lineType);
		dynaBean.set(IN03Line.lineType+i, 2, line.getMovementType());
		dynaBean.set(IN03Line.lineType+i, 3, StringUtils.leftPad(line.getMaterialNumber(), 18, "0"));
		dynaBean.set(IN03Line.lineType+i, 4, line.getQuantity());
		dynaBean.set(IN03Line.lineType+i, 5, line.getPlant());
		dynaBean.set(IN03Line.lineType+i, 6, line.getSourceLocation());
		dynaBean.set(IN03Line.lineType+i, 7, line.getDestinationLocation());
		dynaBean.set(IN03Line.lineType+i, 8, line.getBatchName());
		dynaBean.set(IN03Line.lineType+i, 9, line.getUom());
		dynaBean.set(IN03Line.lineType+i, 10, line.getEngineersPlant());
		dynaBean.set(IN03Line.lineType+i, 11, line.getEngineersSloc());
		dynaBean.set(IN03Line.lineType+i, 12, line.getSalesOrderNumber());
		dynaBean.set(IN03Line.lineType+i, 13, line.getSalesOrderLineNumber());
		}
		return dynaBean;
	}
	
	public static DynaBean prepareIN09CSVLinePosition(IN09 in09){
		DynaBean dynaBean = new LazyDynaMap();
		IN09Header header = in09.getIn09Header();
		List<IN09Line> listOfLines = in09.getListOfLines();
		
		dynaBean.set(IN09Header.lineType, 0, header.getInterfaceType());
		dynaBean.set(IN09Header.lineType, 1, IN09Header.lineType);
		dynaBean.set(IN09Header.lineType, 2, header.getDocumentDate());
		dynaBean.set(IN09Header.lineType, 3, header.getPostingDate());
		dynaBean.set(IN09Header.lineType, 4, header.getDeliveryNumber());
		dynaBean.set(IN09Header.lineType, 5, header.getHeaderText());
		dynaBean.set(IN09Header.lineType, 6, header.getGmCode());
		/*dynaBean.set(IN03Header.lineType, 7, "");
		dynaBean.set(IN03Header.lineType, 8, "");
		dynaBean.set(IN03Header.lineType, 9, "");*/
		
		for (int i=0; i<listOfLines.size(); i++){
		IN09Line line = listOfLines.get(i);
		dynaBean.set(IN09Line.lineType+i, 0, line.getInterfaceType());
		dynaBean.set(IN09Line.lineType+i, 1, IN09Line.lineType);
		dynaBean.set(IN09Line.lineType+i, 2, line.getMovementType());
		dynaBean.set(IN09Line.lineType+i, 3, StringUtils.leftPad(line.getMaterialNumber(), 18, "0"));
		dynaBean.set(IN09Line.lineType+i, 4, line.getQuantity());
		dynaBean.set(IN09Line.lineType+i, 5, line.getPlant());
		dynaBean.set(IN09Line.lineType+i, 6, line.getSourceLocation());
		dynaBean.set(IN09Line.lineType+i, 7, line.getDestinationLocation());
		dynaBean.set(IN09Line.lineType+i, 8, line.getBatchName());
		dynaBean.set(IN09Line.lineType+i, 9, line.getUom());
		dynaBean.set(IN09Line.lineType+i, 10, line.getLineNumber());
		}
		return dynaBean;
	}
	

	public static DynaBean prepareIN11CSVLinePosition(IN11 in11) {
		DynaBean dynaBean = new LazyDynaMap();
		IN11Header header = in11.getIn11Header();
		List<IN11Line> lines = in11.getIn11Lines();
		
		dynaBean.set(IN11Header.lineType, 0, header.getInterfaceType());
		dynaBean.set(IN11Header.lineType, 1, IN11Header.lineType);
		dynaBean.set(IN11Header.lineType, 2, header.getPhysicalInventoryDocument());
		dynaBean.set(IN11Header.lineType, 3, header.getFiscalYear());
		dynaBean.set(IN11Header.lineType, 4, header.getDateOfLastCount());
		
		for (int i=0; i<lines.size(); i++){
			IN11Line line = lines.get(i);
			dynaBean.set(IN11Line.lineType+i, 0, line.getInterfaceType());
			dynaBean.set(IN11Line.lineType+i, 1, IN11Line.lineType);
			dynaBean.set(IN11Line.lineType+i, 2, line.getLineNumber());
			dynaBean.set(IN11Line.lineType+i, 3, line.getMaterialNumber());
			dynaBean.set(IN11Line.lineType+i, 4, line.getBatchNumber());
			dynaBean.set(IN11Line.lineType+i, 5, line.getQuantity());
			dynaBean.set(IN11Line.lineType+i, 6, line.getUnitOfEntry());
			dynaBean.set(IN11Line.lineType+i, 7, line.getZeroCount());
		}
		return dynaBean;
	}

/**
	public static DynaBean prepareIN02CSVLinePosition(IN02 in02){
		DynaBean dynaBean = new LazyDynaMap();
		
		IN02Header in02Header = in02.getIn02Header();
		IN02Line in02Line = in02.getIn02Line();
		
		dynaBean.set(IN02Header.lineType, 0, GeneralHelper.IN02);
		dynaBean.set(IN02Header.lineType, 1, IN02Header.lineType);
		dynaBean.set(IN02Header.lineType, 2, in02Header.getDocumentDate());
		dynaBean.set(IN02Header.lineType, 3, in02Header.getPostingDate());
		dynaBean.set(IN02Header.lineType, 4, in02Header.getDeliveryNumber());
		dynaBean.set(IN02Header.lineType, 5, in02Header.getHeaderText());
		dynaBean.set(IN02Header.lineType, 6, in02Header.getGmCode());
		dynaBean.set(IN02Header.lineType, 7, "");
		dynaBean.set(IN02Header.lineType, 8, "");
		dynaBean.set(IN02Header.lineType, 9, "");
		
		dynaBean.set(IN02Line.lineType, 0, GeneralHelper.IN02);
		dynaBean.set(IN02Line.lineType, 1, IN02Line.lineType);
		dynaBean.set(IN02Line.lineType, 2, in02Line.getMovementType());
		dynaBean.set(IN02Line.lineType, 3, StringUtils.leftPad(in02Line.getMaterialNumber(), 18, "0"));
		dynaBean.set(IN02Line.lineType, 4, in02Line.getQuantity());
		dynaBean.set(IN02Line.lineType, 5, in02Line.getPlant());
		dynaBean.set(IN02Line.lineType, 6, in02Line.getSourceLocation());
		dynaBean.set(IN02Line.lineType, 7, in02Line.getDestinationLocation());
		dynaBean.set(IN02Line.lineType, 8, in02Line.getBatchNumber());
		dynaBean.set(IN02Line.lineType, 9, in02Line.getUom());
**/		
		/**
		IN02ControlRecord cr = in02.getIn02ControlRecord();
		//IN02Header header = in02.getIn02Header();
		IN02GMHeaderData hData = in02.getIn02GmHeaderData();
		IN02GMMaterialDocumentItem item = in02.getIn02GmMaterialDocumentItem();
				
		//Control Record 1st line
		dynaBean.set(IN02Enum.EDIDC.toString(), 0, IN02Enum.EDIDC.toString());
		dynaBean.set(IN02Enum.EDIDC.toString(), 1, "");
		dynaBean.set(IN02Enum.EDIDC.toString(), 2, "");
		dynaBean.set(IN02Enum.EDIDC.toString(), 3, "");
		dynaBean.set(IN02Enum.EDIDC.toString(), 4, "");
		//dynaBean.set(IN02Enum.EDIDC.toString(), 5, cr.getReceiverPort());
		//dynaBean.set(IN02Enum.EDIDC.toString(), 6, cr.getReceiverPartnerType());
		//dynaBean.set(IN02Enum.EDIDC.toString(), 7, cr.getReceiverPartnerNumber());
		dynaBean.set(IN02Enum.EDIDC.toString(), 5, "");
		dynaBean.set(IN02Enum.EDIDC.toString(), 6, "");
		dynaBean.set(IN02Enum.EDIDC.toString(), 7, "");
		dynaBean.set(IN02Enum.EDIDC.toString(), 8, "");
		dynaBean.set(IN02Enum.EDIDC.toString(), 9, "");
		dynaBean.set(IN02Enum.EDIDC.toString(), 10, "");
		//dynaBean.set(IN02Enum.EDIDC.toString(), 11, cr.getSenderPort());
		//dynaBean.set(IN02Enum.EDIDC.toString(), 12, cr.getSenderPartnerType());
		//dynaBean.set(IN02Enum.EDIDC.toString(), 13, cr.getSenderPartnerNumber());
		dynaBean.set(IN02Enum.EDIDC.toString(), 11, "");
		dynaBean.set(IN02Enum.EDIDC.toString(), 12, "");
		dynaBean.set(IN02Enum.EDIDC.toString(), 13, "");
		dynaBean.set(IN02Enum.EDIDC.toString(), 14, "");
		dynaBean.set(IN02Enum.EDIDC.toString(), 15, "");
		//dynaBean.set(IN02Enum.EDIDC.toString(), 16, cr.getMessageType());
		//dynaBean.set(IN02Enum.EDIDC.toString(), 17, cr.getBasicType());
		dynaBean.set(IN02Enum.EDIDC.toString(), 16, "MBGMCR");
		dynaBean.set(IN02Enum.EDIDC.toString(), 17, "MBGMCR03");
		dynaBean.set(IN02Enum.EDIDC.toString(), 18, "");

		//Header Segment 2nd line
		dynaBean.set(IN02Enum.E1MBGMCR.toString(), 0, IN02Enum.E1MBGMCR.toString());
		dynaBean.set(IN02Enum.E1MBGMCR.toString(), 1, "");
		
		//GM Header 3rd line
		dynaBean.set(IN02Enum.E1BP2017_GM_HEAD_01.toString(), 0, IN02Enum.E1BP2017_GM_HEAD_01.toString());
		//dynaBean.set(IN02Enum.E1BP2017_GM_HEAD_01.toString(), 1, hData.getPostingDate());
		//dynaBean.set(IN02Enum.E1BP2017_GM_HEAD_01.toString(), 2, hData.getDocumentDate());
		//dynaBean.set(IN02Enum.E1BP2017_GM_HEAD_01.toString(), 3, hData.getReferenceDocumentNumber());
		dynaBean.set(IN02Enum.E1BP2017_GM_HEAD_01.toString(), 1, hData.getPostingDate());
		dynaBean.set(IN02Enum.E1BP2017_GM_HEAD_01.toString(), 2, hData.getDocumentDate());
		dynaBean.set(IN02Enum.E1BP2017_GM_HEAD_01.toString(), 3, hData.getReferenceDocumentNumber());
		dynaBean.set(IN02Enum.E1BP2017_GM_HEAD_01.toString(), 4, "");
		
		//GM Item 4th line
		dynaBean.set(IN02Enum.E1BP2017_GM_ITEM_CREATE.toString(), 0, IN02Enum.E1BP2017_GM_ITEM_CREATE.toString());
		//dynaBean.set(IN02Enum.E1BP2017_GM_ITEM_CREATE.toString(), 1, item.getMaterialNumber());
		//dynaBean.set(IN02Enum.E1BP2017_GM_ITEM_CREATE.toString(), 2, item.getPlant());
		//dynaBean.set(IN02Enum.E1BP2017_GM_ITEM_CREATE.toString(), 3, item.getLocation());
		//dynaBean.set(IN02Enum.E1BP2017_GM_ITEM_CREATE.toString(), 4, item.getMovementType());
		dynaBean.set(IN02Enum.E1BP2017_GM_ITEM_CREATE.toString(), 1, item.getMaterialNumber());
		dynaBean.set(IN02Enum.E1BP2017_GM_ITEM_CREATE.toString(), 2, item.getPlant());
		dynaBean.set(IN02Enum.E1BP2017_GM_ITEM_CREATE.toString(), 3, item.getLocation());
		dynaBean.set(IN02Enum.E1BP2017_GM_ITEM_CREATE.toString(), 4, item.getMovementType());
		
		dynaBean.set(IN02Enum.E1BP2017_GM_ITEM_CREATE.toString(), 5, "");
		dynaBean.set(IN02Enum.E1BP2017_GM_ITEM_CREATE.toString(), 6, "");
		dynaBean.set(IN02Enum.E1BP2017_GM_ITEM_CREATE.toString(), 7, "");
		dynaBean.set(IN02Enum.E1BP2017_GM_ITEM_CREATE.toString(), 8, "");
		dynaBean.set(IN02Enum.E1BP2017_GM_ITEM_CREATE.toString(), 9, "");
		dynaBean.set(IN02Enum.E1BP2017_GM_ITEM_CREATE.toString(), 10, "");
		dynaBean.set(IN02Enum.E1BP2017_GM_ITEM_CREATE.toString(), 11, "");
		dynaBean.set(IN02Enum.E1BP2017_GM_ITEM_CREATE.toString(), 12, "");
		//dynaBean.set(IN02Enum.E1BP2017_GM_ITEM_CREATE.toString(), 13, item.getQuantity());
		dynaBean.set(IN02Enum.E1BP2017_GM_ITEM_CREATE.toString(), 13, item.getQuantity());
		dynaBean.set(IN02Enum.E1BP2017_GM_ITEM_CREATE.toString(), 14, "");
		**/	
/**		
		return dynaBean;
	}
**/
	/**
	public static DynaBean prepareIN03CSVLinePosition(IN03 in03) {
		DynaBean dynaBean = new LazyDynaMap();
		IN03Header in03Header = in03.getIn03Header();
		IN03Line in03Line = in03.getIn03Line();
		
		dynaBean.set(IN03Header.lineType, 0, GeneralHelper.IN03);
		dynaBean.set(IN03Header.lineType, 1, IN03Header.lineType);
		dynaBean.set(IN03Header.lineType, 2, in03Header.getDocumentDate());
		dynaBean.set(IN03Header.lineType, 3, in03Header.getPostingDate());
		dynaBean.set(IN03Header.lineType, 4, in03Header.getDeliveryNumber());
		dynaBean.set(IN03Header.lineType, 5, in03Header.getHeaderText());
		dynaBean.set(IN03Header.lineType, 6, in03Header.getGmCode());
		dynaBean.set(IN03Header.lineType, 7, "");
		dynaBean.set(IN03Header.lineType, 8, "");
		dynaBean.set(IN03Header.lineType, 9, "");
		
		dynaBean.set(IN03Line.lineType, 0, GeneralHelper.IN03);
		dynaBean.set(IN03Line.lineType, 1, IN03Line.lineType);
		dynaBean.set(IN03Line.lineType, 2, in03Line.getMovementType());
		dynaBean.set(IN03Line.lineType, 3, StringUtils.leftPad(in03Line.getMaterialNumber(), 18, "0"));
		dynaBean.set(IN03Line.lineType, 4, in03Line.getQuantity());
		dynaBean.set(IN03Line.lineType, 5, in03Line.getPlant());
		dynaBean.set(IN03Line.lineType, 6, in03Line.getSourceLocation());
		dynaBean.set(IN03Line.lineType, 7, in03Line.getDestinationLocation());
		dynaBean.set(IN03Line.lineType, 8, in03Line.getBatchNumber());
		dynaBean.set(IN03Line.lineType, 9, in03Line.getUom());
		**/
		
		
		
		/** irrelevant as of 2 May 2016
		IN03ControlRecord cr = in03.getIn03ControlRecord();
		//IN03Header header = in03.getIn03Header();
		IN03GMHeaderData hData = in03.getIn03GmHeaderData();
		IN03GMCode gmCode = in03.getIn03GmCode();
		IN03GMMaterialDocumentItem item = in03.getIn03GmMaterialDocumentItem();
		
		//Control Record 1st line
		dynaBean.set(IN03Enum.EDIDC.toString(), 0, IN03Enum.EDIDC.toString());
		dynaBean.set(IN03Enum.EDIDC.toString(), 1, "");
		dynaBean.set(IN03Enum.EDIDC.toString(), 2, "");
		dynaBean.set(IN03Enum.EDIDC.toString(), 3, "");
		dynaBean.set(IN03Enum.EDIDC.toString(), 4, "");
		//dynaBean.set(IN03Enum.EDIDC.toString(), 5, cr.getReceiverPort());
		//dynaBean.set(IN03Enum.EDIDC.toString(), 6, cr.getReceiverPartnerType());
		//dynaBean.set(IN03Enum.EDIDC.toString(), 7, cr.getReceiverPartnerNumber());
		dynaBean.set(IN03Enum.EDIDC.toString(), 5, "");
		dynaBean.set(IN03Enum.EDIDC.toString(), 6, "");
		dynaBean.set(IN03Enum.EDIDC.toString(), 7, "");
		dynaBean.set(IN03Enum.EDIDC.toString(), 8, "");
		dynaBean.set(IN03Enum.EDIDC.toString(), 9, "");
		dynaBean.set(IN03Enum.EDIDC.toString(), 10, "");
		//dynaBean.set(IN03Enum.EDIDC.toString(), 11, cr.getSenderPort());
		//dynaBean.set(IN03Enum.EDIDC.toString(), 12, cr.getSenderPartnerType());
		//dynaBean.set(IN03Enum.EDIDC.toString(), 13, cr.getSenderPartnerNumber());
		dynaBean.set(IN03Enum.EDIDC.toString(), 11, "");
		dynaBean.set(IN03Enum.EDIDC.toString(), 12, "");
		dynaBean.set(IN03Enum.EDIDC.toString(), 13, "");
		dynaBean.set(IN03Enum.EDIDC.toString(), 14, "");
		dynaBean.set(IN03Enum.EDIDC.toString(), 15, "");
		//dynaBean.set(IN03Enum.EDIDC.toString(), 16, cr.getMessageType());
		//dynaBean.set(IN03Enum.EDIDC.toString(), 17, cr.getBasicType());
		//dynaBean.set(IN03Enum.EDIDC.toString(), 16, "MBGMCR");
		//dynaBean.set(IN03Enum.EDIDC.toString(), 17, "MBGMCR03");
		//dynaBean.set(IN03Enum.EDIDC.toString(), 18, "");
		
		//Header Segment 2nd line
		dynaBean.set(IN03Enum.E1MBGMCR.toString(), 0, IN03Enum.E1MBGMCR.toString());
		dynaBean.set(IN03Enum.E1MBGMCR.toString(), 1, "");
		
		//GM Header 3rd line
		dynaBean.set(IN03Enum.E1BP2017_GM_HEAD_01.toString(), 0, IN03Enum.E1BP2017_GM_HEAD_01.toString());
		//dynaBean.set(IN03Enum.E1BP2017_GM_HEAD_01.toString(), 1, hData.getPostingDate());
		//dynaBean.set(IN03Enum.E1BP2017_GM_HEAD_01.toString(), 2, hData.getDocumentDate());
		//dynaBean.set(IN03Enum.E1BP2017_GM_HEAD_01.toString(), 3, hData.getReferenceDocumentNumber());
		dynaBean.set(IN03Enum.E1BP2017_GM_HEAD_01.toString(), 1, hData.getPostingDate());
		dynaBean.set(IN03Enum.E1BP2017_GM_HEAD_01.toString(), 2, hData.getDocumentDate());
		dynaBean.set(IN03Enum.E1BP2017_GM_HEAD_01.toString(), 3, hData.getReferenceDocumentNumber());
		dynaBean.set(IN03Enum.E1BP2017_GM_HEAD_01.toString(), 4, "");
		
		//GM Code 4th line
		dynaBean.set(IN03Enum.E1BP2017_GM_CODE.toString(), 0, IN03Enum.E1BP2017_GM_CODE.toString());
		dynaBean.set(IN03Enum.E1BP2017_GM_CODE.toString(), 1, gmCode.getGmCode());
		dynaBean.set(IN03Enum.E1BP2017_GM_CODE.toString(), 2, "");
		
		//GM Item 5th line
		dynaBean.set(IN03Enum.E1BP2017_GM_ITEM_CREATE.toString(), 0, IN03Enum.E1BP2017_GM_ITEM_CREATE.toString());
		//dynaBean.set(IN03Enum.E1BP2017_GM_ITEM_CREATE.toString(), 1, item.getMaterialNumber());
		//dynaBean.set(IN03Enum.E1BP2017_GM_ITEM_CREATE.toString(), 2, item.getPlant());
		//dynaBean.set(IN03Enum.E1BP2017_GM_ITEM_CREATE.toString(), 3, item.getLocation());
		//dynaBean.set(IN03Enum.E1BP2017_GM_ITEM_CREATE.toString(), 4, item.getMovementType());
		dynaBean.set(IN03Enum.E1BP2017_GM_ITEM_CREATE.toString(), 1, item.getMaterialNumber());
		dynaBean.set(IN03Enum.E1BP2017_GM_ITEM_CREATE.toString(), 2, item.getPlant());
		dynaBean.set(IN03Enum.E1BP2017_GM_ITEM_CREATE.toString(), 3, item.getSourceLocation());
		dynaBean.set(IN03Enum.E1BP2017_GM_ITEM_CREATE.toString(), 4, item.getMovementType());
		
		dynaBean.set(IN03Enum.E1BP2017_GM_ITEM_CREATE.toString(), 5, "");
		dynaBean.set(IN03Enum.E1BP2017_GM_ITEM_CREATE.toString(), 6, "");
		dynaBean.set(IN03Enum.E1BP2017_GM_ITEM_CREATE.toString(), 7, "");
		dynaBean.set(IN03Enum.E1BP2017_GM_ITEM_CREATE.toString(), 8, "");
		dynaBean.set(IN03Enum.E1BP2017_GM_ITEM_CREATE.toString(), 9, "");
		dynaBean.set(IN03Enum.E1BP2017_GM_ITEM_CREATE.toString(), 10, "");
		dynaBean.set(IN03Enum.E1BP2017_GM_ITEM_CREATE.toString(), 11, "");
		dynaBean.set(IN03Enum.E1BP2017_GM_ITEM_CREATE.toString(), 12, "");
		//dynaBean.set(IN03Enum.E1BP2017_GM_ITEM_CREATE.toString(), 13, item.getQuantity());
		dynaBean.set(IN03Enum.E1BP2017_GM_ITEM_CREATE.toString(), 13, item.getQuantity());
		dynaBean.set(IN03Enum.E1BP2017_GM_ITEM_CREATE.toString(), 14, item.getDestinationLocation());
		dynaBean.set(IN03Enum.E1BP2017_GM_ITEM_CREATE.toString(), 15, "");
**/
	/**	
		return dynaBean;
	}
	**/
}
