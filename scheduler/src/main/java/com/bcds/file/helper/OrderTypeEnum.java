package com.bcds.file.helper;

public enum OrderTypeEnum {

	SCPO_01("01"),
	STO_02("02"),
	STO_03("03"),
	STO_04("04"),
	SCPO_05("05"),
	STO_06("06"),
	GR("07"),
	SCPO("08"),
	GI("09");
	
	private final String mescod;
	
	public String toString(){
		switch (this){
			case SCPO_01: return "SCPO_01";
			case STO_02: return "STO_02";
			case STO_03: return "STO_03";
			case STO_04: return "STO_04";
			case SCPO_05: return "SCPO_05";
			case STO_06: return "STO_06";
			case GR: return "GR";
			case SCPO: return "SCPO";
			case GI: return "GI";
			default: return "";
		}
	}
	
	public String getOrderReturnCode(){
		switch (this){
		case SCPO_01: return "975";
		case STO_02: return "973";
		case STO_03: return "975";
		case STO_04: return "973";
		case SCPO_05: return "973";
		case STO_06: return "";
		default: return "";
	}
	}
	
	OrderTypeEnum(String mescod){
		this.mescod = mescod;
	}
	
	public String getMescod(){
		return this.mescod;
	}
}
