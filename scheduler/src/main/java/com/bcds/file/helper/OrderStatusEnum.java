package com.bcds.file.helper;

public enum OrderStatusEnum {

	PENDING,
	INPROGRESS,
	FINISHED,
	CANCELLED,
	ISSUED,
	RECEIVED,
	CLOSED;
	
}
