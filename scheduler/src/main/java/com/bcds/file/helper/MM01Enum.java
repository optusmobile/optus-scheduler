package com.bcds.file.helper;

import java.util.HashMap;
import java.util.Map;


public enum MM01Enum {

	EDIDC,
	E1MARAM,
	E1MAKTM,
	E1MARCM,
	E1MARDM,
	EDI_DC40;
	
	public static String MATERIAL_NUMBER = "MATERIAL_NUMBER";
	public static String MATERIAL_DESC = "MATERIAL_DESC";
	public static String PART_NUMBER = "PART_NUMBER";
	public static String SLOC = "SLOC";
	
	public static Map <String, Integer> getFieldPosition(){
		HashMap<String, Integer> positions = new HashMap<String, Integer>();
			positions.put(MATERIAL_NUMBER, 0);
			positions.put(MATERIAL_DESC, 1);
			positions.put(PART_NUMBER, 2);
			positions.put(SLOC, 60);

		return positions;
	}

}
