package com.bcds.file.helper;

import java.util.HashMap;
import java.util.Map;

public enum IN01Enum {

	EDI_DC40,
	AUD,
	SLOC,
	SCPO,
	UB,
	MATERIAL,
	INSTALL,
	Z1NL,
	LF,
	NF,
	IN06,
	NA
	;
	
	
	public static String MESCOD = "MESCOD";
	public static String DOCUMENT_TYPE = "DOCUMENT_TYPE";
	public static String IDOC_DOCUMENT_NUMBER = "IDOC_DOCUMENT_NUMBER";
	public static String PO_ITEM_NUMBER = "PO_ITEM_NUMBER";
	public static String QUANTITY = "QUANTITY";
	public static String PLANT = "PLANT";
	public static String SLOCATION = "SLOCATION";
	public static String MRFID = "MRFID";
	public static String LINE_NUMBER = "LINE_NUMBER";
	public static String UNIT_OF_MEASURE = "UNIT_OF_MEASURE";
	public static String CROSS_REFERENCE = "CROSS_REFERENCE";
	public static String QUALIFIER = "QUALIFIER";
	public static String MATERIAL_NUMBER = "MATERIAL_NUMBER";
	public static String SHORT_TEXT = "SHORT_TEXT";
	public static String SALES_ORDER_NUMBER = "SALES_ORDER_NUMBER";
	public static String SALES_ORDER_LINE = "SALES_ORDER_LINE";
	public static String SERIAL_NUMBER = "SERIAL_NUMBER";
	public static String SALES_ORDER_LINE_NUMBER = "SALES_ORDER_LINE_NUMBER";
	public static String STO = "STO";
	public static String BATCH_NUMBER = "BATCH_NUMBER";
	
	@Override
	public  String toString(){
		switch (this){
			//case EDIDC: return "EDIDC";
			//case E1EDK01: return "E1EDK01";
			//case E1EDP01: return "E1EDP01";
			//case E1EDP19: return "E1EDP19";
			case EDI_DC40: return "EDI_DC40";
			case SCPO: return "SCPO";
			case SLOC: return "SLOC";
			case UB: return "UB";
			case AUD: return "AUD";
			case INSTALL: return "INSTALL";
			default: return "";
		}
	}
	
	public Map <String, Integer> getFieldPosition(){
		HashMap<String, Integer> positions = new HashMap<String, Integer>();
		/**
		if (this == EDIDC){
			positions.put(MESCOD, 8);
		}else if (this == E1EDK01){
			positions.put(DOCUMENT_TYPE, 8);
			positions.put(IDOC_DOCUMENT_NUMBER, 9);
		}else if (this == E1EDP01){
			positions.put(PO_ITEM_NUMBER, 1);
			positions.put(QUANTITY, 5);
			positions.put(PLANT, 35);
			positions.put(LOCATION, 41);
		}else if (this == E1EDP19){
			positions.put(QUALIFIER, 1);
			positions.put(MATERIAL_NUMBER, 2);
			positions.put(SHORT_TEXT, 3);
		}**/
		
		if (this == IN01Enum.AUD){
			positions.put(DOCUMENT_TYPE, 4);
			positions.put(IDOC_DOCUMENT_NUMBER, 5);
		}else if (this == IN01Enum.SLOC){
			positions.put(QUANTITY, 3);
			positions.put(PLANT, 14);
			positions.put(SLOCATION, 15);
			positions.put(MESCOD, 16);
		}else if (this == IN01Enum.MATERIAL){
			positions.put(SHORT_TEXT, 2);
		}else if (this == IN01Enum.SCPO) {
			positions.put(LINE_NUMBER, 0);
			positions.put(QUANTITY, 1);
			positions.put(UNIT_OF_MEASURE, 2);
			positions.put(PLANT, 3);
			positions.put(SLOCATION, 4);
			positions.put(MATERIAL_NUMBER, 5);
			positions.put(MRFID, 6);
			positions.put(SERIAL_NUMBER, 7);
			positions.put(BATCH_NUMBER, 8);
		}
		else if (this == IN01Enum.UB) {
			positions.put(LINE_NUMBER, 0);
			positions.put(QUANTITY, 1);
			positions.put(UNIT_OF_MEASURE, 2);
			positions.put(PLANT, 3);
			positions.put(SLOCATION, 4);
			positions.put(MATERIAL_NUMBER, 5);
			positions.put(MRFID, 6);
			positions.put(SERIAL_NUMBER, 7);
			positions.put(BATCH_NUMBER, 8);
		}
		else if (this == IN01Enum.INSTALL) {
			positions.put(LINE_NUMBER, 2);
			positions.put(MATERIAL_NUMBER, 3);
			positions.put(PLANT, 4);
			positions.put(SLOCATION, 5);
			positions.put(QUANTITY, 6);
			positions.put(UNIT_OF_MEASURE, 7);
			positions.put(SALES_ORDER_NUMBER, 8);
			positions.put(SALES_ORDER_LINE, 9);
			positions.put(BATCH_NUMBER, 11);
		}
		else if (this == IN01Enum.Z1NL) {
			positions.put(LINE_NUMBER, 2);
			positions.put(MATERIAL_NUMBER, 3);
			positions.put(PLANT, 4);
			positions.put(SLOCATION, 5);
			positions.put(QUANTITY, 6);
			positions.put(UNIT_OF_MEASURE, 7);
			positions.put(STO, 8);
			positions.put(SALES_ORDER_LINE_NUMBER, 9);
			positions.put(SALES_ORDER_NUMBER, 10);
			positions.put(BATCH_NUMBER,11);
		}
		else if (this == IN01Enum.LF) {
			positions.put(LINE_NUMBER, 2);
			positions.put(MATERIAL_NUMBER, 3);
			positions.put(PLANT, 4);
			positions.put(SLOCATION, 5);
			positions.put(QUANTITY, 6);
			positions.put(UNIT_OF_MEASURE, 7);
			positions.put(SALES_ORDER_NUMBER, 8);
			positions.put(SALES_ORDER_LINE_NUMBER, 9);
			positions.put(SALES_ORDER_NUMBER, 10);
			positions.put(BATCH_NUMBER,11);
		}else if (this == IN01Enum.IN06){
			positions.put(DOCUMENT_TYPE, 3);
			positions.put(IDOC_DOCUMENT_NUMBER, 2);
		}
		return positions;
	}
}
