package com.bcds.file.helper;

public class GeneralHelper {

	public static final String MM01 = "MM01";
	public static final String IN01 = "IN01";
	public static final String IN02 = "IN02";
	public static final String IN03 = "INO3";
	public static final String IN10 = "IN10";
	public static final String IN11 = "IN11";
	public static final String DB_TABLE = "DB_TABLE";
	public static final String FILE_READ = "FILE_READ"; 
	public static final String FILE_IO = "FILE_IO";
	public static final String SMTP_SERVER = "SMTP_SERVER";
	public static final String FTP_SERVER = "FTP_SERVER";
	public static final String R01 = "R01";
	public static final String PROCESSING_ERROR = "PROCESSING_ERROR";
	public static final String SENDING_ERROR = "SENDING_ERROR";
	public static final String IN09 = "IN09";
	public static final String IN04 = "IN04";
	public static final String INTSOH = "INTSOH";
}
