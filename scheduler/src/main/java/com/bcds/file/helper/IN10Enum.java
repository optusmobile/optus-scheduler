package com.bcds.file.helper;

import java.util.HashMap;
import java.util.Map;

public enum IN10Enum {

	EDI_DC40,
	AUD,
	SLOC,
	SCPO,
	UB,
	MATERIAL
	;
	
	public static String MESCOD = "MESCOD";
	public static String DOCUMENT_TYPE = "DOCUMENT_TYPE";
	public static String IDOC_DOCUMENT_NUMBER = "IDOC_DOCUMENT_NUMBER";
	public static String PO_ITEM_NUMBER = "PO_ITEM_NUMBER";
	public static String YEAR = "YEAR";
	public static String QUANTITY = "QUANTITY";
	public static String PLANT = "PLANT";
	public static String SLOCATION = "SLOCATION";
	public static String COUNT_DATE = "COUNT_DATE";
	public static String REFERENCE_DATA = "REFERENCE_DATA";
	public static String LINE_NUMBER = "LINE_NUMBER";
	public static String METERIAL_NUMBER = "METERIAL_NUMBER";
	public static String COUNT_NUMBER = "COUNT_NUMBER";
	public static String QUALIFIER = "QUALIFIER";
	public static String SHORT_TEXT = "SHORT_TEXT";
	
	
	@Override
	public  String toString(){
		switch (this){
			case EDI_DC40: return "EDI_DC40";
			case SCPO: return "SCPO";
			case SLOC: return "SLOC";
			case UB: return "UB";
			case AUD: return "AUD";
			default: return "";
		}
	}
	
	public Map <String, Integer> getFieldPosition(){
		HashMap<String, Integer> positions = new HashMap<String, Integer>();
		/**
		if (this == EDIDC){
			positions.put(MESCOD, 8);
		}else if (this == E1EDK01){
			positions.put(DOCUMENT_TYPE, 8);
			positions.put(IDOC_DOCUMENT_NUMBER, 9);
		}else if (this == E1EDP01){
			positions.put(PO_ITEM_NUMBER, 1);
			positions.put(QUANTITY, 5);
			positions.put(PLANT, 35);
			positions.put(LOCATION, 41);
		}else if (this == E1EDP19){
			positions.put(QUALIFIER, 1);
			positions.put(MATERIAL_NUMBER, 2);
			positions.put(SHORT_TEXT, 3);
		}**/
		
		if (this == IN10Enum.AUD){
			positions.put(DOCUMENT_TYPE, 4);
			positions.put(IDOC_DOCUMENT_NUMBER, 5);
		}else if (this == IN10Enum.SLOC){
			positions.put(QUANTITY, 3);
			positions.put(PLANT, 14);
			positions.put(SLOCATION, 15);
			positions.put(MESCOD, 16);
		}else if (this == IN10Enum.MATERIAL){
			positions.put(SHORT_TEXT, 2);
		}else if (this == IN10Enum.SCPO) {
			positions.put(LINE_NUMBER, 0);
			positions.put(QUANTITY, 1);
			//positions.put(UNIT_OF_MEASURE, 2);
			positions.put(PLANT, 3);
			positions.put(SLOCATION, 4);
			//positions.put(MATERIAL_NUMBER, 5);
			//positions.put(MRFID, 6);
			//positions.put(CROSS_REFERENCE, 7);
		}
		else if (this == IN10Enum.EDI_DC40) {
			positions.put(LINE_NUMBER, 0);
			positions.put(QUANTITY, 1);
			//positions.put(UNIT_OF_MEASURE, 2);
			positions.put(PLANT, 3);
			positions.put(SLOCATION, 4);
			//positions.put(MATERIAL_NUMBER, 5);
			//positions.put(MRFID, 6);
			//positions.put(CROSS_REFERENCE, 7);
		}
		return positions;
	}
}
