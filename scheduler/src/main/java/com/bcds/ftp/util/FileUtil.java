package com.bcds.ftp.util;

import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileUtil {

	public static List<String> getSimilarBatchFiles(String baseDir, String filename) throws NoSuchFileException{
		
		List<String> filenames = null;
		Path base = Paths.get(baseDir);
		int maxDepth = 1;
		try(Stream<Path> stream = Files.find(base, maxDepth, (path, attr) -> String.valueOf(path).contains(filename))) {
		    filenames = stream.map(String::valueOf).collect(Collectors.toList());
		}catch (Exception ioe){
			//ioe.printStackTrace();
		}
		return filenames;
	}
	
	public static void main(String[] args){
		List<String> filenames;
		try {
			filenames = FileUtil.getSimilarBatchFiles("c:\\in", "O3_PURM");
			filenames.forEach(System.out::println);
		} catch (NoSuchFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
