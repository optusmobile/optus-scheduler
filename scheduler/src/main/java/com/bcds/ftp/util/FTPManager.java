package com.bcds.ftp.util;

import java.io.File;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemOptions;
import org.apache.commons.vfs2.Selectors;
import org.apache.commons.vfs2.impl.StandardFileSystemManager;
import org.apache.commons.vfs2.provider.sftp.SftpFileSystemConfigBuilder;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
public class FTPManager {

	final static Logger logger = Logger.getLogger(FTPManager.class);

	//@Value( "${ftp.dev.server}" )
	public String hostname;
	
	//@Value( "${ftp.dev.username}" )
	public String username;
	
	//@Value( "${ftp.dev.password}" )
	public String password;
	
	//@Value( "${ftp.dev.destination.folder}" )
	public String destinationFolder;
	
	
	public void upload(File file) throws FileSystemException {

	    //File file = new File(localFilePath);
	
	    StandardFileSystemManager manager = new StandardFileSystemManager();
	
	    
        manager.init();

        // Create local file object
        FileObject localFile = manager.resolveFile(file.getAbsolutePath());

        // Create remote file object
        FileObject remoteFile = manager.resolveFile(
                        createConnectionString(this.hostname, this.username, this.password, this.destinationFolder), createDefaultOptions());

        // Copy local file to sftp server
        remoteFile.copyFrom(localFile, Selectors.SELECT_SELF);

        logger.info("File upload success");
        manager.close();
	    
	}	
	
	private FileSystemOptions createDefaultOptions() throws FileSystemException {
	    // Create SFTP options
	    FileSystemOptions opts = new FileSystemOptions();
	
	    // SSH Key checking
	    SftpFileSystemConfigBuilder.getInstance().setStrictHostKeyChecking(
	                    opts, "no");
	
	    // Root directory set to user home
	    SftpFileSystemConfigBuilder.getInstance().setUserDirIsRoot(opts, true);
	
	    // Timeout is count by Milliseconds
	    SftpFileSystemConfigBuilder.getInstance().setTimeout(opts, 10000);
	
	    return opts;
	}
	
	// Establishing connection
    private String createConnectionString(String hostname, String username, String password, String remoteFilePath) {
    	
    	logger.info("hostname" + hostname);
    	logger.info("username" + username);
    	logger.info("password" + password);
    	logger.info("remoteFilePath" + remoteFilePath);
    	
	    return "sftp://" + username + ":" + password + "@" + hostname + "/" + remoteFilePath;
	    //return "sftp://optus_oliver3:saveIt4Oliver@52.63.125.128";
    }
}
