package com.bcds.jpa.model.in03;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IN03 {
	
	private IN03Header in03Header;
	private IN03Line in03Line;
	private IN03ControlRecord in03ControlRecord;
	private IN03HeaderSegment in03HeaderSegment;
	private IN03GMHeaderData in03GmHeaderData;
	private IN03GMCode in03GmCode;
	private IN03GMMaterialDocumentItem in03GmMaterialDocumentItem;
	private List<IN03Line> in03Lines = new ArrayList<IN03Line>();
	
	public IN03Header getIn03Header() {
		return in03Header;
	}
	public void setIn03Header(IN03Header in03Header) {
		this.in03Header = in03Header;
	}
	public List<IN03Line> getIn03Lines() {
		return in03Lines;
	}
	public void setIn03Lines(List<IN03Line> in03Lines) {
		this.in03Lines = in03Lines;
	}
	public IN03Line getIn03Line() {
		return in03Line;
	}
	public void setIn03Line(IN03Line in03Line) {
		this.in03Line = in03Line;
	}
	public IN03HeaderSegment getIn03HeaderSegment() {
		return in03HeaderSegment;
	}
	public void setIn03HeaderSegment(IN03HeaderSegment in03HeaderSegment) {
		this.in03HeaderSegment = in03HeaderSegment;
	}
	public IN03ControlRecord getIn03ControlRecord() {
		return in03ControlRecord;
	}
	public void setIn03ControlRecord(IN03ControlRecord in03ControlRecord) {
		this.in03ControlRecord = in03ControlRecord;
	}
	
	public IN03GMHeaderData getIn03GmHeaderData() {
		return in03GmHeaderData;
	}
	public void setIn03GmHeaderData(IN03GMHeaderData in03GmHeaderData) {
		this.in03GmHeaderData = in03GmHeaderData;
	}
	public IN03GMMaterialDocumentItem getIn03GmMaterialDocumentItem() {
		return in03GmMaterialDocumentItem;
	}
	public void setIn03GmMaterialDocumentItem(IN03GMMaterialDocumentItem in03GmMaterialDocumentItem) {
		this.in03GmMaterialDocumentItem = in03GmMaterialDocumentItem;
	}
	public IN03GMCode getIn03GmCode() {
		return in03GmCode;
	}
	public void setIn03GmCode(IN03GMCode in03GmCode) {
		this.in03GmCode = in03GmCode;
	}
	
	
	public static void main(String[] args){
		Map<String, String> map = new HashMap <String, String>();
		map.put("0.1", "Hello");
		map.put("1.2", "World");
		map.put("2.3", "welcome");
		map.put("3.4", "CCL");
		
		List<String> list = map.keySet().stream().collect(Collectors.toList());
		list.forEach(System.out::println);
	}
}
