package com.bcds.jpa.model.mm01;

public class MM01Location {
	private String e1mardm;
	private String function;
	private String location;
	
	public String getE1mardm() {
		return e1mardm;
	}
	public void setE1mardm(String e1mardm) {
		this.e1mardm = e1mardm;
	}
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
	
}
