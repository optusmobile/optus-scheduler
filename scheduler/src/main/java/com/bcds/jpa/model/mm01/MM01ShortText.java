package com.bcds.jpa.model.mm01;

public class MM01ShortText {
	private String e1maktm;
	private String function;
	private String languageKey;
	private String materialDescription;
	
	public String getE1maktm() {
		return e1maktm;
	}
	public void setE1maktm(String e1maktm) {
		this.e1maktm = e1maktm;
	}
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public String getLanguageKey() {
		return languageKey;
	}
	public void setLanguageKey(String languageKey) {
		this.languageKey = languageKey;
	}
	public String getMaterialDescription() {
		return materialDescription;
	}
	public void setMaterialDescription(String materialDescription) {
		this.materialDescription = materialDescription;
	}
	
	
}
