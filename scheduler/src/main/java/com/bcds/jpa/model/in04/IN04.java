package com.bcds.jpa.model.in04;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IN04 {
	
	private IN04Header in04Header;
	private List<IN04Line> in04Line = new ArrayList<IN04Line>();
	private IN04ControlRecord in04ControlRecord;
	private IN04HeaderSegment in04HeaderSegment;
	private IN04GMHeaderData in04GmHeaderData;
	private IN04GMMaterialDocumentItem in04GmMaterialDocumentItem;
	public IN04Header getIn04Header() {
		return in04Header;
	}
	public void setIn04Header(IN04Header in04Header) {
		this.in04Header = in04Header;
	}
	public List<IN04Line> getIn04Line() {
		return in04Line;
	}
	public void setIn04Line(List<IN04Line> in04Line) {
		this.in04Line = in04Line;
	}
	public IN04ControlRecord getIn04ControlRecord() {
		return in04ControlRecord;
	}
	public void setIn04ControlRecord(IN04ControlRecord in04ControlRecord) {
		this.in04ControlRecord = in04ControlRecord;
	}
	public IN04HeaderSegment getIn04HeaderSegment() {
		return in04HeaderSegment;
	}
	public void setIn04HeaderSegment(IN04HeaderSegment in04HeaderSegment) {
		this.in04HeaderSegment = in04HeaderSegment;
	}
	public IN04GMHeaderData getIn04GmHeaderData() {
		return in04GmHeaderData;
	}
	public void setIn04GmHeaderData(IN04GMHeaderData in04GmHeaderData) {
		this.in04GmHeaderData = in04GmHeaderData;
	}
	public IN04GMMaterialDocumentItem getIn04GmMaterialDocumentItem() {
		return in04GmMaterialDocumentItem;
	}
	public void setIn04GmMaterialDocumentItem(IN04GMMaterialDocumentItem in04GmMaterialDocumentItem) {
		this.in04GmMaterialDocumentItem = in04GmMaterialDocumentItem;
	}
	
}
