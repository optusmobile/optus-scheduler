package com.bcds.jpa.model.in03;

import com.bcds.jpa.model.base.INHeaderInterface;

import lombok.Getter;
import lombok.Setter;

@Getter 
@Setter
public class IN03Header implements INHeaderInterface {

	private String interfaceType;
	private String postingDate;
	private String documentDate;
	private String deliveryNumber;
	private String headerText;
	private String gmCode;
	
	public String getPostingDate() {
		return postingDate;
	}
	public void setPostingDate(String postingDate) {
		this.postingDate = postingDate;
	}
	public String getDocumentDate() {
		return documentDate;
	}
	public void setDocumentDate(String documentDate) {
		this.documentDate = documentDate;
	}
	public String getDeliveryNumber() {
		return deliveryNumber;
	}
	public void setDeliveryNumber(String deliveryNumber) {
		this.deliveryNumber = deliveryNumber;
	}
	public String getHeaderText() {
		return headerText;
	}
	public void setHeaderText(String headerText) {
		this.headerText = headerText;
	}
	public String getGmCode() {
		return gmCode;
	}
	public void setGmCode(String gmCode) {
		this.gmCode = gmCode;
	}
	@Override
	public void setInterfaceType(String interfaceType) {
		this.interfaceType = interfaceType;
	}
	
	@Override
	public String getInterfaceType() {
		return this.interfaceType;
	}
	
}
