package com.bcds.jpa.model.in11;

import java.util.List;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IN11 {

	private IN11Header in11Header;
	private List<IN11Line> in11Lines;
	
	public IN11Header getIn11Header() {
		return in11Header;
	}
	public void setIn11Header(IN11Header in11Header) {
		this.in11Header = in11Header;
	}
	public List<IN11Line> getIn11Lines() {
		return in11Lines;
	}
	public void setIn11Lines(List<IN11Line> in11Lines) {
		this.in11Lines = in11Lines;
	}
	
	
}
