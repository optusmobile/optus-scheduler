package com.bcds.jpa.model.in01;

import com.bcds.jobs.beans.Order;
import com.bcds.jobs.beans.OrderDDBean;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IN01OrderLine {

	private String quantity;
	private String unitOfMeasure;
	private String plant;
	private String sLoc;
	private String mescod;
	private String materialNumber;
	private String mrfTypeId;
	private String tag;
	private String salesOrderNumber;
	private String salesOrderLine;
	private Order order;
	private OrderDDBean orderDDBean;
	
	public String getSalesOrderNumber() {
		return salesOrderNumber;
	}
	public void setSalesOrderNumber(String salesOrderNumber) {
		this.salesOrderNumber = salesOrderNumber;
	}
	
	public String getSalesOrderLine() {
		return salesOrderLine;
	}
	public void setSalesOrderLine(String salesOrderLine) {
		this.salesOrderLine = salesOrderLine;
	}
	
	public String getTag() {
		return tag;
	}
	
	public void setTag(String tag) {
		this.tag = tag;
	}
	public String getUnitOfMeasure() {
		return unitOfMeasure;
	}
	public void setUnitOfMeasure(String unitOfMeasure) {
		this.unitOfMeasure = unitOfMeasure;
	}
	public String getMrfTypeId() {
		return mrfTypeId;
	}
	public void setMrfTypeId(String mrfTypeId) {
		this.mrfTypeId = mrfTypeId;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getPlant() {
		return plant;
	}
	public void setPlant(String plant) {
		this.plant = plant;
	}
	
	public String getsLoc() {
		return sLoc;
	}
	public void setsLoc(String sLoc) {
		this.sLoc = sLoc;
	}
	public String getMescod() {
		return mescod;
	}
	public void setMescod(String mescod) {
		this.mescod = mescod;
	}
	public String getMaterialNumber() {
		return materialNumber;
	}
	public void setMaterialNumber(String materialNumber) {
		this.materialNumber = materialNumber;
	}
	public OrderDDBean getOrderDDBean() {
		return orderDDBean;
	}
	public void setOrderDDBean(OrderDDBean orderDDBean) {
		this.orderDDBean = orderDDBean;
	}
	
}
 