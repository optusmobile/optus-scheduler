package com.bcds.jpa.model.in10;

public class IN10HeaderGeneralData {
	
	private String e1edk01;
	private String documentType;
	private String idocDocumentNumber;
	
	public String getE1edk01() {
		return e1edk01;
	}
	public void setE1edk01(String e1edk01) {
		this.e1edk01 = e1edk01;
	}
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	public String getIdocDocumentNumber() {
		return idocDocumentNumber;
	}
	public void setIdocDocumentNumber(String idocDocumentNumber) {
		this.idocDocumentNumber = idocDocumentNumber;
	}
	
	
}
