package com.bcds.jpa.model.in09;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.bcds.file.helper.GeneralHelper;
import com.bcds.jpa.model.base.INBaseModel;
import com.bcds.jpa.model.in04.IN04Line;
import com.bcds.jpa.model.in11.IN11Header;
import com.bcds.jpa.model.in11.IN11Line;
import com.gs.collections.impl.lazy.primitive.SelectIntIterable;

import java.util.List;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class IN09 {
	
	private IN09Header in09Header;
	private IN09Line in09Line;
	private IN09ControlRecord in09ControlRecord;
	private IN09HeaderSegment in09HeaderSegment;
	private IN09GMHeaderData in09GmHeaderData;
	private IN09GMCode in09GmCode;
	private IN09GMMaterialDocumentItem in09GmMaterialDocumentItem;
	private List<IN09Line> listOfLines = new ArrayList<IN09Line>();
	
	public IN09Header getIn09Header() {
		return in09Header;
	}
	public void setIn09Header(IN09Header in09Header) {
		this.in09Header = in09Header;
	}
	public IN09Line getIn09Line() {
		return in09Line;
	}
	public void setIn09Line(IN09Line in09Line) {
		this.in09Line = in09Line;
	}
	public IN09HeaderSegment getIn09HeaderSegment() {
		return in09HeaderSegment;
	}
	public void setIn09HeaderSegment(IN09HeaderSegment in09HeaderSegment) {
		this.in09HeaderSegment = in09HeaderSegment;
	}
	public IN09ControlRecord getIn09ControlRecord() {
		return in09ControlRecord;
	}
	public void setIn09ControlRecord(IN09ControlRecord in09ControlRecord) {
		this.in09ControlRecord = in09ControlRecord;
	}
	
	public IN09GMHeaderData getIn09GmHeaderData() {
		return in09GmHeaderData;
	}
	public void setIn09GmHeaderData(IN09GMHeaderData in09GmHeaderData) {
		this.in09GmHeaderData = in09GmHeaderData;
	}
	public IN09GMMaterialDocumentItem getIn09GmMaterialDocumentItem() {
		return in09GmMaterialDocumentItem;
	}
	public void setIn09GmMaterialDocumentItem(IN09GMMaterialDocumentItem in09GmMaterialDocumentItem) {
		this.in09GmMaterialDocumentItem = in09GmMaterialDocumentItem;
	}
	public IN09GMCode getIn09GmCode() {
		return in09GmCode;
	}
	public void setIn09GmCode(IN09GMCode in09GmCode) {
		this.in09GmCode = in09GmCode;
	}
	public List<IN09Line> getListOfLines() {
		return listOfLines;
	}
	public void setListOfLines(List<IN09Line> listOfLines) {
		this.listOfLines = listOfLines;
	}
	public static void main(String[] args){
		Map<String, String> map = new HashMap <String, String>();
		map.put("0.1", "Hello");
		map.put("1.2", "World");
		map.put("2.3", "welcome");
		map.put("3.4", "CCL");
		
		List<String> list = map.keySet().stream().collect(Collectors.toList());
		list.forEach(System.out::println);
	}
}
