package com.bcds.jpa.model.in01;

import java.util.List;

import org.codehaus.plexus.util.StringUtils;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IN01 {
	
	private IN01ControlRecord in01ControlRecord;
	private IN01HeaderGeneralData in01HeaderGeneralData;
	private IN01ItemGeneralData in01ItemGeneralData;
	private IN01ItemRecord in01ItemRecord;
	private IN01OrderHeader in01OrderHeader;
	private List<IN01OrderLine> in01OrderLinesList;
	
	public IN01ControlRecord getIn01ControlRecord() {
		return in01ControlRecord;
	}

	public void setIn01ControlRecord(IN01ControlRecord in01ControlRecord) {
		this.in01ControlRecord = in01ControlRecord;
	}

	public IN01HeaderGeneralData getIn01HeaderGeneralData() {
		return in01HeaderGeneralData;
	}

	public void setIn01HeaderGeneralData(IN01HeaderGeneralData in01HeaderGeneralData) {
		this.in01HeaderGeneralData = in01HeaderGeneralData;
	}

	public IN01ItemGeneralData getIn01ItemGeneralData() {
		return in01ItemGeneralData;
	}

	public void setIn01ItemGeneralData(IN01ItemGeneralData in01ItemGeneralData) {
		this.in01ItemGeneralData = in01ItemGeneralData;
	}

	public IN01ItemRecord getIn01ItemRecord() {
		return in01ItemRecord;
	}

	public void setIn01ItemRecord(IN01ItemRecord in01ItemRecord) {
		this.in01ItemRecord = in01ItemRecord;
	}

	public IN01OrderHeader getIn01OrderHeader() {
		return in01OrderHeader;
	}

	public void setIn01OrderHeader(IN01OrderHeader in01OrderHeader) {
		this.in01OrderHeader = in01OrderHeader;
	}

	public List<IN01OrderLine> getIn01OrderLines() {
		return in01OrderLinesList;
	}

	public void setIn01OrderLines(List<IN01OrderLine> in01OrderLines) {
		this.in01OrderLinesList = in01OrderLines;
	}

	public static void main(String[] args){
		System.out.println(StringUtils.isNumeric("100"));
		
	}
}
