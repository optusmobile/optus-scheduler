package com.bcds.jpa.model.base;

public interface INInterface {

	public void setInterfaceType(String interfaceType);
	public String getInterfaceType();
}
