package com.bcds.jpa.model.in01;

public class IN01ControlRecord {

	private String edidc;
	private String mescod;
	
	public String getEdidc() {
		return edidc;
	}

	public void setEdidc(String edidc) {
		this.edidc = edidc;
	}
	
	public String getMescod() {
		return mescod;
	}

	public void setMescod(String mescod) {
		this.mescod = mescod;
	}

}
