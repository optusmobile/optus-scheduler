package com.bcds.jpa.model.mm01;

public class MM01GeneralData {
	private String e1maram;
	private String function;
	private String materialNumber;
	
	public String getE1maram() {
		return e1maram;
	}
	public void setE1maram(String e1maram) {
		this.e1maram = e1maram;
	}
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public String getMaterialNumber() {
		return materialNumber;
	}
	public void setMaterialNumber(String materialNumber) {
		this.materialNumber = materialNumber;
	}	
}
