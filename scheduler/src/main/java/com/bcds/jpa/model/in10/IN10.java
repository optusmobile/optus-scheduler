package com.bcds.jpa.model.in10;

import java.util.List;

/**
 * @author arpita.das
 *
 */

public class IN10 {
	
	private IN10ControlRecord in10ControlRecord;
	private IN10HeaderGeneralData in10HeaderGeneralData;
	private IN10ItemGeneralData in10ItemGeneralData;
	private IN10ItemRecord in10ItemRecord;
	private IN10PIDHeader in10PIDHeader;
	private List<IN10PIDLine> in10PIDLine;
	
	public IN10ControlRecord getIn10ControlRecord() {
		return in10ControlRecord;
	}
	public void setIn10ControlRecord(IN10ControlRecord in10ControlRecord) {
		this.in10ControlRecord = in10ControlRecord;
	}
	public IN10HeaderGeneralData getIn10HeaderGeneralData() {
		return in10HeaderGeneralData;
	}
	public void setIn10HeaderGeneralData(IN10HeaderGeneralData in10HeaderGeneralData) {
		this.in10HeaderGeneralData = in10HeaderGeneralData;
	}
	public IN10ItemGeneralData getIn10ItemGeneralData() {
		return in10ItemGeneralData;
	}
	public void setIn10ItemGeneralData(IN10ItemGeneralData in10ItemGeneralData) {
		this.in10ItemGeneralData = in10ItemGeneralData;
	}
	public IN10ItemRecord getIn10ItemRecord() {
		return in10ItemRecord;
	}
	public void setIn10ItemRecord(IN10ItemRecord in10ItemRecord) {
		this.in10ItemRecord = in10ItemRecord;
	}
	public IN10PIDHeader getIn10PIDHeader() {
		return in10PIDHeader;
	}
	public void setIn10PIDHeader(IN10PIDHeader in10pidHeader) {
		in10PIDHeader = in10pidHeader;
	}
	public List<IN10PIDLine> getIn10PIDLine() {
		return in10PIDLine;
	}
	public void setIn10PIDLine(List<IN10PIDLine> in10pidLine) {
		in10PIDLine = in10pidLine;
	}
	
}
