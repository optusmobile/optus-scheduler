package com.bcds.jpa.model.in09;

import com.bcds.jpa.model.base.INLineInterface;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IN09Line implements INLineInterface {

	private String interfaceType;
	private String movementType;
	private String materialNumber;
	private String quantity;
	private String plant;
	private String sourceLocation;
	private String destinationLocation;
	private String batchName;
	private String uom;
	private String engineersPlant;
	private String engineersSloc;
	private String lineNumber;
	
	@Override
	public void setInterfaceType(String interfaceType) {
		this.interfaceType = interfaceType;
		
	}
	@Override
	public String getInterfaceType() {
		return interfaceType;
	}
	public String getMovementType() {
		return movementType;
	}
	public void setMovementType(String movementType) {
		this.movementType = movementType;
	}
	public String getMaterialNumber() {
		return materialNumber;
	}
	public void setMaterialNumber(String materialNumber) {
		this.materialNumber = materialNumber;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getPlant() {
		return plant;
	}
	public void setPlant(String plant) {
		this.plant = plant;
	}
	public String getSourceLocation() {
		return sourceLocation;
	}
	public void setSourceLocation(String sourceLocation) {
		this.sourceLocation = sourceLocation;
	}
	public String getDestinationLocation() {
		return destinationLocation;
	}
	public void setDestinationLocation(String destinationLocation) {
		this.destinationLocation = destinationLocation;
	}
	public String getBatchName() {
		return batchName;
	}
	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}
	public String getUom() {
		return uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public String getEngineersPlant() {
		return engineersPlant;
	}
	public void setEngineersPlant(String engineersPlant) {
		this.engineersPlant = engineersPlant;
	}
	public String getEngineersSloc() {
		return engineersSloc;
	}
	public void setEngineersSloc(String engineersSloc) {
		this.engineersSloc = engineersSloc;
	}
}
