package com.bcds.jpa.model.in03;

public class IN03HeaderSegment {

	private String e1mbgmcr;

	public String getE1mbgmcr() {
		return e1mbgmcr;
	}

	public void setE1mbgmcr(String e1mbgmcr) {
		this.e1mbgmcr = e1mbgmcr;
	}
}
