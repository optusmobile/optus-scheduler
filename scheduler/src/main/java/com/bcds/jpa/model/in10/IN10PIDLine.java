package com.bcds.jpa.model.in10;

import com.bcds.jobs.beans.PidBean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IN10PIDLine {

	private String pidNumber;
	private String year;
	private String plant;
	private String sLoc;
	private String countDate;
	private String refData;
	private String lineNumber;
	private String meterialNumber;
	private String countNumber;
	private int unitOfMeasure;
	private Double quantity;
	private String batchNumber;
	private PidBean pidBean;
	
	public String getPidNumber() {
		return pidNumber;
	}
	public void setPidNumber(String pidNumber) {
		this.pidNumber = pidNumber;
	}
	public String getCountDate() {
		return countDate;
	}
	public void setCountDate(String countDate) {
		this.countDate = countDate;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getPlant() {
		return plant;
	}
	public void setPlant(String plant) {
		this.plant = plant;
	}
	public String getsLoc() {
		return sLoc;
	}
	public void setsLoc(String sLoc) {
		this.sLoc = sLoc;
	}
	public String getRefData() {
		return refData;
	}
	public void setRefData(String refData) {
		this.refData = refData;
	}
	public String getLineNumber() {
		return lineNumber;
	}
	public void setLineNumber(String lineNumber) {
		this.lineNumber = lineNumber;
	}
	public String getMeterialNumber() {
		return meterialNumber;
	}
	public void setMeterialNumber(String meterialNumber) {
		this.meterialNumber = meterialNumber;
	}
	public String getCountNumber() {
		return countNumber;
	}
	public void setCountNumber(String countNumber) {
		this.countNumber = countNumber;
	}
	public int getUnitOfMeasure() {
		return unitOfMeasure;
	}
	public void setUnitOfMeasure(int unitOfMeasure) {
		this.unitOfMeasure = unitOfMeasure;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	
	public PidBean getPidBean() {
		return pidBean;
	}
	public void setPidBean(PidBean pidBean) {
		this.pidBean = pidBean;
	}
	
}
 