package com.bcds.jpa.model.in04;

import com.bcds.jpa.model.base.INLineInterface;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IN04Line implements INLineInterface {

	private String interfaceType;
	private String movementType;
	private String materialNumber;
	private String quantity;
	private String plant;
	private String sourceLocation;
	private String destinationLocation;
	private String batchNumber;
	private String uom;
	private String salesOrderNum;
	private String salesOrderLineNum;
	private String visitingEngSloc;
	
	@Override
	public void setInterfaceType(String interfaceType) {
		this.interfaceType = interfaceType;
	}
	@Override
	public String getInterfaceType() {
		return this.interfaceType;
	}
	public String getMovementType() {
		return movementType;
	}
	public void setMovementType(String movementType) {
		this.movementType = movementType;
	}
	public String getMaterialNumber() {
		return materialNumber;
	}
	public void setMaterialNumber(String materialNumber) {
		this.materialNumber = materialNumber;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getPlant() {
		return plant;
	}
	public void setPlant(String plant) {
		this.plant = plant;
	}
	public String getSourceLocation() {
		return sourceLocation;
	}
	public void setSourceLocation(String sourceLocation) {
		this.sourceLocation = sourceLocation;
	}
	public String getDestinationLocation() {
		return destinationLocation;
	}
	public void setDestinationLocation(String destinationLocation) {
		this.destinationLocation = destinationLocation;
	}
	public String getBatchNumber() {
		return batchNumber;
	}
	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}
	public String getUom() {
		return uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public String getSalesOrderNum() {
		return salesOrderNum;
	}
	public void setSalesOrderNum(String salesOrderNum) {
		this.salesOrderNum = salesOrderNum;
	}
	
	public String getSalesOrderLineNum() {
		return salesOrderLineNum;
	}
	public void setSalesOrderLineNum(String salesOrderLineNum) {
		this.salesOrderLineNum = salesOrderLineNum;
	}
	
	public String getVisitingEngSloc() {
		return visitingEngSloc;
	}
	public void setVisitingEngSloc(String visitingEngSloc) {
		this.visitingEngSloc = visitingEngSloc;
	}
	
}
