package com.bcds.jpa.model.mm01;

import java.util.List;

public class MM01Plant {

	private String e1marcm;
	private String function;
	private String plant;
	private List<MM01Location> mm01Locations;
	
	public String getE1marcm() {
		return e1marcm;
	}
	public void setE1marcm(String e1marcm) {
		this.e1marcm = e1marcm;
	}
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public String getPlant() {
		return plant;
	}
	public void setPlant(String plant) {
		this.plant = plant;
	}
	public List<MM01Location> getMm01Locations() {
		return mm01Locations;
	}
	public void setMm01Locations(List<MM01Location> mm01Locations) {
		this.mm01Locations = mm01Locations;
	}
	
}
