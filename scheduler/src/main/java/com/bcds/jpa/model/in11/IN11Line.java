package com.bcds.jpa.model.in11;

import com.bcds.jpa.model.base.INLineInterface;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IN11Line implements INLineInterface {

	private String interfaceType;	
	private String materialNumber;
	private String batchNumber;
	private String quantity;
	private String unitOfEntry;
	private String zeroCount;
	private String lineNumber;
	@Override
	public void setInterfaceType(String interfaceType) {
		this.interfaceType = interfaceType;
		
	}
	@Override
	public String getInterfaceType() {
		return interfaceType;
	}
	public String getMaterialNumber() {
		return materialNumber;
	}
	public void setMaterialNumber(String materialNumber) {
		this.materialNumber = materialNumber;
	}
	public String getBatchNumber() {
		return batchNumber;
	}
	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getUnitOfEntry() {
		return unitOfEntry;
	}
	public void setUnitOfEntry(String unitOfEntry) {
		this.unitOfEntry = unitOfEntry;
	}
	public String getZeroCount() {
		return zeroCount;
	}
	public void setZeroCount(String zeroCount) {
		this.zeroCount = zeroCount;
	}
	public String getLineNumber() {
		return lineNumber;
	}
	public void setLineNumber(String lineNumber) {
		this.lineNumber = lineNumber;
	}
	
}
