package com.bcds.jpa.model.in09;

public class IN09HeaderSegment {

	private String e1mbgmcr;

	public String getE1mbgmcr() {
		return e1mbgmcr;
	}

	public void setE1mbgmcr(String e1mbgmcr) {
		this.e1mbgmcr = e1mbgmcr;
	}
}
