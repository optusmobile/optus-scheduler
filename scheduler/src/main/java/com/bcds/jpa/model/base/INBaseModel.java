package com.bcds.jpa.model.base;

//import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class INBaseModel {

	private INHeaderRecord inHeaderRecord;
	private INLineRecord inLineRecord;
	
	private String interfaceType;
}
