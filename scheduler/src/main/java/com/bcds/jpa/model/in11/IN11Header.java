package com.bcds.jpa.model.in11;

import com.bcds.jpa.model.base.INHeaderInterface;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IN11Header implements INHeaderInterface {

	private String interfaceType;	
	private String physicalInventoryDocument; //pid
	private String fiscalYear;
	private String dateOfLastCount;
	@Override
	public void setInterfaceType(String interfaceType) {
		this.interfaceType = interfaceType;
		
	}
	@Override
	public String getInterfaceType() {
		return interfaceType;
	}
	public String getPhysicalInventoryDocument() {
		return physicalInventoryDocument;
	}
	public void setPhysicalInventoryDocument(String physicalInventoryDocument) {
		this.physicalInventoryDocument = physicalInventoryDocument;
	}
	public String getFiscalYear() {
		return fiscalYear;
	}
	public void setFiscalYear(String fiscalYear) {
		this.fiscalYear = fiscalYear;
	}
	public String getDateOfLastCount() {
		return dateOfLastCount;
	}
	public void setDateOfLastCount(String dateOfLastCount) {
		this.dateOfLastCount = dateOfLastCount;
	}
	
}
