package com.bcds.jpa.model.base;

import lombok.Data;

@Data
public class INLineRecord {

	public static final String lineType = "L";
	
	private String movementType;
	private String materialNumber;
	private String quantity;
	private String plant;
	private String sourceLocation;
	private String destinationLocation;
	private String batchNumber;
	private String uom;
	
}
