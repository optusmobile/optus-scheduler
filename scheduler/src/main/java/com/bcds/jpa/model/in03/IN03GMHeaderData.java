package com.bcds.jpa.model.in03;

public class IN03GMHeaderData {
	
	private String e1bp2017GmHead01;
	private String postingDate;
	private String documentDate;
	private String referenceDocumentNumber;
	
	
	public String getE1bp2017GmHead01() {
		return e1bp2017GmHead01;
	}
	public void setE1bp2017GmHead01(String e1bp2017GmHead01) {
		this.e1bp2017GmHead01 = e1bp2017GmHead01;
	}
	public String getPostingDate() {
		return postingDate;
	}
	public void setPostingDate(String postingDate) {
		this.postingDate = postingDate;
	}
	public String getDocumentDate() {
		return documentDate;
	}
	public void setDocumentDate(String documentDate) {
		this.documentDate = documentDate;
	}
	public String getReferenceDocumentNumber() {
		return referenceDocumentNumber;
	}
	public void setReferenceDocumentNumber(String referenceDocumentNumber) {
		this.referenceDocumentNumber = referenceDocumentNumber;
	}
	
}
