package com.bcds.jpa.model.in02;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IN02 {
	
	private IN02Header in02Header;
	private List<IN02Line> in02Line = new ArrayList<IN02Line>();
	private IN02ControlRecord in02ControlRecord;
	private IN02HeaderSegment in02HeaderSegment;
	private IN02GMHeaderData in02GmHeaderData;
	private IN02GMMaterialDocumentItem in02GmMaterialDocumentItem;
	public IN02Header getIn02Header() {
		return in02Header;
	}
	public void setIn02Header(IN02Header in02Header) {
		this.in02Header = in02Header;
	}
	public List<IN02Line> getIn02Line() {
		return in02Line;
	}
	public void setIn02Line(List<IN02Line> in02Line) {
		this.in02Line = in02Line;
	}
	public IN02ControlRecord getIn02ControlRecord() {
		return in02ControlRecord;
	}
	public void setIn02ControlRecord(IN02ControlRecord in02ControlRecord) {
		this.in02ControlRecord = in02ControlRecord;
	}
	public IN02HeaderSegment getIn02HeaderSegment() {
		return in02HeaderSegment;
	}
	public void setIn02HeaderSegment(IN02HeaderSegment in02HeaderSegment) {
		this.in02HeaderSegment = in02HeaderSegment;
	}
	public IN02GMHeaderData getIn02GmHeaderData() {
		return in02GmHeaderData;
	}
	public void setIn02GmHeaderData(IN02GMHeaderData in02GmHeaderData) {
		this.in02GmHeaderData = in02GmHeaderData;
	}
	public IN02GMMaterialDocumentItem getIn02GmMaterialDocumentItem() {
		return in02GmMaterialDocumentItem;
	}
	public void setIn02GmMaterialDocumentItem(IN02GMMaterialDocumentItem in02GmMaterialDocumentItem) {
		this.in02GmMaterialDocumentItem = in02GmMaterialDocumentItem;
	}
	
}
