package com.bcds.jpa.model.in01;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IN01OrderHeader {

	private String documentType;
	private String documentNumber;
	private String assetTypeHeader;
	private String A5GIIndicator;
	
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	public String getDocumentNumber() {
		return documentNumber;
	}
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	public String getAssetTypeHeader() {
		return assetTypeHeader;
	}
	public void setAssetTypeHeader(String assetTypeHeader) {
		this.assetTypeHeader = assetTypeHeader;
	}
	public String getA5GIIndicator() {
		return A5GIIndicator;
	}
	public void setA5GIIndicator(String A5GIIndicator) {
		this.A5GIIndicator = A5GIIndicator;
	}
}
