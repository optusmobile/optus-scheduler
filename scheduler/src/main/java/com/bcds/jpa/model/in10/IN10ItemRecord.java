package com.bcds.jpa.model.in10;

public class IN10ItemRecord {

	private String e1edp19;
	private String qualifier;
	private String materialNumber;
	private String shortText;
	
	public String getE1edp19() {
		return e1edp19;
	}
	public void setE1edp19(String e1edp19) {
		this.e1edp19 = e1edp19;
	}
	public String getQualifier() {
		return qualifier;
	}
	public void setQualifier(String qualifier) {
		this.qualifier = qualifier;
	}
	public String getMaterialNumber() {
		return materialNumber;
	}
	public void setMaterialNumber(String materialNumber) {
		this.materialNumber = materialNumber;
	}
	public String getShortText() {
		return shortText;
	}
	public void setShortText(String shortText) {
		this.shortText = shortText;
	}
}
