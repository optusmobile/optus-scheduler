package com.bcds.jpa.model.r01;

import com.bcds.jobs.beans.StockVarianceBean;

public class R01StkVarLine {

	private StockVarianceBean stockVarianceBean;

	public StockVarianceBean getStockVarianceBean() {
		return stockVarianceBean;
	}

	public void setStockVarianceBean(StockVarianceBean stockVarianceBean) {
		this.stockVarianceBean = stockVarianceBean;
	}
	
	
}
 