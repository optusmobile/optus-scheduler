package com.bcds.jpa.model.in01;

public class IN01ItemGeneralData {

	private String e1edp01;
	private String poItemNumber;
	private String quantity;
	private String plant;
	private String location;

	public String getE1edp01() {
		return e1edp01;
	}
	public void setE1edp01(String e1edp01) {
		this.e1edp01 = e1edp01;
	}
	public String getPoItemNumber() {
		return poItemNumber;
	}
	public void setPoItemNumber(String poItemNumber) {
		this.poItemNumber = poItemNumber;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getPlant() {
		return plant;
	}
	public void setPlant(String plant) {
		this.plant = plant;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}

	
}
