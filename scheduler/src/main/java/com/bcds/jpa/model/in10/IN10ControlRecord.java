package com.bcds.jpa.model.in10;

public class IN10ControlRecord {

	private String edidc;
	private String mescod;
	
	public String getEdidc() {
		return edidc;
	}

	public void setEdidc(String edidc) {
		this.edidc = edidc;
	}
	
	public String getMescod() {
		return mescod;
	}

	public void setMescod(String mescod) {
		this.mescod = mescod;
	}

}
