package com.bcds.jpa.model.mm01;

import java.util.ArrayList;
import java.util.List;
import com.bcds.jobs.beans.CompanyAssetCategoryBean;
import com.bcds.jobs.beans.CompanyAssetParts;

public class MM01 {

	private MM01ControlRecord mm01ControlRecord;
	private MM01GeneralData mm01GeneralData;
	private MM01ShortText mm01ShortText;
	private List<MM01Plant> mm01Plants;
	private CompanyAssetParts companyAssetParts;
	private List<CompanyAssetCategoryBean> companyAssetCategoryBeanList = new ArrayList<CompanyAssetCategoryBean>();
	 
	public MM01ControlRecord getMm01ControlRecord() {
		return mm01ControlRecord;
	}
	public void setMm01ControlRecord(MM01ControlRecord mm01ControlRecord) {
		this.mm01ControlRecord = mm01ControlRecord;
	}
	public MM01GeneralData getMm01GeneralData() {
		return mm01GeneralData;
	}
	public void setMm01GeneralData(MM01GeneralData mm01GeneralData) {
		this.mm01GeneralData = mm01GeneralData;
	}
	public MM01ShortText getMm01ShortText() {
		return mm01ShortText;
	}
	public void setMm01ShortText(MM01ShortText mm01ShortText) {
		this.mm01ShortText = mm01ShortText;
	}
	public List<MM01Plant> getMm01Plants() {
		return mm01Plants;
	}
	public void setMm01Plants(List<MM01Plant> mm01Plants) {
		this.mm01Plants = mm01Plants;
	}
	public CompanyAssetParts getCompanyAssetParts() {
		return companyAssetParts;
	}
	public void setCompanyAssetParts(CompanyAssetParts companyAssetParts) {
		this.companyAssetParts = companyAssetParts;
	}
	public List<CompanyAssetCategoryBean> getCompanyAssetCategoryBeanList() {
		return companyAssetCategoryBeanList;
	}
	public void setCompanyAssetCategoryBeanList(List<CompanyAssetCategoryBean> companyAssetCategoryBeanList) {
		this.companyAssetCategoryBeanList = companyAssetCategoryBeanList;
	}
	
}
