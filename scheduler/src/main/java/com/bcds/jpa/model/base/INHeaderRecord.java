package com.bcds.jpa.model.base;

import lombok.Data;

@Data
public class INHeaderRecord {

	public static final String lineType = "H";
	
	private String postingDate;
	private String documentDate;
	private String deliveryNumber;
	private String headerText;
	private String gmCode;
}
