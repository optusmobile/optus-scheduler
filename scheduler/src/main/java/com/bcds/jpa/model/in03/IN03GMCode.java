package com.bcds.jpa.model.in03;

public class IN03GMCode {

	private String e1bp2017GmCode;
	private String gmCode;
	
	public String getE1bp2017GmCode() {
		return e1bp2017GmCode;
	}
	public void setE1bp2017GmCode(String e1bp2017GmCode) {
		this.e1bp2017GmCode = e1bp2017GmCode;
	}
	public String getGmCode() {
		return gmCode;
	}
	public void setGmCode(String gmCode) {
		this.gmCode = gmCode;
	}
	

}
