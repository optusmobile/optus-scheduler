package com.bcds.jpa.model.in03;

public class IN03GMMaterialDocumentItem {

	private String e1bp2017GmItemCreate;
	private String materialNumber;
	private String plant;
	private String sourceLocation;
	private String destinationLocation;
	private String movementType;
	private String quantity;
	
	
	public String getE1bp2017GmItemCreate() {
		return e1bp2017GmItemCreate;
	}
	public void setE1bp2017GmItemCreate(String e1bp2017GmItemCreate) {
		this.e1bp2017GmItemCreate = e1bp2017GmItemCreate;
	}
	public String getMaterialNumber() {
		return materialNumber;
	}
	public void setMaterialNumber(String materialNumber) {
		this.materialNumber = materialNumber;
	}
	public String getPlant() {
		return plant;
	}
	public void setPlant(String plant) {
		this.plant = plant;
	}
	
	public String getSourceLocation() {
		return sourceLocation;
	}
	public void setSourceLocation(String sourceLocation) {
		this.sourceLocation = sourceLocation;
	}
	public String getDestinationLocation() {
		return destinationLocation;
	}
	public void setDestinationLocation(String destinationLocation) {
		this.destinationLocation = destinationLocation;
	}
	public String getMovementType() {
		return movementType;
	}
	public void setMovementType(String movementType) {
		this.movementType = movementType;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
}
