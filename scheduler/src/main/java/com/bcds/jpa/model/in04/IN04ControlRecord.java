package com.bcds.jpa.model.in04;

public class IN04ControlRecord {
	private String edidc;
	private String receiverPort;
	private String receiverPartnerType;
	private String receiverPartnerNumber;
	private String senderPort;
	private String senderPartnerType;
	private String senderPartnerNumber;
	private String messageType;
	private String basicType;

	public String getEdidc() {
		return edidc;
	}
	public void setEdidc(String edidc) {
		this.edidc = edidc;
	}
	public String getReceiverPort() {
		return receiverPort;
	}
	public void setReceiverPort(String receiverPort) {
		this.receiverPort = receiverPort;
	}
	public String getReceiverPartnerType() {
		return receiverPartnerType;
	}
	public void setReceiverPartnerType(String receiverPartnerType) {
		this.receiverPartnerType = receiverPartnerType;
	}
	public String getReceiverPartnerNumber() {
		return receiverPartnerNumber;
	}
	public void setReceiverPartnerNumber(String receiverPartnerNumber) {
		this.receiverPartnerNumber = receiverPartnerNumber;
	}
	public String getSenderPort() {
		return senderPort;
	}
	public void setSenderPort(String senderPort) {
		this.senderPort = senderPort;
	}
	public String getSenderPartnerType() {
		return senderPartnerType;
	}
	public void setSenderPartnerType(String senderPartnerType) {
		this.senderPartnerType = senderPartnerType;
	}
	public String getSenderPartnerNumber() {
		return senderPartnerNumber;
	}
	public void setSenderPartnerNumber(String senderPartnerNumber) {
		this.senderPartnerNumber = senderPartnerNumber;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public String getBasicType() {
		return basicType;
	}
	public void setBasicType(String basicType) {
		this.basicType = basicType;
	}
	
	
}
