package com.bcds.jpa.model.in10;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IN10PIDHeader {

	private String documentType;
	private String documentNumber;
	
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	public String getDocumentNumber() {
		return documentNumber;
	}
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	
}
