package com.bcds.jpa.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.bcds.jpa.entity.OrderStatus;
import com.bcds.jpa.entity.ReturnTransactionsEntity;

@Repository
public interface ReturnTransactionsRepo  extends JpaRepository <ReturnTransactionsEntity, Integer>{
	
	List<ReturnTransactionsEntity> findByOrderStatus(OrderStatus orderStatus);
	
}
