package com.bcds.jpa.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bcds.jpa.entity.Order;
import com.bcds.jpa.entity.OrderRelationshipEntity;

@Repository
public interface OrderRelationshopRepo extends JpaRepository <OrderRelationshipEntity, Integer>{
	
	public List<OrderRelationshipEntity> findByParentOrderId(Order parentOrderId);
	
	public List<OrderRelationshipEntity> findByChildOrderId(Order childOrderId);
	
	
}
