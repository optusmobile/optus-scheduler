package com.bcds.jpa.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bcds.jpa.entity.CompanyEntity;

@Repository
public interface CompanyRepo  extends JpaRepository <CompanyEntity, Integer>{
	
	public CompanyEntity findByCompanyName(String companyName);
	
}
