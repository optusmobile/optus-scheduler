package com.bcds.jpa.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bcds.jpa.entity.User;

@Repository
public interface UserRepository extends JpaRepository <User, Integer>{
	User findByFirstNameAndLastName(String firstName, String lastName);
}
