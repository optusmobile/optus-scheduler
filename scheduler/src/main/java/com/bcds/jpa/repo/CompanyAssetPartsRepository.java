package com.bcds.jpa.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bcds.jpa.entity.CompanyAssetParts;

@Repository
public interface CompanyAssetPartsRepository extends JpaRepository<CompanyAssetParts, Integer>{

	CompanyAssetParts findByPartName(String partName);
	
}
