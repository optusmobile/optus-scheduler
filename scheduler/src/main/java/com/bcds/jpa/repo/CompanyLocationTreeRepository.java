package com.bcds.jpa.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bcds.jpa.entity.CompanyLocation;
import com.bcds.jpa.entity.CompanyLocationTree;

@Repository
public interface CompanyLocationTreeRepository extends JpaRepository <CompanyLocationTree, Integer>{

	
}
