package com.bcds.jpa.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bcds.jpa.entity.AssetMovementInfo;
import com.bcds.jpa.entity.InterfaceError;

@Repository
public interface InterfaceErrorRepository extends JpaRepository <InterfaceError, Integer>{

}
