package com.bcds.jpa.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.bcds.jpa.entity.AssetStatusEntity;

@Repository
public interface AssetStatusRepo extends JpaRepository <AssetStatusEntity, Integer>{
 
	AssetStatusEntity findByStatus(String status);
	
}
