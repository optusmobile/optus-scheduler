package com.bcds.jpa.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bcds.jpa.entity.CompanyAssetParts;
import com.bcds.jpa.entity.CompanyLocation;
import com.bcds.jpa.entity.StockVarianceEntity;

@Repository
public interface StockVarianceRepository extends JpaRepository<StockVarianceEntity, Integer> {

	@Query("SELECT e FROM StockVarianceEntity e WHERE e.sapQuantity != e.intrackQuantity")
	public List<StockVarianceEntity> findStockVariance();

	@Query("SELECT e.companyLocation.parentLocationId FROM StockVarianceEntity e GROUP BY e.plantName")
	public List<Integer> findAllPlantIds();

	@Query("SELECT COUNT(1)>0 FROM StockVarianceEntity e WHERE e.companyAssetParts=:companyAssetParts AND e.companyLocation = :location AND e.stockType = :stockType")
	public boolean isCompanyAssetPartsAndCompanyLocationAndStockTypeExist(
			@Param("companyAssetParts") CompanyAssetParts companyAssetParts,
			@Param("location") CompanyLocation location, @Param("stockType") String stockType);

}
