package com.bcds.jpa.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.bcds.jpa.entity.AssetConditionEntity;

@Repository
public interface AssetConditionRepo  extends JpaRepository <AssetConditionEntity, Integer>{
	
	AssetConditionEntity findByCondition(String  conditionName);

}
