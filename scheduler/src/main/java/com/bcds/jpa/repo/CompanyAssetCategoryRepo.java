package com.bcds.jpa.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bcds.jpa.entity.CompanyAssetCategoryEntity;

@Repository
public interface CompanyAssetCategoryRepo  extends JpaRepository <CompanyAssetCategoryEntity, Integer>{

	/**
	 * @param categoryName
	 * @param parentCatId
	 * @return
	 */
	public CompanyAssetCategoryEntity findByCategoryNameAndParentCatId(String categoryName, Integer parentCatId);

}
