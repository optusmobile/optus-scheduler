/**
 * 
 */
package com.bcds.jpa.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bcds.jpa.entity.CompanyLocation;
import com.bcds.jpa.entity.User;
import com.bcds.jpa.entity.UserLocation;

/**
 * @author jason.chen
 *
 */
@Repository
public interface UserLocationRepo extends JpaRepository <UserLocation, Integer>{

	/**
	 * @param companyLocation
	 * @return
	 */
	public List<UserLocation> findByCompanyLocation(CompanyLocation companyLocation);
	
	/**
	 * @param user
	 * @return
	 */
	public List<UserLocation> findByUser(User user);
}
