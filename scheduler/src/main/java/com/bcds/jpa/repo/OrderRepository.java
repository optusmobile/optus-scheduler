package com.bcds.jpa.repo;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.bcds.jpa.entity.CompanyLocation;
import com.bcds.jpa.entity.Order;
import com.bcds.jpa.entity.OrderActionType;
import com.bcds.jpa.entity.OrderStatus;
import com.bcds.jpa.entity.OrderType;

@Repository
public interface OrderRepository extends JpaRepository <Order, Integer>{

	List<Order> findByOrderStatusId(int orderStatusId);
	
	Order findByOrderNumberAndOrderActionTypeAndOrderTypeAndCompanyLocation(String orderNumber,OrderActionType orderActionType,OrderType orderType,
			CompanyLocation companyLocation);
	
	Order findByOrderNumber(String orderNumber);
	
	List<Order> findByOrderStatusAndMrfRef(OrderStatus orderStatus, String mrfRef);
	
	Order findByOrderNumberAndOrderActionTypeAndMrfRef(String orderNumber,  OrderActionType orderActionType, String mrfRef);
	
	List<Order> findByOrderTypeAndOrderActionTypeAndMrfRefAndOrderStatus(OrderType orderType, OrderActionType orderActionType, 
			String mrfRef, OrderStatus orderStatus);
	
	Order findByOrderNumberAndCompanyLocationAndOrderTypeAndOrderActionTypeAndOrderStatus(String orderNumber, CompanyLocation companyLocation,
			OrderType orderType, OrderActionType orderActionType,OrderStatus orderStatus);
	
}
