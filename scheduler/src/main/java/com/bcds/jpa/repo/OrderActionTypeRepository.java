package com.bcds.jpa.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.bcds.jpa.entity.OrderActionType;

@Repository
public interface OrderActionTypeRepository extends JpaRepository <OrderActionType, Integer>{

	OrderActionType findByActionNameAndActionTypeSap (String actionName, String actionTypeSap);
	OrderActionType findByActionName(String actionName);
	OrderActionType findByActionNameAndDeliveyDocumentType(String actionName, String deliveyDocumentType);
}
