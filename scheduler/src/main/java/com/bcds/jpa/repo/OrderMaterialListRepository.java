package com.bcds.jpa.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bcds.jpa.entity.Order;
import com.bcds.jpa.entity.OrderMaterialList;

@Repository
public interface OrderMaterialListRepository extends JpaRepository <OrderMaterialList, Integer>{

	List<OrderMaterialList> findByOrderId(int orderId);
}
