package com.bcds.jpa.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bcds.jpa.entity.OrderStatus;

@Repository
public interface OrderStatusRepository extends JpaRepository <OrderStatus, Integer>{

	OrderStatus findByOrderStatus(String orderStatus);
}
