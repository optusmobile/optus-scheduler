package com.bcds.jpa.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.bcds.jpa.entity.Asset;
import com.bcds.jpa.entity.AssetCompositeId;
import com.bcds.jpa.entity.AssetConditionEntity;
import com.bcds.jpa.entity.AssetStatusEntity;
import com.bcds.jpa.entity.CompanyAssetParts;
import com.bcds.jpa.entity.CompanyLocation;

@Repository
public interface AssetRepository extends JpaRepository<Asset, AssetCompositeId> {

	Asset findByCompanyAssetParts(CompanyAssetParts companyAssetParts);
	
//	Asset findByIdAndDeviceId(int id, int deviceId);

	List<Asset> findByLocationIdAndConditionIdAndStatusId(CompanyLocation locationId, AssetConditionEntity conditionId,
			AssetStatusEntity statusId);
	
	List<Asset> findByCompanyAssetPartsAndLocationIdAndConditionIdAndStatusId(CompanyAssetParts companyAssetPartsEntity, CompanyLocation locationId, AssetConditionEntity conditionId,
			AssetStatusEntity statusId);

	@Query("SELECT a FROM Asset a WHERE a.companyAssetParts = :companyAssetParts AND a.locationId = :locationId AND a.statusId = :statusIdU AND a.conditionId != :conditionId AND a.assetValue>0")
	List<Asset> findByCompanyAssetPartsAndLocationIdForStockCountU(@Param("companyAssetParts") CompanyAssetParts companyAssetParts, @Param("locationId") CompanyLocation locationId, @Param("statusIdU") AssetStatusEntity statusIdU, @Param("conditionId") AssetConditionEntity conditionId);
	
	@Query("SELECT a FROM Asset a WHERE a.companyAssetParts = :companyAssetParts AND a.locationId = :locationId AND (a.statusId = :statusIdQ OR (a.statusId = :statusIdU AND a.conditionId = :conditionId)) AND a.assetValue>0")
	List<Asset> findByCompanyAssetPartsAndLocationIdForStockCountQ(@Param("companyAssetParts") CompanyAssetParts companyAssetParts, @Param("locationId") CompanyLocation locationId, @Param("statusIdQ") AssetStatusEntity statusIdQ, @Param("statusIdU")  AssetStatusEntity statusIdU, @Param("conditionId")  AssetConditionEntity conditionId);
	
	@Query("SELECT a FROM Asset a WHERE a.companyAssetParts = :companyAssetParts AND a.locationId = :locationId AND a.statusId = :statusIdB AND a.assetValue>0")
	List<Asset> findByCompanyAssetPartsAndLocationIdForStockCountB(@Param("companyAssetParts") CompanyAssetParts companyAssetParts, @Param("locationId") CompanyLocation locationId, @Param("statusIdB") AssetStatusEntity statusIdB);

	@Query("SELECT a FROM Asset a LEFT JOIN a.locationId sloc WHERE sloc.parentLocationId=:plantId AND a.statusId != :statusId AND a.companyAssetParts IS NOT NULL AND a.assetValue>0 AND a.assetType = 2")
	public List<Asset> findByPlantIdForStockCount(@Param("plantId") int plantId, @Param("statusId") AssetStatusEntity statusId);
	
	@Query("SELECT a FROM Asset a LEFT JOIN a.locationId sloc WHERE sloc.parentLocationId=:plantId AND a.statusId = :statusId AND a.companyAssetParts IS NOT NULL AND a.assetValue>0 AND a.assetType = 4")
	public List<Asset> findByPlantIdForStockCountForInstall(@Param("plantId") int plantId, @Param("statusId") AssetStatusEntity statusId);
}
