package com.bcds.jpa.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bcds.jpa.entity.OrderStatus;
import com.bcds.jpa.entity.PhysicalInventoryDocument;

@Repository
public interface PhysicalInventoryDocumentRepository extends JpaRepository <PhysicalInventoryDocument, Integer>{

	List<PhysicalInventoryDocument> findByOrderStatus(OrderStatus status);
	
	PhysicalInventoryDocument findByPidNumber(String pidNumber);
}
