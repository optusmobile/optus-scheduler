package com.bcds.jpa.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bcds.jpa.entity.OrderType;

@Repository
public interface OrderTypeRepository extends JpaRepository <OrderType, Integer>{

	OrderType findByOrderTypeIdAndOrderTypeName(String orderTypeId, String orderTypeName);
	OrderType findByOrderTypeName(String orderTypeName);
}
