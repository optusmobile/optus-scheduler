package com.bcds.jpa.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bcds.jpa.entity.AssetMovementInfo;
import com.bcds.jpa.entity.AssetMovementInfoCompositeId;
import com.bcds.jpa.entity.OrderStatus;


@Repository
public interface AssetMovementInfoRepository extends JpaRepository <AssetMovementInfo, AssetMovementInfoCompositeId>{

	//List<AssetMovementInfo> findByFtpStatusId(int ftpStatusId);
	List<AssetMovementInfo> findByOrderStatus(OrderStatus orderStatus);
	
	
}
