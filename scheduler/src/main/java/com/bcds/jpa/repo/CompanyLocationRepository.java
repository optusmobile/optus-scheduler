package com.bcds.jpa.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bcds.jpa.entity.CompanyLocation;


@Repository
public interface CompanyLocationRepository extends JpaRepository <CompanyLocation, Integer>{

	List<CompanyLocation> findByLocationName(String locationName);
	CompanyLocation findByBarcodeAndTag(String barcode, String tag);
	CompanyLocation findByBarcode(String barcode);
	List<CompanyLocation> findByTagAndIsRootLocation(String tag, boolean isRootLocation);
	
}
