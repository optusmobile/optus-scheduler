package com.bcds.jpa.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "tbl_order_action_type")
public class OrderActionType {

	@Id
	@GeneratedValue
	private int id;
	
	@Column(name = "action_name")
	private String actionName;
	
	@Column(name = "action_type_sap")
	private String actionTypeSap;
	
	@Column(name = "delivery_doc_type")
	private String deliveyDocumentType;
	
	@Column(name = "last_update_date", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdatedDate;
	
	public int getId() {
		return id;
	}

	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getActionName() {
		return actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

	public String getActionTypeSap() {
		return actionTypeSap;
	}

	public void setActionTypeSap(String actionTypeSap) {
		this.actionTypeSap = actionTypeSap;
	}

	public String getDeliveyDocumentType() {
		return deliveyDocumentType;
	}

	public void setDeliveyDocumentType(String deliveyDocumentType) {
		this.deliveyDocumentType = deliveyDocumentType;
	}
	
}
