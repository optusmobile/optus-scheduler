package com.bcds.jpa.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_company_location_tree")
public class CompanyLocationTree {
	
	@Id
	@Column(name="location_id")
	private int id;
	
	private int locationDepth;
	private String locationPath;
	
	@Column(columnDefinition = "TEXT")
	private String fullLocationName;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getLocationDepth() {
		return locationDepth;
	}
	public void setLocationDepth(int locationDepth) {
		this.locationDepth = locationDepth;
	}
	public String getLocationPath() {
		return locationPath;
	}
	public void setLocationPath(String locationPath) {
		this.locationPath = locationPath;
	}
	public String getFullLocationName() {
		return fullLocationName;
	}
	public void setFullLocationName(String fullLocationName) {
		this.fullLocationName = fullLocationName;
	}
	
	

}
