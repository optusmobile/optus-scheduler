/**
 * 
 */
package com.bcds.jpa.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

/**
 * @author jason.chen
 *
 */
@Embeddable
@Data
public class AssetMovementInfoCompositeId implements Serializable {
	
	@Column(name="id")
	private int id;
	
	@Column(name="attribute_device_id")
	private int attributeDeviceId;
}
