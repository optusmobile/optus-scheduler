package com.bcds.jpa.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_company")
public class CompanyEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name="company_id")
	private int id;
	
	@Column(name="company_name")
	private String companyName;
	
	@ManyToOne
	@JoinColumn(name="licensee_id")
	private LicenseeEntity licenseeId;
	
	@ManyToOne
	@JoinColumn(name="admin_id")
	private User admin;
	
	@Column(name="update_flag")
	private Character updateFlag;
	
	@Column(name="last_update_date")
	private Date lastUpdateDate;
	
	@Column(name="is_active")
	private boolean isActive;
	
	@Column(name="delivery_address")
	private String deliveryAddress;
	
	@Column(name="postal_address")
	private String postalAddress;
	
	@Column(name="clone_company_id")
	private Integer cloneCompanyId;
	
	@Column(name="multi_scan")
	private Character multiScan;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public LicenseeEntity getLicenseeId() {
		return licenseeId;
	}

	public void setLicenseeId(LicenseeEntity licenseeId) {
		this.licenseeId = licenseeId;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public User getAdmin() {
		return admin;
	}

	public void setAdmin(User admin) {
		this.admin = admin;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public String getPostalAddress() {
		return postalAddress;
	}

	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}

	public Character getUpdateFlag() {
		return updateFlag;
	}

	public void setUpdateFlag(Character updateFlag) {
		this.updateFlag = updateFlag;
	}

	public Integer getCloneCompanyId() {
		return cloneCompanyId;
	}

	public void setCloneCompanyId(Integer cloneCompanyId) {
		this.cloneCompanyId = cloneCompanyId;
	}

	public Character getMultiScan() {
		return multiScan;
	}

	public void setMultiScan(Character multiScan) {
		this.multiScan = multiScan;
	}

}
