package com.bcds.jpa.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "tbl_interface_error")
public class InterfaceError implements Cloneable,Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	private int id;
	
	@Column(name="module")
	private String module;
	
	@Column(name="section")
	private String section;
	
	@Column(name="description", columnDefinition="TEXT")
	private String description;
	
	@Column(name="file_name")
	private String filename;
	
	@Column(name = "error_type")
	private String errorType;

	@Column(name = "processing_date", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date processingDate;

	@Column(name="file_path")
	private String filePath;
	
	
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getErrorType() {
		return errorType;
	}

	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}

	public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
	
	public InterfaceError() {
		
	}
	
	public InterfaceError(String module, String fileName, Date processingDate, String section, String description, String errorType, String filePath){
		this.module = module;
		this.filename = fileName;
		this.processingDate = processingDate;
		this.section = section;
		this.description = description;
		this.errorType = errorType;
		this.filePath = filePath;
	}
}
