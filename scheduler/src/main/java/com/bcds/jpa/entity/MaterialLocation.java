package com.bcds.jpa.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_material_location")
public class MaterialLocation  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private int id;
	
	@ManyToOne
	@JoinColumn(name="material_id")
	private CompanyAssetParts materialId;
	
	@ManyToOne
	@JoinColumn(name="location_id")
	private CompanyLocation locationId;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public CompanyAssetParts getMaterialId() {
		return materialId;
	}

	public void setMaterialId(CompanyAssetParts materialId) {
		this.materialId = materialId;
	}

	public CompanyLocation getLocationId() {
		return locationId;
	}

	public void setLocationId(CompanyLocation locationId) {
		this.locationId = locationId;
	}

}
