package com.bcds.jpa.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Data
@Entity
@Table(name = "tbl_asset")
public class Asset {
	
	@EmbeddedId
    private AssetCompositeId assetCompositeId;
	
	@Column(name="asset_type")
	private int assetType;
	
	@Column(name="tag")
	private String tag;
	
	@Column(name="barcode")
	private String barcode;
	
	@Column(name="serial_number")
	private String serialNumber;
	
	@ManyToOne
	@JoinColumn(name="part_id")
	private CompanyAssetParts companyAssetParts;
	
	@Column(name="responsible_user_id")
	private Integer responsibleUserId;
	
	@Column(name="long_desc")
	private String longDesc;
	
	@Column(name="title")
	private String title;
	
	@ManyToOne
	@JoinColumn(name="location_id")
	private CompanyLocation locationId;
	
	@Column(name="gps_lat")
	private BigDecimal  gpsLat;
	
	@Column(name="gps_long")
	private BigDecimal  gpsLong;
	
	@Column(name="category_id")
	private Integer categoryId;
	
	@Column(name = "purchase_date", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date purchaseDate;
	
	@Column(name = "asset_value")
	private BigDecimal assetValue;
	
	@Column(name = "asset_image")
	private String assetImage;

	@Column(name = "create_date", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;
	
	@Column(name="created_by")
	private int createdBy;
	
	@ManyToOne
	@JoinColumn(name="status_id")
	private AssetStatusEntity statusId;
	
	@Column(name="company_id")
	private int companyId;
	
	@ManyToOne
	@JoinColumn(name="condition_id")
	private AssetConditionEntity conditionId;
	
	@Column(name="company_asset_id")
	private String companyAssetId;
	
	@Column(name="update_flag")
	private Character updateFlag;
	
	@Column(name = "last_update_date", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdateDate;
	
	@Column(name = "img_last_update_date", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date imgLastUpdateDate;
	
	@Column(name = "last_updated_by")
	private int lastUpdatedBy;
	
	@Column(name = "previous_plant_id")
	private int previousPlantId;
	
	@Column(name = "last_inspection_date" , columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastInspectionDate;
	
	@Column(name = "weight")
	private String weight;
	
	@Column(name = "weight_uom")
	private String weightUom;
	
	@Column(name = "batch_number")
	private String batchNumber;

}
