package com.bcds.jpa.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.bcds.file.helper.MovementTypeEnum;

import lombok.Data;

@Data
@Entity
@Table(name = "tbl_asset_movement_info")
public class AssetMovementInfo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
    private AssetMovementInfoCompositeId assetMovementInfoCompositeId;
	
	@ManyToOne
    @JoinColumns({
        @JoinColumn(
            name = "asset_id",
            referencedColumnName = "asset_id"),
        @JoinColumn(
            name = "device_id",
            referencedColumnName = "device_id")
    })
    private Asset asset;
	
	@Column(name = "movement_date", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date movementDate;
	
	@Column(name="moved_by")
	private int movedBy;
	
	@Enumerated(EnumType.STRING)
	@Column(name="movement_type", columnDefinition = "ENUM('IN','GR','GI')")
	private MovementTypeEnum movementTypeEnum;
	
	@Column(name="movement_recorded_by")
	private int movementRecordedBy;
	
	@ManyToOne
	@JoinColumn(name="source_location_id")
	private CompanyLocation sourceLocation;
	
	@ManyToOne
	@JoinColumn(name="destination_location_id")
	private CompanyLocation destinationLocation;

	@Column(name="work_order_number")
	private String workOrderNumber;
	
	@Column(name="update_flag")
	private char updateFlag;
	
	@Column(name="last_update_date")
	private Date lastUpdateDate;
	
	@Column(name="updated_by")
	private int updatedBy;
	
	@ManyToOne
	@JoinColumn(name="ftp_status_id")
	private OrderStatus orderStatus;
	
	@Column(name="plant_type")
	private String plantType;
	
}
