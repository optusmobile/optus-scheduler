package com.bcds.jpa.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_asset_condition")
public class AssetConditionEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name="id")
	private int id;
	
	@Column(name="condition")
	private String condition;
	
	@ManyToOne
	@JoinColumn(name="company_id")
	private CompanyEntity compnay;
	
	@Column(name="update_flag")
	private Character updateFlag;
	
	@Column(name="last_update_date")
	private Date lastUpdateDate;
	
	@Column(name="updated_by")
	private Integer updatedBy;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public CompanyEntity getCompnay() {
		return compnay;
	}

	public void setCompnay(CompanyEntity compnay) {
		this.compnay = compnay;
	}

	public Character getUpdateFlag() {
		return updateFlag;
	}

	public void setUpdateFlag(Character updateFlag) {
		this.updateFlag = updateFlag;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

}
