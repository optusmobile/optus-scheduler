package com.bcds.jpa.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_user")
public class User {

	@Id
	@GeneratedValue
	@Column(name="user_id")
	private int id;
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="middle_name")
	private String middleName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="email")
	private String email;
	
	@Column(name="phone")
	private String phone;
	
	@Column(name="loginname")
	private String loginname;
	
	@Column(name="password")
	private String password;
	
	@Column(name="parent_user_id")
	private Integer parentUserId;
	
	@Column(name="current_device_udid")
	private String currentDeviceUdid;
	
	@Column(name="current_device_type")
	private int currentDeviceType;
	
	@Column(name="licensee_id")
	private Integer licenseeId;
	
	@Basic
	@Column(name = "is_active", columnDefinition = "BIT", length = 1)
	private byte isActive;
	
	@Column(name="password_generator_number")
	private String passwordGeneratorNumber;
	
	@Column(name="is_web_user")
	private boolean isWebUser;
	
	@Column(name="is_device_user")
	private char isDeviceUser;
	
	@Column(name="is_inspection_user")
	private char isInspectionUser;
	
	@Column(name="is_maintenance_user")
	private char isMaintenanceUser;
	
	@Column(name="is_mobile_device_user")
	private boolean isMobileDeviceUser;
	
	@Column(name="is_rhd_mt_user")
	private boolean isRhdMtUser;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getLoginname() {
		return loginname;
	}
	public void setLoginname(String loginname) {
		this.loginname = loginname;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getCurrentDeviceUdid() {
		return currentDeviceUdid;
	}
	public void setCurrentDeviceUdid(String currentDeviceUdid) {
		this.currentDeviceUdid = currentDeviceUdid;
	}
	public int getCurrentDeviceType() {
		return currentDeviceType;
	}
	public void setCurrentDeviceType(int currentDeviceType) {
		this.currentDeviceType = currentDeviceType;
	}
	
	public String getPasswordGeneratorNumber() {
		return passwordGeneratorNumber;
	}
	public void setPasswordGeneratorNumber(String passwordGeneratorNumber) {
		this.passwordGeneratorNumber = passwordGeneratorNumber;
	}

	public char isDeviceUser() {
		return isDeviceUser;
	}
	public void setDeviceUser(char isDeviceUser) {
		this.isDeviceUser = isDeviceUser;
	}
	public char isInspectionUser() {
		return isInspectionUser;
	}
	public void setInspectionUser(char isInspectionUser) {
		this.isInspectionUser = isInspectionUser;
	}
	public char isMaintenanceUser() {
		return isMaintenanceUser;
	}
	public void setMaintenanceUser(char isMaintenanceUser) {
		this.isMaintenanceUser = isMaintenanceUser;
	}
	public Integer getParentUserId() {
		return parentUserId;
	}
	public void setParentUserId(Integer parentUserId) {
		this.parentUserId = parentUserId;
	}
	public Integer getLicenseeId() {
		return licenseeId;
	}
	public void setLicenseeId(Integer licenseeId) {
		this.licenseeId = licenseeId;
	}
	public byte getIsActive() {
		return isActive;
	}
	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}
	public boolean isWebUser() {
		return isWebUser;
	}
	public void setWebUser(boolean isWebUser) {
		this.isWebUser = isWebUser;
	}
	public char getIsDeviceUser() {
		return isDeviceUser;
	}
	public void setIsDeviceUser(char isDeviceUser) {
		this.isDeviceUser = isDeviceUser;
	}
	public char getIsInspectionUser() {
		return isInspectionUser;
	}
	public void setIsInspectionUser(char isInspectionUser) {
		this.isInspectionUser = isInspectionUser;
	}
	public char getIsMaintenanceUser() {
		return isMaintenanceUser;
	}
	public void setIsMaintenanceUser(char isMaintenanceUser) {
		this.isMaintenanceUser = isMaintenanceUser;
	}
	public boolean isMobileDeviceUser() {
		return isMobileDeviceUser;
	}
	public void setMobileDeviceUser(boolean isMobileDeviceUser) {
		this.isMobileDeviceUser = isMobileDeviceUser;
	}
	public boolean isRhdMtUser() {
		return isRhdMtUser;
	}
	public void setRhdMtUser(boolean isRhdMtUser) {
		this.isRhdMtUser = isRhdMtUser;
	}
	
}
