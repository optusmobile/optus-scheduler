package com.bcds.jpa.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "tbl_order_relationship")
public class OrderRelationshipEntity  implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="parent_order_id")
	private Order parentOrderId;
	
	@ManyToOne
	@JoinColumn(name="child_order_id")
	private Order childOrderId;
	
	@Column(name = "last_update_date", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdateDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Order getParentOrderId() {
		return parentOrderId;
	}

	public void setParentOrderId(Order parentOrderId) {
		this.parentOrderId = parentOrderId;
	}

	public Order getChildOrderId() {
		return childOrderId;
	}

	public void setChildOrderId(Order childOrderId) {
		this.childOrderId = childOrderId;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

}
