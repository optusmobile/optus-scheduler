package com.bcds.jpa.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "tbl_pid_list")
public class PhysicalInventoryDocumentList  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private int id;
	
	@ManyToOne
	@JoinColumn(name="pid_id")
	private PhysicalInventoryDocument pid;
	
	@ManyToOne
	@JoinColumn(name="location_id")
	private CompanyLocation location;
	
	@ManyToOne
	@JoinColumn(name="part_id")
	private CompanyAssetParts companyAssetParts;
	
	@Column(name = "expected_quantity")
	private Integer expectedQuantity;
	
	@Column(name = "actual_quantity")
	private Integer actualQuantity;
	
	@Column(name = "line_number")
	private String lineNumber;
	
	@Column(name = "last_update_date", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdatedDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public PhysicalInventoryDocument getPid() {
		return pid;
	}

	public void setPid(PhysicalInventoryDocument pid) {
		this.pid = pid;
	}

	public CompanyLocation getLocation() {
		return location;
	}

	public void setLocation(CompanyLocation location) {
		this.location = location;
	}
	
	public Integer getExpectedQuantity() {
		return expectedQuantity;
	}

	public void setExpectedQuantity(Integer expectedQuantity) {
		this.expectedQuantity = expectedQuantity;
	}

	public Integer getActualQuantity() {
		return actualQuantity;
	}

	public void setActualQuantity(Integer actualQuantity) {
		this.actualQuantity = actualQuantity;
	}

	public String getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(String lineNumber) {
		this.lineNumber = lineNumber;
	}

	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	
	public CompanyAssetParts getCompanyAssetParts() {
		return companyAssetParts;
	}

	public void setCompanyAssetParts(CompanyAssetParts companyAssetParts) {
		this.companyAssetParts = companyAssetParts;
	}

}
