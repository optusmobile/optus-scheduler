package com.bcds.jpa.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "tbl_company_location")
public class CompanyLocation implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name="location_id")
	private int id;
	
	@Column(name="location_name")
	private String locationName;
	
	@Column(name="is_root_location")
	private boolean isRootLocation;
	
	//data error due to root=0 and there is no 0 in the table
	//@OneToOne
	//@JoinColumn(name="parent_location_id")
	@Column(name="parent_location_id")
	private int parentLocationId;
	
	@ManyToOne
	@JoinColumn(name="company_id")
	private CompanyEntity companyId;
	
	@ManyToOne
	@JoinColumn(name="responsible_user_id")
	private User responsibleUserId;
	
	@Column(name="location_address")
	private String locationAddress;
	
	@Column(name="rfid_reader_id")
	private String rfidReaderId;
	
	@Column(name="clone_location_id")
	private Integer cloneLocationId;
	
	@Column(name="tag")
	private String tag;
	
	@Column(name="barcode")
	private String barcode;
	
	@Column(name="update_flag")
	private Character updateFlag;
	
	@Column(name = "last_update_date", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdateDate;
	
	@Column(name="location_type")
	private String locationType;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public boolean isRootLocation() {
		return isRootLocation;
	}

	public void setRootLocation(boolean isRootLocation) {
		this.isRootLocation = isRootLocation;
	}

	public int getParentLocationId() {
		return parentLocationId;
	}

	public void setParentLocation(int parentLocationId) {
		this.parentLocationId = parentLocationId;
	}

	public CompanyEntity getCompanyId() {
		return companyId;
	}

	public void setCompanyId(CompanyEntity companyId) {
		this.companyId = companyId;
	}

	public void setParentLocationId(int parentLocationId) {
		this.parentLocationId = parentLocationId;
	}

	public User getResponsibleUserId() {
		return responsibleUserId;
	}

	public void setResponsibleUserId(User responsibleUserId) {
		this.responsibleUserId = responsibleUserId;
	}

	public String getLocationAddress() {
		return locationAddress;
	}

	public void setLocationAddress(String locationAddress) {
		this.locationAddress = locationAddress;
	}

	public String getRfidReaderId() {
		return rfidReaderId;
	}

	public void setRfidReaderId(String rfidReaderId) {
		this.rfidReaderId = rfidReaderId;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public Character getUpdateFlag() {
		return updateFlag;
	}

	public void setUpdateFlag(Character updateFlag) {
		this.updateFlag = updateFlag;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public Integer getCloneLocationId() {
		return cloneLocationId;
	}

	public void setCloneLocationId(Integer cloneLocationId) {
		this.cloneLocationId = cloneLocationId;
	}

	public String getLocationType() {
		return locationType;
	}

	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}
}
