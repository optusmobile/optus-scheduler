package com.bcds.jpa.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "tbl_order")
public class Order implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private int id;
	
	@ManyToOne
	@JoinColumn(name="order_type_id")
	private OrderType orderType;
	
	@Column(name = "order_number")
	private String orderNumber;
	
	@ManyToOne
	@JoinColumn(name="order_action_type_id")
	private OrderActionType orderActionType;
	
	@ManyToOne
	@JoinColumn(name="location_id")
	private CompanyLocation companyLocation;
	
	@Column(name = "order_receive_date", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date orderReceiveDate;
	
	@ManyToOne
	@JoinColumn(name="order_status_id")
	private OrderStatus orderStatus;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "order", cascade = CascadeType.ALL)
	private List<OrderMaterialList> orderMeterialList = new ArrayList<OrderMaterialList>();
	
	@Column(name = "last_update_date", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdateDate;
	
	@ManyToOne
	@JoinColumn(name="updated_by")
	private User user;
	
	@Column(name = "mrf_ref")
	private String mrfRef;
	
	@Column(name = "mer_ref")
	private String merRef;
	
	@Column(name = "cross_ref")
	private String crossRef;
	
	@Column(name = "serial_number_ref")
	private String serialNumberRef;
	
	@Column(name = "dd_type")
	private String ddType;
	
	@Column(name = "ref4")
	private String ref4;
	
	public Integer getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public OrderType getOrderType() {
		return orderType;
	}
	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public OrderActionType getOrderActionType() {
		return orderActionType;
	}
	public void setOrderActionType(OrderActionType orderActionType) {
		this.orderActionType = orderActionType;
	}
	public CompanyLocation getCompanyLocation() {
		return companyLocation;
	}
	public void setCompanyLocation(CompanyLocation companyLocation) {
		this.companyLocation = companyLocation;
	}
	public Date getOrderReceiveDate() {
		return orderReceiveDate;
	}
	public void setOrderReceiveDate(Date orderReceiveDate) {
		this.orderReceiveDate = orderReceiveDate;
	}
	public OrderStatus getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getMrfRef() {
		return mrfRef;
	}
	public void setMrfRef(String mrfRef) {
		this.mrfRef = mrfRef;
	}
	public String getMerRef() {
		return merRef;
	}
	public void setMerRef(String merRef) {
		this.merRef = merRef;
	}
	
	public String getCrossRef() {
		return crossRef;
	}
	public void setCrossRef(String crossRef) {
		this.crossRef = crossRef;
	}
	public String getSerialNumberRef() {
		return serialNumberRef;
	}
	public void setSerialNumberRef(String serialNumberRef) {
		this.serialNumberRef = serialNumberRef;
	}
	public String getDdType() {
		return ddType;
	}
	public void setDdType(String ddType) {
		this.ddType = ddType;
	}
	public String getRef4() {
		return ref4;
	}
	public void setRef4(String ref4) {
		this.ref4 = ref4;
	}
	public List<OrderMaterialList> getOrderMeterialList() {
		return orderMeterialList;
	}
	public void setOrderMeterialList(List<OrderMaterialList> orderMeterialList) {
		this.orderMeterialList = orderMeterialList;
	}
}
