package com.bcds.jpa.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Entity
@Table(name = "tbl_sap_stock_variance")
@Data
public class StockVarianceEntity  implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	private int id;
	
	@ManyToOne
	@JoinColumn(name="material_id")	
	private CompanyAssetParts companyAssetParts;
	
//	@Column(name = "plant_name")
//	private String plantName;
	
	@ManyToOne
	@JoinColumn(name="location_id")
	private CompanyLocation companyLocation;
	
	@Column(name = "sap_quantity")
	private Integer sapQuantity;
	
	@Column(name = "intrack_quantity")
	private Integer intrackQuantity;
	
	@Column(name = "stock_type")
	private String stockType;
	
	@Column(name = "stock_received_date", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date stockReceiveDate;
	
	@Column(name = "batch_name")
	private String batchName;
	
	@Column(name = "plant_name")
	private String plantName;
}
