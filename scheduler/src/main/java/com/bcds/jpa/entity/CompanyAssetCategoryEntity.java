package com.bcds.jpa.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_company_asset_category")
public class CompanyAssetCategoryEntity  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="id")
	private int id;
	
	@Column(name="category_name")
	private String categoryName;
	
	@ManyToOne
	@JoinColumn(name="responsible_user_id")
	private User responsibleUserId;
	
	@ManyToOne
	@JoinColumn(name="company_id")
	private CompanyEntity company;
	
	@Column(name="parent_cat_id")
	private Integer parentCatId;
	
	@Column(name="update_flag")
	private char updateFlag;
	
	@Column(name="last_update_date")
	private Date lastUpdateDate;
	
	@Column(name="updated_by")
	private int updatedBy;
	
	@Column(name="is_root_category")
	private boolean isRootCategory;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public User getResponsibleUserId() {
		return responsibleUserId;
	}

	public void setResponsibleUserId(User responsibleUserId) {
		this.responsibleUserId = responsibleUserId;
	}

	public CompanyEntity getCompany() {
		return company;
	}

	public void setCompany(CompanyEntity company) {
		this.company = company;
	}

	public Integer getParentCatId() {
		return parentCatId;
	}

	public void setParentCatId(Integer parentCatId) {
		this.parentCatId = parentCatId;
	}

	public char getUpdateFlag() {
		return updateFlag;
	}

	public void setUpdateFlag(char updateFlag) {
		this.updateFlag = updateFlag;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public int getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}

	public boolean isRootCategory() {
		return isRootCategory;
	}

	public void setRootCategory(boolean isRootCategory) {
		this.isRootCategory = isRootCategory;
	}

}
