/**
 * 
 */
package com.bcds.jpa.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

/**
 * @author jason.chen
 *
 */
@Embeddable
@Data
public class AssetCompositeId implements Serializable {
	
	@Column(name="asset_id")
	private int assetId;
	
	@Column(name="device_id")
	private int deviceId;
}