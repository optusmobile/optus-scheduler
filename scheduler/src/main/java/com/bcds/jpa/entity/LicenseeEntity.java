package com.bcds.jpa.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_licensee")
public class LicenseeEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="licensee_id")
	private int id;
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private User user;
	
	@Column(name="organization_name")
	private String organizationName;
	
	@Column(name="delivery_address")
	private String deliveryAddress;
	
	@Column(name="postal_address")
	private String postalAddress;
	
	@Column(name="numbers_of_web_users")
	private Integer numbersOfWebUsers;
	
	@Column(name="numbers_of_device_users")
	private Integer numbersOfDeviceUsers;
	
	@Column(name="numbers_of_inspection_users")
	private Integer numbersOfInspectionUsers;
	
	@Column(name="numbers_of_maintenance_users")
	private Integer numbersOfmaintenanceUsers;
	
	@Column(name="licensee_logo")
	private String licenseeLogo;
	
	@Column(name="numbers_of_company")
	private Integer numbersOfCompany;
	
	@Column(name="numbers_of_location")
	private Integer numbersOfLocation;
	
	@Column(name="numbers_of_asset")
	private Integer numbersOfAsssets;
	
	@Column(name="account_expire_date")
	private Date accountExpireDate;
	
	@Basic
	@Column(name = "account_type", columnDefinition = "BIT", length = 1)
	private byte accountType;
	
	@Column(name="licensee_key")
	private String licenseeKey;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public String getPostalAddress() {
		return postalAddress;
	}

	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}

	public Integer getNumbersOfWebUsers() {
		return numbersOfWebUsers;
	}

	public void setNumbersOfWebUsers(Integer numbersOfWebUsers) {
		this.numbersOfWebUsers = numbersOfWebUsers;
	}

	public Integer getNumbersOfDeviceUsers() {
		return numbersOfDeviceUsers;
	}

	public void setNumbersOfDeviceUsers(Integer numbersOfDeviceUsers) {
		this.numbersOfDeviceUsers = numbersOfDeviceUsers;
	}

	public Integer getNumbersOfInspectionUsers() {
		return numbersOfInspectionUsers;
	}

	public void setNumbersOfInspectionUsers(Integer numbersOfInspectionUsers) {
		this.numbersOfInspectionUsers = numbersOfInspectionUsers;
	}

	public Integer getNumbersOfmaintenanceUsers() {
		return numbersOfmaintenanceUsers;
	}

	public void setNumbersOfmaintenanceUsers(Integer numbersOfmaintenanceUsers) {
		this.numbersOfmaintenanceUsers = numbersOfmaintenanceUsers;
	}

	public String getLicenseeLogo() {
		return licenseeLogo;
	}

	public void setLicenseeLogo(String licenseeLogo) {
		this.licenseeLogo = licenseeLogo;
	}

	public Integer getNumbersOfCompany() {
		return numbersOfCompany;
	}

	public void setNumbersOfCompany(Integer numbersOfCompany) {
		this.numbersOfCompany = numbersOfCompany;
	}

	public Integer getNumbersOfLocation() {
		return numbersOfLocation;
	}

	public void setNumbersOfLocation(Integer numbersOfLocation) {
		this.numbersOfLocation = numbersOfLocation;
	}

	public Integer getNumbersOfAsssets() {
		return numbersOfAsssets;
	}

	public void setNumbersOfAsssets(Integer numbersOfAsssets) {
		this.numbersOfAsssets = numbersOfAsssets;
	}

	public Date getAccountExpireDate() {
		return accountExpireDate;
	}

	public void setAccountExpireDate(Date accountExpireDate) {
		this.accountExpireDate = accountExpireDate;
	}

	public byte getAccountType() {
		return accountType;
	}

	public void setAccountType(byte accountType) {
		this.accountType = accountType;
	}

	public String getLicenseeKey() {
		return licenseeKey;
	}

	public void setLicenseeKey(String licenseeKey) {
		this.licenseeKey = licenseeKey;
	}

}
