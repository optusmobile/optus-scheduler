package com.bcds.jpa.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * PhysicalInventoryDocument entity class to store the PID and PID list details in the database.
 */

@Entity
@Table(name = "tbl_pid")
public class PhysicalInventoryDocument implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private int id;
	
	@Column(name = "pid_number")
	private String pidNumber;
	
	@Column(name = "fiscal_year")
	private String fiscalYear;
	
	@Column(name = "date_of_last_count", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateOfLastCount;
	
	@ManyToOne
	@JoinColumn(name="status_id")
	private OrderStatus orderStatus;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "pid", cascade = CascadeType.ALL)
	private List<PhysicalInventoryDocumentList> pidList = new ArrayList<PhysicalInventoryDocumentList>();
	
	@Column(name = "last_update_date", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdatedDate;
	
	@ManyToOne
	@JoinColumn(name="location_id")
	private CompanyLocation location;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDateOfLastCount() {
		return dateOfLastCount;
	}

	public void setDateOfLastCount(Date dateOfLastCount) {
		this.dateOfLastCount = dateOfLastCount;
	}

	public OrderStatus getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}

	public List<PhysicalInventoryDocumentList> getPidList() {
		return pidList;
	}

	public void setPidList(List<PhysicalInventoryDocumentList> pidList) {
		this.pidList = pidList;
	}

	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getPidNumber() {
		return pidNumber;
	}

	public void setPidNumber(String pidNumber) {
		this.pidNumber = pidNumber;
	}

	public String getFiscalYear() {
		return fiscalYear;
	}

	public void setFiscalYear(String fiscalYear) {
		this.fiscalYear = fiscalYear;
	}

	public CompanyLocation getLocation() {
		return location;
	}

	public void setLocation(CompanyLocation location) {
		this.location = location;
	}

}
