package com.bcds.service.iface;

import java.util.List;
import java.util.Map;
import com.bcds.jobs.beans.ReturnTransactionBean;

public interface ReturnTransactionsService {
	
	List<ReturnTransactionBean> findAllFinishedTransactions();
	
	public void updateReturnMovementStatusToClosed(ReturnTransactionBean returnTransactionBean);
	
	public Map<String,List<ReturnTransactionBean>> returnTransBeanMap();
}
