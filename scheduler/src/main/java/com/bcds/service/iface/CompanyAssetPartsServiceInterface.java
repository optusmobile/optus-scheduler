package com.bcds.service.iface;

import javax.persistence.EntityNotFoundException;
import com.bcds.jobs.beans.CompanyAssetParts;

public interface CompanyAssetPartsServiceInterface {
	
	public CompanyAssetParts addCompanyAssetPart(CompanyAssetParts compnayassetParts) throws EntityNotFoundException;

}
