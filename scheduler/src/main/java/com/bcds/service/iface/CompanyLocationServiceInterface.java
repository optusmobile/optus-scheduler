package com.bcds.service.iface;

import java.util.List;
import com.bcds.jobs.beans.CompanyLocation;

public interface CompanyLocationServiceInterface {

	List<CompanyLocation> getCompanyLicationByUserId(String userid);

	List<com.bcds.jpa.entity.CompanyLocation> findByTagAndIsRootLocation(String tag, boolean isRootLocation);
}
