package com.bcds.service.iface;

import com.bcds.jpa.entity.CompanyLocationTree;

public interface CompanyServiceInterface {

	public CompanyLocationTree findPlantLocation(int locationId);
}
