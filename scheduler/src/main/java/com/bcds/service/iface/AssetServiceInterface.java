package com.bcds.service.iface;

import java.util.List;
import javax.persistence.EntityNotFoundException;
import com.bcds.file.helper.AssetConditionEnum;
import com.bcds.file.helper.AssetStatusEnum;
import com.bcds.jobs.beans.AssetMovementInfoBean;
import com.bcds.jobs.exception.BCDSDataException;
import com.bcds.jpa.entity.Asset;
import com.bcds.jpa.entity.AssetMovementInfo;
import com.bcds.jpa.entity.PhysicalInventoryDocument;
import com.bcds.jpa.model.mm01.MM01;

public interface AssetServiceInterface {

	public List<AssetMovementInfo> fillIN03Records();

	public void updateAssetMovementStatusToClosed(AssetMovementInfoBean assetMovementInfoBean);
	
	public void updatePIDStatusToClosed(PhysicalInventoryDocument physicalInventoryDocument);

	public Asset findAssetByLocationIdAndConditionIdAndStatusId(String meterialNumber, String plant, 
			AssetConditionEnum assetCondition, AssetStatusEnum assetStatusO) throws EntityNotFoundException;
	
	public Asset updateAsset(Asset asset);
	
	public List<AssetMovementInfoBean> findAllFinishedAssets();
	
	public List<Asset> getAssetsAssociatedWithZ1NL(String barcode);
		
		
}
