package com.bcds.service.iface;

import java.io.File;
import java.util.List;

import com.bcds.jobs.beans.StockVarianceBean;
import com.bcds.jobs.exception.BCDSDataException;
import com.bcds.jobs.exception.FileProcessingException;
import com.bcds.jpa.entity.StockVarianceEntity;
import com.bcds.jpa.model.r01.R01;

public interface StockVarianceServiceInterface {

	/**
	 * @param r01
	 * @throws FileProcessingException
	 */
	public void addSapStock(R01 r01) throws FileProcessingException;

	/**
	 * 
	 */
	public void updateIntrackQuantity() throws BCDSDataException;

	/**
	 * @return
	 */
	public File generateSapStockVarianceReport(List<StockVarianceEntity> stockVarianceList);

	public List<StockVarianceEntity> getStockVarianceList();

	public void addIntrackStock();

	/**
	 * @param locationId
	 * @return
	 */
	public List<StockVarianceBean> getStockVarianceListForInstallByLocationId(int locationId, String plantName);

	/**
	 * @param stockVarianceList
	 * @return
	 */
	public File generateSapStockVarianceReportForInstall(List<StockVarianceBean> stockVarianceList);
}
