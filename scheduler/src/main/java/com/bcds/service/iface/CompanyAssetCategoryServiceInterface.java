package com.bcds.service.iface;

import javax.persistence.EntityNotFoundException;
import com.bcds.jobs.beans.CompanyAssetCategoryBean;
import com.bcds.jpa.entity.CompanyAssetCategoryEntity;

public interface CompanyAssetCategoryServiceInterface {
	
	public CompanyAssetCategoryBean addCompanyAssetCategory(CompanyAssetCategoryBean companyAssetCategory) throws EntityNotFoundException;

	/**
	 * @param categoryName
	 * @param parentCatId
	 * @return
	 * @throws EntityNotFoundException
	 */
	public CompanyAssetCategoryBean findByCategoryNameAndParentCatId(String categoryName, Integer parentCatId) throws EntityNotFoundException;

}
