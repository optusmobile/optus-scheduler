package com.bcds.service.iface;

import java.util.List;
import com.bcds.file.helper.OrderStatusEnum;
import com.bcds.jobs.beans.OrderActionType;
import com.bcds.jobs.beans.OrderRelationshipBean;
import com.bcds.jobs.exception.BCDSDataException;
import com.bcds.jobs.exception.FileProcessingException;
import com.bcds.jpa.entity.Order;
import com.bcds.jpa.entity.OrderMaterialList;
import com.bcds.jpa.model.in01.IN01;

public interface OrderServiceInterface {
	
	public void insertIN01Order(IN01 in01) throws BCDSDataException,FileProcessingException;
	
	public List<com.bcds.jobs.beans.Order> fillIN02Records();
	
	public void updateOrderStatusToClosed(com.bcds.jobs.beans.Order order);
	
	public List<OrderMaterialList> getOrderMaterialList(Order order);
	
	public List<com.bcds.jobs.beans.Order> filterOrderBySCPOAndA7();
	
	public Order findOrderByOrderId(String orderId);
	
	public List<Order> findOrdersByMrfCodeAndStatus(OrderStatusEnum orderStatus, String mrfCode);
	
	public List<Order> findByOrderTypeAndOrderActionTypeAndMrfCode(String orderType, String orderActionType, 
			String mrfCode, OrderStatusEnum orderStatus);
	
	public List<com.bcds.jobs.beans.Order> filterOrderBySCPOAndA5();
	
	public List<OrderRelationshipBean> findAllDNs();
	
	public Order findByOrderNumberAndLocationIdAndOrderTypeIdAndOrderActionTypeidAndOrderStatus(String orderNumber, 
																								String barcode, 
																								String orderActionName, 
																								String orderActionTypeName,
																								String orderStatus);
	
	public List<com.bcds.jobs.beans.Order> getAllDDsNotAssociatedWithDNS();
	
	public OrderActionType getByActionTypeAndDocType(String orderActionType, String docType);
	
	public List<com.bcds.jobs.beans.Order> findAllFinishedDNs();
	
	public void updateZ1NL(com.bcds.jobs.beans.Order order);

	public List<com.bcds.jobs.beans.Order> getAllDDNotAssociatedWithDN();

}
