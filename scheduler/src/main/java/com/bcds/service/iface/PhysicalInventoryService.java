package com.bcds.service.iface;

import javax.persistence.EntityNotFoundException;

import com.bcds.jobs.exception.BCDSDataException;
import com.bcds.jobs.exception.FileProcessingException;
import com.bcds.jpa.model.in10.IN10;

/**
 * @author arpita.das
 *
 */
public interface PhysicalInventoryService {
	
	public void addPhysicalInventoryDocument(IN10 in10) throws BCDSDataException,FileProcessingException, EntityNotFoundException;
}
