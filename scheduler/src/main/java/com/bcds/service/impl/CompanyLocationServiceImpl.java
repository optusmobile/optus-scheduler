package com.bcds.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bcds.jobs.beans.CompanyLocation;
import com.bcds.jobs.beans.EntityToBeanClass;
import com.bcds.jobs.dao.CompanyLocationDao;
import com.bcds.jpa.repo.CompanyLocationRepository;
import com.bcds.service.iface.CompanyLocationServiceInterface;

@Service("companyLocationService")
public class CompanyLocationServiceImpl implements CompanyLocationServiceInterface{
	
	@Autowired
	CompanyLocationDao companyLocationDao;
	
	@Autowired
	EntityToBeanClass entityToBeanClass;
	
	@Autowired
	private CompanyLocationRepository companyLocationRepo;

	@Override
	public List<CompanyLocation> getCompanyLicationByUserId(String userid) {
		List<com.bcds.jpa.entity.CompanyLocation> companyLocationEntityList = companyLocationDao.getCompanyLicationByUserId(userid);
		List<CompanyLocation> companyLocatioList= new ArrayList<CompanyLocation>();
		for (com.bcds.jpa.entity.CompanyLocation companyLocationEntity:companyLocationEntityList) {
			companyLocatioList.add(entityToBeanClass.convertEntityToBean(companyLocationEntity));
		}
		return companyLocatioList;
	}
	
	
	@Override
	public List<com.bcds.jpa.entity.CompanyLocation> findByTagAndIsRootLocation(String tag, boolean isRootLocation){
		return companyLocationRepo.findByTagAndIsRootLocation(tag, isRootLocation);
	}
}
