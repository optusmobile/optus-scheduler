package com.bcds.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bcds.jpa.entity.CompanyLocation;
import com.bcds.jpa.entity.CompanyLocationTree;
import com.bcds.jpa.repo.CompanyLocationTreeRepository;
import com.bcds.service.iface.CompanyServiceInterface;

@Service("companyService")
public class CompanyServiceImpl implements CompanyServiceInterface{

	@Autowired
	CompanyLocationTreeRepository companyLocationTreeRepo;
	
	@Override
	public CompanyLocationTree findPlantLocation(int locationId) {
	
		CompanyLocationTree tree = companyLocationTreeRepo.findOne(Integer.valueOf(locationId));
		String path = tree.getLocationPath();
		String plantId = path;
		if (path.contains("-")) {
		  plantId = path.substring(0, path.indexOf("-"));
		}
		return tree;
	}

}
