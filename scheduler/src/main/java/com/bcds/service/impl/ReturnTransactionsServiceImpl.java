package com.bcds.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bcds.jobs.beans.EntityToBeanClass;
import com.bcds.jobs.beans.ReturnTransactionBean;
import com.bcds.jobs.dao.ReturnTransactionDao;
import com.bcds.jpa.entity.OrderStatus;
import com.bcds.jpa.entity.ReturnTransactionsEntity;
import com.bcds.jpa.repo.OrderStatusRepository;
import com.bcds.jpa.repo.ReturnTransactionsRepo;
import com.bcds.service.iface.ReturnTransactionsService;

@Service("returnTransactionsService")
public class ReturnTransactionsServiceImpl implements ReturnTransactionsService {
	
	@Autowired
	EntityToBeanClass entityToBeanClass;
	
	@Autowired
	ReturnTransactionDao returnTransactionDao;
	
	@Autowired
	OrderStatusRepository orderStatusRepo;
	
	@Autowired
	ReturnTransactionsRepo returnTransactionsRepo;

	@Override
	@Transactional
	public List<ReturnTransactionBean> findAllFinishedTransactions() {
		List<ReturnTransactionsEntity> returnTransactionEnityList = returnTransactionDao.filterByOrderStatus();
		List<ReturnTransactionBean> returnTransactionBeanList = new ArrayList<ReturnTransactionBean>();
		for (ReturnTransactionsEntity returnTransactionEnity : returnTransactionEnityList) {
			returnTransactionBeanList.add(entityToBeanClass.convertEntityToBean(returnTransactionEnity));
		}
		return returnTransactionBeanList;
	}

	@Override
	public Map<String,List<ReturnTransactionBean>> returnTransBeanMap() {
		List<ReturnTransactionBean> listOfReturnTransBean = this.findAllFinishedTransactions();
		Map<String,List<ReturnTransactionBean>> returnTransBeanMap = new HashMap<String,List<ReturnTransactionBean>>();
		List<ReturnTransactionBean> returnTransBeanList = new ArrayList<ReturnTransactionBean>();
		for(ReturnTransactionBean returnTransactionBean: listOfReturnTransBean){
			returnTransBeanList = returnTransBeanMap.get(returnTransactionBean.getOrder().getOrderNumber());
			if (returnTransBeanList == null) {
				returnTransBeanList = new ArrayList<ReturnTransactionBean>();
			}
			returnTransBeanList.add(returnTransactionBean);
			returnTransBeanMap.put(returnTransactionBean.getOrder().getOrderNumber(), returnTransBeanList);
		}
		return returnTransBeanMap;
	}
	
	@Override
	@Transactional
	public void updateReturnMovementStatusToClosed(ReturnTransactionBean returnTransactionBean) {
		ReturnTransactionsEntity returnTransactionEntityReturned = returnTransactionDao.getReturnById(returnTransactionBean.getId());
		OrderStatus orderStatus = orderStatusRepo.findByOrderStatus("CLOSED");
		returnTransactionEntityReturned.setOrderStatus(orderStatus);
		returnTransactionsRepo.save(returnTransactionEntityReturned);
	}

}


