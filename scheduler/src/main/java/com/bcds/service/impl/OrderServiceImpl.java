package com.bcds.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bcds.file.helper.AssetConditionEnum;
import com.bcds.file.helper.AssetStatusEnum;
import com.bcds.file.helper.OrderStatusEnum;
import com.bcds.jobs.beans.BeanToEntityClass;
import com.bcds.jobs.beans.EntityToBeanClass;
import com.bcds.jobs.beans.OrderDDBean;
import com.bcds.jobs.beans.OrderRelationshipBean;
import com.bcds.jobs.dao.OrderDao;
import com.bcds.jobs.exception.BCDSDataException;
import com.bcds.jobs.exception.FileProcessingException;
import com.bcds.jpa.entity.Asset;
import com.bcds.jpa.entity.AssetConditionEntity;
import com.bcds.jpa.entity.AssetStatusEntity;
import com.bcds.jpa.entity.CompanyLocation;
import com.bcds.jpa.entity.Order;
import com.bcds.jpa.entity.OrderActionType;
import com.bcds.jpa.entity.OrderMaterialList;
import com.bcds.jpa.entity.OrderRelationshipEntity;
import com.bcds.jpa.entity.OrderStatus;
import com.bcds.jpa.entity.OrderType;
import com.bcds.jpa.model.in01.IN01;
import com.bcds.jpa.model.in01.IN01OrderHeader;
import com.bcds.jpa.model.in01.IN01OrderLine;
import com.bcds.jpa.repo.AssetConditionRepo;
import com.bcds.jpa.repo.AssetRepository;
import com.bcds.jpa.repo.AssetStatusRepo;
import com.bcds.jpa.repo.CompanyAssetPartsRepository;
import com.bcds.jpa.repo.CompanyLocationRepository;
import com.bcds.jpa.repo.OrderActionTypeRepository;
import com.bcds.jpa.repo.OrderMaterialListRepository;
import com.bcds.jpa.repo.OrderRelationshopRepo;
import com.bcds.jpa.repo.OrderRepository;
import com.bcds.jpa.repo.OrderStatusRepository;
import com.bcds.jpa.repo.OrderTypeRepository;
import com.bcds.jpa.repo.UserRepository;
import com.bcds.service.iface.AssetServiceInterface;
import com.bcds.service.iface.CompanyServiceInterface;
import com.bcds.service.iface.OrderServiceInterface;

@Service("orderService")
public class OrderServiceImpl implements OrderServiceInterface {
	
	final static Logger logger = Logger.getLogger(OrderServiceImpl.class);
	
	private static final String SCPO = "SCPO";
	private static final String A1= "A.1";
	private static final String A2 = "A.2";
	private static final String A5 = "A.5";
	private static final String SPARE = "SPARE";
	private static final String VENDOR = "VENDOR";
	private static final String UB = "UB";
	private static final String USED = "USED";
	
	@Autowired
	OrderRepository orderRepo;
	
	@Autowired
	OrderStatusRepository orderStatusRepo;

	@Autowired 
	OrderMaterialListRepository orderMaterialListRepo;
	
	@Autowired
	OrderTypeRepository orderTypeRepo;
	
	@Autowired
	OrderActionTypeRepository orderActionTypeRepo;
	
	@Autowired
	CompanyAssetPartsRepository companyAssetPartsRepo;
	
	@Autowired
	UserRepository userRepo;
	
	@Autowired
	CompanyLocationRepository companyLocationRepo;
	
	@Autowired
	CompanyServiceInterface companyService;
	
	@Autowired
	AssetRepository assetRepo;
	
	@Autowired
	AssetConditionRepo assetConditionRepo;
	
	@Autowired
	OrderRelationshopRepo orderRelationshipRepo;
	
	@Autowired
	OrderDao orderDao;
	
	@Autowired
	AssetStatusRepo assetStatusRepo;
	
	@Autowired
	AssetServiceInterface assetService;
	
	@Autowired
	BeanToEntityClass beanToEntityClass;
	
	@Autowired
	EntityToBeanClass entityToBeanClass;

	public OrderDao getOrderDao() {
		return orderDao;
	}

	public void setOrderDao(OrderDao orderDao) {
		this.orderDao = orderDao;
	}
	
	@PersistenceContext
	public EntityManager entityManager;
	
	 public EntityManager getEntityManager() {
	        return entityManager;
	    }

	@Override
	public void insertIN01Order(IN01 in01) throws BCDSDataException,FileProcessingException{
		
		IN01OrderHeader in01OrderHeader = null;
		String assetType ="";
		String isA5GI = "";
		if(in01 != null) {
			in01OrderHeader = in01.getIn01OrderHeader();
		}
		if (in01OrderHeader != null) {
					String docType = in01OrderHeader.getDocumentType();
					String docNumber = in01OrderHeader.getDocumentNumber();
					assetType = in01OrderHeader.getAssetTypeHeader();
					isA5GI = in01OrderHeader.getA5GIIndicator();
					
		}
		List<IN01OrderLine> in01OrderLines = in01.getIn01OrderLines();
		for (IN01OrderLine line: in01OrderLines){
			logger.info(">>>>>>>>>> " + line.getMaterialNumber());
			String metrialNumber = line.getMaterialNumber();
			String mescod = line.getMescod();
			String plant = line.getPlant();
			String location = line.getsLoc();
			String quantity = line.getQuantity();
			com.bcds.jobs.beans.Order orderBean = line.getOrder();
			int metiralNumnerInt = 0;
			boolean isUpdateAssetEntityFlag = false;
			
			if (line.getOrder() != null  && line.getOrder().getCompanyLocation() == null ) {
				throw new FileProcessingException("IN01 Plant: " + plant + " not found for Storage Location: " + location);
			}
			if (metrialNumber == null ) {
				throw new FileProcessingException("IN01 material : " + metrialNumber + " not found for Storage Location: " + location);
			}
			if(metrialNumber.length() != 18) {
				throw new  FileProcessingException("IN01 material is not of length 18 charecter " + metrialNumber);
			}
			if (orderBean != null && orderBean.getOrderNumber() == null){
				throw new  FileProcessingException("IN01 document number can't be null " + orderBean.getOrderNumber());
			}
			if (orderBean != null && orderBean.getOrderType().getOrderTypeName().equalsIgnoreCase(SCPO) && orderBean.getMrf_ref().equalsIgnoreCase(A2) && assetType.equalsIgnoreCase(SPARE)) {
				throw new BCDSDataException("Invalid file exception. order type: SCPO; MRF: A2; Material type: Spare part");
			}
			try{
				metiralNumnerInt = Integer.parseInt(metrialNumber);
			}catch(NumberFormatException exception) {
				throw new  FileProcessingException("IN01 material number is not a number: " + metrialNumber);
			}
			if (isDuplicateOrder(line.getOrder(),line.getOrderDDBean())) {
				if (line.getOrder() != null) {
					throw new BCDSDataException("Order number "+ line.getOrder().getOrderNumber()+" already exists.");
				}
				if (line.getOrderDDBean() != null) {
					String orderDDNumber = null;
					List<com.bcds.jobs.beans.Order> orderDDList = line.getOrderDDBean().getOrderListDD();
					for (com.bcds.jobs.beans.Order orderDDBean: orderDDList) {
						if(orderDDBean.getOrderType().getOrderTypeName().equalsIgnoreCase("DN")) {
							Order duplicateOrder = orderRepo.findByOrderNumber(orderDDBean.getOrderNumber());
							if (duplicateOrder != null) {
								orderDDNumber = duplicateOrder.getOrderNumber();
							}
						}
					}
					if (orderDDNumber != null) {
						throw new BCDSDataException("Order number "+ orderDDNumber+" already exists.");
					}
				}
			}
			OrderDDBean orderDDBean = line.getOrderDDBean();
			if (orderDDBean != null){
				boolean isUpdate = false;
				orderDDBean.getOrderRelationshipList();
				List<com.bcds.jobs.beans.Order> orderList = orderDDBean.getOrderListDD();
				for (com.bcds.jobs.beans.Order orderObj:orderList) {
					if (orderObj.getDdType() != null 
							&& orderObj.getDdType().equalsIgnoreCase("Z1NL")) {
						Order duplicateOrder = orderRepo.findByOrderNumber(orderObj.getOrderNumber());
						if (duplicateOrder != null) {
							updateZ1NL(orderObj);
							isUpdate = true;
						}
					}
					if (orderObj.getDdType() != null 
							&& orderObj.getDdType().equalsIgnoreCase("NL")) {
						Order duplicateOrderNL = orderRepo.findByOrderNumber(orderObj.getOrderNumber());
						if (duplicateOrderNL != null) {
							throw new BCDSDataException("Order number "+ duplicateOrderNL.getOrderNumber()+" already exists.");
						}
					}
				}
				if (!isUpdate) {
					insertAllOrders(orderList);
					insertOrderRelationship(orderDDBean.getOrderRelationshipList());
				}
			}
			if (orderBean != null) {
				String mrfValue = orderBean.getMrf_ref();
				String orderTypeName = orderBean.getOrderType().getOrderTypeName();
				/*boolean isdocumentType = in01OrderHeader.getDocumentType().trim().equalsIgnoreCase("A00062");
				if ((orderTypeName.equalsIgnoreCase(UB)) 
						&& (in01OrderHeader != null) 
						&& (in01OrderHeader.getDocumentType().equalsIgnoreCase("A00062"))) {
					com.bcds.jobs.beans.CompanyLocation  companyLocationUB = orderBean.getCompanyLocation();
					String barcode = companyLocationUB.get;
					companyLocationUB.setLocationName("A000");
					orderBean.setCompanyLocation(companyLocationUB);
				}*/
				if (orderTypeName.equalsIgnoreCase(SCPO) && mrfValue.equalsIgnoreCase(A1) && assetType.equalsIgnoreCase(SPARE)) {
					Asset assetEntity = assetService.findAssetByLocationIdAndConditionIdAndStatusId(metrialNumber,plant,AssetConditionEnum.USED,AssetStatusEnum.Active);
					if (assetEntity == null) {
						throw new EntityNotFoundException("Asset Entity not found for company asset parts material number "
					+ metrialNumber.replaceAll("^0*", "") + " at location: " + plant +"/A000");
					}
					isUpdateAssetEntityFlag = true;
				}
				if (orderTypeName.equalsIgnoreCase(SCPO)  && mrfValue.equalsIgnoreCase(A2) && assetType.equalsIgnoreCase(SPARE)) {
					throw new BCDSDataException("Invalid file exception. order type: SCPO; MRF: A2; Material type: Spare part");
					/*Asset assetEntity = assetService.findAssetByLocationIdAndConditionIdAndStatusId(metrialNumber,plant,AssetConditionEnum.USED,AssetStatusEnum.Active);
					if (assetEntity == null) {
						throw new EntityNotFoundException("Asset Entity not found for company asset parts meterial number "+ metrialNumber + " at location: " + plant +"/A000");
					}
					isUpdateAssetEntityFlag = true;*/
				}
				if (orderTypeName.equalsIgnoreCase(UB)  && mrfValue.equalsIgnoreCase(A2) && assetType.equalsIgnoreCase(SPARE)) {
					Asset assetEntity = assetService.findAssetByLocationIdAndConditionIdAndStatusId(metrialNumber,plant,AssetConditionEnum.USED,AssetStatusEnum.Active);
					if (assetEntity == null) {
						throw new EntityNotFoundException("Asset Entity not found for company asset parts material number "
					+ metrialNumber.replaceAll("^0*", "") + " at location: " + plant +"/A000");
					}
					isUpdateAssetEntityFlag = true;
				}
				if (orderTypeName.equalsIgnoreCase(SCPO)  && mrfValue.equalsIgnoreCase("A.6") && assetType.equalsIgnoreCase(SPARE)) {
					Asset assetEntity = assetService.findAssetByLocationIdAndConditionIdAndStatusId(metrialNumber,plant,AssetConditionEnum.USED, AssetStatusEnum.Active);
					if (assetEntity == null) {
						throw new EntityNotFoundException("Asset Entity not found for company asset parts material number "
					+ metrialNumber.replaceAll("^0*", "") + " at location: " + plant +"/A000");
					}
				}
				if (orderTypeName.equalsIgnoreCase(SCPO)  && mrfValue.equalsIgnoreCase("A.5") && assetType.equalsIgnoreCase(SPARE) && isA5GI != null && isA5GI.equalsIgnoreCase("GI")) {
					Asset assetEntity = assetService.findAssetByLocationIdAndConditionIdAndStatusId(metrialNumber,plant,AssetConditionEnum.USED, AssetStatusEnum.Q);
					if (assetEntity == null) {
						throw new EntityNotFoundException("Asset Entity not found for company asset parts material number "
					+ metrialNumber.replaceAll("^0*", "") + " at location: " + plant +"/A000");
					}
				}
				if (orderTypeName.equalsIgnoreCase(SCPO)  && mrfValue.equalsIgnoreCase("A.7") && assetType.equalsIgnoreCase(SPARE)) {
					Asset assetTObeUpdated = null;
					String meterialNumberWithoutLeadingZero= metrialNumber.replaceAll("^0*", "");
					AssetConditionEntity assetConditionEntity = assetConditionRepo.findByCondition(AssetConditionEnum.USEDSWAP.toString());
					AssetStatusEntity assetStatusEntity = assetStatusRepo.findByStatus(AssetStatusEnum.Active.toString());
					CompanyLocation companyLocationEntity = companyLocationRepo.findByBarcode(plant.concat("A000"));
					List<Asset> assetEntityList = assetRepo.findByLocationIdAndConditionIdAndStatusId(companyLocationEntity,assetConditionEntity,assetStatusEntity);
					if (assetEntityList.isEmpty()) {
						throw new EntityNotFoundException("Asset Entity not found for company asset parts material number "
					+ metrialNumber.replaceAll("^0*", "") + " at location: " + plant +"/A000");
					}
					for (Asset assetEntity:assetEntityList) {
						String longDescription = assetEntity.getLongDesc();
						String materialNumber = "";
						if (longDescription.contains("|")) {
							materialNumber = longDescription.substring(0, longDescription.indexOf("|"));
						}
						if (meterialNumberWithoutLeadingZero.equalsIgnoreCase(materialNumber)) {
							assetTObeUpdated = assetEntity;
						}
					}
					if (assetTObeUpdated == null) {
						throw new EntityNotFoundException("Asset Entity not found for company asset parts material number "
					+ metrialNumber.replaceAll("^0*", "") + " at location: " + plant +"/A000");
					}
				}
				insertOrder(orderBean);
			if (orderTypeName.equalsIgnoreCase(SCPO) && mrfValue.equalsIgnoreCase(A1) && assetType.equalsIgnoreCase(SPARE)) {
				isUpdateAssetEntityFlag = true;
			}
			if (orderTypeName.equalsIgnoreCase(SCPO)  && mrfValue.equalsIgnoreCase(A2) && assetType.equalsIgnoreCase(SPARE)) {
				isUpdateAssetEntityFlag = true;
			}
			if (orderTypeName.equalsIgnoreCase(UB)  && mrfValue.equalsIgnoreCase(A2) && assetType.equalsIgnoreCase(SPARE)) {
				isUpdateAssetEntityFlag = true;
			}
			if (orderTypeName.equalsIgnoreCase(UB)  && mrfValue.equalsIgnoreCase("A.6") && assetType.equalsIgnoreCase(SPARE)) {
			}
			if (orderTypeName.equalsIgnoreCase(SCPO)  && mrfValue.equalsIgnoreCase("A.6") && assetType.equalsIgnoreCase(SPARE)) {
				deactivateAsset(metrialNumber,plant, AssetConditionEnum.USED, AssetStatusEnum.Active);
				updateOrderStatusToClosed(entityToBeanClass.convertEntityToBean(this.findByOrderNumberAndLocationIdAndOrderTypeIdAndOrderActionTypeidAndOrderStatus(orderBean.getOrderNumber(),
						orderBean.getCompanyLocation().getBarcode(), orderBean.getOrderType().getOrderTypeName(),
						orderBean.getOrderActionType().getActionName(),OrderStatusEnum.RECEIVED.toString())));
			}
			if (orderTypeName.equalsIgnoreCase(SCPO)  && mrfValue.equalsIgnoreCase("A.7") && assetType.equalsIgnoreCase(SPARE)) {
				deactivateAssetA7(metrialNumber,plant, AssetConditionEnum.USEDSWAP, AssetStatusEnum.Active);
				updateOrderStatusToClosed(entityToBeanClass.convertEntityToBean(this.findByOrderNumberAndLocationIdAndOrderTypeIdAndOrderActionTypeidAndOrderStatus(orderBean.getOrderNumber(),
						orderBean.getCompanyLocation().getBarcode(), orderBean.getOrderType().getOrderTypeName(),
						orderBean.getOrderActionType().getActionName(),OrderStatusEnum.RECEIVED.toString())));
			}
			if (orderTypeName.equalsIgnoreCase(SCPO)  && mrfValue.equalsIgnoreCase("A.5") && assetType.equalsIgnoreCase(SPARE) && isA5GI != null && isA5GI.equalsIgnoreCase("GI")) {
				deactivateAsset(metrialNumber,plant, AssetConditionEnum.USED, AssetStatusEnum.Q);
				updateOrderStatus(this.findByOrderNumberAndLocationIdAndOrderTypeIdAndOrderActionTypeidAndOrderStatus(orderBean.getOrderNumber(),
						orderBean.getCompanyLocation().getBarcode(), orderBean.getOrderType().getOrderTypeName(),
						orderBean.getOrderActionType().getActionName(),OrderStatusEnum.RECEIVED.toString()), OrderStatusEnum.FINISHED.toString());
			}
			if (isUpdateAssetEntityFlag) {
				updatedAssetConditionToVender(metrialNumber, plant, AssetConditionEnum.USED, AssetStatusEnum.Active);
			}
		  }
		}
}
	
			private void updateA5OrderGI(com.bcds.jobs.beans.Order order) {
				com.bcds.jpa.entity.Order orderEntity =  findOrder(order);
				if (orderEntity != null) {
				}
			}
	
			private com.bcds.jpa.entity.Order findOrder(com.bcds.jobs.beans.Order order){
				return orderRepo.findByOrderNumber(order.getOrderNumber());
			}
	
		
		@Transactional(timeout=25)
		private void insertOrder(com.bcds.jobs.beans.Order orderBean) {
			try{
				com.bcds.jpa.entity.Order orderEntity = beanToEntityClass.convertBeanToEntity(orderBean);
				orderRepo.save(orderEntity);
			}catch(HibernateException hibernateException) {
				hibernateException.printStackTrace();
			}
		}
		
		@Transactional(timeout=25)
		private void insertAllOrders(List<com.bcds.jobs.beans.Order> orderBeanList) {
			try{
				for(com.bcds.jobs.beans.Order orderBean:orderBeanList) {
				com.bcds.jpa.entity.Order orderEntity = beanToEntityClass.convertBeanToEntity(orderBean);
					orderRepo.save(orderEntity);
				}
			}catch(HibernateException hibernateException) {
				hibernateException.printStackTrace();
			}
		}

	@Transactional
	private void insertOrderRelationship(List<com.bcds.jobs.beans.OrderRelationshipBean> orderRelationshipList) {
		for (com.bcds.jobs.beans.OrderRelationshipBean orderRelationshipBean: orderRelationshipList){
				OrderRelationshipEntity orderRelationshipEntity = beanToEntityClass.converBeanToEntity(orderRelationshipBean);
				orderRelationshipRepo.save(orderRelationshipEntity);
			}
		}
		
	@Override
	@Transactional
	public List<com.bcds.jobs.beans.Order> fillIN02Records() {		
		List<Order> orderEntityList = orderDao.fillIN02Records();
		List<com.bcds.jobs.beans.Order> orderBeanList = new ArrayList<com.bcds.jobs.beans.Order>();
		for (Order orderEntity:orderEntityList) {
			orderBeanList.add(entityToBeanClass.convertEntityToBean(orderEntity));
		}
		return orderBeanList;
	}
	

	public void updateOrderStatus(Order order, String orderStatusValue) {		
		OrderStatus orderStatus = orderStatusRepo.findByOrderStatus(orderStatusValue);		
		order.setOrderStatus(orderStatus);
		order.setLastUpdateDate(new Date());
		orderRepo.save(order);
	}
	
	@Override
	@Transactional
	public void updateOrderStatusToClosed(com.bcds.jobs.beans.Order order){
		OrderStatus orderStatus = orderStatusRepo.findByOrderStatus("CLOSED");
		Order orderEntity = orderRepo.findOne(order.getId());
		orderEntity.setOrderStatus(orderStatus);
		orderEntity.setLastUpdateDate(new Date());
		orderRepo.save(orderEntity);
	}
	
	@Override
	public List<OrderMaterialList> getOrderMaterialList(Order order){
		return orderMaterialListRepo.findByOrderId(order.getId());		
	}
	
	@Transactional
	private void updatedAssetConditionToVender(String meterialNumber, String plant, AssetConditionEnum assetCondition, AssetStatusEnum assetStatus) {
		Asset assetEntity = assetService.findAssetByLocationIdAndConditionIdAndStatusId(meterialNumber,plant,assetCondition,assetStatus);
		// = assetRepo.findByLocationIdAndConditionIdAndStatusId(companyLocationEntity, assetConditionEntity, assetStatusEntity);
		if (assetEntity == null) {
			throw new EntityNotFoundException("Asset Entity not found for company asset parts material number "
		+ meterialNumber.replaceAll("^0*", "") + " at location: " + plant +"/A000");
		}
		AssetConditionEntity conditionEntity = assetConditionRepo.findByCondition(VENDOR);
		assetEntity.setConditionId(conditionEntity);
		assetService.updateAsset(assetEntity);
	}
	
	@Transactional
	private void deactivateAsset(String meterialNumber, String plant, AssetConditionEnum assetCondition, AssetStatusEnum assetStatus) {
		Asset assetEntity = assetService.findAssetByLocationIdAndConditionIdAndStatusId(meterialNumber,plant,assetCondition,assetStatus);
		if (assetEntity == null) {
			throw new EntityNotFoundException("Asset Entity not found for company asset parts material number "
		+ meterialNumber.replaceAll("^0*", "") + " at location: " + plant +"/A000");
		}
		AssetStatusEntity assetStatusEntity  = assetStatusRepo.findByStatus(AssetStatusEnum.Deactive.toString());
		assetEntity.setStatusId(assetStatusEntity);
		assetService.updateAsset(assetEntity);
	}
	
	@Transactional
	private void updatedAssetStatusToDeactivate(String meterialNumber, String plant, AssetConditionEnum assetCondition, AssetStatusEnum assetStatus) {
		Asset assetEntity = assetService.findAssetByLocationIdAndConditionIdAndStatusId(meterialNumber, plant,assetCondition, assetStatus);
		if (assetEntity == null) {
			throw new EntityNotFoundException("Asset Entity not found for company asset parts material number "
		+ meterialNumber.replaceAll("^0*", "") + " at location: " + plant +"/A000");
		}
		AssetStatusEntity assetStatuDeactivesEntity = assetStatusRepo.findByStatus(AssetStatusEnum.Deactive.toString());
		assetEntity.setStatusId(assetStatuDeactivesEntity);
		assetService.updateAsset(assetEntity);
	}

	@Override
	public List<com.bcds.jobs.beans.Order> filterOrderBySCPOAndA7() {
		List<com.bcds.jpa.entity.Order> orderEntityList = orderDao.filterOrderBySCPOAndA7();
		List<com.bcds.jobs.beans.Order> orderBeanList = new ArrayList<com.bcds.jobs.beans.Order>();
		for (com.bcds.jpa.entity.Order orderEntity : orderEntityList) {
			orderBeanList.add(entityToBeanClass.convertEntityToBean(orderEntity));
		}
		return orderBeanList;
	}

	@Override
	public Order findOrderByOrderId(String orderNumber) {
		return orderRepo.findByOrderNumber(orderNumber);
	}

	@Override
	public List<Order> findOrdersByMrfCodeAndStatus(OrderStatusEnum orderStatus, String mrfCode) {
		OrderStatus orderStatusEntity = orderStatusRepo.findByOrderStatus(orderStatus.toString());
		return orderRepo.findByOrderStatusAndMrfRef(orderStatusEntity, mrfCode);
	}

	@Override
	public List<Order> findByOrderTypeAndOrderActionTypeAndMrfCode(String orderTypeName,
			String orderActionTypeName, String mrfCode, OrderStatusEnum orderStatus) {
		OrderType orderTypeEntity = orderTypeRepo.findByOrderTypeName(orderTypeName.toString());
		OrderStatus orderStatusEntity = orderStatusRepo.findByOrderStatus(orderStatus.toString());
		OrderActionType orderActionTypeEntity = orderActionTypeRepo.findByActionName(orderActionTypeName.toString());
		return orderRepo.findByOrderTypeAndOrderActionTypeAndMrfRefAndOrderStatus(orderTypeEntity,orderActionTypeEntity,mrfCode,orderStatusEntity);
	}

	@Override
	public List<com.bcds.jobs.beans.Order> filterOrderBySCPOAndA5() {
		List<com.bcds.jpa.entity.Order> orderEntityList = orderDao.filterOrderBySCPOAndA5();
		List<com.bcds.jobs.beans.Order> orderBeanList = new ArrayList<com.bcds.jobs.beans.Order>();
		for (com.bcds.jpa.entity.Order orderEntity: orderEntityList) {
			orderBeanList.add(entityToBeanClass.convertEntityToBean(orderEntity));
		}
		return orderBeanList;
	}

	@Override
	public Order findByOrderNumberAndLocationIdAndOrderTypeIdAndOrderActionTypeidAndOrderStatus(String orderNumber,
			String barcode, String orderTypeName, String orderActionTypeName,String orderStatus) {
		CompanyLocation companyLocationEntity = companyLocationRepo.findByBarcode(barcode);
		OrderType orderTypeEntity = orderTypeRepo.findByOrderTypeName(orderTypeName);
		OrderActionType orderActionTypeEntity = orderActionTypeRepo.findByActionNameAndDeliveyDocumentType(orderActionTypeName,"");
		OrderStatus orderStatusEntity = orderStatusRepo.findByOrderStatus(orderStatus);
		return orderRepo.findByOrderNumberAndCompanyLocationAndOrderTypeAndOrderActionTypeAndOrderStatus(orderNumber, 
				companyLocationEntity, orderTypeEntity,orderActionTypeEntity, orderStatusEntity);
	}
	
	@Override
	public List<OrderRelationshipBean> findAllDNs() {
		List<OrderRelationshipEntity> orderRelationshipListEntity = orderDao.findAllDNs();
		List<OrderRelationshipBean> orderRelationshipBeanList = new ArrayList<OrderRelationshipBean>();
		for (OrderRelationshipEntity orderRelationshipEntity: orderRelationshipListEntity) {
			orderRelationshipBeanList.add(entityToBeanClass.convertEntityToBean(orderRelationshipEntity));
		}
		return orderRelationshipBeanList;
	}

	
	@Override
	public List<com.bcds.jobs.beans.Order> getAllDDsNotAssociatedWithDNS() {
		List<OrderRelationshipBean> orderRelationshipBeanList = this.findAllDNs();
		List<com.bcds.jobs.beans.Order> orderDDList = new ArrayList<com.bcds.jobs.beans.Order>();
		List<com.bcds.jobs.beans.Order> orderDDListFinal = new ArrayList<com.bcds.jobs.beans.Order>();
		for (OrderRelationshipBean orderRelationshipBean: orderRelationshipBeanList) {
			com.bcds.jobs.beans.Order orderBean = orderRelationshipBean.getChildOrderId();
			if (!orderBean.getOrderType().getOrderTypeName().equalsIgnoreCase("DN")) {
				if (!orderDDList.contains(orderRelationshipBean.getParenOrderId())) {
				orderDDList.add(orderRelationshipBean.getParenOrderId());
				}
			}
		}
		for (com.bcds.jobs.beans.Order orderBean: orderDDList) {
			List<OrderRelationshipEntity>  orderRelationshipEntityList = orderRelationshipRepo.findByParentOrderId(beanToEntityClass.convertBeanToEntity(orderBean));
			if (!containsAnExistingDN(orderRelationshipEntityList) && orderBean.getOrderStatus().toString().equals(OrderStatusEnum.CLOSED.toString())) {
				if  (orderDDListFinal.isEmpty()) {
					orderDDListFinal.add(orderBean);
				}
				else {
				/*for (com.bcds.jobs.beans.Order orderListBean:orderDDListFinal) {*/
					if (!listContainsItems(orderDDListFinal,orderBean.getId())){
						orderDDListFinal.add(orderBean);
						}
					//}
				}	
				
			}
		}
		return orderDDListFinal;
	}
	
	@Override
	public List<com.bcds.jobs.beans.Order> getAllDDNotAssociatedWithDN() {
		List<Order> orderList = orderDao.findAllDDNotAssociatedWithDN();
		List<com.bcds.jobs.beans.Order> orderBeanList = new ArrayList<com.bcds.jobs.beans.Order>();
		for (Order order: orderList) {
			orderBeanList.add(entityToBeanClass.convertEntityToBean(order));
		}
		return orderBeanList;
	}
	
	private boolean listContainsItems(List<com.bcds.jobs.beans.Order> list, Integer idValue){
		for(com.bcds.jobs.beans.Order orderRelationshipEntity: list) {
			if (orderRelationshipEntity.getId().equals(idValue)) {
				return true;
			}
		}
		return false;
	}
	
	private boolean containsAnExistingDN(List<OrderRelationshipEntity>  orderRelationshipEntityList) {
		for (OrderRelationshipEntity orderRelationshipEntity : orderRelationshipEntityList) {
			if (orderRelationshipEntity.getChildOrderId().getOrderType().getOrderTypeName().equalsIgnoreCase("DN")) {
				return true;
			}
		}
		return false;
	}
	
	
	private boolean isDuplicateOrder(com.bcds.jobs.beans.Order order, OrderDDBean orderDDbean) {
		if(order != null) {
			Order duplicateOrder = null;
			if(order.getMrf_ref().equalsIgnoreCase("A.5")){
				if(order.getOrderActionType().getActionName().equalsIgnoreCase("GI")) {
				OrderActionType orderActionGITypeEntity = orderActionTypeRepo.findByActionNameAndDeliveyDocumentType("GI","");
					duplicateOrder = orderRepo.findByOrderNumberAndOrderActionTypeAndMrfRef(order.getOrderNumber(), orderActionGITypeEntity,order.getMrf_ref());
				} else if(order.getOrderActionType().getActionName().equalsIgnoreCase("GR")) {
					OrderActionType orderActionGRTypeEntity = orderActionTypeRepo.findByActionNameAndDeliveyDocumentType("GR","");
					duplicateOrder = orderRepo.findByOrderNumberAndOrderActionTypeAndMrfRef(order.getOrderNumber(), orderActionGRTypeEntity,order.getMrf_ref());
				}
				if(duplicateOrder == null){
					duplicateOrder = orderRepo.findByOrderNumber(order.getOrderNumber());
				}
			}
			else {
				 duplicateOrder = orderRepo.findByOrderNumber(order.getOrderNumber());
			}
			if (duplicateOrder != null) {
				return true;
			}
		}
		if (orderDDbean != null) {
			List<com.bcds.jobs.beans.Order> orderDDList = orderDDbean.getOrderListDD();
			for (com.bcds.jobs.beans.Order orderBean: orderDDList) {
				if((orderBean.getOrderType().getOrderTypeName().equalsIgnoreCase("DD")) || 
						(orderBean.getOrderType().getOrderTypeName().equalsIgnoreCase("DN"))) {
					Order duplicateOrder = orderRepo.findByOrderNumber(orderBean.getOrderNumber());
					if (duplicateOrder != null) {
						return true;
					}
				}
			}
		}
		return false;
	}

	
	@Override
	public com.bcds.jobs.beans.OrderActionType getByActionTypeAndDocType(String orderActionType, String docType) {
		return entityToBeanClass.convertEntityToBean(orderActionTypeRepo.findByActionNameAndDeliveyDocumentType(orderActionType, docType));
	}

	@Override
	public List<com.bcds.jobs.beans.Order> findAllFinishedDNs() {
		List<Order> orderEntityList = orderDao.findAllFinishedDNs();
		List<com.bcds.jobs.beans.Order> orderBeanList = new ArrayList<com.bcds.jobs.beans.Order>();
		for(Order orderEntity: orderEntityList) {
			orderBeanList.add(entityToBeanClass.convertEntityToBean(orderEntity));
		}
		return orderBeanList;
	}
	
	@Transactional
	private void deactivateAssetA7(String meterialNumber, String plant, AssetConditionEnum assetCondition, AssetStatusEnum assetStatus) {
		String meterialNumberWithoutLeadingZero= meterialNumber.replaceAll("^0*", "");
		AssetConditionEntity assetConditionEntity = assetConditionRepo.findByCondition(assetCondition.toString());
		AssetStatusEntity assetStatusEntity = assetStatusRepo.findByStatus(assetStatus.toString());
		CompanyLocation companyLocationEntity = companyLocationRepo.findByBarcode(plant.concat("A000"));
		Asset assetTObeUpdated = null;
		List<Asset> assetEntityList = assetRepo.findByLocationIdAndConditionIdAndStatusId(companyLocationEntity,assetConditionEntity,assetStatusEntity);
		if (assetEntityList.isEmpty()) {
			throw new EntityNotFoundException("Asset Entity not found for company asset parts material number "
		+ meterialNumber.replaceAll("^0*", "") + " at location: " + plant +"/A000");
		}
		for (Asset assetEntity:assetEntityList) {
			String longDescription = assetEntity.getLongDesc();
			String materialNumber = "";
			if (longDescription.contains("|")) {
				materialNumber = longDescription.substring(0, longDescription.indexOf("|"));
			}
			if (meterialNumberWithoutLeadingZero.equalsIgnoreCase(materialNumber)) {
				assetTObeUpdated = assetEntity;
			}
		}
		if (assetTObeUpdated == null) {
			throw new EntityNotFoundException("Asset Entity not found for company asset parts material number "
		+ meterialNumber.replaceAll("^0*", "") + " at location: " + plant +"/A000");
		}
		AssetStatusEntity assetStatusEntityDeactivate  = assetStatusRepo.findByStatus(AssetStatusEnum.Deactive.toString());
		assetTObeUpdated.setStatusId(assetStatusEntityDeactivate);
		assetService.updateAsset(assetTObeUpdated);
	}

	@Override
	@Transactional
	public void updateZ1NL(com.bcds.jobs.beans.Order order) {
		Order orderEntity = orderRepo.findByOrderNumber(order.getOrderNumber());
		List<Integer> idList= new ArrayList<Integer>();
		List<String> orderMaterialIdList = new ArrayList<String>();
		//String queryName = "getAssetsAssociatedWithZ1NL";
		String barcode = orderEntity.getOrderNumber();
		//List<Asset> assetEntityList = (List<Asset>) getEntityManager().createNamedQuery(queryName, Asset.class)
				//.setParameter("barcode", orderEntity.getOrderNumber());
		List<Asset> assetEntityList = assetService.getAssetsAssociatedWithZ1NL(barcode);
		if (orderEntity.getDdType().equalsIgnoreCase("Z1NL")){
			EntityManager entityManager = getEntityManager();
			List<OrderMaterialList> orderMaterialList = orderEntity.getOrderMeterialList();
			List<com.bcds.jobs.beans.OrderMeterialList> orderMaterialListBean = order.getOrderMeterialList();
			for (OrderMaterialList orderMaterialEntity : orderMaterialList) {
				for (com.bcds.jobs.beans.OrderMeterialList orderMaterialBean : orderMaterialListBean) {
					if ((orderMaterialEntity.getCompanyAssetParts().getPartName().
							equalsIgnoreCase(orderMaterialBean.getMeterial().getPartName())) 
							&& (orderMaterialEntity.getSalesOrderLineNumber().equalsIgnoreCase(orderMaterialBean.getSalesOrderLineNumber()))) {
						orderMaterialEntity.setBatchNumber(orderMaterialBean.getBatchNumber());
						orderMaterialEntity.setLastUpdatedDate(new Date());
						//orderMaterialEntity.setSalesOrderLineNumber(orderMaterialBean.getSalesOrderLineNumber());
					}
				}
			}
			orderEntity.setOrderMeterialList(orderMaterialList);
			orderEntity.setLastUpdateDate(new Date());
			for (Asset assetEntity : assetEntityList) {
				for (com.bcds.jobs.beans.OrderMeterialList orderMaterialBean : orderMaterialListBean) {
					boolean doubleValue = orderMaterialBean.getQuantity().equals(Double.parseDouble(assetEntity.getAssetValue().toString()));
					if (assetEntity.getCompanyAssetParts() != null && orderMaterialBean.getMeterial().getPartName()
							.equalsIgnoreCase(assetEntity.getCompanyAssetParts().getPartName())
							&& orderMaterialBean.getQuantity().equals(Double.parseDouble(assetEntity.getAssetValue().toString()))) {
						if (idList.size()==0){
							assetEntity.setBatchNumber(orderMaterialBean.getBatchNumber());
							assetEntity.setLastUpdateDate(new Date());
							orderMaterialIdList.add(orderMaterialBean.getLineNumber());
							idList.add(assetEntity.getAssetCompositeId().getAssetId());
							break;
						}
						if (!idList.isEmpty() && !idList.contains(assetEntity.getAssetCompositeId().getAssetId()) 
								&& !orderMaterialIdList.contains(orderMaterialBean.getLineNumber())) {
							assetEntity.setBatchNumber(orderMaterialBean.getBatchNumber());
							assetEntity.setLastUpdateDate(new Date());
							orderMaterialIdList.add(orderMaterialBean.getLineNumber());
							idList.add(assetEntity.getAssetCompositeId().getAssetId());
							break;
						}
					}
				}
			}
		}
		for (Asset assetEntity : assetEntityList) {
			assetRepo.save(assetEntity);
			//getEntityManager().merge(assetEntity);
		}
		//getEntityManager().merge(orderEntity);
		orderRepo.save(orderEntity);
	}
}
