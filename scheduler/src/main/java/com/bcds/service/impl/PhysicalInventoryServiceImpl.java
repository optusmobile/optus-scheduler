/**
 * 
 */
package com.bcds.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bcds.jobs.beans.PidBean;
import com.bcds.jobs.beans.PidListBean;
import com.bcds.jobs.exception.BCDSDataException;
import com.bcds.jobs.exception.FileProcessingException;
import com.bcds.jpa.entity.CompanyAssetParts;
import com.bcds.jpa.entity.CompanyLocation;
import com.bcds.jpa.entity.OrderStatus;
import com.bcds.jpa.entity.PhysicalInventoryDocument;
import com.bcds.jpa.entity.PhysicalInventoryDocumentList;
import com.bcds.jpa.model.in10.IN10;
import com.bcds.jpa.model.in10.IN10PIDLine;
import com.bcds.jpa.repo.AssetRepository;
import com.bcds.jpa.repo.CompanyAssetPartsRepository;
import com.bcds.jpa.repo.CompanyLocationRepository;
import com.bcds.jpa.repo.OrderStatusRepository;
import com.bcds.jpa.repo.PhysicalInventoryDocumentRepository;
import com.bcds.service.iface.CompanyServiceInterface;
import com.bcds.service.iface.PhysicalInventoryService;

/**
 * @author arpita.das
 *
 */

@Service("physicalInventoryService")
public class PhysicalInventoryServiceImpl implements PhysicalInventoryService {

	final static Logger logger = Logger.getLogger(PhysicalInventoryServiceImpl.class);
	
	@Autowired
	PhysicalInventoryDocumentRepository pidRepo;
	
	@Autowired
	OrderStatusRepository orderStatusRepo;
	
	@Autowired
	CompanyLocationRepository companyLocationRepo;
	
	@Autowired
	CompanyServiceInterface companyService;
	
	@Autowired
	CompanyAssetPartsRepository companyAssetPartsRepo;
	
	@Autowired
	AssetRepository assetRepo;
	
	@Override
	@Transactional
	public void addPhysicalInventoryDocument(IN10 in10) throws BCDSDataException, FileProcessingException, EntityNotFoundException {
		
		List<IN10PIDLine> in10PIDLineList = in10.getIn10PIDLine();
		PhysicalInventoryDocument pidEntity = new PhysicalInventoryDocument();
		List<PhysicalInventoryDocumentList> pidList = new ArrayList<PhysicalInventoryDocumentList>();
		CompanyLocation sLoc = new CompanyLocation();
		PhysicalInventoryDocument pidEntityReturn = new PhysicalInventoryDocument();
		CompanyAssetParts companyAssetPartsEntity = null;
		try {
			for (IN10PIDLine line: in10PIDLineList){
				PidBean pidBean = line.getPidBean();
				String barcode = pidBean.getPlant() + pidBean.getsLocs();
				List<PidListBean> pidListBeanList = pidBean.getPidList();
				
				if (pidBean.getPidNumber() != null) {
					pidEntityReturn = getPidByPidNumber(pidBean.getPidNumber().toString());
				}
				if (pidEntityReturn == null) {
					sLoc = companyLocationRepo.findByBarcode(barcode);
					if (sLoc == null){ 
						throw new EntityNotFoundException("IN10 Storage Location not found: " + barcode);
					}
					pidEntity.setLocation(sLoc);
					pidEntity.setPidNumber(pidBean.getPidNumber().toString());
					pidEntity.setFiscalYear(pidBean.getFiscalYear());
					pidEntity.setDateOfLastCount(pidBean.getLastUpdateDate());
					OrderStatus orderStatus = orderStatusRepo.findByOrderStatus("PENDING");
					pidEntity.setOrderStatus(orderStatus);
					pidEntity.setDateOfLastCount(new Date());
					pidEntity.setPidList(pidList);
					for (PidListBean pidListobj : pidListBeanList) {
						PhysicalInventoryDocumentList physicalInventoryDocumentList = new PhysicalInventoryDocumentList();
						physicalInventoryDocumentList.setActualQuantity(pidListobj.getExpectedQuantity().intValue());
						String meterialNumber = pidListobj.getAsset().getCompanyAssetParts().getPartName();
						if (meterialNumber == null ) {
							throw new FileProcessingException("IN10 Meterial number not found: " + meterialNumber);
						}
						meterialNumber = pidListobj.getAsset().getCompanyAssetParts().getPartName().replaceFirst("0+", "");
						companyAssetPartsEntity = companyAssetPartsRepo.findByPartName(pidListobj.getAsset().getCompanyAssetParts().getPartName());
						if (companyAssetPartsEntity == null) {
							throw new EntityNotFoundException("Material number is not found: " + meterialNumber);
//							System.out.println(meterialNumber);
//							continue;
						}
						physicalInventoryDocumentList.setCompanyAssetParts(companyAssetPartsRepo.findByPartName(pidListobj.getAsset().getCompanyAssetParts().getPartName()));
						physicalInventoryDocumentList.setExpectedQuantity(pidListobj.getExpectedQuantity().intValue());
						physicalInventoryDocumentList.setLineNumber(pidListobj.getLineNumber());
						physicalInventoryDocumentList.setLocation(companyLocationRepo.findByBarcode(barcode));
						physicalInventoryDocumentList.setPid(pidEntity);
		//				logger.info(">>>>>>>>>> " + line.getMeterialNumber());
						pidList.add(physicalInventoryDocumentList);
					}
					pidRepo.save(pidEntity);
				}
			}
//		} catch (FileProcessingException fpe) {
//			throw fpe;
//		} catch (EntityNotFoundException enfe){
//			throw enfe;
		} catch (Exception e) {
//			logger.error(e ,e);
			throw e;
		}
	}
	
	@Transactional
	private PhysicalInventoryDocument getPidByPidNumber(String pidNumber) {
		return pidRepo.findByPidNumber(pidNumber);
	}

}
