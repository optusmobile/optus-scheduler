package com.bcds.service.impl;

import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bcds.jobs.beans.BeanToEntityClass;
import com.bcds.jobs.beans.CompanyAssetCategoryBean;
import com.bcds.jobs.beans.EntityToBeanClass;
import com.bcds.jpa.entity.CompanyAssetCategoryEntity;
import com.bcds.jpa.repo.CompanyAssetCategoryRepo;
import com.bcds.service.iface.CompanyAssetCategoryServiceInterface;

@Service("companyAssetCategoryService")
public class CompanyAssetCategoryServiceImpl implements CompanyAssetCategoryServiceInterface{

	@Autowired
	BeanToEntityClass beanToEntityClass;
	
	@Autowired
	EntityToBeanClass entityToBeanClass;
	
	@Autowired
	private CompanyAssetCategoryRepo companyAssetCategoryRepo;
	
	@Override
	public CompanyAssetCategoryBean addCompanyAssetCategory(CompanyAssetCategoryBean companyAssetCategory) throws EntityNotFoundException {
		CompanyAssetCategoryEntity companyAssetCategoryEntity = beanToEntityClass.convertBeanToEntity(companyAssetCategory);
		CompanyAssetCategoryEntity companyAssetCategoryEntityAdded = companyAssetCategoryRepo.save(companyAssetCategoryEntity);
		return entityToBeanClass.convertEntityToBean(companyAssetCategoryEntityAdded);
	}
	
	@Override
	public CompanyAssetCategoryBean findByCategoryNameAndParentCatId(String categoryName, Integer parentCatId) throws EntityNotFoundException {
		return entityToBeanClass.convertEntityToBean(companyAssetCategoryRepo.findByCategoryNameAndParentCatId(categoryName, parentCatId));
	}
}
