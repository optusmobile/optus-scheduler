package com.bcds.service.impl;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityNotFoundException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.bcds.jobs.beans.BeanToEntityClass;
import com.bcds.jobs.beans.EntityToBeanClass;
import com.bcds.jobs.beans.MaterialLocationBean;
import com.bcds.jpa.entity.CompanyLocation;
import com.bcds.jpa.entity.MaterialLocation;
import com.bcds.jpa.repo.CompanyAssetPartsRepository;
import com.bcds.jpa.repo.CompanyLocationRepository;
import com.bcds.service.iface.CompanyAssetPartsServiceInterface;

@Service("companyAssetPartsService")
public class CompanyAssetPartsServiceImpl  implements CompanyAssetPartsServiceInterface{
	
	final static Logger logger = Logger.getLogger(CompanyAssetPartsServiceImpl.class);
	
	@Autowired
	CompanyAssetPartsRepository companyAssetPartsRepo;
	
	@Autowired
	CompanyLocationRepository companyLocationRepository;
	
	@Autowired
	BeanToEntityClass beanToEntityClass;
	
	@Autowired
	EntityToBeanClass entityToBeanClass;

	@Override
	@Transactional
	public com.bcds.jobs.beans.CompanyAssetParts addCompanyAssetPart(com.bcds.jobs.beans.CompanyAssetParts companyAssetParts) throws EntityNotFoundException{
		com.bcds.jpa.entity.CompanyAssetParts companyAssetPartEntity = companyAssetPartsRepo.findByPartName(companyAssetParts.getPartName());
		if (companyAssetPartEntity == null){
			companyAssetPartEntity = beanToEntityClass.convertBeanToEntity(companyAssetParts);
		}
		List<MaterialLocation> materialLocationEntityList = new ArrayList<MaterialLocation>();
		List<MaterialLocationBean> materialLocationBeanList = companyAssetParts.getMaterialLocationBeanList();
		for (MaterialLocationBean materialLocationBean : materialLocationBeanList) {
			MaterialLocation materialLocationEntity = new MaterialLocation();
			CompanyLocation companyLocationEntity = null;
			materialLocationEntity.setMaterialId(companyAssetPartEntity);
			if (materialLocationBean.getCompanyLocation() != null) {
				companyLocationEntity = companyLocationRepository.findByBarcode(materialLocationBean.getCompanyLocation().getBarcode());
			}
			/*if (companyLocationEntity == null){
				throw new EntityNotFoundException("CompanyLocationEntity Entity not found for barcode ::" + materialLocationBean.getCompanyLocation().getBarcode());
			}*/
			materialLocationEntity.setLocationId(companyLocationEntity);
			materialLocationEntityList.add(materialLocationEntity);
		}
		companyAssetPartEntity.setPartDesc(companyAssetParts.getPartDescription());
		//companyAssetPartEntity.setMaterialLocationList(materialLocationEntityList);
		companyAssetPartEntity.setPartNum(companyAssetParts.getPartNumber());
		companyAssetPartEntity =  companyAssetPartsRepo.save(companyAssetPartEntity);
		return entityToBeanClass.convertEntityToBean(companyAssetPartEntity);
	}
	

}
