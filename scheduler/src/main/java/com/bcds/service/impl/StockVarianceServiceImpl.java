package com.bcds.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.bcds.file.helper.AssetConditionEnum;
import com.bcds.file.helper.AssetStatusEnum;
import com.bcds.jobs.beans.StockVarianceBean;
import com.bcds.jobs.exception.FileProcessingException;
import com.bcds.jpa.entity.Asset;
import com.bcds.jpa.entity.CompanyAssetParts;
import com.bcds.jpa.entity.CompanyLocation;
import com.bcds.jpa.entity.StockVarianceEntity;
import com.bcds.jpa.model.r01.R01;
import com.bcds.jpa.model.r01.R01StkVarLine;
import com.bcds.jpa.repo.AssetConditionRepo;
import com.bcds.jpa.repo.AssetRepository;
import com.bcds.jpa.repo.AssetStatusRepo;
import com.bcds.jpa.repo.CompanyAssetPartsRepository;
import com.bcds.jpa.repo.CompanyLocationRepository;
import com.bcds.jpa.repo.StockVarianceRepository;
import com.bcds.service.iface.StockVarianceServiceInterface;
import com.bcds.util.DateUtil;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

@Service("stockVarianceService")
public class StockVarianceServiceImpl  implements StockVarianceServiceInterface{
	
	@Value("${r01.report.folder}")
	private String reportFolder;
	
	@Value( "${serverType}" )
	private String serverType;
	
	@Autowired
	private CompanyAssetPartsRepository companyAssetPartsRepo;
	
	@Autowired
	private CompanyLocationRepository companyLocationRepo;
	
	@Autowired
	private StockVarianceRepository stockVarianceRepository;
	
	@Autowired
	private AssetRepository assetRepository;
	
	@Autowired
	private AssetConditionRepo assetConditionRepo;
	
	@Autowired
	private AssetStatusRepo assetStatusRepo;
	
	final static Logger logger = Logger.getLogger(StockVarianceServiceImpl.class);
	
	@Override
	public void addSapStock(R01 r01) throws FileProcessingException{
		List<R01StkVarLine> r01StkVarList = r01.getR01StkVarLine();
		stockVarianceRepository.deleteAll();
		for (R01StkVarLine r01StkVarLine: r01StkVarList) {
			StockVarianceEntity stockVarianceEntity = convertBeanToEntity(r01StkVarLine.getStockVarianceBean());
			stockVarianceRepository.save(stockVarianceEntity);
		}
	}
	
	private StockVarianceEntity convertBeanToEntity(StockVarianceBean stockVarianceBean) throws FileProcessingException{
		StockVarianceEntity stockVarianceEntity = new StockVarianceEntity();
		if (stockVarianceBean != null) {
			CompanyAssetParts companyAssetParts = companyAssetPartsRepo.findByPartName(stockVarianceBean.getCompanyAssetParts().getPartName());
			CompanyLocation companyLocation = companyLocationRepo.findByBarcode(stockVarianceBean.getCompanyLocation().getBarcode());
			if (companyAssetParts == null) {
				throw new FileProcessingException("R01 Stock material not found: "+ stockVarianceBean.getCompanyAssetParts().getPartName());
			}
			if (companyLocation == null) {
				throw new FileProcessingException("R01 Stock storage location not found for barcode: "+ stockVarianceBean.getCompanyLocation().getBarcode());
			}
			stockVarianceEntity.setCompanyAssetParts(companyAssetParts);
			stockVarianceEntity.setCompanyLocation(companyLocation);
//			stockVarianceEntity.setPlantName(stockVarianceBean.getPlant());
			stockVarianceEntity.setSapQuantity(stockVarianceBean.getSapQuantity());
			stockVarianceEntity.setStockReceiveDate(stockVarianceBean.getStockReceiveDate());
			stockVarianceEntity.setStockType(stockVarianceBean.getStockType());
			stockVarianceEntity.setPlantName(stockVarianceBean.getPlantName());
		}
		return stockVarianceEntity;
	}

	@Override
	public void addIntrackStock(){
		try {
			StockVarianceEntity stockVarianceEntity = null;
			String stockType = null;
			for(Integer plantId: stockVarianceRepository.findAllPlantIds()){
				if(plantId!=null){
					for(Asset asset: assetRepository.findByPlantIdForStockCount(plantId, assetStatusRepo.findByStatus(AssetStatusEnum.Deactive.toString()))){
						if(asset.getStatusId().getStatus().equals(AssetStatusEnum.Active.toString())){
							if(asset.getConditionId().getCondition().equals(AssetConditionEnum.VENDOR.toString())){
								stockType = "Q";
							} else {
								stockType = "U";
							}
							
						} else {
							stockType = asset.getStatusId().getStatus();
						}
						if(!stockVarianceRepository.isCompanyAssetPartsAndCompanyLocationAndStockTypeExist(asset.getCompanyAssetParts(), asset.getLocationId(), stockType)){
							stockVarianceEntity = new StockVarianceEntity();
							stockVarianceEntity.setCompanyAssetParts(asset.getCompanyAssetParts());
							stockVarianceEntity.setCompanyLocation(asset.getLocationId());
							stockVarianceEntity.setSapQuantity(0);
							stockVarianceEntity.setStockType(stockType);
							stockVarianceEntity.setPlantName(companyLocationRepo.findOne(plantId).getLocationName());
							stockVarianceEntity.setBatchName(asset.getCompanyAssetParts().getBatchName());
							stockVarianceEntity.setStockReceiveDate(new Date());
							stockVarianceRepository.save(stockVarianceEntity);
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error(e,e);
		} 
	}
	
	@Override
	public void updateIntrackQuantity(){
		int intrackQuantity;
		List<Asset> assetList = new ArrayList<Asset>();
		for(StockVarianceEntity stockVarianceEntity: stockVarianceRepository.findAll()){
			intrackQuantity = 0;
			switch (stockVarianceEntity.getStockType()) {
				case "U":
					assetList = assetRepository.findByCompanyAssetPartsAndLocationIdForStockCountU(stockVarianceEntity.getCompanyAssetParts(), stockVarianceEntity.getCompanyLocation(), assetStatusRepo.findByStatus(AssetStatusEnum.Active.toString()), assetConditionRepo.findByCondition(AssetConditionEnum.VENDOR.toString()));
					break;
				case "Q": 
					assetList = assetRepository.findByCompanyAssetPartsAndLocationIdForStockCountQ(stockVarianceEntity.getCompanyAssetParts(), stockVarianceEntity.getCompanyLocation(), assetStatusRepo.findByStatus(AssetStatusEnum.Q.toString()), assetStatusRepo.findByStatus(AssetStatusEnum.Active.toString()), assetConditionRepo.findByCondition(AssetConditionEnum.VENDOR.toString()));
					break;
				case "B": 
					assetList = assetRepository.findByCompanyAssetPartsAndLocationIdForStockCountB(stockVarianceEntity.getCompanyAssetParts(), stockVarianceEntity.getCompanyLocation(), assetStatusRepo.findByStatus(AssetStatusEnum.B.toString()));
					break;
				default:
					logger.error("Skip for unrecognised SAP indicator: " + stockVarianceEntity.getStockType());
					continue;
			}
			for(Asset asset: assetList){
				intrackQuantity += asset.getAssetValue().intValue();
			}
			stockVarianceEntity.setIntrackQuantity(intrackQuantity);
			stockVarianceRepository.save(stockVarianceEntity);
		}
		
	}
	
	@Override
	public List<StockVarianceEntity> getStockVarianceList(){
		return stockVarianceRepository.findStockVariance();
	}
	
	@Override
	public File generateSapStockVarianceReport(List<StockVarianceEntity> stockVarianceList) {
		
		JRBeanCollectionDataSource beanColDataSource = new JRBeanCollectionDataSource(stockVarianceList,false);
		File exportedfile =null;
		try {
			JasperDesign design = JRXmlLoader.load(getClass().getResourceAsStream("/SapStockVarianceReport.jrxml"));
			JasperReport report = JasperCompileManager.compileReport(design); 
			JasperPrint print = JasperFillManager.fillReport(report, new HashMap(), beanColDataSource);
			
			exportedfile = new File(reportFolder+serverType+"_O3_StockVarianceReport_"+DateUtil.getSimpleDateFormat("yyyyMMdd")+".xlsx");
			JRXlsxExporter exporterXLSX = new JRXlsxExporter();
			exporterXLSX.setParameter(JRXlsExporterParameter.JASPER_PRINT, print);
			exporterXLSX.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
			exporterXLSX.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, new FileOutputStream(exportedfile));
			exporterXLSX.exportReport();
		} catch (Exception e) {
			logger.error(e,e);
		} 
		
		logger.debug("generateSapStockVarianceReport... end");
		
		return exportedfile;
		
	}

	@Override
	public List<StockVarianceBean> getStockVarianceListForInstallByLocationId(int plantId, String plantName){
		logger.info("------- Start getStockVarianceListForInstallByLocationId ---------");
		Map<String, Integer> map = new HashMap <String, Integer>();
		String str = null;
		for(Asset asset: assetRepository.findByPlantIdForStockCountForInstall(plantId, assetStatusRepo.findByStatus(AssetStatusEnum.Active.toString()))){
			str = asset.getCompanyAssetParts().getPartName().concat("-").concat(asset.getLocationId().getLocationName());
			map.put(str, map.getOrDefault(str, 0) + asset.getAssetValue().intValue());
		}
		List<StockVarianceBean> stockVarianceList = new ArrayList<StockVarianceBean>();
		StockVarianceBean stockVariance = null;
		for(String s: map.keySet()){
			stockVariance = new StockVarianceBean();
			stockVariance.setBatchName(s.split("-")[0].replaceFirst("0+", ""));
			stockVariance.setStockType(s.split("-")[1]);
			stockVariance.setPlantName(plantName);
			stockVariance.setIntrackQuantity(map.get(s));
			stockVarianceList.add(stockVariance);
		}
		return stockVarianceList;
	}
	
	@Override
	public File generateSapStockVarianceReportForInstall(List<StockVarianceBean> stockVarianceList) {
		
		JRBeanCollectionDataSource beanColDataSource = new JRBeanCollectionDataSource(stockVarianceList,false);
		File exportedfile =null;
		try {
			JasperDesign design = JRXmlLoader.load(getClass().getResourceAsStream("/StockVarianceReportForInstall.jrxml"));
			JasperReport report = JasperCompileManager.compileReport(design); 
			JasperPrint print = JasperFillManager.fillReport(report, new HashMap(), beanColDataSource);
			
			exportedfile = new File(reportFolder+serverType+"_O3_StockVarianceReportForInstall_"+DateUtil.getSimpleDateFormat("yyyyMMdd")+".xlsx");
			JRXlsxExporter exporterXLSX = new JRXlsxExporter();
			exporterXLSX.setParameter(JRXlsExporterParameter.JASPER_PRINT, print);
			exporterXLSX.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
			exporterXLSX.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, new FileOutputStream(exportedfile));
			exporterXLSX.exportReport();
		} catch (Exception e) {
			logger.error(e,e);
		} 
		
		logger.debug("generateSapStockVarianceReport... end");
		
		return exportedfile;
		
	}
}
