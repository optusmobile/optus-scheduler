package com.bcds.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bcds.file.helper.AssetConditionEnum;
import com.bcds.file.helper.AssetStatusEnum;
import com.bcds.file.helper.OrderStatusEnum;
import com.bcds.jobs.beans.AssetMovementInfoBean;
import com.bcds.jobs.beans.EntityToBeanClass;
import com.bcds.jobs.dao.AssetDao;
import com.bcds.jobs.exception.BCDSDataException;
import com.bcds.jpa.entity.Asset;
import com.bcds.jpa.entity.AssetConditionEntity;
import com.bcds.jpa.entity.AssetMovementInfo;
import com.bcds.jpa.entity.AssetMovementInfoCompositeId;
import com.bcds.jpa.entity.AssetStatusEntity;
import com.bcds.jpa.entity.CompanyAssetParts;
import com.bcds.jpa.entity.CompanyLocation;
import com.bcds.jpa.entity.OrderStatus;
import com.bcds.jpa.entity.PhysicalInventoryDocument;
import com.bcds.jpa.model.mm01.MM01;
import com.bcds.jpa.repo.AssetConditionRepo;
import com.bcds.jpa.repo.AssetMovementInfoRepository;
import com.bcds.jpa.repo.AssetRepository;
import com.bcds.jpa.repo.AssetStatusRepo;
import com.bcds.jpa.repo.CompanyAssetPartsRepository;
import com.bcds.jpa.repo.CompanyLocationRepository;
import com.bcds.jpa.repo.InterfaceErrorRepository;
import com.bcds.jpa.repo.OrderStatusRepository;
import com.bcds.jpa.repo.PhysicalInventoryDocumentRepository;
import com.bcds.service.iface.AssetServiceInterface;
import com.bcds.service.iface.CompanyServiceInterface;

@Service("assetService")
public class AssetServiceImpl implements AssetServiceInterface {
	
	final static Logger logger = Logger.getLogger(AssetServiceImpl.class);
	
	@Autowired
	CompanyAssetPartsRepository companyAssetPartsRepo;
	
	@Autowired
	CompanyLocationRepository companyLocationRepo;
	
	@Autowired
	AssetMovementInfoRepository assetMovementInfoRepo;
	
	@Autowired
	OrderStatusRepository orderStatusRepo;
	
	@Autowired
	InterfaceErrorRepository interfaceErrorRepo;
	
	@Autowired
	CompanyServiceInterface companyService;
	
	@Autowired
	PhysicalInventoryDocumentRepository physicalInventoryDocumentRepo;
	
	@Autowired
	AssetRepository assetRepo;
	
	@Autowired
	AssetConditionRepo assetConditionRepo;
	
	@Autowired
	AssetStatusRepo assetStatusRepo;
	
	@Autowired
	AssetDao assetDao;
	
	@Autowired
	EntityToBeanClass entityToBeanClass;
	
	@Override
	public List<AssetMovementInfo> fillIN03Records() {
		OrderStatus orderStatus = orderStatusRepo.findByOrderStatus(OrderStatusEnum.FINISHED.toString());
		logger.info("order status id: " + orderStatus.getId());
		List<AssetMovementInfo> moves = assetMovementInfoRepo.findByOrderStatus(orderStatus);
		logger.info("orders size: " + moves.size());
		return moves;
	}

	
	@Override
	@Transactional 
	public void updateAssetMovementStatusToClosed(AssetMovementInfoBean assetMovementInfoBean) {
		AssetMovementInfoCompositeId assetMovementInfoCompositeId = new AssetMovementInfoCompositeId();
		assetMovementInfoCompositeId.setId(assetMovementInfoBean.getId());
		assetMovementInfoCompositeId.setAttributeDeviceId(assetMovementInfoBean.getAttributeDeviceId());
		AssetMovementInfo assetMovementInfoEntity = assetMovementInfoRepo.getOne(assetMovementInfoCompositeId);
		OrderStatus orderStatus = orderStatusRepo.findByOrderStatus("CLOSED");
		assetMovementInfoEntity.setOrderStatus(orderStatus);
		try {
			assetMovementInfoRepo.save(assetMovementInfoEntity);
		}catch (Exception exception) {
			exception.printStackTrace();
		}
	}
	
	
	@Override
	@Transactional 
	public void updatePIDStatusToClosed(PhysicalInventoryDocument physicalInventoryDocument) {
		OrderStatus orderStatus = orderStatusRepo.findByOrderStatus("CLOSED");
		physicalInventoryDocument.setOrderStatus(orderStatus);
		physicalInventoryDocument.setLastUpdatedDate(new Date());
		physicalInventoryDocumentRepo.save(physicalInventoryDocument);
	}

	@Override
	@Transactional 
	public Asset findAssetByLocationIdAndConditionIdAndStatusId(String meterialNumber, String plant, 
			AssetConditionEnum assetCondition, AssetStatusEnum assetStatus) throws EntityNotFoundException
	{
		CompanyLocation companyLocationEntity = companyLocationRepo.findByBarcode(plant.concat("A000"));
		if(companyLocationEntity == null){
			throw new EntityNotFoundException("CompanyLocation Entity not found for company location: " + plant +"/A000" );
		}
		CompanyAssetParts companyAssetPartsEntity = companyAssetPartsRepo.findByPartName(meterialNumber);
		if (companyAssetPartsEntity == null) {
			throw new EntityNotFoundException("CompanyAssetParts Entity not found for company asset parts meterial number "+ meterialNumber );
		}
		AssetConditionEntity assetConditionEntity =  assetConditionRepo.findByCondition(assetCondition.toString());
		AssetStatusEntity assetStatusEntity = assetStatusRepo.findByStatus(assetStatus.toString());
		List<Asset> assetList = assetRepo.findByCompanyAssetPartsAndLocationIdAndConditionIdAndStatusId(companyAssetPartsEntity, companyLocationEntity, assetConditionEntity, assetStatusEntity);
		if (assetList == null) {
			assetList = assetRepo.findByLocationIdAndConditionIdAndStatusId(companyLocationEntity, assetConditionEntity, assetStatusEntity);
			if(assetList == null){
				return null;
			}			
		}
		if (assetList.isEmpty()){
			return null;
		}
		else {
			return  assetList.get(0);
		}
	}

	@Override
	@Transactional 
	public Asset updateAsset(Asset asset) {
		if (asset != null && asset.getAssetCompositeId().getAssetId() != 0) {
			asset.setLastUpdateDate(new Date());
			return assetRepo.save(asset);
		}
		return null;
	}

	@Override
	public List<AssetMovementInfoBean> findAllFinishedAssets() {
		List<AssetMovementInfo> assetMovementInfoEnityList = assetDao.filterByAssetStatus();
		List<AssetMovementInfoBean> assetMovementInfoBeanList = new ArrayList<AssetMovementInfoBean>();
		for (AssetMovementInfo assetMovementInfoEntity : assetMovementInfoEnityList) {
//			if(assetMovementInfoEntity.getDeviceId() != assetMovementInfoEntity.getAsset().getDeviceId()){
//				assetMovementInfoEntity.setAsset(assetRepo.findByIdAndDeviceId(assetMovementInfoEntity.getAsset().getId(), assetMovementInfoEntity.getDeviceId()));
//			}
			assetMovementInfoBeanList.add(entityToBeanClass.convertEntityToBean(assetMovementInfoEntity));
		}
		return assetMovementInfoBeanList;
	}

	@Override
	public List<Asset> getAssetsAssociatedWithZ1NL(String barcode) {
		List< com.bcds.jpa.entity.Asset> assetEntityList = assetDao.getAssetsAssociatedWithZ1NL(barcode);
		return  assetEntityList;
	}

}


